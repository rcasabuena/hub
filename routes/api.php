<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api'])->group(function () {

	Route::resource('news', 'NewsController', ['only' => ['index', 'store', 'update', 'destroy']]);
	Route::resource('users', 'UsersController', ['only' => ['index', 'store', 'update', 'destroy']]);
	Route::resource('modules', 'ModulesController', ['only' => ['index']]);
	Route::resource('groups', 'UserGroupsController', ['only' => ['index', 'store', 'update', 'destroy']]);
	//Route::resource('events', 'EventsController', ['only' => ['index', 'store', 'update', 'destroy']]);
	Route::resource('tasks', 'TasksController', ['only' => ['index', 'store', 'update', 'destroy']]);
	Route::resource('todos', 'TodosController', ['only' => ['index', 'store', 'update', 'destroy']]);
	Route::resource('documents', 'DocumentsController', ['only' => ['index', 'store', 'update', 'destroy']]);
	Route::resource('downloads', 'DownloadsController', ['only' => ['index']]);
	Route::resource('topics', 'TopicsController', ['only' => ['index', 'store', 'update', 'destroy']]);
	Route::resource('locations', 'LocationsController', ['only' => ['index', 'store', 'update', 'destroy']]);
	Route::resource('attachments', 'AttachmentsController', ['only' => ['index', 'store', 'update', 'destroy']]);
	Route::resource('questions', 'QuestionsController', ['only' => ['index', 'store', 'update', 'destroy']]);
	Route::resource('answers', 'AnswersController', ['only' => ['index', 'store', 'update', 'destroy']]);
	Route::resource('todo-comments', 'TodoCommentsController', ['only' => ['index', 'store', 'update', 'destroy']]);
	Route::resource('document-files', 'DocumentFilesController', ['only' => ['store', 'destroy']]);

});



// Route::group(['middleware' => ['tenancy.enforce', 'auth']], function () {
// 	Route::get('api/current-logged-in-user', function(Request $request) {
// 		return $request->user();
// 	});
// });
