<?php
//dd(md5(uniqid()));
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

// Route::get('upload', 'HomeController@upload');
// Route::get('s3', 'HomeController@s3');

//Route::group(['middleware' => ['tenancy.enforce', 'auth']], function () {
Route::group(['middleware' => ['auth']], function () {

	Route::get('current-logged-in-user', 'TenancyController@currentLoggedInUser');
	Route::get('current-tenant', 'TenancyController@currentTenant');
	Route::get('files/{unique_id}/download', 'DocumentFileController@download');
	Route::get('attachments/{unique_id}/download', 'AttachmentController@download');


	Route::get('/email', function() {
		return view('email');
	});
	

    Route::fallback(function () {
	    return view('app');
	});
});
