(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[35],{

/***/ "./resources/js/components/functions/Utils.js":
/*!****************************************************!*\
  !*** ./resources/js/components/functions/Utils.js ***!
  \****************************************************/
/*! exports provided: handleSearch, handleSearch2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleSearch", function() { return handleSearch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleSearch2", function() { return handleSearch2; });
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! fuse.js */ "./node_modules/fuse.js/dist/fuse.js");
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fuse_js__WEBPACK_IMPORTED_MODULE_0__);
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


function handleSearch(e, searchKeys) {
  var _this = this;

  var searchTerms = e.target.value;
  searchKeys.forEach(function (key) {
    if (searchTerms.trim() !== "") {
      var options = _objectSpread({}, _globalFuseOptions, {
        keys: _globalFuseKeys[key]
      });

      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_0___default.a(_this.state[key], options);

      _this.setState(_defineProperty({}, "".concat(key, "Filtered"), fuse.search(searchTerms)));
    } else {
      _this.setState(_defineProperty({}, "".concat(key, "Filtered"), _this.state[key]));
    }
  });
}
function handleSearch2(e) {
  this.setState({
    searchTerms: e.target.value
  });
}

/***/ }),

/***/ "./resources/js/components/ui/ModuleSearchBox.js":
/*!*******************************************************!*\
  !*** ./resources/js/components/ui/ModuleSearchBox.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");



var ModuleSearchBox = function ModuleSearchBox(props) {
  var handleSearch = props.handleSearch,
      placeholder = props.placeholder;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["InputGroup"], {
    className: "input-prepend mb-4"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["InputGroupAddon"], {
    addonType: "prepend"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["InputGroupText"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-search"
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    bsSize: "lg",
    type: "text",
    placeholder: placeholder,
    onChange: handleSearch
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (ModuleSearchBox);

/***/ }),

/***/ "./resources/js/components/ui/PageHeader.js":
/*!**************************************************!*\
  !*** ./resources/js/components/ui/PageHeader.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");




var PageHeader = function PageHeader(props) {
  var pageTitle = props.pageTitle,
      pageInfo = props.pageInfo,
      pageResource = props.pageResource;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mb-4"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "card-title mb-0"
  }, pageTitle), pageInfo && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "text-muted"
  }, pageInfo), pageResource && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: "/".concat(pageResource, "/new")
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
    color: "primary"
  }, "Add New")));
};

/* harmony default export */ __webpack_exports__["default"] = (PageHeader);

/***/ }),

/***/ "./resources/js/components/views/Tasks/Tasks.js":
/*!******************************************************!*\
  !*** ./resources/js/components/views/Tasks/Tasks.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _ui_ModuleSearchBox__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../ui/ModuleSearchBox */ "./resources/js/components/ui/ModuleSearchBox.js");
/* harmony import */ var _ui_Tables_DataTable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../ui/Tables/DataTable */ "./resources/js/components/ui/Tables/DataTable.js");
/* harmony import */ var _functions_Utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../functions/Utils */ "./resources/js/components/functions/Utils.js");
/* harmony import */ var _ui_PageHeader__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../ui/PageHeader */ "./resources/js/components/ui/PageHeader.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }










var Tasks =
/*#__PURE__*/
function (_Component) {
  _inherits(Tasks, _Component);

  function Tasks(props) {
    var _this;

    _classCallCheck(this, Tasks);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Tasks).call(this, props));
    _this.state = {
      searchTerms: ""
    };
    _this.handleSearch = _functions_Utils__WEBPACK_IMPORTED_MODULE_5__["handleSearch2"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(Tasks, [{
    key: "render",
    value: function render() {
      var searchTerms = this.state.searchTerms;
      var todos = this.props.todos;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "animated fadeIn"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_ModuleSearchBox__WEBPACK_IMPORTED_MODULE_3__["default"], {
        handleSearch: this.handleSearch,
        placeholder: "Search..."
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Col"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["Card"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_7__["CardBody"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_PageHeader__WEBPACK_IMPORTED_MODULE_6__["default"], {
        pageTitle: "Tasks",
        pageInfo: "\xA0",
        pageResource: "tasks"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Tables_DataTable__WEBPACK_IMPORTED_MODULE_4__["default"], {
        todos: todos,
        tableMap: "tasks",
        searchTerms: searchTerms
      }))))));
    }
  }]);

  return Tasks;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref) {
  var todos = _ref.todos;
  return {
    todos: todos.data
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps)(Tasks));

/***/ })

}]);