(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[13],{

/***/ "./resources/js/HOC/formConnect.js":
/*!*****************************************!*\
  !*** ./resources/js/HOC/formConnect.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../actions */ "./resources/js/actions.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../functions/FormFunctions.js */ "./resources/js/functions/FormFunctions.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }






/* harmony default export */ __webpack_exports__["default"] = (function (ChildComponent, resource, mapStateToProps) {
  var ComposedComponent =
  /*#__PURE__*/
  function (_Component) {
    _inherits(ComposedComponent, _Component);

    function ComposedComponent() {
      _classCallCheck(this, ComposedComponent);

      return _possibleConstructorReturn(this, _getPrototypeOf(ComposedComponent).apply(this, arguments));
    }

    _createClass(ComposedComponent, [{
      key: "render",
      value: function render() {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ChildComponent, _extends({}, this.props, {
          formUtils: _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_4__
        }));
      }
    }]);

    return ComposedComponent;
  }(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

  var mapDispatchToProps = function mapDispatchToProps(dispatch, _ref) {
    var history = _ref.history;
    return {
      save: function save(data) {
        dispatch(Object(_actions__WEBPACK_IMPORTED_MODULE_2__["saveResource"])(resource, data, history));
      },
      delete: function _delete(id) {
        dispatch(Object(_actions__WEBPACK_IMPORTED_MODULE_2__["deleteResource"])(resource, id, history));
      }
    };
  };

  var enhance = compose(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["withRouter"], Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps, mapDispatchToProps));
  return enhance(ComposedComponent);
});

/***/ }),

/***/ "./resources/js/components/functions/CommonFormFunctions.js":
/*!******************************************************************!*\
  !*** ./resources/js/components/functions/CommonFormFunctions.js ***!
  \******************************************************************/
/*! exports provided: handleSubmit, handleSelect, handleChange, handleToggle, handleUpload, handleEditorChange, handleDelete */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleSubmit", function() { return handleSubmit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleSelect", function() { return handleSelect; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleChange", function() { return handleChange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleToggle", function() { return handleToggle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleUpload", function() { return handleUpload; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleEditorChange", function() { return handleEditorChange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleDelete", function() { return handleDelete; });
function handleSubmit(e) {
  e.preventDefault();
  var item = this.state.item;
  this.props.save(item.formData);
}
function handleSelect(res, meta) {
  var item = this.state.item;
  item[meta.name] = res ? res.value : "";
  this.setState({
    item: item
  });
}
function handleChange(e) {
  var item = this.state.item;
  item[e.target.id] = e.target.value;
  this.setState({
    item: item
  });
}
function handleToggle(e) {
  var inverse = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var item = this.state.item;
  item[e.target.id] = inverse ? !e.target.checked : e.target.checked;
  this.setState({
    item: item
  });
}
function handleUpload(e, name) {
  var item = this.state.item;
  item[name] = e.target.files[0];
  e.preventDefault();
  this.setState({
    item: item
  });
}
function handleEditorChange(data, name) {
  var item = this.state.item;
  item[name] = data;
  this.setState({
    item: item
  });
}
function handleDelete(id) {
  this.props.delete(id);
}

/***/ }),

/***/ "./resources/js/components/ui/Form/FormSubmitActions.js":
/*!**************************************************************!*\
  !*** ./resources/js/components/ui/Form/FormSubmitActions.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");



var FormSubmitActions = function FormSubmitActions(props) {
  var id = props.id,
      handleDelete = props.handleDelete;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormGroup"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    type: "submit",
    size: "sm",
    color: "primary",
    className: "mr-2"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-dot-circle-o"
  }), " Submit"), id.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    size: "sm",
    color: "danger",
    onClick: function onClick() {
      return handleDelete(id);
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-ban"
  }), " Delete"));
};

/* harmony default export */ __webpack_exports__["default"] = (FormSubmitActions);

/***/ }),

/***/ "./resources/js/components/ui/Form/TextInput.js":
/*!******************************************************!*\
  !*** ./resources/js/components/ui/Form/TextInput.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");



var TextInput = function TextInput(props) {
  var inputName = props.inputName,
      inputDefaultValue = props.inputDefaultValue,
      inputLabel = props.inputLabel,
      handleChange = props.handleChange,
      inputHelpText = props.inputHelpText,
      inputError = props.inputError,
      isRequired = props.isRequired,
      inputType = props.inputType,
      horizontal = props.horizontal;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormGroup"], {
    row: true,
    className: "mb-3"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    md: {
      size: !!!horizontal ? 3 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Label"], {
    htmlFor: inputName
  }, inputLabel)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    md: {
      size: !!!horizontal ? 9 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    type: inputType ? inputType : 'text',
    id: inputName,
    required: isRequired ? true : false,
    onChange: handleChange,
    defaultValue: inputDefaultValue
  }), inputHelpText && inputHelpText.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormText"], {
    color: "muted"
  }, inputHelpText)));
};

/* harmony default export */ __webpack_exports__["default"] = (TextInput);

/***/ }),

/***/ "./resources/js/components/ui/PageHeader.js":
/*!**************************************************!*\
  !*** ./resources/js/components/ui/PageHeader.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");




var PageHeader = function PageHeader(props) {
  var pageTitle = props.pageTitle,
      pageInfo = props.pageInfo,
      pageResource = props.pageResource;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mb-4"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "card-title mb-0"
  }, pageTitle), pageInfo && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "text-muted"
  }, pageInfo), pageResource && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: "/".concat(pageResource, "/new")
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
    color: "primary"
  }, "Add New")));
};

/* harmony default export */ __webpack_exports__["default"] = (PageHeader);

/***/ }),

/***/ "./resources/js/components/views/Locations/LocationForm.js":
/*!*****************************************************************!*\
  !*** ./resources/js/components/views/Locations/LocationForm.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ui_Form_TextInput__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../ui/Form/TextInput */ "./resources/js/components/ui/Form/TextInput.js");
/* harmony import */ var _ui_Form_FormSubmitActions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../ui/Form/FormSubmitActions */ "./resources/js/components/ui/Form/FormSubmitActions.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _functions_CommonFormFunctions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../functions/CommonFormFunctions */ "./resources/js/components/functions/CommonFormFunctions.js");
/* harmony import */ var _objects_AppLocation__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../objects/AppLocation */ "./resources/js/objects/AppLocation.js");
/* harmony import */ var _HOC_formConnect__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../HOC/formConnect */ "./resources/js/HOC/formConnect.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }









var LocationForm =
/*#__PURE__*/
function (_Component) {
  _inherits(LocationForm, _Component);

  function LocationForm(props) {
    var _this;

    _classCallCheck(this, LocationForm);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(LocationForm).call(this, props));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      item: new _objects_AppLocation__WEBPACK_IMPORTED_MODULE_5__["default"]()
    });

    var formUtils = props.formUtils;
    _this.handleChange = formUtils.handleChange.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleDelete = formUtils.handleDelete.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleSubmit = formUtils.handleSubmit.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(LocationForm, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var item = this.props.item;
      this.setState({
        item: item
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$state$item = this.state.item,
          id = _this$state$item.id,
          name = _this$state$item.name,
          address_line1 = _this$state$item.address_line1,
          address_line2 = _this$state$item.address_line2,
          address_city = _this$state$item.address_city,
          address_county = _this$state$item.address_county,
          address_postcode = _this$state$item.address_postcode,
          email = _this$state$item.email,
          telephone = _this$state$item.telephone;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Form"], {
        onSubmit: this.handleSubmit
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_TextInput__WEBPACK_IMPORTED_MODULE_1__["default"], {
        handleChange: this.handleChange,
        inputDefaultValue: name,
        inputName: "name",
        inputLabel: "Name",
        inputHelpText: "This is a help text",
        isRequired: true
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_TextInput__WEBPACK_IMPORTED_MODULE_1__["default"], {
        handleChange: this.handleChange,
        inputDefaultValue: address_line1,
        inputName: "address_line1",
        inputLabel: "Address Line 1",
        inputHelpText: "This is a help text"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_TextInput__WEBPACK_IMPORTED_MODULE_1__["default"], {
        handleChange: this.handleChange,
        inputDefaultValue: address_line2,
        inputName: "address_line2",
        inputLabel: "Address Line 2",
        inputHelpText: "This is a help text"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_TextInput__WEBPACK_IMPORTED_MODULE_1__["default"], {
        handleChange: this.handleChange,
        inputDefaultValue: address_city,
        inputName: "address_city",
        inputLabel: "Town/City",
        inputHelpText: "This is a help text"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_TextInput__WEBPACK_IMPORTED_MODULE_1__["default"], {
        handleChange: this.handleChange,
        inputDefaultValue: address_county,
        inputName: "address_county",
        inputLabel: "County",
        inputHelpText: "This is a help text"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_TextInput__WEBPACK_IMPORTED_MODULE_1__["default"], {
        handleChange: this.handleChange,
        inputDefaultValue: address_postcode,
        inputName: "address_postcode",
        inputLabel: "Postcode",
        inputHelpText: "This is a help text"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_TextInput__WEBPACK_IMPORTED_MODULE_1__["default"], {
        handleChange: this.handleChange,
        inputDefaultValue: email,
        inputName: "email",
        inputLabel: "Email",
        inputHelpText: "This is a help text"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_TextInput__WEBPACK_IMPORTED_MODULE_1__["default"], {
        handleChange: this.handleChange,
        inputDefaultValue: telephone,
        inputName: "telephone",
        inputLabel: "Telephone",
        inputHelpText: "This is a help text"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_FormSubmitActions__WEBPACK_IMPORTED_MODULE_2__["default"], {
        id: id,
        handleDelete: this.handleDelete
      }));
    }
  }]);

  return LocationForm;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref, _ref2) {
  var locations = _ref.locations;
  var match = _ref2.match;
  var loc = locations.data.find(function (item) {
    return item.id === match.params.unique_id;
  });
  var id = loc ? loc.id : "",
      name = loc ? loc.name : "",
      address_line1 = loc ? loc.address_line1 : "",
      address_line2 = loc ? loc.address_line2 : "",
      address_city = loc ? loc.address_city : "",
      address_county = loc ? loc.address_county : "",
      address_postcode = loc ? loc.address_postcode : "",
      email = loc ? loc.email : "",
      telephone = loc ? loc.telephone : "";
  return {
    item: new _objects_AppLocation__WEBPACK_IMPORTED_MODULE_5__["default"](id, name, address_line1, address_line2, address_city, address_county, address_postcode, email, telephone)
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(_HOC_formConnect__WEBPACK_IMPORTED_MODULE_6__["default"])(LocationForm, "Locations", mapStateToProps));

/***/ }),

/***/ "./resources/js/functions/FormFunctions.js":
/*!*************************************************!*\
  !*** ./resources/js/functions/FormFunctions.js ***!
  \*************************************************/
/*! exports provided: handleSubmit, handleSelect, handleMultiSelect, handleChange, handleToggle, handleUpload, handleUploadMulti, handleEditorChange, handleDelete */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleSubmit", function() { return handleSubmit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleSelect", function() { return handleSelect; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleMultiSelect", function() { return handleMultiSelect; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleChange", function() { return handleChange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleToggle", function() { return handleToggle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleUpload", function() { return handleUpload; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleUploadMulti", function() { return handleUploadMulti; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleEditorChange", function() { return handleEditorChange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleDelete", function() { return handleDelete; });
function handleSubmit(e) {
  e.preventDefault();
  var item = this.state.item;
  var history = this.props.history;
  this.props.save(item.formData, history);
  e.currentTarget.reset();
}
function handleSelect(res, meta) {
  var item = this.state.item;
  item[meta.name] = res ? res.value : "";
  this.setState({
    item: item
  });
}
function handleMultiSelect(res, meta) {
  var item = this.state.item;
  item[meta.name] = [];

  if (res.length > 0) {
    res.forEach(function (i) {
      item[meta.name].push(i.value);
    });
  }

  this.setState({
    item: item
  });
}
function handleChange(e) {
  var item = this.state.item;
  item[e.target.id] = e.target.value;
  this.setState({
    item: item
  });
}
function handleToggle(e) {
  var inverse = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var item = this.state.item;
  item[e.target.id] = inverse ? !e.target.checked : e.target.checked;
  this.setState({
    item: item
  });
}
function handleUpload(e, name) {
  var item = this.state.item;
  item[name] = e.target.files[0];
  e.preventDefault();
  this.setState({
    item: item
  });
}
function handleUploadMulti(e, name) {
  var item = this.state.item;
  Array.from(e.target.files).forEach(function (fileItem, idx) {
    item[name][idx] = fileItem;
  });
  e.preventDefault();
  this.setState({
    item: item
  });
}
function handleEditorChange(data, name) {
  var item = this.state.item;
  item[name] = data;
  this.setState({
    item: item
  });
}
function handleDelete(id) {
  this.props.delete(id);
}

/***/ }),

/***/ "./resources/js/objects/AppItem.js":
/*!*****************************************!*\
  !*** ./resources/js/objects/AppItem.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AppItem; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var AppItem =
/*#__PURE__*/
function () {
  function AppItem(id) {
    _classCallCheck(this, AppItem);

    this.id = id;
  }

  _createClass(AppItem, [{
    key: "formData",
    get: function get() {
      var _this = this;

      var data = new FormData();
      Object.keys(this).forEach(function (key) {
        if (key === 'attachments') {
          _this[key].forEach(function (file) {
            return data.append('attachments[]', file);
          });
        } else {
          data.append(key, _this[key]);
        }
      });
      return data;
    }
  }]);

  return AppItem;
}();



/***/ }),

/***/ "./resources/js/objects/AppLocation.js":
/*!*********************************************!*\
  !*** ./resources/js/objects/AppLocation.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AppLocation; });
/* harmony import */ var _AppItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AppItem */ "./resources/js/objects/AppItem.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }



var AppLocation =
/*#__PURE__*/
function (_AppItem) {
  _inherits(AppLocation, _AppItem);

  function AppLocation() {
    var _this;

    var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
    var name = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
    var address_line1 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
    var address_line2 = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "";
    var address_city = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : "";
    var address_county = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : "";
    var address_postcode = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : "";
    var email = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : "";
    var telephone = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : "";

    _classCallCheck(this, AppLocation);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(AppLocation).call(this, id));
    _this.name = name;
    _this.address_line1 = address_line1;
    _this.address_line2 = address_line1;
    _this.address_city = address_city;
    _this.address_county = address_county;
    _this.address_postcode = address_postcode;
    _this.email = email;
    _this.telephone = telephone;
    return _this;
  }

  _createClass(AppLocation, [{
    key: "address",
    get: function get() {
      return "Address";
    }
  }]);

  return AppLocation;
}(_AppItem__WEBPACK_IMPORTED_MODULE_0__["default"]);



/***/ })

}]);