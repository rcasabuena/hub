(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./node_modules/react-confirm-alert/src/react-confirm-alert.css":
/*!******************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/postcss-loader/src??ref--6-2!./node_modules/react-confirm-alert/src/react-confirm-alert.css ***!
  \******************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "body.react-confirm-alert-body-element {\n  overflow: hidden;\n}\n\n.react-confirm-alert-blur {\n  -webkit-filter: url(#gaussian-blur);\n          filter: url(#gaussian-blur);\n  filter: blur(2px);\n  -webkit-filter: blur(2px);\n}\n\n.react-confirm-alert-overlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  z-index: 99;\n  background: rgba(255, 255, 255, 0.9);\n  display: -moz-flex;\n  display: -ms-flex;\n  display: -o-flex;\n  display: flex;\n  justify-content: center;\n  -ms-align-items: center;\n  align-items: center;\n  opacity: 0;\n  -webkit-animation: react-confirm-alert-fadeIn 0.5s 0.2s forwards;\n  animation: react-confirm-alert-fadeIn 0.5s 0.2s forwards;\n}\n\n.react-confirm-alert-body {\n  font-family: Arial, Helvetica, sans-serif;\n  width: 400px;\n  padding: 30px;\n  text-align: left;\n  background: #fff;\n  border-radius: 10px;\n  box-shadow: 0 20px 75px rgba(0, 0, 0, 0.13);\n  color: #666;\n}\n\n.react-confirm-alert-svg {\n  position: absolute;\n  top: 0;\n  left: 0;\n}\n\n.react-confirm-alert-body > h1 {\n  margin-top: 0;\n}\n\n.react-confirm-alert-body > h3 {\n  margin: 0;\n  font-size: 16px;\n}\n\n.react-confirm-alert-button-group {\n  display: -moz-flex;\n  display: -ms-flex;\n  display: -o-flex;\n  display: flex;\n  justify-content: flex-start;\n  margin-top: 20px;\n}\n\n.react-confirm-alert-button-group > button {\n  outline: none;\n  background: #333;\n  border: none;\n  display: inline-block;\n  padding: 6px 18px;\n  color: #eee;\n  margin-right: 10px;\n  border-radius: 5px;\n  font-size: 12px;\n  cursor: pointer;\n}\n\n@-webkit-keyframes react-confirm-alert-fadeIn {\n  from {\n    opacity: 0;\n  }\n  to {\n    opacity: 1;\n  }\n}\n\n@keyframes react-confirm-alert-fadeIn {\n  from {\n    opacity: 0;\n  }\n  to {\n    opacity: 1;\n  }\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/react-confirm-alert/lib/index.js":
/*!*******************************************************!*\
  !*** ./node_modules/react-confirm-alert/lib/index.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

exports.confirmAlert = confirmAlert;

var _react = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ReactConfirmAlert = (_temp2 = _class = function (_Component) {
  _inherits(ReactConfirmAlert, _Component);

  function ReactConfirmAlert() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, ReactConfirmAlert);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = ReactConfirmAlert.__proto__ || Object.getPrototypeOf(ReactConfirmAlert)).call.apply(_ref, [this].concat(args))), _this), _this.handleClickButton = function (button) {
      if (button.onClick) button.onClick();
      _this.close();
    }, _this.handleClickOverlay = function (e) {
      var onClickOutside = _this.props.onClickOutside;

      if (e.target === _this.overlay) {
        onClickOutside();
        _this.close();
      }
    }, _this.close = function () {
      removeBodyClass();
      removeElementReconfirm();
      removeSVGBlurReconfirm();
    }, _this.keyboardClose = function (event) {
      var onKeypressEscape = _this.props.onKeypressEscape;

      if (event.keyCode === 27) {
        onKeypressEscape(event);
        _this.close();
      }
    }, _this.componentDidMount = function () {
      document.addEventListener('keydown', _this.keyboardClose, false);
    }, _this.componentWillUnmount = function () {
      document.removeEventListener('keydown', _this.keyboardClose, false);
      _this.props.willUnmount();
    }, _this.renderCustomUI = function () {
      var _this$props = _this.props,
          title = _this$props.title,
          message = _this$props.message,
          customUI = _this$props.customUI;

      var dataCustomUI = {
        title: title,
        message: message,
        onClose: _this.close
      };

      return customUI(dataCustomUI);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(ReactConfirmAlert, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          title = _props.title,
          message = _props.message,
          buttons = _props.buttons,
          childrenElement = _props.childrenElement,
          customUI = _props.customUI;


      return _react2.default.createElement(
        'div',
        {
          className: 'react-confirm-alert-overlay',
          ref: function ref(dom) {
            return _this2.overlay = dom;
          },
          onClick: this.handleClickOverlay
        },
        _react2.default.createElement(
          'div',
          { className: 'react-confirm-alert' },
          customUI ? this.renderCustomUI() : _react2.default.createElement(
            'div',
            { className: 'react-confirm-alert-body' },
            title && _react2.default.createElement(
              'h1',
              null,
              title
            ),
            message,
            childrenElement(),
            _react2.default.createElement(
              'div',
              { className: 'react-confirm-alert-button-group' },
              buttons.map(function (button, i) {
                return _react2.default.createElement(
                  'button',
                  {
                    key: i,
                    onClick: function onClick() {
                      return _this2.handleClickButton(button);
                    }
                  },
                  button.label
                );
              })
            )
          )
        )
      );
    }
  }]);

  return ReactConfirmAlert;
}(_react.Component), _class.propTypes = {
  title: _propTypes2.default.string,
  message: _propTypes2.default.string,
  buttons: _propTypes2.default.array.isRequired,
  childrenElement: _propTypes2.default.func,
  customUI: _propTypes2.default.func,
  willUnmount: _propTypes2.default.func,
  onClickOutside: _propTypes2.default.func,
  onKeypressEscape: _propTypes2.default.func
}, _class.defaultProps = {
  buttons: [{
    label: 'Cancel',
    onClick: function onClick() {
      return null;
    }
  }, {
    label: 'Confirm',
    onClick: function onClick() {
      return null;
    }
  }],
  childrenElement: function childrenElement() {
    return null;
  },
  willUnmount: function willUnmount() {
    return null;
  },
  onClickOutside: function onClickOutside() {
    return null;
  },
  onKeypressEscape: function onKeypressEscape() {
    return null;
  }
}, _temp2);
exports.default = ReactConfirmAlert;


function createSVGBlurReconfirm() {
  // If has svg ignore to create the svg
  var svg = document.getElementById('react-confirm-alert-firm-svg');
  if (svg) return;
  var svgNS = 'http://www.w3.org/2000/svg';
  var feGaussianBlur = document.createElementNS(svgNS, 'feGaussianBlur');
  feGaussianBlur.setAttribute('stdDeviation', '0.3');

  var filter = document.createElementNS(svgNS, 'filter');
  filter.setAttribute('id', 'gaussian-blur');
  filter.appendChild(feGaussianBlur);

  var svgElem = document.createElementNS(svgNS, 'svg');
  svgElem.setAttribute('id', 'react-confirm-alert-firm-svg');
  svgElem.setAttribute('class', 'react-confirm-alert-svg');
  svgElem.appendChild(filter);

  document.body.appendChild(svgElem);
}

function removeSVGBlurReconfirm() {
  var svg = document.getElementById('react-confirm-alert-firm-svg');
  svg.parentNode.removeChild(svg);
  document.body.children[0].classList.remove('react-confirm-alert-blur');
}

function createElementReconfirm(properties) {
  var divTarget = document.getElementById('react-confirm-alert');
  if (divTarget) {
    // Rerender - the mounted ReactConfirmAlert
    (0, _reactDom.render)(_react2.default.createElement(ReactConfirmAlert, properties), divTarget);
  } else {
    // Mount the ReactConfirmAlert component
    document.body.children[0].classList.add('react-confirm-alert-blur');
    divTarget = document.createElement('div');
    divTarget.id = 'react-confirm-alert';
    document.body.appendChild(divTarget);
    (0, _reactDom.render)(_react2.default.createElement(ReactConfirmAlert, properties), divTarget);
  }
}

function removeElementReconfirm() {
  var target = document.getElementById('react-confirm-alert');
  (0, _reactDom.unmountComponentAtNode)(target);
  target.parentNode.removeChild(target);
}

function addBodyClass() {
  document.body.classList.add('react-confirm-alert-body-element');
}

function removeBodyClass() {
  document.body.classList.remove('react-confirm-alert-body-element');
}

function confirmAlert(properties) {
  addBodyClass();
  createSVGBlurReconfirm();
  createElementReconfirm(properties);
}

/***/ }),

/***/ "./node_modules/react-confirm-alert/src/react-confirm-alert.css":
/*!**********************************************************************!*\
  !*** ./node_modules/react-confirm-alert/src/react-confirm-alert.css ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../css-loader??ref--6-1!../../postcss-loader/src??ref--6-2!./react-confirm-alert.css */ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./node_modules/react-confirm-alert/src/react-confirm-alert.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ })

}]);