(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[26],{

/***/ "./resources/js/components/functions/Utils.js":
/*!****************************************************!*\
  !*** ./resources/js/components/functions/Utils.js ***!
  \****************************************************/
/*! exports provided: handleSearch, handleSearch2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleSearch", function() { return handleSearch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleSearch2", function() { return handleSearch2; });
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! fuse.js */ "./node_modules/fuse.js/dist/fuse.js");
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fuse_js__WEBPACK_IMPORTED_MODULE_0__);
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


function handleSearch(e, searchKeys) {
  var _this = this;

  var searchTerms = e.target.value;
  searchKeys.forEach(function (key) {
    if (searchTerms.trim() !== "") {
      var options = _objectSpread({}, _globalFuseOptions, {
        keys: _globalFuseKeys[key]
      });

      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_0___default.a(_this.state[key], options);

      _this.setState(_defineProperty({}, "".concat(key, "Filtered"), fuse.search(searchTerms)));
    } else {
      _this.setState(_defineProperty({}, "".concat(key, "Filtered"), _this.state[key]));
    }
  });
}
function handleSearch2(e) {
  this.setState({
    searchTerms: e.target.value
  });
}

/***/ }),

/***/ "./resources/js/components/ui/ModuleSearchBox.js":
/*!*******************************************************!*\
  !*** ./resources/js/components/ui/ModuleSearchBox.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");



var ModuleSearchBox = function ModuleSearchBox(props) {
  var handleSearch = props.handleSearch,
      placeholder = props.placeholder;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["InputGroup"], {
    className: "input-prepend mb-4"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["InputGroupAddon"], {
    addonType: "prepend"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["InputGroupText"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-search"
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    bsSize: "lg",
    type: "text",
    placeholder: placeholder,
    onChange: handleSearch
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (ModuleSearchBox);

/***/ }),

/***/ "./resources/js/components/ui/PageHeader.js":
/*!**************************************************!*\
  !*** ./resources/js/components/ui/PageHeader.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");




var PageHeader = function PageHeader(props) {
  var pageTitle = props.pageTitle,
      pageInfo = props.pageInfo,
      pageResource = props.pageResource;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mb-4"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "card-title mb-0"
  }, pageTitle), pageInfo && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "text-muted"
  }, pageInfo), pageResource && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: "/".concat(pageResource, "/new")
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
    color: "primary"
  }, "Add New")));
};

/* harmony default export */ __webpack_exports__["default"] = (PageHeader);

/***/ }),

/***/ "./resources/js/components/views/DropdownEditMenu.js":
/*!***********************************************************!*\
  !*** ./resources/js/components/views/DropdownEditMenu.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../actions */ "./resources/js/actions.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







var DropdownEditMenu =
/*#__PURE__*/
function (_Component) {
  _inherits(DropdownEditMenu, _Component);

  function DropdownEditMenu() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, DropdownEditMenu);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(DropdownEditMenu)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      card: false
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleClick", function (e, action) {
      var _this$props = _this.props,
          id = _this$props.id,
          resource = _this$props.resource;
      e.preventDefault();

      switch (action) {
        case "EDIT":
          _this.props.history.push("/".concat(resource, "/").concat(id, "/edit"));

          return true;

        case "DELETE":
          _this.props.deleteItem(resource, id, _this.props.history);

          return true;

        default:
          _this.props.history.push("/".concat(resource, "/").concat(id, "/view"));

          return true;
      }
    });

    return _this;
  }

  _createClass(DropdownEditMenu, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var actions = this.props.actions;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["ButtonGroup"], {
        className: "float-right ml-2"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["ButtonDropdown"], {
        id: "card1",
        isOpen: this.state.card,
        toggle: function toggle() {
          _this2.setState({
            card: !_this2.state.card
          });
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownToggle"], {
        className: "p-0",
        color: "default"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "icon-settings"
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownMenu"], {
        right: true
      }, actions.map(function (act, idx) {
        switch (act) {
          case 'EDIT':
            return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownItem"], {
              key: idx,
              onClick: function onClick(e) {
                return _this2.handleClick(e, "EDIT");
              }
            }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
              className: "fa fa-edit"
            }), " Edit");

          case 'DELETE':
            return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownItem"], {
              key: idx,
              onClick: function onClick(e) {
                return _this2.handleClick(e, "DELETE");
              }
            }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
              className: "fa fa-ban"
            }), " Delete");

          default:
            return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["DropdownItem"], {
              key: idx,
              onClick: function onClick(e) {
                return _this2.handleClick(e, "VIEW");
              }
            }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
              className: "fa fa-eye"
            }), " View");
        }
      }))));
    }
  }]);

  return DropdownEditMenu;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  return {};
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    deleteItem: function deleteItem(resource, id, history) {
      dispatch(Object(_actions__WEBPACK_IMPORTED_MODULE_4__["deleteResource"])(resource, id, history));
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["withRouter"])(Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps, mapDispatchToProps)(DropdownEditMenu)));

/***/ }),

/***/ "./resources/js/components/views/News/News.js":
/*!****************************************************!*\
  !*** ./resources/js/components/views/News/News.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _ui_ModuleSearchBox__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../ui/ModuleSearchBox */ "./resources/js/components/ui/ModuleSearchBox.js");
/* harmony import */ var _ui_Tables_DataTable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../ui/Tables/DataTable */ "./resources/js/components/ui/Tables/DataTable.js");
/* harmony import */ var _functions_Utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../functions/Utils */ "./resources/js/components/functions/Utils.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _ui_PageHeader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../ui/PageHeader */ "./resources/js/components/ui/PageHeader.js");
/* harmony import */ var _ui_PublishedNews__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./ui/PublishedNews */ "./resources/js/components/views/News/ui/PublishedNews.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }











var News =
/*#__PURE__*/
function (_Component) {
  _inherits(News, _Component);

  function News(props) {
    var _this;

    _classCallCheck(this, News);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(News).call(this, props));
    _this.state = {
      searchTerms: ""
    };
    _this.handleSearch = _functions_Utils__WEBPACK_IMPORTED_MODULE_5__["handleSearch2"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(News, [{
    key: "render",
    value: function render() {
      var searchTerms = this.state.searchTerms;
      var currentView = this.props.currentView;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "animated fadeIn"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_ModuleSearchBox__WEBPACK_IMPORTED_MODULE_3__["default"], {
        handleSearch: this.handleSearch,
        placeholder: "Search..."
      }), currentView.admin && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Card"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["CardBody"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_PageHeader__WEBPACK_IMPORTED_MODULE_7__["default"], {
        pageTitle: "News",
        pageInfo: "Here you\u2019ll find all the latest news and updates \u2013 including training event announcements, inspection results and information about new legislation. Check this page regularly to keep up to date with what\u2019s going on in the Banana Moon network.",
        pageResource: "news"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Tables_DataTable__WEBPACK_IMPORTED_MODULE_4__["default"], {
        tableMap: "news",
        searchTerms: searchTerms
      }))))), !currentView.admin && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_PublishedNews__WEBPACK_IMPORTED_MODULE_8__["default"], {
        searchTerms: searchTerms
      }));
    }
  }]);

  return News;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref) {
  var currentView = _ref.currentView;
  return {
    currentView: currentView
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_6__["connect"])(mapStateToProps)(News));

/***/ }),

/***/ "./resources/js/components/views/News/ui/Featured.js":
/*!***********************************************************!*\
  !*** ./resources/js/components/views/News/ui/Featured.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _DropdownEditMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../DropdownEditMenu */ "./resources/js/components/views/DropdownEditMenu.js");






var Featured = function Featured(props) {
  var title = props.title,
      headline = props.headline,
      publish = props.publish,
      author = props.author,
      id = props.id,
      featured_image = props.featured_image,
      isAdmin = props.isAdmin,
      location = props.location;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Card"], {
    className: "animated fadeIn"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["CardBody"], null, isAdmin && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DropdownEditMenu__WEBPACK_IMPORTED_MODULE_4__["default"], {
    resource: "News",
    id: id,
    actions: ['DELETE', 'EDIT']
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "card-title mb-0"
  }, "Latest News"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "text-muted"
  }, "Here you\u2019ll find all the latest news and updates \u2013 including training event announcements, inspection results and information about new legislation. Check this page regularly to keep up to date with what\u2019s going on in the Banana Moon network."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], null, featured_image && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
    md: "6"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["CardImg"], {
    width: "100%",
    src: featured_image
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], {
    md: featured_image ? "6" : "12"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "display-4"
  }, title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "lead"
  }, headline), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
    className: "my-2"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Published ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", null, moment__WEBPACK_IMPORTED_MODULE_2___default()(publish).fromNow()), " by ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("em", null, author, ", ", location)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "lead"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    className: "btn btn-primary btn-lg",
    to: "/news/".concat(id)
  }, "Read More"))))));
};

/* harmony default export */ __webpack_exports__["default"] = (Featured);

/***/ }),

/***/ "./resources/js/components/views/News/ui/NewsCard.js":
/*!***********************************************************!*\
  !*** ./resources/js/components/views/News/ui/NewsCard.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _DropdownEditMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../DropdownEditMenu */ "./resources/js/components/views/DropdownEditMenu.js");






var NewsCard = function NewsCard(props) {
  var title = props.title,
      headline = props.headline,
      publish = props.publish,
      author = props.author,
      id = props.id,
      featured_image = props.featured_image,
      isAdmin = props.isAdmin,
      showAll = props.showAll,
      published = props.published,
      updated = props.updated,
      draft = props.draft,
      location = props.location;
  var draftBadge = showAll && draft ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Badge"], {
    color: "warning"
  }, "Draft"), " Last updated ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", null, moment__WEBPACK_IMPORTED_MODULE_1___default()(updated).fromNow()), " by ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("em", null, author, ", ", location)) : null;
  var upcomingBadge = showAll && !draft && !published ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Badge"], {
    color: "success"
  }, "Upcoming"), " Will be published ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", null, moment__WEBPACK_IMPORTED_MODULE_1___default()(publish).fromNow()), " by ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("em", null, author, ", ", location)) : null;
  var publishedBadge = !draft && published ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, showAll && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Badge"], {
    color: "primary"
  }, "Published"), " Published ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", null, moment__WEBPACK_IMPORTED_MODULE_1___default()(publish).fromNow()), " by ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("em", null, author, ", ", location)) : null;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Card"], {
    className: "mb-4 animated fadeIn"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["CardImg"], {
    top: true,
    width: "100%",
    src: featured_image
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["CardBody"], null, isAdmin && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DropdownEditMenu__WEBPACK_IMPORTED_MODULE_4__["default"], {
    resource: "News",
    id: id,
    actions: ['EDIT', 'DELETE']
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["CardTitle"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    className: "display-5"
  }, title)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["CardText"], null, headline), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", {
    className: "my-2"
  }), draftBadge, " ", upcomingBadge, " ", publishedBadge, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
    className: "btn btn-primary",
    to: "/news/".concat(id)
  }, "Read more")));
};

/* harmony default export */ __webpack_exports__["default"] = (NewsCard);

/***/ }),

/***/ "./resources/js/components/views/News/ui/PublishedNews.js":
/*!****************************************************************!*\
  !*** ./resources/js/components/views/News/ui/PublishedNews.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! fuse.js */ "./node_modules/fuse.js/dist/fuse.js");
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(fuse_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _Featured__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Featured */ "./resources/js/components/views/News/ui/Featured.js");
/* harmony import */ var _NewsCard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./NewsCard */ "./resources/js/components/views/News/ui/NewsCard.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }








var PublishedNews =
/*#__PURE__*/
function (_Component) {
  _inherits(PublishedNews, _Component);

  function PublishedNews() {
    _classCallCheck(this, PublishedNews);

    return _possibleConstructorReturn(this, _getPrototypeOf(PublishedNews).apply(this, arguments));
  }

  _createClass(PublishedNews, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          latestNews = _this$props.latestNews,
          publishedNews = _this$props.publishedNews,
          searchTerms = _this$props.searchTerms;
      var newsGridItems = this.props.newsGridItems;

      if (undefined != searchTerms && searchTerms.trim() !== "") {
        var searchOptions = _objectSpread({}, _globalFuseOptions, {
          keys: _globalFuseKeys['news']
        });

        var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_1___default.a(publishedNews, searchOptions);
        newsGridItems = fuse.search(searchTerms.trim());
      }

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], null, latestNews && undefined !== searchTerms && searchTerms.trim() === "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Featured__WEBPACK_IMPORTED_MODULE_4__["default"], latestNews))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Col"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["CardColumns"], null, newsGridItems.map(function (item) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_NewsCard__WEBPACK_IMPORTED_MODULE_5__["default"], _extends({
          key: item.id
        }, item));
      })))));
    }
  }]);

  return PublishedNews;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref) {
  var news = _ref.news;
  var publishedNews = news.data.filter(function (item) {
    return !item.draft && item.published;
  });
  var latestNews = publishedNews.length > 0 ? publishedNews[0] : null;
  var newsGridItems = publishedNews.length > 0 ? publishedNews.filter(function (item) {
    return item.id !== latestNews.id;
  }) : [];
  return {
    latestNews: latestNews,
    newsGridItems: newsGridItems,
    publishedNews: publishedNews
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps)(PublishedNews));

/***/ })

}]);