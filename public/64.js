(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[64],{

/***/ "./resources/js/components/views/Tasks/ViewTodo.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/views/Tasks/ViewTodo.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_render_html__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-render-html */ "./node_modules/react-render-html/index.js");
/* harmony import */ var react_render_html__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_render_html__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _ui_Attachments__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../ui/Attachments */ "./resources/js/components/ui/Attachments.js");
/* harmony import */ var _ui_DeadlineBadge__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../ui/DeadlineBadge */ "./resources/js/components/ui/DeadlineBadge.js");
/* harmony import */ var _ui_CommentForm__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./ui/CommentForm */ "./resources/js/components/views/Tasks/ui/CommentForm.js");
/* harmony import */ var _ui_Comment__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./ui/Comment */ "./resources/js/components/views/Tasks/ui/Comment.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }












var ViewTodo =
/*#__PURE__*/
function (_Component) {
  _inherits(ViewTodo, _Component);

  function ViewTodo() {
    _classCallCheck(this, ViewTodo);

    return _possibleConstructorReturn(this, _getPrototypeOf(ViewTodo).apply(this, arguments));
  }

  _createClass(ViewTodo, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          task = _this$props.task,
          todo = _this$props.todo;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "animated fadeIn"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
        md: "12"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Card"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardBody"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
        className: "card-title"
      }, "Task"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
        className: "display-5"
      }, task.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "mb-0",
        title: "Deadline"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "icon-clock"
      }), "\xA0", moment__WEBPACK_IMPORTED_MODULE_3___default()(todo.deadline).format('DD MMMM YYYY'), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_DeadlineBadge__WEBPACK_IMPORTED_MODULE_7__["default"], {
        item: todo
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "icon-user",
        title: "User"
      }), "\xA0", todo.name), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react_render_html__WEBPACK_IMPORTED_MODULE_4___default()(task.body)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Attachments__WEBPACK_IMPORTED_MODULE_6__["default"], {
        parentId: task.id
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Card"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardHeader"], null, todo.comments.length === 1 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "1 Comment"), todo.comments.length !== 1 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, todo.comments.length, " Comments")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardBody"], null, todo.comments.length < 1 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "There are no comments yet."), todo.comments.length > 0 && todo.comments.map(function (comment, idx) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Comment__WEBPACK_IMPORTED_MODULE_9__["default"], _extends({
          key: idx
        }, comment));
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_CommentForm__WEBPACK_IMPORTED_MODULE_8__["default"], {
        id: todo.id
      }))));
    }
  }]);

  return ViewTodo;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref, _ref2) {
  var todos = _ref.todos,
      tasks = _ref.tasks,
      users = _ref.users;
  var match = _ref2.match;
  var todo = todos.data.find(function (item) {
    return item.id === match.params.unique_id;
  });
  var task = tasks.data.find(function (item) {
    return item.id === todo.task_id;
  });
  return {
    todo: todo,
    task: task
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["withRouter"])(Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps, null)(ViewTodo)));

/***/ })

}]);