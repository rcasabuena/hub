(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[44],{

/***/ "./resources/js/components/views/Tasks/TodoView.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/views/Tasks/TodoView.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_render_html__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-render-html */ "./node_modules/react-render-html/index.js");
/* harmony import */ var react_render_html__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_render_html__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _ui_Attachments__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../ui/Attachments */ "./resources/js/components/ui/Attachments.js");
/* harmony import */ var _ui_DeadlineBadge__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../ui/DeadlineBadge */ "./resources/js/components/ui/DeadlineBadge.js");
/* harmony import */ var _objects_AppTodo__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../objects/AppTodo */ "./resources/js/objects/AppTodo.js");
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../actions */ "./resources/js/actions.js");
/* harmony import */ var _ui_CommentForm__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./ui/CommentForm */ "./resources/js/components/views/Tasks/ui/CommentForm.js");
/* harmony import */ var _ui_Comment__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./ui/Comment */ "./resources/js/components/views/Tasks/ui/Comment.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }














var TodoView =
/*#__PURE__*/
function (_Component) {
  _inherits(TodoView, _Component);

  function TodoView() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, TodoView);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(TodoView)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleAction", function (e, action) {
      e.preventDefault();
      var _this$props = _this.props,
          todo = _this$props.todo,
          updateTodo = _this$props.updateTodo;
      var data = new _objects_AppTodo__WEBPACK_IMPORTED_MODULE_8__["default"](todo.id, todo.task_id, todo.user_id, moment__WEBPACK_IMPORTED_MODULE_3___default()(todo.deadline).format('YYYY-MM-DD'));
      data[action] = true;
      updateTodo(data.formData);
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "goToNextTodo", function () {
      var _this$props2 = _this.props,
          history = _this$props2.history,
          todos = _this$props2.todos;
      var location = 0;
      if (undefined != history.location.todoId) location = todos.findIndex(function (item) {
        return item.id === history.location.todoId;
      });
      var nextLocation = undefined != todos[location + 1] ? todos[location + 1] : todos[0];
      history.push({
        pathName: '/todos',
        todoId: nextLocation.id
      });
    });

    return _this;
  }

  _createClass(TodoView, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props3 = this.props,
          task = _this$props3.task,
          todo = _this$props3.todo,
          assignee = _this$props3.assignee,
          todos = _this$props3.todos;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "animated fadeIn"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Col"], {
        md: "12"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Card"], null, todos.length < 1 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardBody"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
        className: "card-title"
      }, "Task"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You do not have any tasks outstanding.")), todos.length > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardBody"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
        className: "card-title"
      }, "Task"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
        className: "display-5"
      }, task.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "mb-0",
        title: "Deadline"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "icon-clock"
      }), "\xA0", moment__WEBPACK_IMPORTED_MODULE_3___default()(todo.deadline).format('DD MMMM YYYY'), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_DeadlineBadge__WEBPACK_IMPORTED_MODULE_7__["default"], {
        item: todo
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "icon-user-follow",
        title: "Assignee"
      }), "\xA0", assignee.name, " on ", moment__WEBPACK_IMPORTED_MODULE_3___default()(todo.updated_at.date).format('DD MMMM YYYY')), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react_render_html__WEBPACK_IMPORTED_MODULE_4___default()(task.body)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Attachments__WEBPACK_IMPORTED_MODULE_6__["default"], {
        parentId: task.id
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
        size: "sm",
        color: "primary",
        disabled: todos.length < 2,
        className: "mr-2",
        onClick: this.goToNextTodo
      }, "Next Task"), !todo.completed && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
        size: "sm",
        color: "success",
        className: "mr-2",
        onClick: function onClick(e, action) {
          return _this2.handleAction(e, 'completed');
        }
      }, "Complete"), !todo.completed && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
        size: "sm",
        color: "danger",
        className: "pull-right",
        onClick: function onClick(e, action) {
          return _this2.handleAction(e, 'rejected');
        }
      }, "Reject"))), todos.length > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Card"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardHeader"], null, todo.comments.length === 1 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "1 Comment"), todo.comments.length !== 1 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, todo.comments.length, " Comments")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["CardBody"], null, todo.comments.length < 1 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "There are no comments yet."), todo.comments.length > 0 && todo.comments.map(function (comment, idx) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Comment__WEBPACK_IMPORTED_MODULE_11__["default"], _extends({
          key: idx
        }, comment));
      }))), todos.length > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_CommentForm__WEBPACK_IMPORTED_MODULE_10__["default"], {
        id: todo.id
      }))));
    }
  }]);

  return TodoView;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref, _ref2) {
  var todos = _ref.todos,
      tasks = _ref.tasks,
      users = _ref.users,
      currentUser = _ref.currentUser;
  var history = _ref2.history;
  var userTodos = todos.data.filter(function (item) {
    return item.user_id === currentUser.id && !item.rejected && (!item.completed || item.completed && moment__WEBPACK_IMPORTED_MODULE_3___default()(item.completed_at).isSame(new Date(), 'day'));
  }).sort(function (a, b) {
    a = new Date(a.deadline);
    b = new Date(b.deadline);
    return a > b ? -1 : a < b ? 1 : 0;
  });
  var todo = userTodos[0];
  if (undefined !== history.location.todoId && todos.data.find(function (item) {
    return item.id === history.location.todoId && !item.rejected;
  })) todo = todos.data.find(function (item) {
    return item.id === history.location.todoId;
  });
  return {
    todo: todo,
    todos: userTodos,
    task: undefined != todo ? tasks.data.find(function (item) {
      return item.id === todo.task_id;
    }) : null,
    assignee: undefined != todo ? users.data.find(function (item) {
      return item.id === todo.added_by;
    }) : null
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    updateTodo: function updateTodo(data) {
      dispatch(Object(_actions__WEBPACK_IMPORTED_MODULE_9__["saveResource"])('Todos', data));
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["withRouter"])(Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps, mapDispatchToProps)(TodoView)));

/***/ }),

/***/ "./resources/js/objects/AppTodo.js":
/*!*****************************************!*\
  !*** ./resources/js/objects/AppTodo.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AppTodo; });
/* harmony import */ var _AppItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AppItem */ "./resources/js/objects/AppItem.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }



var AppTodo =
/*#__PURE__*/
function (_AppItem) {
  _inherits(AppTodo, _AppItem);

  function AppTodo() {
    var _this;

    var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
    var task_id = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
    var user_id = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
    var deadline = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "";
    var completed = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
    var rejected = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : false;
    var accepted = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : false;

    _classCallCheck(this, AppTodo);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(AppTodo).call(this, id));
    _this.task_id = task_id;
    _this.user_id = user_id;
    _this.deadline = deadline;
    _this.completed = completed;
    _this.rejected = rejected;
    _this.accepted = accepted;
    return _this;
  }

  return AppTodo;
}(_AppItem__WEBPACK_IMPORTED_MODULE_0__["default"]);



/***/ })

}]);