(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[79],{

/***/ "./resources/js/components/ui/PageHeader.js":
/*!**************************************************!*\
  !*** ./resources/js/components/ui/PageHeader.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");




var PageHeader = function PageHeader(props) {
  var pageTitle = props.pageTitle,
      pageInfo = props.pageInfo,
      pageResource = props.pageResource;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mb-4"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "card-title mb-0"
  }, pageTitle), pageInfo && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: "text-muted"
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam dapibus tempus ultricies. Sed ullamcorper elit sit amet gravida dictum. Aenean odio tellus, auctor non interdum vitae, dapibus at odio."), pageResource && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
    to: "/".concat(pageResource, "/new")
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
    color: "primary"
  }, "Add New")));
};

/* harmony default export */ __webpack_exports__["default"] = (PageHeader);

/***/ }),

/***/ "./resources/js/components/ui/Tables/DataTable.js":
/*!********************************************************!*\
  !*** ./resources/js/components/ui/Tables/DataTable.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, exports) {

throw new Error("Module build failed (from ./node_modules/babel-loader/lib/index.js):\nSyntaxError: /Users/RitchieC/Documents/Sites/multitenant/resources/js/components/ui/Tables/DataTable.js: Unexpected token (68:2)\n\n\u001b[0m \u001b[90m 66 | \u001b[39m    fetching \u001b[33m=\u001b[39m state[\u001b[32m'todos'\u001b[39m]\u001b[33m.\u001b[39mfetching\u001b[0m\n\u001b[0m \u001b[90m 67 | \u001b[39m    map \u001b[33m=\u001b[39m \u001b[0m\n\u001b[0m\u001b[31m\u001b[1m>\u001b[22m\u001b[39m\u001b[90m 68 | \u001b[39m  }\u001b[0m\n\u001b[0m \u001b[90m    | \u001b[39m  \u001b[31m\u001b[1m^\u001b[22m\u001b[39m\u001b[0m\n\u001b[0m \u001b[90m 69 | \u001b[39m\u001b[0m\n\u001b[0m \u001b[90m 70 | \u001b[39m  \u001b[36mif\u001b[39m (undefined \u001b[33m!==\u001b[39m filterBy \u001b[33m&&\u001b[39m undefined \u001b[33m!==\u001b[39m filterByValue) tableData \u001b[33m=\u001b[39m tableData\u001b[33m.\u001b[39mfilter(item \u001b[33m=>\u001b[39m item[filterBy] \u001b[33m==\u001b[39m filterByValue)\u001b[0m\n\u001b[0m \u001b[90m 71 | \u001b[39m\u001b[0m\n    at Object.raise (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:3834:17)\n    at Object.unexpected (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:5142:16)\n    at Object.parseExprAtom (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:6279:20)\n    at Object.parseExprAtom (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:3547:20)\n    at Object.parseExprSubscripts (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:5848:23)\n    at Object.parseMaybeUnary (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:5828:21)\n    at Object.parseExprOps (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:5717:23)\n    at Object.parseMaybeConditional (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:5690:23)\n    at Object.parseMaybeAssign (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:5635:21)\n    at Object.parseMaybeAssign (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:5676:25)\n    at Object.parseExpression (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:5587:23)\n    at Object.parseStatementContent (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:7321:23)\n    at Object.parseStatement (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:7199:17)\n    at Object.parseBlockOrModuleBlockBody (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:7757:25)\n    at Object.parseBlockBody (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:7744:10)\n    at Object.parseBlock (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:7733:10)\n    at Object.parseStatementContent (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:7262:21)\n    at Object.parseStatement (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:7199:17)\n    at Object.parseIfStatement (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:7544:28)\n    at Object.parseStatementContent (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:7234:21)\n    at Object.parseStatement (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:7199:17)\n    at Object.parseBlockOrModuleBlockBody (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:7757:25)\n    at Object.parseBlockBody (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:7744:10)\n    at Object.parseBlock (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:7733:10)\n    at Object.parseFunctionBody (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:6865:24)\n    at Object.parseArrowExpression (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:6818:10)\n    at Object.parseParenAndDistinguishExpression (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:6448:12)\n    at Object.parseExprAtom (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:6211:21)\n    at Object.parseExprAtom (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:3547:20)\n    at Object.parseExprSubscripts (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:5848:23)\n    at Object.parseMaybeUnary (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:5828:21)\n    at Object.parseExprOps (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:5717:23)\n    at Object.parseMaybeConditional (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:5690:23)\n    at Object.parseMaybeAssign (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:5635:21)\n    at Object.parseVar (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:7827:26)\n    at Object.parseVarStatement (/Users/RitchieC/Documents/Sites/multitenant/node_modules/@babel/core/node_modules/@babel/parser/lib/index.js:7656:10)");

/***/ }),

/***/ "./resources/js/components/views/Users/ViewUser.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/views/Users/ViewUser.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _ui_PageHeader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../ui/PageHeader */ "./resources/js/components/ui/PageHeader.js");
/* harmony import */ var _ui_Tables_DataTable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../ui/Tables/DataTable */ "./resources/js/components/ui/Tables/DataTable.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }







var ViewUser =
/*#__PURE__*/
function (_Component) {
  _inherits(ViewUser, _Component);

  function ViewUser() {
    _classCallCheck(this, ViewUser);

    return _possibleConstructorReturn(this, _getPrototypeOf(ViewUser).apply(this, arguments));
  }

  _createClass(ViewUser, [{
    key: "render",
    value: function render() {
      var user = this.props.user;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "animated fadeIn"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Card"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["CardBody"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", null, user.name), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "icon-envelope"
      }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        href: "mailto:".concat(user.email)
      }, user.email)), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "ml-2"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "icon-location-pin"
      }), " ", user.location && user.location.name)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Tables_DataTable__WEBPACK_IMPORTED_MODULE_4__["default"], {
        tableMap: "downloads",
        filterBy: "user_id",
        filterByValue: user.id
      }))))));
    }
  }]);

  return ViewUser;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref, ownProps) {
  var users = _ref.users;
  return {
    user: users.data.find(function (item) {
      return item.id === ownProps.match.params.unique_id;
    })
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(ViewUser));

/***/ })

}]);