(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./resources/js/components/ui/DeadlineBadge.js":
/*!*****************************************************!*\
  !*** ./resources/js/components/ui/DeadlineBadge.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");




var DeadlineBadge = function DeadlineBadge(props) {
  var item = props.item;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, item.rejected && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Badge"], {
    className: "ml-1",
    color: "danger"
  }, "Rejected"), !item.rejected && item.completed && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Badge"], {
    className: "ml-1",
    color: "success"
  }, "Completed"), !item.rejected && !item.completed && moment__WEBPACK_IMPORTED_MODULE_1___default()(item.deadline).isBefore(new Date(), 'day') && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Badge"], {
    className: "ml-1",
    color: "danger"
  }, "Overdue"), !item.rejected && !item.completed && moment__WEBPACK_IMPORTED_MODULE_1___default()(item.deadline).isSame(new Date(), 'day') && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Badge"], {
    className: "ml-1",
    color: "warning"
  }, "Due today"));
};

/* harmony default export */ __webpack_exports__["default"] = (DeadlineBadge);

/***/ }),

/***/ "./resources/js/components/ui/DropdownEditMenu.js":
/*!********************************************************!*\
  !*** ./resources/js/components/ui/DropdownEditMenu.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_confirm_alert__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-confirm-alert */ "./node_modules/react-confirm-alert/lib/index.js");
/* harmony import */ var react_confirm_alert__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_confirm_alert__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../actions */ "./resources/js/actions.js");
/* harmony import */ var react_confirm_alert_src_react_confirm_alert_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-confirm-alert/src/react-confirm-alert.css */ "./node_modules/react-confirm-alert/src/react-confirm-alert.css");
/* harmony import */ var react_confirm_alert_src_react_confirm_alert_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_confirm_alert_src_react_confirm_alert_css__WEBPACK_IMPORTED_MODULE_6__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







 // Import css

var DropdownEditMenu =
/*#__PURE__*/
function (_Component) {
  _inherits(DropdownEditMenu, _Component);

  function DropdownEditMenu() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, DropdownEditMenu);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(DropdownEditMenu)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      card: false
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleClick", function (e, action) {
      var _this$props = _this.props,
          id = _this$props.id,
          resource = _this$props.resource;
      e.preventDefault();

      switch (action) {
        case "EDIT":
          _this.props.history.push("/".concat(resource, "/").concat(id, "/edit"));

          return true;

        case "DELETE":
          Object(react_confirm_alert__WEBPACK_IMPORTED_MODULE_1__["confirmAlert"])({
            customUI: function customUI(_ref) {
              var onClose = _ref.onClose;
              return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Card"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["CardBody"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", null, "Confirm to delete"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "You want to delete this item?")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["CardFooter"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
                className: "btn btn-dark mr-2",
                onClick: onClose
              }, "Cancel"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
                className: "btn btn-danger",
                onClick: function onClick() {
                  _this.props.deleteItem(resource, id, _this.props.history);

                  onClose();
                }
              }, "Delete")));
            }
          });
          return false;

        default:
          _this.props.history.push("/".concat(resource, "/").concat(id, "/view"));

          return true;
      }
    });

    return _this;
  }

  _createClass(DropdownEditMenu, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var actions = this.props.actions;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["ButtonGroup"], {
        className: "float-right ml-2"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["ButtonDropdown"], {
        id: "card1",
        isOpen: this.state.card,
        toggle: function toggle() {
          _this2.setState({
            card: !_this2.state.card
          });
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownToggle"], {
        className: "p-0",
        color: "default"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "icon-options-vertical"
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownMenu"], {
        right: true
      }, actions.map(function (act, idx) {
        switch (act) {
          case 'EDIT':
            return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownItem"], {
              key: idx,
              onClick: function onClick(e) {
                return _this2.handleClick(e, "EDIT");
              }
            }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
              className: "fa fa-edit"
            }), " Edit");

          case 'DELETE':
            return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownItem"], {
              key: idx,
              onClick: function onClick(e) {
                return _this2.handleClick(e, "DELETE");
              }
            }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
              className: "fa fa-ban"
            }), " Delete");

          default:
            return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["DropdownItem"], {
              key: idx,
              onClick: function onClick(e) {
                return _this2.handleClick(e, "VIEW");
              }
            }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
              className: "fa fa-eye"
            }), " View");
        }
      }))));
    }
  }]);

  return DropdownEditMenu;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(state) {
  return {};
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    deleteItem: function deleteItem(resource, id, history) {
      dispatch(Object(_actions__WEBPACK_IMPORTED_MODULE_5__["deleteResource"])(resource, id, history));
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["withRouter"])(Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps, mapDispatchToProps)(DropdownEditMenu)));

/***/ }),

/***/ "./resources/js/components/ui/Spinner.js":
/*!***********************************************!*\
  !*** ./resources/js/components/ui/Spinner.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


var Spinner = function Spinner() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle1 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle2 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle3 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle4 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle5 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle6 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle7 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle8 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle9 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle10 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle11 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle12 sk-child"
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Spinner);

/***/ }),

/***/ "./resources/js/components/ui/Tables/DataTable.js":
/*!********************************************************!*\
  !*** ./resources/js/components/ui/Tables/DataTable.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! fuse.js */ "./node_modules/fuse.js/dist/fuse.js");
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(fuse_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _maps__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./maps */ "./resources/js/components/ui/Tables/maps.js");
/* harmony import */ var _DropdownEditMenu__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../DropdownEditMenu */ "./resources/js/components/ui/DropdownEditMenu.js");
/* harmony import */ var _Spinner__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../Spinner */ "./resources/js/components/ui/Spinner.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }









var DataTable = function DataTable(props) {
  var tableMap = props.tableMap,
      isAdmin = props.isAdmin,
      searchTerms = props.searchTerms,
      fetching = props.fetching,
      tableData = props.tableData,
      todos = props.todos,
      tableMapActual = props.tableMapActual;
  var tableDataFiltered = tableData;
  var tMap = _maps__WEBPACK_IMPORTED_MODULE_4__["default"][tableMap];

  var searchOptions = _objectSpread({}, _globalFuseOptions, {
    keys: _globalFuseKeys[tableMapActual]
  });

  if (undefined !== searchTerms && searchTerms.trim() !== "") {
    var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_2___default.a(tableDataFiltered, searchOptions);
    tableDataFiltered = fuse.search(searchTerms.trim());
  } else {
    tableDataFiltered = tableData;
  }

  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, !fetching && tableData.length < 1 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "There are no ", tableMapActual, " yet."), !fetching && tableData.length > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Table"], {
    id: tableMapActual,
    hover: true,
    responsive: true,
    className: "table-outline mb-0 d-none d-sm-table"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("thead", {
    className: "thead-light"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, tMap.map(function (tHeader, idx) {
    return !tHeader.admin || isAdmin && tHeader.admin ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
      key: idx,
      className: tHeader.classNames
    }, tHeader.label) : null;
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tbody", null, tableDataFiltered.map(function (tRow, idx) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Fade"], {
      in: true,
      tag: "tr",
      key: idx
    }, tMap.map(function (item, idx) {
      return !item.admin || isAdmin && item.admin ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        key: idx,
        className: item.classNames
      }, item.value(tRow, tableMap, todos)) : null;
    }));
  }))), fetching && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Spinner__WEBPACK_IMPORTED_MODULE_6__["default"], null));
};

var mapStateToProps = function mapStateToProps(state, _ref) {
  var tableMap = _ref.tableMap,
      filterBy = _ref.filterBy,
      filterByValue = _ref.filterByValue;
  var tableData = undefined == state[tableMap] ? [] : state[tableMap].data;
  var fetching = undefined == state[tableMap] ? true : state[tableMap].fetching;
  var tableMapActual = undefined == state[tableMap] ? "" : tableMap;

  if (tableMap == 'userTodos') {
    tableData = state['todos'].data;
    fetching = state['todos'].fetching;
    tableMapActual = 'todos';
  }

  if (undefined !== filterBy && undefined !== filterByValue) tableData = tableData.filter(function (item) {
    return item[filterBy] == filterByValue;
  });

  if (tableMap === 'tasks') {
    if (state.currentUser.role === 'USER') {
      if (state.currentUser.director) tableData = tableData.filter(function (item) {
        return item.locationId == state.currentUser.location.id;
      });else tableData = [];
    } else {
      tableData = tableData.filter(function (item) {
        return item.locationId == null;
      });
    }
  }

  if (tableMap === 'tasks' && state.currentUser.role === 'USER') {}

  return {
    tableData: tableData,
    tableMapActual: tableMapActual,
    fetching: fetching,
    isAdmin: state.currentUser.role === 'ADMIN'
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(DataTable));

/***/ }),

/***/ "./resources/js/components/ui/Tables/maps.js":
/*!***************************************************!*\
  !*** ./resources/js/components/ui/Tables/maps.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _DropdownEditMenu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../DropdownEditMenu */ "./resources/js/components/ui/DropdownEditMenu.js");
/* harmony import */ var _maps_locations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./maps/locations */ "./resources/js/components/ui/Tables/maps/locations.js");
/* harmony import */ var _maps_users__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./maps/users */ "./resources/js/components/ui/Tables/maps/users.js");
/* harmony import */ var _maps_groups__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./maps/groups */ "./resources/js/components/ui/Tables/maps/groups.js");
/* harmony import */ var _maps_news__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./maps/news */ "./resources/js/components/ui/Tables/maps/news.js");
/* harmony import */ var _maps_tasks__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./maps/tasks */ "./resources/js/components/ui/Tables/maps/tasks.js");
/* harmony import */ var _maps_todos__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./maps/todos */ "./resources/js/components/ui/Tables/maps/todos.js");
/* harmony import */ var _maps_userTodos__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./maps/userTodos */ "./resources/js/components/ui/Tables/maps/userTodos.js");
/* harmony import */ var _maps_events__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./maps/events */ "./resources/js/components/ui/Tables/maps/events.js");
/* harmony import */ var _maps_topics__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./maps/topics */ "./resources/js/components/ui/Tables/maps/topics.js");
/* harmony import */ var _maps_downloads__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./maps/downloads */ "./resources/js/components/ui/Tables/maps/downloads.js");
/* harmony import */ var _maps_questions__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./maps/questions */ "./resources/js/components/ui/Tables/maps/questions.js");














var maps = {
  locations: _maps_locations__WEBPACK_IMPORTED_MODULE_3__["default"],
  users: _maps_users__WEBPACK_IMPORTED_MODULE_4__["default"],
  groups: _maps_groups__WEBPACK_IMPORTED_MODULE_5__["default"],
  news: _maps_news__WEBPACK_IMPORTED_MODULE_6__["default"],
  events: _maps_events__WEBPACK_IMPORTED_MODULE_10__["default"],
  tasks: _maps_tasks__WEBPACK_IMPORTED_MODULE_7__["default"],
  todos: _maps_todos__WEBPACK_IMPORTED_MODULE_8__["default"],
  userTodos: _maps_userTodos__WEBPACK_IMPORTED_MODULE_9__["default"],
  topics: _maps_topics__WEBPACK_IMPORTED_MODULE_11__["default"],
  downloads: _maps_downloads__WEBPACK_IMPORTED_MODULE_12__["default"],
  questions: _maps_questions__WEBPACK_IMPORTED_MODULE_13__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = (maps);

/***/ }),

/***/ "./resources/js/components/ui/Tables/maps/downloads.js":
/*!*************************************************************!*\
  !*** ./resources/js/components/ui/Tables/maps/downloads.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);



var downloads = [{
  label: "Document",
  value: function value(item) {
    var icon = _mimeTypes[item.mime_type] ? _mimeTypes[item.mime_type] : 'file-o';
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
      style: {
        display: 'inline-block',
        width: '20px'
      }
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
      className: "fa fa-".concat(icon)
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/documents/file/".concat(item.documentId)
    }, item.filename), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", {
      className: "pull-right"
    }, moment__WEBPACK_IMPORTED_MODULE_2___default()(item.created_at).fromNow()));
  },
  class: "",
  admin: false
}];
/* harmony default export */ __webpack_exports__["default"] = (downloads);

/***/ }),

/***/ "./resources/js/components/ui/Tables/maps/events.js":
/*!**********************************************************!*\
  !*** ./resources/js/components/ui/Tables/maps/events.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _DropdownEditMenu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../DropdownEditMenu */ "./resources/js/components/ui/DropdownEditMenu.js");



var events = [{
  label: "Title",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, item.title);
  },
  class: "",
  admin: false
}, {
  label: "Venue",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, item.venue);
  },
  classNames: "text-center",
  admin: false
}, {
  label: "",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DropdownEditMenu__WEBPACK_IMPORTED_MODULE_2__["default"], {
      resource: "events",
      id: item.id,
      actions: ['EDIT', 'DELETE']
    });
  },
  classNames: "text-center",
  admin: true
}];
/* harmony default export */ __webpack_exports__["default"] = (events);

/***/ }),

/***/ "./resources/js/components/ui/Tables/maps/groups.js":
/*!**********************************************************!*\
  !*** ./resources/js/components/ui/Tables/maps/groups.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _DropdownEditMenu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../DropdownEditMenu */ "./resources/js/components/ui/DropdownEditMenu.js");



var users = [{
  label: "Name",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, item.name);
  },
  class: "",
  admin: false
}, {
  label: "",
  value: function value(item, resource) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DropdownEditMenu__WEBPACK_IMPORTED_MODULE_2__["default"], {
      resource: resource,
      id: item.id,
      actions: ['EDIT', 'DELETE']
    });
  },
  classNames: "text-center",
  admin: true
}];
/* harmony default export */ __webpack_exports__["default"] = (users);

/***/ }),

/***/ "./resources/js/components/ui/Tables/maps/locations.js":
/*!*************************************************************!*\
  !*** ./resources/js/components/ui/Tables/maps/locations.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _DropdownEditMenu__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../DropdownEditMenu */ "./resources/js/components/ui/DropdownEditMenu.js");



var locations = [{
  label: "Name",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, item.name);
  },
  class: "",
  admin: false
}, {
  label: "Address",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, item.address);
  },
  classNames: "",
  admin: false
}, {
  label: "Email",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, item.email);
  },
  classNames: "text-center",
  admin: false
}, {
  label: "Telephone",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, item.telephone);
  },
  classNames: "text-center",
  admin: false
}, {
  label: "",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DropdownEditMenu__WEBPACK_IMPORTED_MODULE_2__["default"], {
      resource: "locations",
      id: item.id,
      actions: ['EDIT', 'DELETE']
    });
  },
  classNames: "text-center",
  admin: true
}];
/* harmony default export */ __webpack_exports__["default"] = (locations);

/***/ }),

/***/ "./resources/js/components/ui/Tables/maps/news.js":
/*!********************************************************!*\
  !*** ./resources/js/components/ui/Tables/maps/news.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _DropdownEditMenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../DropdownEditMenu */ "./resources/js/components/ui/DropdownEditMenu.js");




var news = [{
  label: "Title",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, item.title);
  },
  class: "",
  admin: false
}, {
  label: "Status",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, item.draft == true && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Badge"], {
      color: "warning"
    }, "Draft"), !item.draft && item.published && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Badge"], {
      color: "primary"
    }, "Published"), !item.draft && !item.published && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Badge"], {
      color: "success"
    }, "Upcoming"));
  },
  class: "",
  admin: false
}, {
  label: "Publish Date",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, moment__WEBPACK_IMPORTED_MODULE_2___default()(item.publish).format('DD MMM YYYY'));
  },
  class: "",
  admin: false
}, {
  label: "Featured Image",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: item.featured_image,
      alt: "",
      style: {
        height: '100px'
      }
    }));
  },
  class: "",
  admin: false
}, {
  label: "",
  value: function value(item, resource) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DropdownEditMenu__WEBPACK_IMPORTED_MODULE_3__["default"], {
      resource: resource,
      id: item.id,
      actions: ['EDIT', 'DELETE']
    });
  },
  classNames: "text-center",
  admin: true
}];
/* harmony default export */ __webpack_exports__["default"] = (news);

/***/ }),

/***/ "./resources/js/components/ui/Tables/maps/questions.js":
/*!*************************************************************!*\
  !*** ./resources/js/components/ui/Tables/maps/questions.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var _DropdownEditMenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../DropdownEditMenu */ "./resources/js/components/ui/DropdownEditMenu.js");




var questions = [{
  label: "Title",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
      to: "/forum/question/".concat(item.id)
    }, item.title));
  },
  class: "",
  admin: false
}, {
  label: "Topic",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, item.topic && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
      to: "forum/topic/".concat(item.topic.id)
    }, item.topic.title));
  },
  class: "",
  admin: false
}, {
  label: "",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DropdownEditMenu__WEBPACK_IMPORTED_MODULE_3__["default"], {
      resource: "questions",
      id: item.id,
      actions: ['EDIT', 'DELETE']
    });
  },
  classNames: "text-center",
  admin: true
}];
/* harmony default export */ __webpack_exports__["default"] = (questions);

/***/ }),

/***/ "./resources/js/components/ui/Tables/maps/tasks.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/ui/Tables/maps/tasks.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _DropdownEditMenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../DropdownEditMenu */ "./resources/js/components/ui/DropdownEditMenu.js");




var tasks = [{
  label: "Title",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/tasks/".concat(item.id, "/view")
    }, item.title));
  },
  class: "",
  admin: false
}, {
  label: "Progress",
  value: function value(item, resource, todos) {
    var taskTodos = todos.filter(function (todo) {
      return todo.task_id == item.id;
    });
    var active = taskTodos.filter(function (todo) {
      return !todo.rejected;
    });
    var rejected = taskTodos.filter(function (todo) {
      return todo.rejected;
    });
    var completed = taskTodos.filter(function (todo) {
      return todo.completed;
    });
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", null, completed.length, "/", active.length, " ", active.length == 1 ? "Assigned" : "Assigned", " ", active.length > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "(", (completed.length / active.length * 100).toFixed(2), "%)")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Progress"], {
      className: "progress-xs mt-2",
      color: "success",
      value: completed.length / active.length * 100
    }));
  },
  class: "",
  admin: false
}, {
  label: "",
  value: function value(item, resource) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DropdownEditMenu__WEBPACK_IMPORTED_MODULE_3__["default"], {
      resource: resource,
      id: item.id,
      actions: ['EDIT', 'DELETE']
    });
  },
  classNames: "text-center",
  admin: true
}];
/* harmony default export */ __webpack_exports__["default"] = (tasks);

/***/ }),

/***/ "./resources/js/components/ui/Tables/maps/todos.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/ui/Tables/maps/todos.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _DropdownEditMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../DropdownEditMenu */ "./resources/js/components/ui/DropdownEditMenu.js");
/* harmony import */ var _DeadlineBadge__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../DeadlineBadge */ "./resources/js/components/ui/DeadlineBadge.js");






var todos = [{
  label: "Name",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/todos/".concat(item.id, "/view")
    }, item.name), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DeadlineBadge__WEBPACK_IMPORTED_MODULE_5__["default"], {
      item: item
    }));
  },
  class: "",
  admin: false
}, {
  label: "Deadline",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, moment__WEBPACK_IMPORTED_MODULE_2___default()(item.deadline).format('DD MMMM YYYY'));
  },
  class: "",
  admin: false
}, {
  label: "Progress",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, item.completed && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", null, "100%"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Progress"], {
      className: "progress-xs mt-2",
      color: "success",
      value: "100"
    })), !item.completed && !item.rejected && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", null, "0%"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Progress"], {
      className: "progress-xs mt-2",
      color: "success",
      value: "0"
    })));
  },
  class: "",
  admin: false
}, {
  label: "",
  value: function value(item, resource) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DropdownEditMenu__WEBPACK_IMPORTED_MODULE_4__["default"], {
      resource: resource,
      id: item.id,
      actions: ['DELETE']
    });
  },
  classNames: "text-center",
  admin: true
}];
/* harmony default export */ __webpack_exports__["default"] = (todos);

/***/ }),

/***/ "./resources/js/components/ui/Tables/maps/topics.js":
/*!**********************************************************!*\
  !*** ./resources/js/components/ui/Tables/maps/topics.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var _DropdownEditMenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../DropdownEditMenu */ "./resources/js/components/ui/DropdownEditMenu.js");




var topics = [{
  label: "Title",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Link"], {
      to: "forum/topic/".concat(item.id)
    }, item.title));
  },
  class: "",
  admin: false
}, {
  label: "",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DropdownEditMenu__WEBPACK_IMPORTED_MODULE_3__["default"], {
      resource: "topics",
      id: item.id,
      actions: ['EDIT', 'DELETE']
    });
  },
  classNames: "text-center",
  admin: true
}];
/* harmony default export */ __webpack_exports__["default"] = (topics);

/***/ }),

/***/ "./resources/js/components/ui/Tables/maps/userTodos.js":
/*!*************************************************************!*\
  !*** ./resources/js/components/ui/Tables/maps/userTodos.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _DropdownEditMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../DropdownEditMenu */ "./resources/js/components/ui/DropdownEditMenu.js");
/* harmony import */ var _DeadlineBadge__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../DeadlineBadge */ "./resources/js/components/ui/DeadlineBadge.js");






var userTodos = [{
  label: "Task",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/todos/".concat(item.id, "/view")
    }, item.taskName));
  },
  class: "",
  admin: false
}, {
  label: "Status",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DeadlineBadge__WEBPACK_IMPORTED_MODULE_5__["default"], {
      item: item
    }));
  },
  class: "",
  admin: false
}, {
  label: "Deadline",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, moment__WEBPACK_IMPORTED_MODULE_2___default()(item.deadline).format('DD MMMM YYYY'));
  },
  class: "",
  admin: false
}, {
  label: "Completed",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, item.completed && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, moment__WEBPACK_IMPORTED_MODULE_2___default()(item.completed_at).format('DD MMMM YYYY')), !item.completed && !item.rejected && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, "N/A"));
  },
  class: "",
  admin: false
}];
/* harmony default export */ __webpack_exports__["default"] = (userTodos);

/***/ }),

/***/ "./resources/js/components/ui/Tables/maps/users.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/ui/Tables/maps/users.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _DropdownEditMenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../DropdownEditMenu */ "./resources/js/components/ui/DropdownEditMenu.js");




var users = [{
  label: "Name",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
      to: "/users/".concat(item.id, "/view")
    }, item.name));
  },
  class: "",
  admin: false
}, {
  label: "Email",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      href: "mailto:".concat(item.email)
    }, item.email));
  },
  class: "",
  admin: false
}, {
  label: "Role",
  value: function value(item) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, item.role);
  },
  class: "",
  admin: false
}, {
  label: "",
  value: function value(item, resource) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_DropdownEditMenu__WEBPACK_IMPORTED_MODULE_3__["default"], {
      resource: resource,
      id: item.id,
      actions: ['EDIT', 'DELETE']
    });
  },
  classNames: "text-center",
  admin: true
}];
/* harmony default export */ __webpack_exports__["default"] = (users);

/***/ })

}]);