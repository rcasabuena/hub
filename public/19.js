(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[19],{

/***/ "./resources/js/HOC/formConnect.js":
/*!*****************************************!*\
  !*** ./resources/js/HOC/formConnect.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../actions */ "./resources/js/actions.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../functions/FormFunctions.js */ "./resources/js/functions/FormFunctions.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }






/* harmony default export */ __webpack_exports__["default"] = (function (ChildComponent, resource, mapStateToProps) {
  var ComposedComponent =
  /*#__PURE__*/
  function (_Component) {
    _inherits(ComposedComponent, _Component);

    function ComposedComponent() {
      _classCallCheck(this, ComposedComponent);

      return _possibleConstructorReturn(this, _getPrototypeOf(ComposedComponent).apply(this, arguments));
    }

    _createClass(ComposedComponent, [{
      key: "render",
      value: function render() {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ChildComponent, _extends({}, this.props, {
          formUtils: _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_4__
        }));
      }
    }]);

    return ComposedComponent;
  }(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

  var mapDispatchToProps = function mapDispatchToProps(dispatch, _ref) {
    var history = _ref.history;
    return {
      save: function save(data) {
        dispatch(Object(_actions__WEBPACK_IMPORTED_MODULE_2__["saveResource"])(resource, data, history));
      },
      delete: function _delete(id) {
        dispatch(Object(_actions__WEBPACK_IMPORTED_MODULE_2__["deleteResource"])(resource, id, history));
      }
    };
  };

  var enhance = compose(react_router_dom__WEBPACK_IMPORTED_MODULE_3__["withRouter"], Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps, mapDispatchToProps));
  return enhance(ComposedComponent);
});

/***/ }),

/***/ "./resources/js/components/views/Tasks/TaskForm.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/views/Tasks/TaskForm.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ui_Form_TextInput__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../ui/Form/TextInput */ "./resources/js/components/ui/Form/TextInput.js");
/* harmony import */ var _ui_Form_FileInputMulti__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../ui/Form/FileInputMulti */ "./resources/js/components/ui/Form/FileInputMulti.js");
/* harmony import */ var _ui_Form_SelectInput__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../ui/Form/SelectInput */ "./resources/js/components/ui/Form/SelectInput.js");
/* harmony import */ var _ui_Form_WysiwygInput__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../ui/Form/WysiwygInput */ "./resources/js/components/ui/Form/WysiwygInput.js");
/* harmony import */ var _ui_Form_FormSubmitActions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../ui/Form/FormSubmitActions */ "./resources/js/components/ui/Form/FormSubmitActions.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _objects_AppTask__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../objects/AppTask */ "./resources/js/objects/AppTask.js");
/* harmony import */ var _HOC_formConnect__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../HOC/formConnect */ "./resources/js/HOC/formConnect.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }











var TaskForm =
/*#__PURE__*/
function (_Component) {
  _inherits(TaskForm, _Component);

  function TaskForm(props) {
    var _this;

    _classCallCheck(this, TaskForm);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(TaskForm).call(this, props));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      item: new _objects_AppTask__WEBPACK_IMPORTED_MODULE_7__["default"]()
    });

    var formUtils = props.formUtils;
    _this.handleChange = formUtils.handleChange.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleEditorChange = formUtils.handleEditorChange.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleUploadMulti = formUtils.handleUploadMulti.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleSelect = formUtils.handleSelect.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleDelete = formUtils.handleDelete.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleSubmit = formUtils.handleSubmit.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(TaskForm, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var item = this.props.item;
      this.setState({
        item: item
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props$item = this.props.item,
          id = _this$props$item.id,
          title = _this$props$item.title,
          body = _this$props$item.body,
          repeat = _this$props$item.repeat;
      console.log(this.props.item);
      console.log(repeat);
      var frequency = [{
        value: 'DAY',
        label: 'Daily'
      }, {
        value: 'WEEK',
        label: 'Weekly'
      }, {
        value: 'MONTH',
        label: 'Monthly'
      }, {
        value: '3MONTHS',
        label: 'Every 3 months'
      }, {
        value: '6MONTHS',
        label: 'Every 6 months'
      }, {
        value: 'YEAR',
        label: 'Yearly'
      }];
      var taskRepeat = frequency.find(function (item) {
        return item.value == repeat;
      });
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_6__["Form"], {
        onSubmit: this.handleSubmit
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_TextInput__WEBPACK_IMPORTED_MODULE_1__["default"], {
        handleChange: this.handleChange,
        inputDefaultValue: title,
        inputName: "title",
        inputLabel: "Title",
        inputHelpText: "This is a help text"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_SelectInput__WEBPACK_IMPORTED_MODULE_3__["default"], {
        inputName: "repeat",
        inputLabel: "Repeat",
        selectValue: taskRepeat,
        selectOptions: frequency,
        inputHelpText: "This is a help text",
        handleSelect: this.handleSelect
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_WysiwygInput__WEBPACK_IMPORTED_MODULE_4__["default"], {
        inputName: "body",
        inputLabel: "Body",
        inputHelpText: "This is a help text",
        body: body,
        handleEditorChange: this.handleEditorChange
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_FileInputMulti__WEBPACK_IMPORTED_MODULE_2__["default"], {
        handleUpload: this.handleUploadMulti,
        inputDefaultValue: null,
        inputName: "attachments",
        inputLabel: "Attachments",
        inputHelpText: ""
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_FormSubmitActions__WEBPACK_IMPORTED_MODULE_5__["default"], {
        id: id,
        handleDelete: this.handleDelete
      })));
    }
  }]);

  return TaskForm;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref, _ref2) {
  var tasks = _ref.tasks;
  var match = _ref2.match;
  var task = tasks.data.find(function (item) {
    return item.id === match.params.unique_id;
  });
  var id = task ? task.id : "",
      title = task ? task.title : "",
      body = task ? task.body : "",
      repeat = task ? task.repeat : "";
  return {
    item: new _objects_AppTask__WEBPACK_IMPORTED_MODULE_7__["default"](id, title, body, repeat)
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(_HOC_formConnect__WEBPACK_IMPORTED_MODULE_8__["default"])(TaskForm, "Tasks", mapStateToProps));

/***/ }),

/***/ "./resources/js/objects/AppTask.js":
/*!*****************************************!*\
  !*** ./resources/js/objects/AppTask.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AppTask; });
/* harmony import */ var _AppItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AppItem */ "./resources/js/objects/AppItem.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }



var AppTask =
/*#__PURE__*/
function (_AppItem) {
  _inherits(AppTask, _AppItem);

  function AppTask() {
    var _this;

    var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
    var title = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
    var body = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
    var repeat = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "";

    _classCallCheck(this, AppTask);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(AppTask).call(this, id));
    _this.title = title;
    _this.body = body;
    _this.repeat = repeat;
    _this.attachments = [];
    return _this;
  }

  return AppTask;
}(_AppItem__WEBPACK_IMPORTED_MODULE_0__["default"]);



/***/ })

}]);