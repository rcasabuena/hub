(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[38],{

/***/ "./resources/js/components/ui/Form/ToggleSwitch.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/ui/Form/ToggleSwitch.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _coreui_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @coreui/react */ "./node_modules/@coreui/react/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");




var ToggleSwitch = function ToggleSwitch(props) {
  var inputName = props.inputName,
      inputLabel = props.inputLabel,
      handleToggle = props.handleToggle,
      switchColour = props.switchColour,
      switchType = props.switchType,
      inputChecked = props.inputChecked,
      inputHelpText = props.inputHelpText,
      inverseChecked = props.inverseChecked,
      horizontal = props.horizontal;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["FormGroup"], {
    row: true,
    className: "mb-3"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    md: {
      size: !!!horizontal ? 3 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Label"], {
    htmlFor: inputName
  }, inputLabel)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    md: {
      size: !!!horizontal ? 9 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_coreui_react__WEBPACK_IMPORTED_MODULE_1__["AppSwitch"], {
    variant: switchType,
    color: switchColour,
    id: inputName,
    checked: inputChecked,
    onChange: function onChange(e) {
      return handleToggle(e, inverseChecked);
    }
  }), inputHelpText && inputHelpText.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["FormText"], {
    color: "muted"
  }, inputHelpText)));
};

/* harmony default export */ __webpack_exports__["default"] = (ToggleSwitch);

/***/ }),

/***/ "./resources/js/components/views/Forum/AskAQuestion.js":
/*!*************************************************************!*\
  !*** ./resources/js/components/views/Forum/AskAQuestion.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _ui_QuestionForm__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ui/QuestionForm */ "./resources/js/components/views/Forum/ui/QuestionForm.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var AskAQuestion =
/*#__PURE__*/
function (_Component) {
  _inherits(AskAQuestion, _Component);

  function AskAQuestion() {
    _classCallCheck(this, AskAQuestion);

    return _possibleConstructorReturn(this, _getPrototypeOf(AskAQuestion).apply(this, arguments));
  }

  _createClass(AskAQuestion, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "animated fadeIn"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Card"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["CardBody"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
        className: "card-title mb-0"
      }, "Ask a question"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "text-muted"
      }, "We\u2019d love to help you, but the reality is that not every question gets answered. To improve your chances, here are some tips:"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "lead"
      }, "Search, and research"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Have you thoroughly searched for an answer before asking your question? Sharing your research helps everyone. Tell us what you found (on this site or elsewhere) and why it didn\u2019t meet your needs. This demonstrates that you\u2019ve taken the time to try to help yourself, it saves us from reiterating obvious answers, and above all, it helps you get a more specific and relevant answer!"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "lead"
      }, "Be on-topic"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "Our community is defined by a specific set of topics in the help center; please stick to those topics and avoid asking for opinions or open-ended discussion. If your question is about the site itself, ask on our meta-discussion site."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "lead"
      }, "Be specific"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "If you ask a vague question, you\u2019ll get a vague answer. But if you give us details and context, we can provide a useful, relevant answer."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "lead"
      }, "Make it relevant to others"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "We like to help as many people at a time as we can. Make it clear how your question is relevant to more people than just you, and more of us will be interested in your question and willing to look into it."), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "lead"
      }, "Keep an open mind"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "The answer to your question may not always be the one you wanted, but that doesn\u2019t mean it is wrong. A conclusive answer isn\u2019t always possible. When in doubt, ask people to cite their sources, or to explain how/where they learned something. Even if we don\u2019t agree with you, or tell you exactly what you wanted to hear, remember: we\u2019re just trying to help."))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_QuestionForm__WEBPACK_IMPORTED_MODULE_2__["default"], null))));
    }
  }]);

  return AskAQuestion;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (AskAQuestion);

/***/ }),

/***/ "./resources/js/components/views/Forum/ui/QuestionForm.js":
/*!****************************************************************!*\
  !*** ./resources/js/components/views/Forum/ui/QuestionForm.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _ui_Form_TextInput__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../ui/Form/TextInput */ "./resources/js/components/ui/Form/TextInput.js");
/* harmony import */ var _ui_Form_FileInputMulti__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../ui/Form/FileInputMulti */ "./resources/js/components/ui/Form/FileInputMulti.js");
/* harmony import */ var _ui_Form_ToggleSwitch__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../ui/Form/ToggleSwitch */ "./resources/js/components/ui/Form/ToggleSwitch.js");
/* harmony import */ var _ui_Form_WysiwygInput__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../ui/Form/WysiwygInput */ "./resources/js/components/ui/Form/WysiwygInput.js");
/* harmony import */ var _ui_Form_SelectInput__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../ui/Form/SelectInput */ "./resources/js/components/ui/Form/SelectInput.js");
/* harmony import */ var _ui_Form_FormSubmitActions__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../ui/Form/FormSubmitActions */ "./resources/js/components/ui/Form/FormSubmitActions.js");
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../actions */ "./resources/js/actions.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _objects_AppQuestion__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../objects/AppQuestion */ "./resources/js/objects/AppQuestion.js");
/* harmony import */ var _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../../functions/FormFunctions.js */ "./resources/js/functions/FormFunctions.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }















var QuestionForm =
/*#__PURE__*/
function (_Component) {
  _inherits(QuestionForm, _Component);

  function QuestionForm(props) {
    var _this;

    _classCallCheck(this, QuestionForm);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(QuestionForm).call(this, props));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      item: new _objects_AppQuestion__WEBPACK_IMPORTED_MODULE_11__["default"]()
    });

    _this.handleChange = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_12__["handleChange"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleUploadMulti = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_12__["handleUploadMulti"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleToggle = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_12__["handleToggle"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleSelect = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_12__["handleSelect"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleEditorChange = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_12__["handleEditorChange"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleDelete = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_12__["handleDelete"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleSubmit = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_12__["handleSubmit"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(QuestionForm, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var item = this.props.item;
      this.setState({
        item: item
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props$item = this.props.item,
          id = _this$props$item.id,
          title = _this$props$item.title,
          body = _this$props$item.body,
          sticky = _this$props$item.sticky;
      var _this$props = this.props,
          topics = _this$props.topics,
          questionTopic = _this$props.questionTopic;
      console.log(topics);
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__["Card"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__["CardHeader"], null, "Your question"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__["CardBody"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_10__["Form"], {
        onSubmit: this.handleSubmit
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_SelectInput__WEBPACK_IMPORTED_MODULE_7__["default"], {
        inputName: "topic_id",
        inputLabel: "Topic",
        selectValue: questionTopic,
        selectOptions: topics,
        inputHelpText: "",
        handleSelect: this.handleSelect,
        horizontal: true
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_TextInput__WEBPACK_IMPORTED_MODULE_3__["default"], {
        handleChange: this.handleChange,
        inputDefaultValue: title,
        inputName: "title",
        inputLabel: "Title",
        inputHelpText: "",
        horizontal: true
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_WysiwygInput__WEBPACK_IMPORTED_MODULE_6__["default"], {
        inputName: "body",
        inputLabel: "Body",
        inputHelpText: "",
        body: body,
        handleEditorChange: this.handleEditorChange,
        horizontal: true
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_FileInputMulti__WEBPACK_IMPORTED_MODULE_4__["default"], {
        handleUpload: this.handleUploadMulti,
        inputDefaultValue: null,
        inputName: "attachments",
        inputLabel: "Attachments",
        horizontal: true,
        inputHelpText: ""
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_FormSubmitActions__WEBPACK_IMPORTED_MODULE_8__["default"], {
        id: ""
      }))));
    }
  }]);

  return QuestionForm;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref, _ref2) {
  var questions = _ref.questions,
      topics = _ref.topics;
  var match = _ref2.match;
  var question = questions.data.find(function (item) {
    return item.id === match.params.unique_id;
  });
  var id = question ? question.id : "",
      title = question ? question.title : "",
      body = question ? question.body : "",
      sticky = question ? question.sticky : false,
      topic_id = question && question.topic ? question.topic.id : "";
  return {
    topics: topics.data,
    questionTopic: question && question.topic ? question.topic : false,
    item: new _objects_AppQuestion__WEBPACK_IMPORTED_MODULE_11__["default"](id, title, body, sticky, topic_id)
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    save: function save(data, history) {
      dispatch(Object(_actions__WEBPACK_IMPORTED_MODULE_9__["saveResource"])('Questions', data, history, '/forum'));
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["withRouter"])(Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps, mapDispatchToProps)(QuestionForm)));

/***/ }),

/***/ "./resources/js/objects/AppQuestion.js":
/*!*********************************************!*\
  !*** ./resources/js/objects/AppQuestion.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AppQuestion; });
/* harmony import */ var _AppItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AppItem */ "./resources/js/objects/AppItem.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }



var AppQuestion =
/*#__PURE__*/
function (_AppItem) {
  _inherits(AppQuestion, _AppItem);

  function AppQuestion() {
    var _this;

    var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
    var title = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
    var body = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
    var sticky = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
    var topic_id = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : "";

    _classCallCheck(this, AppQuestion);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(AppQuestion).call(this, id));
    _this.title = title;
    _this.body = body;
    _this.sticky = sticky;
    _this.topic_id = topic_id;
    _this.attachments = [];
    return _this;
  }

  return AppQuestion;
}(_AppItem__WEBPACK_IMPORTED_MODULE_0__["default"]);



/***/ })

}]);