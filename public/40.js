(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[40],{

/***/ "./resources/js/_nav.js":
/*!******************************!*\
  !*** ./resources/js/_nav.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  items: [{
    name: 'Home',
    url: '/',
    icon: 'icon-home',
    admin: false
  }, {
    name: 'News',
    url: '/news',
    icon: 'icon-star',
    admin: false
  }, {
    name: 'Documents',
    url: '/documents',
    icon: 'icon-docs',
    admin: false
  }, {
    name: 'Downloads Log',
    url: '/downloads',
    icon: 'icon-cloud-download',
    admin: true
  }, // {
  //   name: 'Events',
  //   url: '/events',
  //   icon: 'icon-calendar',
  //   admin: false
  // },
  {
    name: 'Tasks',
    url: '/tasks',
    icon: 'icon-list',
    admin: false,
    director: true
  }, {
    name: 'Locations',
    url: '/locations',
    icon: 'icon-location-pin',
    admin: true
  }, {
    name: 'Topics',
    url: '/topics',
    icon: 'icon-microphone',
    admin: true
  }, {
    name: 'Questions',
    url: '/questions',
    icon: 'icon-question',
    admin: true
  }, {
    name: 'Users',
    url: '/users',
    icon: 'icon-people',
    admin: true
  }, {
    name: 'User Groups',
    url: '/groups',
    icon: 'icon-people',
    admin: true
  }, {
    name: 'Profile',
    url: '/profile',
    icon: 'icon-user',
    admin: false
  }, {
    name: 'Forum',
    url: '/forum',
    icon: 'icon-bubbles',
    admin: false
  }, {
    name: 'Ask a question',
    url: '/forum/ask-a-question',
    icon: 'icon-bubble',
    //class: 'mt-auto',
    variant: 'secondary',
    admin: false
  }]
});

/***/ }),

/***/ "./resources/js/components/layout/DefaultLayout.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/layout/DefaultLayout.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _coreui_react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @coreui/react */ "./node_modules/@coreui/react/es/index.js");
/* harmony import */ var _nav__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../_nav */ "./resources/js/_nav.js");
/* harmony import */ var _routes__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../routes */ "./resources/js/routes.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }






 // sidebar nav config

 // routes config


var DefaultAside = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(30)]).then(__webpack_require__.bind(null, /*! ./DefaultAside */ "./resources/js/components/layout/DefaultAside.js"));
});
var DefaultFooter = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return __webpack_require__.e(/*! import() */ 45).then(__webpack_require__.bind(null, /*! ./DefaultFooter */ "./resources/js/components/layout/DefaultFooter.js"));
});
var DefaultHeader = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return __webpack_require__.e(/*! import() */ 46).then(__webpack_require__.bind(null, /*! ./DefaultHeader */ "./resources/js/components/layout/DefaultHeader.js"));
});

var DefaultLayout = function DefaultLayout(props) {
  var loading = function loading() {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      className: "animated fadeIn pt-1 text-center"
    }, "Loading...");
  };

  var nav = props.nav,
      userRoutes = props.userRoutes,
      atHome = props.atHome;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "app"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_coreui_react__WEBPACK_IMPORTED_MODULE_5__["AppHeader"], {
    fixed: true
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Suspense"], {
    fallback: loading()
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DefaultHeader, null))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "app-body"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_coreui_react__WEBPACK_IMPORTED_MODULE_5__["AppSidebar"], {
    fixed: true,
    display: "lg"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_coreui_react__WEBPACK_IMPORTED_MODULE_5__["AppSidebarHeader"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_coreui_react__WEBPACK_IMPORTED_MODULE_5__["AppSidebarForm"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Suspense"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_coreui_react__WEBPACK_IMPORTED_MODULE_5__["AppSidebarNav"], _extends({
    navConfig: nav
  }, props))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_coreui_react__WEBPACK_IMPORTED_MODULE_5__["AppSidebarFooter"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_coreui_react__WEBPACK_IMPORTED_MODULE_5__["AppSidebarMinimizer"], null)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("main", {
    className: classnames__WEBPACK_IMPORTED_MODULE_1___default()({
      'main': true,
      'at-home': atHome
    }),
    style: {
      backgroundImage: "url('/images/bg1.jpg')"
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "mb-4"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Container"], {
    fluid: true
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Suspense"], {
    fallback: loading()
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Switch"], null, userRoutes.map(function (route, idx) {
    return route.component ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Route"], {
      key: idx,
      path: route.path,
      exact: route.exact,
      name: route.name,
      render: function render(props) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(route.component, props);
      }
    }) : null;
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Redirect"], {
    from: "/",
    to: "/home"
  }))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_coreui_react__WEBPACK_IMPORTED_MODULE_5__["AppAside"], {
    fixed: true
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Suspense"], {
    fallback: loading()
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DefaultAside, {
    isOpen: true
  })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_coreui_react__WEBPACK_IMPORTED_MODULE_5__["AppFooter"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Suspense"], {
    fallback: loading()
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DefaultFooter, null))));
};

var mapStateToProps = function mapStateToProps(state, ownProps) {
  var items = _nav__WEBPACK_IMPORTED_MODULE_6__["default"].items;
  var userRoutes = _routes__WEBPACK_IMPORTED_MODULE_7__["default"];
  var atHome = ownProps.location.pathname === '/home' ? true : false;

  if (!state.currentView.admin) {
    items = items.filter(function (item) {
      return !item.admin;
    });
    userRoutes = userRoutes.filter(function (item) {
      return !item.admin;
    });
    items = items.filter(function (item) {
      if (item.director != undefined) return item.director === state.currentUser.director;
      return true;
    });
    userRoutes = userRoutes.filter(function (item) {
      if (item.director != undefined) return item.director === state.currentUser.director;
      return true;
    });
  }

  var nav = _objectSpread({}, _nav__WEBPACK_IMPORTED_MODULE_6__["default"], {
    items: items
  });

  return {
    nav: nav,
    userRoutes: userRoutes,
    atHome: atHome
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["connect"])(mapStateToProps)(DefaultLayout));

/***/ }),

/***/ "./resources/js/routes.js":
/*!********************************!*\
  !*** ./resources/js/routes.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_layout_DefaultLayout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/layout/DefaultLayout */ "./resources/js/components/layout/DefaultLayout.js");


var Home = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return __webpack_require__.e(/*! import() */ 53).then(__webpack_require__.bind(null, /*! ./components/views/Home/Home */ "./resources/js/components/views/Home/Home.js"));
});
var Documents = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(2), __webpack_require__.e(7), __webpack_require__.e(12), __webpack_require__.e(22)]).then(__webpack_require__.bind(null, /*! ./components/views/Documents/Documents */ "./resources/js/components/views/Documents/Documents.js"));
});
var AddDocument = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(1), __webpack_require__.e(18), __webpack_require__.e(47)]).then(__webpack_require__.bind(null, /*! ./components/views/Documents/AddDocument */ "./resources/js/components/views/Documents/AddDocument.js"));
});
var EditDocument = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(1), __webpack_require__.e(18), __webpack_require__.e(48)]).then(__webpack_require__.bind(null, /*! ./components/views/Documents/EditDocument */ "./resources/js/components/views/Documents/EditDocument.js"));
});
var ViewDocument = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(7), __webpack_require__.e(12), __webpack_require__.e(27)]).then(__webpack_require__.bind(null, /*! ./components/views/Documents/ViewDocument */ "./resources/js/components/views/Documents/ViewDocument.js"));
});
var Downloads = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2), __webpack_require__.e(28)]).then(__webpack_require__.bind(null, /*! ./components/views/Downloads/Downloads */ "./resources/js/components/views/Downloads/Downloads.js"));
});
var News = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2), __webpack_require__.e(6), __webpack_require__.e(5), __webpack_require__.e(26)]).then(__webpack_require__.bind(null, /*! ./components/views/News/News */ "./resources/js/components/views/News/News.js"));
});
var AddNews = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(10), __webpack_require__.e(56)]).then(__webpack_require__.bind(null, /*! ./components/views/News/AddNews */ "./resources/js/components/views/News/AddNews.js"));
});
var EditNews = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(10), __webpack_require__.e(57)]).then(__webpack_require__.bind(null, /*! ./components/views/News/EditNews */ "./resources/js/components/views/News/EditNews.js"));
});
var NewsView = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(7), __webpack_require__.e(39)]).then(__webpack_require__.bind(null, /*! ./components/views/News/NewsView */ "./resources/js/components/views/News/NewsView.js"));
});
var Events = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2), __webpack_require__.e(6), __webpack_require__.e(5), __webpack_require__.e(31)]).then(__webpack_require__.bind(null, /*! ./components/views/Events/Events */ "./resources/js/components/views/Events/Events.js"));
});
var AddEvent = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(9), __webpack_require__.e(49)]).then(__webpack_require__.bind(null, /*! ./components/views/Events/AddEvent */ "./resources/js/components/views/Events/AddEvent.js"));
});
var EditEvent = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(9), __webpack_require__.e(50)]).then(__webpack_require__.bind(null, /*! ./components/views/Events/EditEvent */ "./resources/js/components/views/Events/EditEvent.js"));
});
var Tasks = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2), __webpack_require__.e(6), __webpack_require__.e(5), __webpack_require__.e(35)]).then(__webpack_require__.bind(null, /*! ./components/views/Tasks/Tasks */ "./resources/js/components/views/Tasks/Tasks.js"));
});
var AddTask = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(1), __webpack_require__.e(3), __webpack_require__.e(4), __webpack_require__.e(8), __webpack_require__.e(19), __webpack_require__.e(62)]).then(__webpack_require__.bind(null, /*! ./components/views/Tasks/AddTask */ "./resources/js/components/views/Tasks/AddTask.js"));
});
var EditTask = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(1), __webpack_require__.e(3), __webpack_require__.e(4), __webpack_require__.e(8), __webpack_require__.e(19), __webpack_require__.e(63)]).then(__webpack_require__.bind(null, /*! ./components/views/Tasks/EditTask */ "./resources/js/components/views/Tasks/EditTask.js"));
});
var ViewTask = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2), __webpack_require__.e(3), __webpack_require__.e(4), __webpack_require__.e(6), __webpack_require__.e(7), __webpack_require__.e(5), __webpack_require__.e(24)]).then(__webpack_require__.bind(null, /*! ./components/views/Tasks/ViewTask */ "./resources/js/components/views/Tasks/ViewTask.js"));
});
var TodoView = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(3), __webpack_require__.e(4), __webpack_require__.e(7), __webpack_require__.e(8), __webpack_require__.e(16), __webpack_require__.e(44)]).then(__webpack_require__.bind(null, /*! ./components/views/Tasks/TodoView */ "./resources/js/components/views/Tasks/TodoView.js"));
});
var ViewTodo = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(3), __webpack_require__.e(4), __webpack_require__.e(7), __webpack_require__.e(8), __webpack_require__.e(16), __webpack_require__.e(64)]).then(__webpack_require__.bind(null, /*! ./components/views/Tasks/ViewTodo */ "./resources/js/components/views/Tasks/ViewTodo.js"));
});
var Users = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2), __webpack_require__.e(6), __webpack_require__.e(5), __webpack_require__.e(37)]).then(__webpack_require__.bind(null, /*! ./components/views/Users/Users */ "./resources/js/components/views/Users/Users.js"));
});
var AddUser = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(3), __webpack_require__.e(4), __webpack_require__.e(15), __webpack_require__.e(68)]).then(__webpack_require__.bind(null, /*! ./components/views/Users/AddUser */ "./resources/js/components/views/Users/AddUser.js"));
});
var EditUser = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(3), __webpack_require__.e(4), __webpack_require__.e(15), __webpack_require__.e(69)]).then(__webpack_require__.bind(null, /*! ./components/views/Users/EditUser */ "./resources/js/components/views/Users/EditUser.js"));
});
var ViewUser = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2), __webpack_require__.e(6), __webpack_require__.e(5), __webpack_require__.e(43)]).then(__webpack_require__.bind(null, /*! ./components/views/Users/ViewUser */ "./resources/js/components/views/Users/ViewUser.js"));
});
var Groups = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2), __webpack_require__.e(6), __webpack_require__.e(5), __webpack_require__.e(32)]).then(__webpack_require__.bind(null, /*! ./components/views/Groups/Groups */ "./resources/js/components/views/Groups/Groups.js"));
});
var AddGroup = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(3), __webpack_require__.e(4), __webpack_require__.e(14), __webpack_require__.e(51)]).then(__webpack_require__.bind(null, /*! ./components/views/Groups/AddGroup */ "./resources/js/components/views/Groups/AddGroup.js"));
});
var EditGroup = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(3), __webpack_require__.e(4), __webpack_require__.e(14), __webpack_require__.e(52)]).then(__webpack_require__.bind(null, /*! ./components/views/Groups/EditGroup */ "./resources/js/components/views/Groups/EditGroup.js"));
});
var Forum = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2), __webpack_require__.e(3), __webpack_require__.e(21), __webpack_require__.e(29)]).then(__webpack_require__.bind(null, /*! ./components/views/Forum/Forum */ "./resources/js/components/views/Forum/Forum.js"));
});
var QuestionView = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(1), __webpack_require__.e(3), __webpack_require__.e(4), __webpack_require__.e(7), __webpack_require__.e(8), __webpack_require__.e(25)]).then(__webpack_require__.bind(null, /*! ./components/views/Forum/QuestionView */ "./resources/js/components/views/Forum/QuestionView.js"));
});
var AskAQuestion = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(1), __webpack_require__.e(3), __webpack_require__.e(4), __webpack_require__.e(8), __webpack_require__.e(38)]).then(__webpack_require__.bind(null, /*! ./components/views/Forum/AskAQuestion */ "./resources/js/components/views/Forum/AskAQuestion.js"));
});
var Topics = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2), __webpack_require__.e(6), __webpack_require__.e(5), __webpack_require__.e(36)]).then(__webpack_require__.bind(null, /*! ./components/views/Topics/Topics */ "./resources/js/components/views/Topics/Topics.js"));
});
var AddTopic = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(1), __webpack_require__.e(17), __webpack_require__.e(65)]).then(__webpack_require__.bind(null, /*! ./components/views/Topics/AddTopic */ "./resources/js/components/views/Topics/AddTopic.js"));
});
var EditTopic = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(1), __webpack_require__.e(17), __webpack_require__.e(66)]).then(__webpack_require__.bind(null, /*! ./components/views/Topics/EditTopic */ "./resources/js/components/views/Topics/EditTopic.js"));
});
var ViewTopic = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return __webpack_require__.e(/*! import() */ 67).then(__webpack_require__.bind(null, /*! ./components/views/Topics/ViewTopic */ "./resources/js/components/views/Topics/ViewTopic.js"));
});
var Questions = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2), __webpack_require__.e(6), __webpack_require__.e(5), __webpack_require__.e(34)]).then(__webpack_require__.bind(null, /*! ./components/views/Questions/Questions */ "./resources/js/components/views/Questions/Questions.js"));
});
var AddQuestion = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(1), __webpack_require__.e(3), __webpack_require__.e(4), __webpack_require__.e(11), __webpack_require__.e(58)]).then(__webpack_require__.bind(null, /*! ./components/views/Questions/AddQuestion */ "./resources/js/components/views/Questions/AddQuestion.js"));
});
var EditQuestion = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(1), __webpack_require__.e(3), __webpack_require__.e(4), __webpack_require__.e(11), __webpack_require__.e(59)]).then(__webpack_require__.bind(null, /*! ./components/views/Questions/EditQuestion */ "./resources/js/components/views/Questions/EditQuestion.js"));
});
var ViewQuestion = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return __webpack_require__.e(/*! import() */ 60).then(__webpack_require__.t.bind(null, /*! ./components/views/Questions/ViewQuestion */ "./resources/js/components/views/Questions/ViewQuestion.js", 7));
});
var TopicView = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2), __webpack_require__.e(42)]).then(__webpack_require__.bind(null, /*! ./components/views/Forum/TopicView */ "./resources/js/components/views/Forum/TopicView.js"));
});
var Locations = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2), __webpack_require__.e(6), __webpack_require__.e(5), __webpack_require__.e(33)]).then(__webpack_require__.bind(null, /*! ./components/views/Locations/Locations */ "./resources/js/components/views/Locations/Locations.js"));
});
var AddLocation = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(13), __webpack_require__.e(54)]).then(__webpack_require__.bind(null, /*! ./components/views/Locations/AddLocation */ "./resources/js/components/views/Locations/AddLocation.js"));
});
var EditLocation = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(13), __webpack_require__.e(55)]).then(__webpack_require__.bind(null, /*! ./components/views/Locations/EditLocation */ "./resources/js/components/views/Locations/EditLocation.js"));
});
var Profile = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2), __webpack_require__.e(6), __webpack_require__.e(5), __webpack_require__.e(41)]).then(__webpack_require__.bind(null, /*! ./components/views/Profile/Profile */ "./resources/js/components/views/Profile/Profile.js"));
});
var Settings = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return __webpack_require__.e(/*! import() */ 61).then(__webpack_require__.bind(null, /*! ./components/views/Settings/Settings */ "./resources/js/components/views/Settings/Settings.js"));
});
var Search = Object(react__WEBPACK_IMPORTED_MODULE_0__["lazy"])(function () {
  return Promise.all(/*! import() */[__webpack_require__.e(0), __webpack_require__.e(2), __webpack_require__.e(7), __webpack_require__.e(23)]).then(__webpack_require__.bind(null, /*! ./components/views/Search/Search */ "./resources/js/components/views/Search/Search.js"));
}); // https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config

var routes = [{
  path: '/',
  exact: true,
  name: 'Home',
  component: _components_layout_DefaultLayout__WEBPACK_IMPORTED_MODULE_1__["default"],
  admin: false
}, {
  path: '/home',
  exact: true,
  name: '',
  component: Home,
  admin: false
}, // { path: '/documents/new', exact: true, name: 'New', component: AddDocument, admin: true },
// { path: '/documents/:unique_id/edit', exact: true, name: 'Edit', component: EditDocument, admin: true },
// { path: '/documents/:unique_id/view', exact: true, name: 'View', component: ViewDocument, admin: true },
{
  path: '/documents*',
  exact: true,
  name: '',
  component: Documents,
  admin: false
}, {
  path: '/downloads',
  exact: true,
  name: 'Downloads',
  component: Downloads,
  admin: true
}, // { path: '/documents/:unique_id', exact: true, name: '', component: Documents, admin: false },
// { path: '/documents/folder/new', exact: true, name: '', component: Documents, admin: false },
// { path: '/documents/file/new', exact: true, name: '', component: Documents, admin: false },
// { path: '/documents/folder/new', exact: true, name: '', component: Documents, admin: false },
// { path: '/documents/file/new', exact: true, name: '', component: Documents, admin: false },
{
  path: '/news',
  exact: true,
  name: 'News',
  component: News,
  admin: false
}, {
  path: '/news/new',
  exact: true,
  name: 'New',
  component: AddNews,
  admin: true
}, {
  path: '/news/:unique_id/edit',
  exact: true,
  name: 'Edit',
  component: EditNews,
  admin: true
}, {
  path: '/news/:id',
  exact: true,
  name: '',
  component: NewsView,
  admin: false
}, {
  path: '/events/new',
  exact: true,
  name: 'New',
  component: AddEvent,
  admin: true
}, {
  path: '/events/:unique_id/edit',
  exact: true,
  name: 'Edit',
  component: EditEvent,
  admin: true
}, {
  path: '/events',
  exact: true,
  name: 'Events',
  component: Events,
  admin: false
}, {
  path: '/tasks',
  exact: true,
  name: 'Tasks',
  component: Tasks,
  admin: false,
  director: true
}, {
  path: '/tasks/new',
  exact: true,
  name: 'New',
  component: AddTask,
  admin: false,
  director: true
}, {
  path: '/tasks/:unique_id/edit',
  exact: true,
  name: 'Edit',
  component: EditTask,
  admin: false,
  director: true
}, {
  path: '/tasks/:unique_id/view',
  exact: true,
  name: 'View',
  component: ViewTask,
  admin: false,
  director: true
}, {
  path: '/todos',
  exact: true,
  name: 'Todo',
  component: TodoView,
  admin: false
}, {
  path: '/todos/:unique_id/view',
  exact: true,
  name: 'Todo',
  component: ViewTodo,
  admin: false
}, {
  path: '/users/new',
  exact: true,
  name: 'New',
  component: AddUser,
  admin: true
}, {
  path: '/users/:unique_id/edit',
  exact: true,
  name: 'Edit',
  component: EditUser,
  admin: true
}, {
  path: '/users/:unique_id/view',
  exact: true,
  name: 'Edit',
  component: ViewUser,
  admin: true
}, {
  path: '/users',
  exact: true,
  name: 'Users',
  component: Users,
  admin: true
}, {
  path: '/groups/new',
  exact: true,
  name: 'New',
  component: AddGroup,
  admin: true
}, {
  path: '/groups/:unique_id/edit',
  exact: true,
  name: 'Edit',
  component: EditGroup,
  admin: true
}, {
  path: '/groups',
  exact: true,
  name: 'Groups',
  component: Groups,
  admin: true
}, {
  path: '/locations',
  exact: true,
  name: 'Locations',
  component: Locations,
  admin: true
}, {
  path: '/locations/new',
  exact: true,
  name: 'New',
  component: AddLocation,
  admin: true
}, {
  path: '/locations/:unique_id/edit',
  exact: true,
  name: 'Edit',
  component: EditLocation,
  admin: true
}, {
  path: '/forum',
  exact: true,
  name: 'Forum',
  component: Forum,
  admin: false
}, {
  path: '/forum/ask-a-question',
  exact: true,
  name: 'Ask a question',
  component: AskAQuestion,
  admin: false
}, {
  path: '/forum/question/:unique_id',
  exact: true,
  name: 'Question',
  component: QuestionView,
  admin: false
}, {
  path: '/forum/topic/:unique_id',
  exact: true,
  name: 'Topic',
  component: TopicView,
  admin: false
}, {
  path: '/profile',
  exact: true,
  name: 'Profile',
  component: Profile,
  admin: false
}, {
  path: '/settings',
  exact: true,
  name: 'Settings',
  component: Settings,
  admin: true
}, {
  path: '/search',
  exact: true,
  name: 'Search',
  component: Search,
  admin: false
}, {
  path: '/topics',
  exact: true,
  name: 'Topics',
  component: Topics,
  admin: true
}, {
  path: '/topics/new',
  exact: true,
  name: 'New',
  component: AddTopic,
  admin: true
}, {
  path: '/topics/:unique_id/edit',
  exact: true,
  name: 'Edit',
  component: EditTopic,
  admin: true
}, {
  path: '/topics/:unique_id/view',
  exact: true,
  name: 'View',
  component: ViewTopic,
  admin: true
}, {
  path: '/questions',
  exact: true,
  name: 'Questions',
  component: Questions,
  admin: true
}, {
  path: '/questions/new',
  exact: true,
  name: 'New',
  component: AddQuestion,
  admin: true
}, {
  path: '/questions/:unique_id/edit',
  exact: true,
  name: 'Edit',
  component: EditQuestion,
  admin: true
}, {
  path: '/questions/:unique_id/view',
  exact: true,
  name: 'View',
  component: ViewQuestion,
  admin: true
}];
/* harmony default export */ __webpack_exports__["default"] = (routes);

/***/ })

}]);