(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[24],{

/***/ "./node_modules/setimmediate/setImmediate.js":
/*!***************************************************!*\
  !*** ./node_modules/setimmediate/setImmediate.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, process) {(function (global, undefined) {
    "use strict";

    if (global.setImmediate) {
        return;
    }

    var nextHandle = 1; // Spec says greater than zero
    var tasksByHandle = {};
    var currentlyRunningATask = false;
    var doc = global.document;
    var registerImmediate;

    function setImmediate(callback) {
      // Callback can either be a function or a string
      if (typeof callback !== "function") {
        callback = new Function("" + callback);
      }
      // Copy function arguments
      var args = new Array(arguments.length - 1);
      for (var i = 0; i < args.length; i++) {
          args[i] = arguments[i + 1];
      }
      // Store and register the task
      var task = { callback: callback, args: args };
      tasksByHandle[nextHandle] = task;
      registerImmediate(nextHandle);
      return nextHandle++;
    }

    function clearImmediate(handle) {
        delete tasksByHandle[handle];
    }

    function run(task) {
        var callback = task.callback;
        var args = task.args;
        switch (args.length) {
        case 0:
            callback();
            break;
        case 1:
            callback(args[0]);
            break;
        case 2:
            callback(args[0], args[1]);
            break;
        case 3:
            callback(args[0], args[1], args[2]);
            break;
        default:
            callback.apply(undefined, args);
            break;
        }
    }

    function runIfPresent(handle) {
        // From the spec: "Wait until any invocations of this algorithm started before this one have completed."
        // So if we're currently running a task, we'll need to delay this invocation.
        if (currentlyRunningATask) {
            // Delay by doing a setTimeout. setImmediate was tried instead, but in Firefox 7 it generated a
            // "too much recursion" error.
            setTimeout(runIfPresent, 0, handle);
        } else {
            var task = tasksByHandle[handle];
            if (task) {
                currentlyRunningATask = true;
                try {
                    run(task);
                } finally {
                    clearImmediate(handle);
                    currentlyRunningATask = false;
                }
            }
        }
    }

    function installNextTickImplementation() {
        registerImmediate = function(handle) {
            process.nextTick(function () { runIfPresent(handle); });
        };
    }

    function canUsePostMessage() {
        // The test against `importScripts` prevents this implementation from being installed inside a web worker,
        // where `global.postMessage` means something completely different and can't be used for this purpose.
        if (global.postMessage && !global.importScripts) {
            var postMessageIsAsynchronous = true;
            var oldOnMessage = global.onmessage;
            global.onmessage = function() {
                postMessageIsAsynchronous = false;
            };
            global.postMessage("", "*");
            global.onmessage = oldOnMessage;
            return postMessageIsAsynchronous;
        }
    }

    function installPostMessageImplementation() {
        // Installs an event handler on `global` for the `message` event: see
        // * https://developer.mozilla.org/en/DOM/window.postMessage
        // * http://www.whatwg.org/specs/web-apps/current-work/multipage/comms.html#crossDocumentMessages

        var messagePrefix = "setImmediate$" + Math.random() + "$";
        var onGlobalMessage = function(event) {
            if (event.source === global &&
                typeof event.data === "string" &&
                event.data.indexOf(messagePrefix) === 0) {
                runIfPresent(+event.data.slice(messagePrefix.length));
            }
        };

        if (global.addEventListener) {
            global.addEventListener("message", onGlobalMessage, false);
        } else {
            global.attachEvent("onmessage", onGlobalMessage);
        }

        registerImmediate = function(handle) {
            global.postMessage(messagePrefix + handle, "*");
        };
    }

    function installMessageChannelImplementation() {
        var channel = new MessageChannel();
        channel.port1.onmessage = function(event) {
            var handle = event.data;
            runIfPresent(handle);
        };

        registerImmediate = function(handle) {
            channel.port2.postMessage(handle);
        };
    }

    function installReadyStateChangeImplementation() {
        var html = doc.documentElement;
        registerImmediate = function(handle) {
            // Create a <script> element; its readystatechange event will be fired asynchronously once it is inserted
            // into the document. Do so, thus queuing up the task. Remember to clean up once it's been called.
            var script = doc.createElement("script");
            script.onreadystatechange = function () {
                runIfPresent(handle);
                script.onreadystatechange = null;
                html.removeChild(script);
                script = null;
            };
            html.appendChild(script);
        };
    }

    function installSetTimeoutImplementation() {
        registerImmediate = function(handle) {
            setTimeout(runIfPresent, 0, handle);
        };
    }

    // If supported, we should attach to the prototype of global, since that is where setTimeout et al. live.
    var attachTo = Object.getPrototypeOf && Object.getPrototypeOf(global);
    attachTo = attachTo && attachTo.setTimeout ? attachTo : global;

    // Don't get fooled by e.g. browserify environments.
    if ({}.toString.call(global.process) === "[object process]") {
        // For Node.js before 0.9
        installNextTickImplementation();

    } else if (canUsePostMessage()) {
        // For non-IE10 modern browsers
        installPostMessageImplementation();

    } else if (global.MessageChannel) {
        // For web workers, where supported
        installMessageChannelImplementation();

    } else if (doc && "onreadystatechange" in doc.createElement("script")) {
        // For IE 6–8
        installReadyStateChangeImplementation();

    } else {
        // For older browsers
        installSetTimeoutImplementation();
    }

    attachTo.setImmediate = setImmediate;
    attachTo.clearImmediate = clearImmediate;
}(typeof self === "undefined" ? typeof global === "undefined" ? this : global : self));

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js"), __webpack_require__(/*! ./../process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./resources/js/components/ui/Form/DateInput.js":
/*!******************************************************!*\
  !*** ./resources/js/components/ui/Form/DateInput.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");



var DateInput = function DateInput(props) {
  var inputName = props.inputName,
      inputDefaultValue = props.inputDefaultValue,
      inputLabel = props.inputLabel,
      handleChange = props.handleChange,
      inputHelpText = props.inputHelpText,
      inputError = props.inputError,
      isRequired = props.isRequired,
      horizontal = props.horizontal;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormGroup"], {
    row: true,
    className: "mb-3"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    md: {
      size: !!!horizontal ? 3 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Label"], {
    htmlFor: inputName
  }, inputLabel)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    md: {
      size: !!!horizontal ? 9 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    required: isRequired ? true : false,
    type: "date",
    id: inputName,
    onChange: handleChange,
    defaultValue: inputDefaultValue
  }), inputHelpText && inputHelpText.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormText"], {
    color: "muted"
  }, inputHelpText)));
};

/* harmony default export */ __webpack_exports__["default"] = (DateInput);

/***/ }),

/***/ "./resources/js/components/ui/Form/FormSubmitActions.js":
/*!**************************************************************!*\
  !*** ./resources/js/components/ui/Form/FormSubmitActions.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");



var FormSubmitActions = function FormSubmitActions(props) {
  var id = props.id,
      handleDelete = props.handleDelete;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormGroup"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    type: "submit",
    size: "sm",
    color: "primary",
    className: "mr-2"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-dot-circle-o"
  }), " Submit"), id.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    size: "sm",
    color: "danger",
    onClick: function onClick() {
      return handleDelete(id);
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-ban"
  }), " Delete"));
};

/* harmony default export */ __webpack_exports__["default"] = (FormSubmitActions);

/***/ }),

/***/ "./resources/js/components/ui/Form/MultiSelectInput.js":
/*!*************************************************************!*\
  !*** ./resources/js/components/ui/Form/MultiSelectInput.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-select */ "./node_modules/react-select/dist/react-select.esm.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");




var MultiSelectInput = function MultiSelectInput(props) {
  var inputName = props.inputName,
      inputLabel = props.inputLabel,
      selectOptions = props.selectOptions,
      selectValue = props.selectValue,
      handleSelect = props.handleSelect,
      inputHelpText = props.inputHelpText,
      horizontal = props.horizontal;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["FormGroup"], {
    row: true,
    className: "mb-3"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    md: {
      size: !!!horizontal ? 3 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Label"], {
    htmlFor: inputName
  }, inputLabel)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    md: {
      size: !!!horizontal ? 9 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_1__["default"], {
    options: selectOptions,
    defaultValue: selectValue,
    isMulti: true,
    name: inputName,
    components: {
      DropdownIndicator: null
    },
    isClearable: true,
    onChange: handleSelect
  }), inputHelpText && inputHelpText.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["FormText"], {
    color: "muted"
  }, inputHelpText)));
};

/* harmony default export */ __webpack_exports__["default"] = (MultiSelectInput);

/***/ }),

/***/ "./resources/js/components/views/Tasks/Todos/TodoForm.js":
/*!***************************************************************!*\
  !*** ./resources/js/components/views/Tasks/Todos/TodoForm.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _ui_Form_DateInput__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../ui/Form/DateInput */ "./resources/js/components/ui/Form/DateInput.js");
/* harmony import */ var _ui_Form_MultiSelectInput__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../ui/Form/MultiSelectInput */ "./resources/js/components/ui/Form/MultiSelectInput.js");
/* harmony import */ var _ui_Form_FormSubmitActions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../ui/Form/FormSubmitActions */ "./resources/js/components/ui/Form/FormSubmitActions.js");
/* harmony import */ var _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../functions/FormFunctions.js */ "./resources/js/functions/FormFunctions.js");
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../actions */ "./resources/js/actions.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _objects_AppTodoUsers__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../objects/AppTodoUsers */ "./resources/js/objects/AppTodoUsers.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }












var TodoForm =
/*#__PURE__*/
function (_Component) {
  _inherits(TodoForm, _Component);

  function TodoForm(props) {
    var _this;

    _classCallCheck(this, TodoForm);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(TodoForm).call(this, props));
    _this.handleChange = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_6__["handleChange"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleSelect = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_6__["handleMultiSelect"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleSubmit = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_6__["handleSubmit"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(TodoForm, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var taskId = this.props.taskId;
      var item = new _objects_AppTodoUsers__WEBPACK_IMPORTED_MODULE_9__["default"](null, taskId, []);
      this.setState({
        item: item
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          users = _this$props.users,
          groups = _this$props.groups;
      var all = [].concat(_toConsumableArray(users), _toConsumableArray(groups)).sort(function (a, b) {
        return a.name > b.name ? 1 : a.name < b.name ? 0 : 1;
      });
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_8__["Form"], {
        onSubmit: this.handleSubmit
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_MultiSelectInput__WEBPACK_IMPORTED_MODULE_4__["default"], {
        inputName: "users",
        inputLabel: "Users/Groups",
        selectOptions: all,
        inputHelpText: "This is a help text",
        handleSelect: this.handleSelect,
        horizontal: true
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_DateInput__WEBPACK_IMPORTED_MODULE_3__["default"], {
        handleChange: this.handleChange,
        inputName: "deadline",
        inputLabel: "Deadline",
        horizontal: true,
        inputHelpText: "This is a help text"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_FormSubmitActions__WEBPACK_IMPORTED_MODULE_5__["default"], {
        id: "",
        handleDelete: this.handleDelete
      })));
    }
  }]);

  return TodoForm;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(state, ownProps) {
  var users = state.users.data;
  var groups = state.groups.data;

  if (state.currentUser.role === 'USER') {
    groups = [];
    if (state.currentUser.director) users = users.filter(function (item) {
      return item.location.id == state.currentUser.location.id;
    });else users = [];
  }

  return {
    users: users,
    groups: groups
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    save: function save(data, history) {
      dispatch(Object(_actions__WEBPACK_IMPORTED_MODULE_7__["saveTodos"])(data, history));
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps, mapDispatchToProps)(TodoForm));

/***/ }),

/***/ "./resources/js/components/views/Tasks/ViewTask.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/views/Tasks/ViewTask.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var react_render_html__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-render-html */ "./node_modules/react-render-html/index.js");
/* harmony import */ var react_render_html__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_render_html__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _ui_Tables_DataTable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../ui/Tables/DataTable */ "./resources/js/components/ui/Tables/DataTable.js");
/* harmony import */ var _Todos_TodoForm__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./Todos/TodoForm */ "./resources/js/components/views/Tasks/Todos/TodoForm.js");
/* harmony import */ var _objects_AppTask__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../objects/AppTask */ "./resources/js/objects/AppTask.js");










var ViewTask = function ViewTask(props) {
  var _props$item = props.item,
      title = _props$item.title,
      body = _props$item.body,
      id = _props$item.id,
      repeat = _props$item.repeat;
  var todos = props.todos,
      attachments = props.attachments;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "animated fadeIn"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Col"], {
    md: "8"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Card"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["CardBody"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "card-title"
  }, "Task"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
    className: "display-5"
  }, title), repeat !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h5", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "icon-refresh"
  }), "\xA0\xA0", repeat), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react_render_html__WEBPACK_IMPORTED_MODULE_5___default()(body)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Tables_DataTable__WEBPACK_IMPORTED_MODULE_6__["default"], {
    tableMap: "todos",
    filterBy: "task_id",
    filterByValue: id
  }))), attachments.length > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Card"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["CardHeader"], null, "Attachments"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["CardBody"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Table"], {
    responsive: true,
    className: "documents-table"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("thead", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
    style: {
      width: '1%'
    },
    className: "pl-0 pr-0"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
    style: {
      width: '90%'
    }
  }, "Name"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
    style: {
      width: '9%'
    }
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tbody", null, attachments.map(function (item) {
    var icon = _mimeTypes[item.mime_type] ? _mimeTypes[item.mime_type] : 'file-o';
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", {
      key: item.id
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
      className: "pr-0 pl-0"
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
      className: "fa fa-".concat(icon)
    })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, item.name, " (", item.size, ")", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", null, "Added ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", null, moment__WEBPACK_IMPORTED_MODULE_2___default()(item.created.date).fromNow()), " by ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("em", null, item.uploaded_by, ", ", item.location))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
      tag: "a",
      className: "p-0 float-right",
      color: "default",
      href: "/attachments/".concat(item.id, "/download")
    }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
      className: "fa fa-download mt-1"
    }))));
  })))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Col"], {
    md: "4"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Card"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["CardHeader"], null, "Assign task to users"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["CardBody"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Todos_TodoForm__WEBPACK_IMPORTED_MODULE_7__["default"], {
    taskId: id
  }))))));
};

var mapStateToProps = function mapStateToProps(_ref, _ref2) {
  var tasks = _ref.tasks,
      users = _ref.users,
      todos = _ref.todos,
      attachments = _ref.attachments;
  var match = _ref2.match;
  var task = tasks.data.find(function (item) {
    return item.id === match.params.unique_id;
  });
  var id = task ? task.id : "",
      title = task ? task.title : "",
      body = task ? task.body : "",
      repeat = task ? task.repeat : "";
  return {
    users: users.data,
    todos: todos.data.filter(function (todo) {
      return todo.task_id === task.id;
    }),
    item: new _objects_AppTask__WEBPACK_IMPORTED_MODULE_8__["default"](id, title, body, repeat),
    attachments: attachments.data.filter(function (item) {
      return item.parentId == match.params.unique_id;
    })
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(ViewTask));

/***/ }),

/***/ "./resources/js/functions/FormFunctions.js":
/*!*************************************************!*\
  !*** ./resources/js/functions/FormFunctions.js ***!
  \*************************************************/
/*! exports provided: handleSubmit, handleSelect, handleMultiSelect, handleChange, handleToggle, handleUpload, handleUploadMulti, handleEditorChange, handleDelete */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleSubmit", function() { return handleSubmit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleSelect", function() { return handleSelect; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleMultiSelect", function() { return handleMultiSelect; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleChange", function() { return handleChange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleToggle", function() { return handleToggle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleUpload", function() { return handleUpload; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleUploadMulti", function() { return handleUploadMulti; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleEditorChange", function() { return handleEditorChange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleDelete", function() { return handleDelete; });
function handleSubmit(e) {
  e.preventDefault();
  var item = this.state.item;
  var history = this.props.history;
  this.props.save(item.formData, history);
  e.currentTarget.reset();
}
function handleSelect(res, meta) {
  var item = this.state.item;
  item[meta.name] = res ? res.value : "";
  this.setState({
    item: item
  });
}
function handleMultiSelect(res, meta) {
  var item = this.state.item;
  item[meta.name] = [];

  if (res.length > 0) {
    res.forEach(function (i) {
      item[meta.name].push(i.value);
    });
  }

  this.setState({
    item: item
  });
}
function handleChange(e) {
  var item = this.state.item;
  item[e.target.id] = e.target.value;
  this.setState({
    item: item
  });
}
function handleToggle(e) {
  var inverse = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var item = this.state.item;
  item[e.target.id] = inverse ? !e.target.checked : e.target.checked;
  this.setState({
    item: item
  });
}
function handleUpload(e, name) {
  var item = this.state.item;
  item[name] = e.target.files[0];
  e.preventDefault();
  this.setState({
    item: item
  });
}
function handleUploadMulti(e, name) {
  var item = this.state.item;
  Array.from(e.target.files).forEach(function (fileItem, idx) {
    item[name][idx] = fileItem;
  });
  e.preventDefault();
  this.setState({
    item: item
  });
}
function handleEditorChange(data, name) {
  var item = this.state.item;
  item[name] = data;
  this.setState({
    item: item
  });
}
function handleDelete(id) {
  this.props.delete(id);
}

/***/ }),

/***/ "./resources/js/objects/AppItem.js":
/*!*****************************************!*\
  !*** ./resources/js/objects/AppItem.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AppItem; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var AppItem =
/*#__PURE__*/
function () {
  function AppItem(id) {
    _classCallCheck(this, AppItem);

    this.id = id;
  }

  _createClass(AppItem, [{
    key: "formData",
    get: function get() {
      var _this = this;

      var data = new FormData();
      Object.keys(this).forEach(function (key) {
        if (key === 'attachments') {
          _this[key].forEach(function (file) {
            return data.append('attachments[]', file);
          });
        } else {
          data.append(key, _this[key]);
        }
      });
      return data;
    }
  }]);

  return AppItem;
}();



/***/ }),

/***/ "./resources/js/objects/AppTask.js":
/*!*****************************************!*\
  !*** ./resources/js/objects/AppTask.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AppTask; });
/* harmony import */ var _AppItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AppItem */ "./resources/js/objects/AppItem.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }



var AppTask =
/*#__PURE__*/
function (_AppItem) {
  _inherits(AppTask, _AppItem);

  function AppTask() {
    var _this;

    var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
    var title = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
    var body = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
    var repeat = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "";

    _classCallCheck(this, AppTask);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(AppTask).call(this, id));
    _this.title = title;
    _this.body = body;
    _this.repeat = repeat;
    _this.attachments = [];
    return _this;
  }

  return AppTask;
}(_AppItem__WEBPACK_IMPORTED_MODULE_0__["default"]);



/***/ }),

/***/ "./resources/js/objects/AppTodoUsers.js":
/*!**********************************************!*\
  !*** ./resources/js/objects/AppTodoUsers.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AppTodoUsers; });
/* harmony import */ var _AppItem__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AppItem */ "./resources/js/objects/AppItem.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }



var AppTodoUsers =
/*#__PURE__*/
function (_AppItem) {
  _inherits(AppTodoUsers, _AppItem);

  function AppTodoUsers() {
    var _this;

    var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
    var task_id = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
    var users = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
    var deadline = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "";
    var completed = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;

    _classCallCheck(this, AppTodoUsers);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(AppTodoUsers).call(this, id));
    _this.task_id = task_id;
    _this.users = users;
    _this.deadline = deadline;
    _this.completed = completed;
    return _this;
  }

  return AppTodoUsers;
}(_AppItem__WEBPACK_IMPORTED_MODULE_0__["default"]);



/***/ }),

/***/ 1:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 2:
/*!**********************!*\
  !*** util (ignored) ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

}]);