(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[22],{

/***/ "./resources/js/components/functions/Utils.js":
/*!****************************************************!*\
  !*** ./resources/js/components/functions/Utils.js ***!
  \****************************************************/
/*! exports provided: handleSearch, handleSearch2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleSearch", function() { return handleSearch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleSearch2", function() { return handleSearch2; });
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! fuse.js */ "./node_modules/fuse.js/dist/fuse.js");
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fuse_js__WEBPACK_IMPORTED_MODULE_0__);
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


function handleSearch(e, searchKeys) {
  var _this = this;

  var searchTerms = e.target.value;
  searchKeys.forEach(function (key) {
    if (searchTerms.trim() !== "") {
      var options = _objectSpread({}, _globalFuseOptions, {
        keys: _globalFuseKeys[key]
      });

      var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_0___default.a(_this.state[key], options);

      _this.setState(_defineProperty({}, "".concat(key, "Filtered"), fuse.search(searchTerms)));
    } else {
      _this.setState(_defineProperty({}, "".concat(key, "Filtered"), _this.state[key]));
    }
  });
}
function handleSearch2(e) {
  this.setState({
    searchTerms: e.target.value
  });
}

/***/ }),

/***/ "./resources/js/components/ui/Form/DateInput.js":
/*!******************************************************!*\
  !*** ./resources/js/components/ui/Form/DateInput.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");



var DateInput = function DateInput(props) {
  var inputName = props.inputName,
      inputDefaultValue = props.inputDefaultValue,
      inputLabel = props.inputLabel,
      handleChange = props.handleChange,
      inputHelpText = props.inputHelpText,
      inputError = props.inputError,
      isRequired = props.isRequired,
      horizontal = props.horizontal;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormGroup"], {
    row: true,
    className: "mb-3"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    md: {
      size: !!!horizontal ? 3 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Label"], {
    htmlFor: inputName
  }, inputLabel)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    md: {
      size: !!!horizontal ? 9 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    required: isRequired ? true : false,
    type: "date",
    id: inputName,
    onChange: handleChange,
    defaultValue: inputDefaultValue
  }), inputHelpText && inputHelpText.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormText"], {
    color: "muted"
  }, inputHelpText)));
};

/* harmony default export */ __webpack_exports__["default"] = (DateInput);

/***/ }),

/***/ "./resources/js/components/ui/Form/TextInput.js":
/*!******************************************************!*\
  !*** ./resources/js/components/ui/Form/TextInput.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");



var TextInput = function TextInput(props) {
  var inputName = props.inputName,
      inputDefaultValue = props.inputDefaultValue,
      inputLabel = props.inputLabel,
      handleChange = props.handleChange,
      inputHelpText = props.inputHelpText,
      inputError = props.inputError,
      isRequired = props.isRequired,
      inputType = props.inputType,
      horizontal = props.horizontal;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormGroup"], {
    row: true,
    className: "mb-3"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    md: {
      size: !!!horizontal ? 3 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Label"], {
    htmlFor: inputName
  }, inputLabel)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    md: {
      size: !!!horizontal ? 9 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    type: inputType ? inputType : 'text',
    id: inputName,
    required: isRequired ? true : false,
    onChange: handleChange,
    defaultValue: inputDefaultValue
  }), inputHelpText && inputHelpText.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormText"], {
    color: "muted"
  }, inputHelpText)));
};

/* harmony default export */ __webpack_exports__["default"] = (TextInput);

/***/ }),

/***/ "./resources/js/components/ui/Form/ToggleSwitch.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/ui/Form/ToggleSwitch.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _coreui_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @coreui/react */ "./node_modules/@coreui/react/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");




var ToggleSwitch = function ToggleSwitch(props) {
  var inputName = props.inputName,
      inputLabel = props.inputLabel,
      handleToggle = props.handleToggle,
      switchColour = props.switchColour,
      switchType = props.switchType,
      inputChecked = props.inputChecked,
      inputHelpText = props.inputHelpText,
      inverseChecked = props.inverseChecked,
      horizontal = props.horizontal;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["FormGroup"], {
    row: true,
    className: "mb-3"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    md: {
      size: !!!horizontal ? 3 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Label"], {
    htmlFor: inputName
  }, inputLabel)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    md: {
      size: !!!horizontal ? 9 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_coreui_react__WEBPACK_IMPORTED_MODULE_1__["AppSwitch"], {
    variant: switchType,
    color: switchColour,
    id: inputName,
    checked: inputChecked,
    onChange: function onChange(e) {
      return handleToggle(e, inverseChecked);
    }
  }), inputHelpText && inputHelpText.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["FormText"], {
    color: "muted"
  }, inputHelpText)));
};

/* harmony default export */ __webpack_exports__["default"] = (ToggleSwitch);

/***/ }),

/***/ "./resources/js/components/ui/Form/WysiwygInput.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/ui/Form/WysiwygInput.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var draftjs_to_html__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! draftjs-to-html */ "./node_modules/draftjs-to-html/lib/draftjs-to-html.js");
/* harmony import */ var draftjs_to_html__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(draftjs_to_html__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_draft_wysiwyg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-draft-wysiwyg */ "./node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.js");
/* harmony import */ var react_draft_wysiwyg__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_draft_wysiwyg__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var react_draft_wysiwyg_dist_react_draft_wysiwyg_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-draft-wysiwyg/dist/react-draft-wysiwyg.css */ "./node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css");
/* harmony import */ var react_draft_wysiwyg_dist_react_draft_wysiwyg_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_draft_wysiwyg_dist_react_draft_wysiwyg_css__WEBPACK_IMPORTED_MODULE_5__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








var WysiwygInput =
/*#__PURE__*/
function (_Component) {
  _inherits(WysiwygInput, _Component);

  function WysiwygInput() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, WysiwygInput);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(WysiwygInput)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      editorState: draft_js__WEBPACK_IMPORTED_MODULE_1__["EditorState"].createEmpty()
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleThisEditorChange", function (editorState) {
      var _this$props = _this.props,
          handleEditorChange = _this$props.handleEditorChange,
          inputName = _this$props.inputName;

      _this.setState({
        editorState: editorState
      });

      handleEditorChange(draftjs_to_html__WEBPACK_IMPORTED_MODULE_2___default()(Object(draft_js__WEBPACK_IMPORTED_MODULE_1__["convertToRaw"])(editorState.getCurrentContent())), inputName);
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "componentDidMount", function () {
      var body = _this.props.body;

      if (body && body.trim() != "" && Object(draft_js__WEBPACK_IMPORTED_MODULE_1__["convertFromHTML"])(body).contentBlocks !== null) {
        _this.setState({
          editorState: draft_js__WEBPACK_IMPORTED_MODULE_1__["EditorState"].createWithContent(draft_js__WEBPACK_IMPORTED_MODULE_1__["ContentState"].createFromBlockArray(Object(draft_js__WEBPACK_IMPORTED_MODULE_1__["convertFromHTML"])(body)))
        });
      }
    });

    return _this;
  }

  _createClass(WysiwygInput, [{
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          inputLabel = _this$props2.inputLabel,
          inputName = _this$props2.inputName,
          inputHelpText = _this$props2.inputHelpText,
          body = _this$props2.body,
          handleEditorChange = _this$props2.handleEditorChange,
          horizontal = _this$props2.horizontal;
      var editorState = this.state.editorState;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["FormGroup"], {
        row: true,
        className: "mb-3"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Col"], {
        md: {
          size: !!!horizontal ? 3 : 12
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Label"], {
        htmlFor: inputName
      }, inputLabel)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Col"], {
        md: {
          size: !!!horizontal ? 9 : 12
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_draft_wysiwyg__WEBPACK_IMPORTED_MODULE_3__["Editor"], {
        editorState: editorState,
        editorClassName: "form-control",
        onEditorStateChange: this.handleThisEditorChange
      }), inputHelpText && inputHelpText.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["FormText"], {
        color: "muted"
      }, inputHelpText)));
    }
  }]);

  return WysiwygInput;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (WysiwygInput);

/***/ }),

/***/ "./resources/js/components/ui/ModuleSearchBox.js":
/*!*******************************************************!*\
  !*** ./resources/js/components/ui/ModuleSearchBox.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");



var ModuleSearchBox = function ModuleSearchBox(props) {
  var handleSearch = props.handleSearch,
      placeholder = props.placeholder;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["InputGroup"], {
    className: "input-prepend mb-4"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["InputGroupAddon"], {
    addonType: "prepend"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["InputGroupText"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-search"
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    bsSize: "lg",
    type: "text",
    placeholder: placeholder,
    onChange: handleSearch
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (ModuleSearchBox);

/***/ }),

/***/ "./resources/js/components/ui/Spinner.js":
/*!***********************************************!*\
  !*** ./resources/js/components/ui/Spinner.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);


var Spinner = function Spinner() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle1 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle2 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle3 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle4 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle5 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle6 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle7 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle8 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle9 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle10 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle11 sk-child"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "sk-circle12 sk-child"
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Spinner);

/***/ }),

/***/ "./resources/js/components/views/Documents/Documents.js":
/*!**************************************************************!*\
  !*** ./resources/js/components/views/Documents/Documents.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _ui_FolderItem__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ui/FolderItem */ "./resources/js/components/views/Documents/ui/FolderItem.js");
/* harmony import */ var _Home__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Home */ "./resources/js/components/views/Documents/Home.js");
/* harmony import */ var _NewFolder__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./NewFolder */ "./resources/js/components/views/Documents/NewFolder.js");
/* harmony import */ var _ViewFolder__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./ViewFolder */ "./resources/js/components/views/Documents/ViewFolder.js");
/* harmony import */ var _EditFolder__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./EditFolder */ "./resources/js/components/views/Documents/EditFolder.js");
/* harmony import */ var _NewFile__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./NewFile */ "./resources/js/components/views/Documents/NewFile.js");
/* harmony import */ var _ViewFile__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./ViewFile */ "./resources/js/components/views/Documents/ViewFile.js");
/* harmony import */ var _EditFile__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./EditFile */ "./resources/js/components/views/Documents/EditFile.js");
/* harmony import */ var _ui_FolderItemChildren__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./ui/FolderItemChildren */ "./resources/js/components/views/Documents/ui/FolderItemChildren.js");
/* harmony import */ var _ui_ModuleSearchBox__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../ui/ModuleSearchBox */ "./resources/js/components/ui/ModuleSearchBox.js");
/* harmony import */ var _ui_Spinner__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../ui/Spinner */ "./resources/js/components/ui/Spinner.js");
/* harmony import */ var _functions_Utils__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../functions/Utils */ "./resources/js/components/functions/Utils.js");
/* harmony import */ var _ui_SearchResults__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./ui/SearchResults */ "./resources/js/components/views/Documents/ui/SearchResults.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




















var Documents =
/*#__PURE__*/
function (_Component) {
  _inherits(Documents, _Component);

  function Documents(props) {
    var _this;

    _classCallCheck(this, Documents);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Documents).call(this, props));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "toggleCollapse", function (e) {
      e.preventDefault();
      var isOpen = _this.state.isOpen;

      _this.setState({
        isOpen: !isOpen
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleClick", function (e) {
      e.preventDefault();

      _this.props.history.push('/documents');
    });

    _this.state = {
      searchTerms: "",
      isOpen: true
    };
    _this.handleSearch = _functions_Utils__WEBPACK_IMPORTED_MODULE_16__["handleSearch2"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(Documents, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          documents = _this$props.documents,
          fetching = _this$props.fetching;
      var _this$state = this.state,
          isOpen = _this$state.isOpen,
          searchTerms = _this$state.searchTerms;
      var caretClassNames = classnames__WEBPACK_IMPORTED_MODULE_3___default()({
        fa: true,
        'fa-lg': true,
        'fa-caret-right': !isOpen,
        'fa-caret-down': isOpen
      });
      var folderClassNames = classnames__WEBPACK_IMPORTED_MODULE_3___default()({
        fa: true,
        'fa-lg': true,
        'fa-folder': !isOpen,
        'fa-folder-open': isOpen
      });
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "animated fadeIn"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_ModuleSearchBox__WEBPACK_IMPORTED_MODULE_14__["default"], {
        handleSearch: this.handleSearch,
        placeholder: "Search..."
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Col"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Card"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["CardBody"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
        className: "card-title mb-0"
      }, "Documents"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "text-muted"
      }, "Here you can find all of our latest Banana Moon documents \u2013 please ensure you check this section frequently, as documents are regularly updated to reflect current legislation. It is essential that you are using the correct version of each document at your nursery."), fetching && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Spinner__WEBPACK_IMPORTED_MODULE_15__["default"], null), searchTerms.trim() === "" && !fetching && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "doc-app mb-4"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("nav", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Nav"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "folder-item"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["ButtonGroup"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "p-0",
        color: "link",
        onClick: this.toggleCollapse
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        style: {
          width: '11px'
        },
        className: caretClassNames
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pl-1",
        color: "link",
        onClick: this.handleClick
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: folderClassNames
      }), " Home"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Collapse"], {
        isOpen: isOpen
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_FolderItemChildren__WEBPACK_IMPORTED_MODULE_13__["default"], {
        folderId: null
      })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("main", {
        className: "docs"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Switch"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Route"], {
        exact: true,
        path: "/documents",
        component: _Home__WEBPACK_IMPORTED_MODULE_6__["default"]
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Route"], {
        exact: true,
        path: "/documents/folder/new",
        component: _NewFolder__WEBPACK_IMPORTED_MODULE_7__["default"]
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Route"], {
        exact: true,
        path: "/documents/file/new",
        component: _NewFile__WEBPACK_IMPORTED_MODULE_10__["default"]
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Route"], {
        exact: true,
        path: "/documents/folder/:unique_id",
        component: _ViewFolder__WEBPACK_IMPORTED_MODULE_8__["default"]
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Route"], {
        exact: true,
        path: "/documents/folder/:unique_id/edit",
        component: _EditFolder__WEBPACK_IMPORTED_MODULE_9__["default"]
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Route"], {
        exact: true,
        path: "/documents/file/:unique_id",
        component: _ViewFile__WEBPACK_IMPORTED_MODULE_11__["default"]
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Route"], {
        exact: true,
        path: "/documents/file/:unique_id/edit",
        component: _EditFile__WEBPACK_IMPORTED_MODULE_12__["default"]
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Route"], {
        exact: true,
        path: "/documents/folder/:unique_id/folder/new",
        component: _NewFolder__WEBPACK_IMPORTED_MODULE_7__["default"]
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["Route"], {
        exact: true,
        path: "/documents/folder/:unique_id/file/new",
        component: _NewFile__WEBPACK_IMPORTED_MODULE_10__["default"]
      })))), searchTerms.trim() !== "" && !fetching && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "doc-app mb-4"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("main", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", null, "Search Results"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_SearchResults__WEBPACK_IMPORTED_MODULE_17__["default"], {
        searchTerms: searchTerms
      }))))))));
    }
  }]);

  return Documents;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref) {
  var documents = _ref.documents;
  var data = documents.data;
  data.sort(function (a, b) {
    if (a.title < b.title) {
      return -1;
    }

    if (a.title > b.title) {
      return 1;
    }

    return 0;
  });
  return {
    documents: data,
    fetching: documents.fetching
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(Documents));

/***/ }),

/***/ "./resources/js/components/views/Documents/EditFile.js":
/*!*************************************************************!*\
  !*** ./resources/js/components/views/Documents/EditFile.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _FileForm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FileForm */ "./resources/js/components/views/Documents/FileForm.js");
/* harmony import */ var _addToolbar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./addToolbar */ "./resources/js/components/views/Documents/addToolbar.js");
/* harmony import */ var _addFolder__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./addFolder */ "./resources/js/components/views/Documents/addFolder.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }







var EditFile =
/*#__PURE__*/
function (_Component) {
  _inherits(EditFile, _Component);

  function EditFile() {
    _classCallCheck(this, EditFile);

    return _possibleConstructorReturn(this, _getPrototypeOf(EditFile).apply(this, arguments));
  }

  _createClass(EditFile, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "animated fadeIn"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
        className: "card-title mb-4"
      }, "Edit File"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_FileForm__WEBPACK_IMPORTED_MODULE_1__["default"], {
        isNew: false
      }));
    }
  }]);

  return EditFile;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(_addFolder__WEBPACK_IMPORTED_MODULE_3__["default"])(Object(_addToolbar__WEBPACK_IMPORTED_MODULE_2__["default"])(EditFile)));

/***/ }),

/***/ "./resources/js/components/views/Documents/EditFolder.js":
/*!***************************************************************!*\
  !*** ./resources/js/components/views/Documents/EditFolder.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _FolderForm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FolderForm */ "./resources/js/components/views/Documents/FolderForm.js");
/* harmony import */ var _addToolbar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./addToolbar */ "./resources/js/components/views/Documents/addToolbar.js");
/* harmony import */ var _addFolder__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./addFolder */ "./resources/js/components/views/Documents/addFolder.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }







var EditFolder =
/*#__PURE__*/
function (_Component) {
  _inherits(EditFolder, _Component);

  function EditFolder() {
    _classCallCheck(this, EditFolder);

    return _possibleConstructorReturn(this, _getPrototypeOf(EditFolder).apply(this, arguments));
  }

  _createClass(EditFolder, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "animated fadeIn"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
        className: "card-title mb-4"
      }, "Edit Folder"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_FolderForm__WEBPACK_IMPORTED_MODULE_1__["default"], {
        isNew: false
      }));
    }
  }]);

  return EditFolder;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(_addFolder__WEBPACK_IMPORTED_MODULE_3__["default"])(Object(_addToolbar__WEBPACK_IMPORTED_MODULE_2__["default"])(EditFolder)));

/***/ }),

/***/ "./resources/js/components/views/Documents/FileForm.js":
/*!*************************************************************!*\
  !*** ./resources/js/components/views/Documents/FileForm.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ui_Form_TextInput__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../ui/Form/TextInput */ "./resources/js/components/ui/Form/TextInput.js");
/* harmony import */ var _ui_Form_FileInput__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../ui/Form/FileInput */ "./resources/js/components/ui/Form/FileInput.js");
/* harmony import */ var _ui_Form_WysiwygInput__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../ui/Form/WysiwygInput */ "./resources/js/components/ui/Form/WysiwygInput.js");
/* harmony import */ var _ui_Form_DateInput__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../ui/Form/DateInput */ "./resources/js/components/ui/Form/DateInput.js");
/* harmony import */ var _ui_Form_ToggleSwitch__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../ui/Form/ToggleSwitch */ "./resources/js/components/ui/Form/ToggleSwitch.js");
/* harmony import */ var _ui_Form_FormSubmitActions__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../ui/Form/FormSubmitActions */ "./resources/js/components/ui/Form/FormSubmitActions.js");
/* harmony import */ var _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../functions/FormFunctions.js */ "./resources/js/functions/FormFunctions.js");
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../actions */ "./resources/js/actions.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _objects_AppFolder__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../objects/AppFolder */ "./resources/js/objects/AppFolder.js");
/* harmony import */ var _addFolder__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./addFolder */ "./resources/js/components/views/Documents/addFolder.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

















var FileForm =
/*#__PURE__*/
function (_Component) {
  _inherits(FileForm, _Component);

  function FileForm(props) {
    var _this;

    _classCallCheck(this, FileForm);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(FileForm).call(this, props));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      item: new _objects_AppFolder__WEBPACK_IMPORTED_MODULE_13__["default"]()
    });

    _this.handleChange = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_10__["handleChange"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleToggle = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_10__["handleToggle"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleUpload = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_10__["handleUpload"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleEditorChange = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_10__["handleEditorChange"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleDelete = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_10__["handleDelete"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleSubmit = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_10__["handleSubmit"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(FileForm, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props = this.props,
          item = _this$props.item,
          isNew = _this$props.isNew,
          folder = _this$props.folder;
      item.parent = folder != undefined ? isNew ? folder.id : folder.parent : "";
      item.folder = false;
      item.title = "Temp";
      this.setState({
        item: item
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props$item = this.props.item,
          id = _this$props$item.id,
          title = _this$props$item.title,
          draft = _this$props$item.draft,
          about = _this$props$item.about,
          updated = _this$props$item.updated,
          publish = _this$props$item.publish,
          expire = _this$props$item.expire;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_12__["Form"], {
        onSubmit: this.handleSubmit
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_ToggleSwitch__WEBPACK_IMPORTED_MODULE_8__["default"], {
        handleToggle: this.handleToggle,
        inputChecked: !draft,
        inputLabel: "Published",
        inputName: "draft",
        inputHelpText: "",
        switchType: "pill",
        switchColour: "primary",
        inverseChecked: true
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_FileInput__WEBPACK_IMPORTED_MODULE_5__["default"], {
        handleUpload: this.handleUpload,
        inputDefaultValue: null,
        inputName: "file",
        inputLabel: "File",
        inputHelpText: "This is a help text"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_WysiwygInput__WEBPACK_IMPORTED_MODULE_6__["default"], {
        inputName: "about",
        inputLabel: "About",
        inputHelpText: "This is a help text",
        body: about,
        handleEditorChange: this.handleEditorChange
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_DateInput__WEBPACK_IMPORTED_MODULE_7__["default"], {
        handleChange: this.handleChange,
        inputDefaultValue: moment__WEBPACK_IMPORTED_MODULE_3___default()(publish).format('YYYY-MM-DD'),
        inputName: "publish",
        inputLabel: "Publish Date",
        inputHelpText: "This is a help text"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_DateInput__WEBPACK_IMPORTED_MODULE_7__["default"], {
        handleChange: this.handleChange,
        inputDefaultValue: moment__WEBPACK_IMPORTED_MODULE_3___default()(expire).format('YYYY-MM-DD'),
        inputName: "expire",
        inputLabel: "Expire Date",
        inputHelpText: "This is a help text"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_FormSubmitActions__WEBPACK_IMPORTED_MODULE_9__["default"], {
        id: id,
        handleDelete: this.handleDelete
      })));
    }
  }]);

  return FileForm;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref, _ref2) {
  var documents = _ref.documents,
      currentUser = _ref.currentUser;
  var match = _ref2.match,
      isNew = _ref2.isNew;
  var doc = documents.data.find(function (item) {
    return item.id === match.params.unique_id && !isNew;
  });
  var id = doc ? doc.id : "",
      title = doc ? doc.title : "",
      about = doc ? doc.about : "",
      draft = doc ? doc.draft : true,
      publish = doc ? doc.publish : "",
      expire = doc ? doc.expire : "";
  return {
    item: new _objects_AppFolder__WEBPACK_IMPORTED_MODULE_13__["default"](id, title, about, draft, publish, expire)
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch, _ref3) {
  var match = _ref3.match,
      history = _ref3.history;
  var unique_id = match.params.unique_id;
  var redirect = '/documents';
  redirect += unique_id ? "/folder/".concat(unique_id) : "";
  return {
    save: function save(data) {
      dispatch(Object(_actions__WEBPACK_IMPORTED_MODULE_11__["saveResource"])('Documents', data, history, redirect));
    },
    delete: function _delete(id) {
      dispatch(Object(_actions__WEBPACK_IMPORTED_MODULE_11__["deleteResource"])('Documents', id, history, redirect));
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["withRouter"])(Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps, mapDispatchToProps)(Object(_addFolder__WEBPACK_IMPORTED_MODULE_14__["default"])(FileForm))));

/***/ }),

/***/ "./resources/js/components/views/Documents/FolderForm.js":
/*!***************************************************************!*\
  !*** ./resources/js/components/views/Documents/FolderForm.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ui_Form_TextInput__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../ui/Form/TextInput */ "./resources/js/components/ui/Form/TextInput.js");
/* harmony import */ var _ui_Form_FileInput__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../ui/Form/FileInput */ "./resources/js/components/ui/Form/FileInput.js");
/* harmony import */ var _ui_Form_WysiwygInput__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../ui/Form/WysiwygInput */ "./resources/js/components/ui/Form/WysiwygInput.js");
/* harmony import */ var _ui_Form_DateInput__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../ui/Form/DateInput */ "./resources/js/components/ui/Form/DateInput.js");
/* harmony import */ var _ui_Form_ToggleSwitch__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../ui/Form/ToggleSwitch */ "./resources/js/components/ui/Form/ToggleSwitch.js");
/* harmony import */ var _ui_Form_FormSubmitActions__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../ui/Form/FormSubmitActions */ "./resources/js/components/ui/Form/FormSubmitActions.js");
/* harmony import */ var _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../functions/FormFunctions.js */ "./resources/js/functions/FormFunctions.js");
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../actions */ "./resources/js/actions.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _objects_AppFolder__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../../objects/AppFolder */ "./resources/js/objects/AppFolder.js");
/* harmony import */ var _addFolder__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./addFolder */ "./resources/js/components/views/Documents/addFolder.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

















var FolderForm =
/*#__PURE__*/
function (_Component) {
  _inherits(FolderForm, _Component);

  function FolderForm(props) {
    var _this;

    _classCallCheck(this, FolderForm);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(FolderForm).call(this, props));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      item: new _objects_AppFolder__WEBPACK_IMPORTED_MODULE_13__["default"]()
    });

    _this.handleChange = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_10__["handleChange"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleToggle = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_10__["handleToggle"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleUpload = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_10__["handleUpload"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleEditorChange = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_10__["handleEditorChange"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleDelete = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_10__["handleDelete"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleSubmit = _functions_FormFunctions_js__WEBPACK_IMPORTED_MODULE_10__["handleSubmit"].bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(FolderForm, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props = this.props,
          item = _this$props.item,
          isNew = _this$props.isNew,
          folder = _this$props.folder;
      item.parent = folder != undefined ? isNew ? folder.id : folder.parent : "";
      item.folder = true;
      this.setState({
        item: item
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props$item = this.props.item,
          id = _this$props$item.id,
          title = _this$props$item.title,
          draft = _this$props$item.draft,
          about = _this$props$item.about,
          updated = _this$props$item.updated,
          publish = _this$props$item.publish,
          expire = _this$props$item.expire;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_12__["Form"], {
        onSubmit: this.handleSubmit
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_ToggleSwitch__WEBPACK_IMPORTED_MODULE_8__["default"], {
        handleToggle: this.handleToggle,
        inputChecked: !draft,
        inputLabel: "Published",
        inputName: "draft",
        inputHelpText: "",
        switchType: "pill",
        switchColour: "primary",
        inverseChecked: true
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_TextInput__WEBPACK_IMPORTED_MODULE_4__["default"], {
        handleChange: this.handleChange,
        inputDefaultValue: title,
        inputName: "title",
        inputLabel: "Title",
        inputHelpText: "This is a help text"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_WysiwygInput__WEBPACK_IMPORTED_MODULE_6__["default"], {
        inputName: "about",
        inputLabel: "About",
        inputHelpText: "This is a help text",
        body: about,
        handleEditorChange: this.handleEditorChange
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_DateInput__WEBPACK_IMPORTED_MODULE_7__["default"], {
        handleChange: this.handleChange,
        inputDefaultValue: moment__WEBPACK_IMPORTED_MODULE_3___default()(publish).format('YYYY-MM-DD'),
        inputName: "publish",
        inputLabel: "Publish Date",
        inputHelpText: "This is a help text"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_DateInput__WEBPACK_IMPORTED_MODULE_7__["default"], {
        handleChange: this.handleChange,
        inputDefaultValue: moment__WEBPACK_IMPORTED_MODULE_3___default()(expire).format('YYYY-MM-DD'),
        inputName: "expire",
        inputLabel: "Expire Date",
        inputHelpText: "This is a help text"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_Form_FormSubmitActions__WEBPACK_IMPORTED_MODULE_9__["default"], {
        id: id,
        handleDelete: this.handleDelete
      })));
    }
  }]);

  return FolderForm;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref, _ref2) {
  var documents = _ref.documents,
      currentUser = _ref.currentUser;
  var match = _ref2.match,
      isNew = _ref2.isNew;
  var doc = documents.data.find(function (item) {
    return item.id === match.params.unique_id && !isNew;
  });
  var id = doc ? doc.id : "",
      title = doc ? doc.title : "",
      about = doc ? doc.about : "",
      draft = doc ? doc.draft : true,
      publish = doc ? doc.publish : "",
      expire = doc ? doc.expire : "";
  return {
    item: new _objects_AppFolder__WEBPACK_IMPORTED_MODULE_13__["default"](id, title, about, draft, publish, expire)
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch, _ref3) {
  var match = _ref3.match,
      history = _ref3.history;
  var unique_id = match.params.unique_id;
  var redirect = '/documents';
  redirect += unique_id ? "/folder/".concat(unique_id) : "";
  return {
    save: function save(data) {
      dispatch(Object(_actions__WEBPACK_IMPORTED_MODULE_11__["saveResource"])('Documents', data, history, redirect));
    },
    delete: function _delete(id) {
      dispatch(Object(_actions__WEBPACK_IMPORTED_MODULE_11__["deleteResource"])('Documents', id, history, redirect));
    }
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["withRouter"])(Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps, mapDispatchToProps)(Object(_addFolder__WEBPACK_IMPORTED_MODULE_14__["default"])(FolderForm))));

/***/ }),

/***/ "./resources/js/components/views/Documents/Home.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/views/Documents/Home.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _addToolbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./addToolbar */ "./resources/js/components/views/Documents/addToolbar.js");
/* harmony import */ var _ui_FolderTable__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ui/FolderTable */ "./resources/js/components/views/Documents/ui/FolderTable.js");
/* harmony import */ var _ui_RecentDocuments__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ui/RecentDocuments */ "./resources/js/components/views/Documents/ui/RecentDocuments.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }






var Home =
/*#__PURE__*/
function (_Component) {
  _inherits(Home, _Component);

  function Home() {
    _classCallCheck(this, Home);

    return _possibleConstructorReturn(this, _getPrototypeOf(Home).apply(this, arguments));
  }

  _createClass(Home, [{
    key: "render",
    value: function render() {
      var documents = this.props.documents;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "animated fadeIn"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", null, "Recent Files"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_RecentDocuments__WEBPACK_IMPORTED_MODULE_3__["default"], null)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "mt-5"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", null, "Root Folder"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_FolderTable__WEBPACK_IMPORTED_MODULE_2__["default"], {
        folderId: null
      })));
    }
  }]);

  return Home;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(_addToolbar__WEBPACK_IMPORTED_MODULE_1__["default"])(Home));

/***/ }),

/***/ "./resources/js/components/views/Documents/NewFile.js":
/*!************************************************************!*\
  !*** ./resources/js/components/views/Documents/NewFile.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _FileForm__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./FileForm */ "./resources/js/components/views/Documents/FileForm.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _addToolbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./addToolbar */ "./resources/js/components/views/Documents/addToolbar.js");
/* harmony import */ var _addFolder__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./addFolder */ "./resources/js/components/views/Documents/addFolder.js");







var NewFile = function NewFile(props) {
  var folder = props.folder;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "animated fadeIn"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "card-title mb-4"
  }, "Upload a file ", folder !== undefined && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "in ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("em", null, folder.title))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_FileForm__WEBPACK_IMPORTED_MODULE_2__["default"], {
    isNew: true
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Object(_addFolder__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_addToolbar__WEBPACK_IMPORTED_MODULE_4__["default"])(NewFile)));

/***/ }),

/***/ "./resources/js/components/views/Documents/NewFolder.js":
/*!**************************************************************!*\
  !*** ./resources/js/components/views/Documents/NewFolder.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var _FolderForm__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./FolderForm */ "./resources/js/components/views/Documents/FolderForm.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _addToolbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./addToolbar */ "./resources/js/components/views/Documents/addToolbar.js");
/* harmony import */ var _addFolder__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./addFolder */ "./resources/js/components/views/Documents/addFolder.js");







var NewFolder = function NewFolder(props) {
  var folder = props.folder;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "animated fadeIn"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    className: "card-title mb-4"
  }, "New Folder ", folder !== undefined && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", null, "in ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("em", null, folder.title))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_FolderForm__WEBPACK_IMPORTED_MODULE_2__["default"], {
    isNew: true
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Object(_addFolder__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_addToolbar__WEBPACK_IMPORTED_MODULE_4__["default"])(NewFolder)));

/***/ }),

/***/ "./resources/js/components/views/Documents/ViewFile.js":
/*!*************************************************************!*\
  !*** ./resources/js/components/views/Documents/ViewFile.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var _addFolder__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./addFolder */ "./resources/js/components/views/Documents/addFolder.js");
/* harmony import */ var _ui_FileTable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ui/FileTable */ "./resources/js/components/views/Documents/ui/FileTable.js");
/* harmony import */ var _ui_FileUploadForm__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ui/FileUploadForm */ "./resources/js/components/views/Documents/ui/FileUploadForm.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }








var ViewFile =
/*#__PURE__*/
function (_Component) {
  _inherits(ViewFile, _Component);

  function ViewFile() {
    _classCallCheck(this, ViewFile);

    return _possibleConstructorReturn(this, _getPrototypeOf(ViewFile).apply(this, arguments));
  }

  _createClass(ViewFile, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          folder = _this$props.folder,
          currentView = _this$props.currentView;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "animated fadeIn"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "pt-2"
      }, "Versions of ", folder.title)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "text-muted"
      }, "Documents keep older versions and are displayed in the order they were uploaded."), currentView.admin && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_FileUploadForm__WEBPACK_IMPORTED_MODULE_5__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_FileTable__WEBPACK_IMPORTED_MODULE_4__["default"], {
        folderId: folder.id
      }));
    }
  }]);

  return ViewFile;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref) {
  var currentView = _ref.currentView;
  return {
    currentView: currentView
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(Object(_addFolder__WEBPACK_IMPORTED_MODULE_3__["default"])(ViewFile)));

/***/ }),

/***/ "./resources/js/components/views/Documents/ViewFolder.js":
/*!***************************************************************!*\
  !*** ./resources/js/components/views/Documents/ViewFolder.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var react_render_html__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-render-html */ "./node_modules/react-render-html/index.js");
/* harmony import */ var react_render_html__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_render_html__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _addToolbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./addToolbar */ "./resources/js/components/views/Documents/addToolbar.js");
/* harmony import */ var _addFolder__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./addFolder */ "./resources/js/components/views/Documents/addFolder.js");
/* harmony import */ var _ui_FolderTable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ui/FolderTable */ "./resources/js/components/views/Documents/ui/FolderTable.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }









var ViewFolder =
/*#__PURE__*/
function (_Component) {
  _inherits(ViewFolder, _Component);

  function ViewFolder() {
    _classCallCheck(this, ViewFolder);

    return _possibleConstructorReturn(this, _getPrototypeOf(ViewFolder).apply(this, arguments));
  }

  _createClass(ViewFolder, [{
    key: "render",
    value: function render() {
      var _this$props$folder = this.props.folder,
          id = _this$props$folder.id,
          about = _this$props$folder.about,
          parent = _this$props$folder.parent,
          parentName = _this$props$folder.parentName;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "animated fadeIn"
      }, react_render_html__WEBPACK_IMPORTED_MODULE_3___default()(about), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_FolderTable__WEBPACK_IMPORTED_MODULE_6__["default"], {
        folderId: id
      }));
    }
  }]);

  return ViewFolder;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(_addFolder__WEBPACK_IMPORTED_MODULE_5__["default"])(Object(_addToolbar__WEBPACK_IMPORTED_MODULE_4__["default"])(ViewFolder)));

/***/ }),

/***/ "./resources/js/components/views/Documents/addToolbar.js":
/*!***************************************************************!*\
  !*** ./resources/js/components/views/Documents/addToolbar.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




/* harmony default export */ __webpack_exports__["default"] = (function (ChildComponent) {
  var ComposedComponent =
  /*#__PURE__*/
  function (_Component) {
    _inherits(ComposedComponent, _Component);

    function ComposedComponent(props) {
      var _this;

      _classCallCheck(this, ComposedComponent);

      _this = _possibleConstructorReturn(this, _getPrototypeOf(ComposedComponent).call(this, props));

      _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleClick", function (e, action) {
        e.preventDefault();
        var folder = _this.props.folder;
        var path = '/documents';
        path += folder != undefined ? "/folder/".concat(folder.id) : "";

        switch (action) {
          case 'NEW_FILE':
            _this.props.history.push("".concat(path, "/file/new"));

            return;

          case 'EDIT_FOLDER':
            _this.props.history.push("".concat(path, "/edit"));

            return;

          default:
            _this.props.history.push("".concat(path, "/folder/new"));

            return;
        }
      });

      _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "toggle", function () {
        _this.setState({
          dropdownOpen: !_this.state.dropdownOpen
        });
      });

      _this.state = {
        dropdownOpen: false
      };
      return _this;
    }

    _createClass(ComposedComponent, [{
      key: "render",
      value: function render() {
        var _this2 = this;

        var _this$props = this.props,
            folder = _this$props.folder,
            currentView = _this$props.currentView;
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "toolbar"
        }, currentView.admin && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["ButtonDropdown"], {
          direction: "right",
          size: "lg",
          className: "mb-3",
          isOpen: this.state.dropdownOpen,
          toggle: this.toggle
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["DropdownToggle"], {
          caret: true,
          color: "light"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
          className: "fa fa-lg fa-folder-open mr-2"
        }), folder == undefined && "Home", folder != undefined && "".concat(folder.title)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["DropdownMenu"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["DropdownItem"], {
          onClick: function onClick(e) {
            return _this2.handleClick(e, "NEW_FOLDER");
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
          className: "fa fa-folder"
        }), " Add new folder"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["DropdownItem"], {
          onClick: function onClick(e) {
            return _this2.handleClick(e, "NEW_FILE");
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
          className: "fa fa-file"
        }), " Upload a file"), folder != undefined && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["DropdownItem"], {
          onClick: function onClick(e) {
            return _this2.handleClick(e, "EDIT_FOLDER");
          }
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
          className: "fa fa-edit"
        }), " Edit folder"))), !currentView.admin && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
          direction: "right",
          size: "lg",
          className: "mb-3",
          color: "light"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
          className: "fa fa-lg fa-folder-open mr-2"
        }), folder == undefined && "Home", folder != undefined && "".concat(folder.title))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ChildComponent, this.props));
      }
    }]);

    return ComposedComponent;
  }(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

  var mapStateToProps = function mapStateToProps(_ref) {
    var currentView = _ref.currentView;
    return {
      currentView: currentView
    };
  };

  return Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(ComposedComponent);
});

/***/ }),

/***/ "./resources/js/components/views/Documents/ui/FileTable.js":
/*!*****************************************************************!*\
  !*** ./resources/js/components/views/Documents/ui/FileTable.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _FileTableItem__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./FileTableItem */ "./resources/js/components/views/Documents/ui/FileTableItem.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }






var FileTable =
/*#__PURE__*/
function (_Component) {
  _inherits(FileTable, _Component);

  function FileTable() {
    _classCallCheck(this, FileTable);

    return _possibleConstructorReturn(this, _getPrototypeOf(FileTable).apply(this, arguments));
  }

  _createClass(FileTable, [{
    key: "render",
    value: function render() {
      var files = this.props.folder.files;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, files.length < 1 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "This folder is empty."), files.length > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Table"], {
        responsive: true,
        className: "documents-table"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("thead", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
        style: {
          width: '1%'
        },
        className: "pl-0 pr-0"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
        style: {
          width: '90%'
        }
      }, "Name"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
        style: {
          width: '9%'
        }
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tbody", null, files.map(function (item, idx) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_FileTableItem__WEBPACK_IMPORTED_MODULE_3__["default"], {
          key: item.id,
          itemDetails: item,
          idx: idx
        });
      }))));
    }
  }]);

  return FileTable;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref, _ref2) {
  var documents = _ref.documents;
  var folderId = _ref2.folderId;
  return {
    folder: documents.data.find(function (item) {
      return item.id === folderId;
    })
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(FileTable));

/***/ }),

/***/ "./resources/js/components/views/Documents/ui/FileTableItem.js":
/*!*********************************************************************!*\
  !*** ./resources/js/components/views/Documents/ui/FileTableItem.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






var FileTableItem =
/*#__PURE__*/
function (_Component) {
  _inherits(FileTableItem, _Component);

  function FileTableItem() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, FileTableItem);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(FileTableItem)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      isOpen: false
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "toggleCollapse", function (e) {
      e.preventDefault();
      var isOpen = _this.state.isOpen;

      _this.setState({
        isOpen: !isOpen
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleDownload", function (e) {
      e.preventDefault();
      window.location = e.currentTarget.dataset.href;
    });

    return _this;
  }

  _createClass(FileTableItem, [{
    key: "render",
    value: function render() {
      var isOpen = this.state.isOpen;
      var _this$props = this.props,
          item = _this$props.itemDetails,
          idx = _this$props.idx;
      var icon = _mimeTypes[item.mime_type] ? _mimeTypes[item.mime_type] : 'file-o';
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "pr-0 pl-0"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "fa fa-".concat(icon)
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, item.name, " (", item.size, ") ", idx === 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Badge"], {
        color: "primary"
      }, "Current"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", null, "Added ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", null, moment__WEBPACK_IMPORTED_MODULE_2___default()(item.created.date).fromNow()), " by ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("em", null, item.uploaded_by, ", ", item.location))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Button"], {
        className: "p-0 float-right",
        color: "default",
        "data-href": "/files/".concat(item.id, "/download"),
        onClick: this.handleDownload
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "fa fa-download mt-1"
      })))));
    }
  }]);

  return FileTableItem;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (FileTableItem);

/***/ }),

/***/ "./resources/js/components/views/Documents/ui/FolderItem.js":
/*!******************************************************************!*\
  !*** ./resources/js/components/views/Documents/ui/FolderItem.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _FolderItemChildren__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./FolderItemChildren */ "./resources/js/components/views/Documents/ui/FolderItemChildren.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








var FolderItem =
/*#__PURE__*/
function (_Component) {
  _inherits(FolderItem, _Component);

  function FolderItem() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, FolderItem);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(FolderItem)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      isOpen: false
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "toggleCollapse", function (e) {
      e.preventDefault();
      var isOpen = _this.state.isOpen;

      _this.setState({
        isOpen: !isOpen
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleClick", function (e) {
      e.preventDefault();
      var folderId = _this.props.folderId;

      _this.props.history.push("/documents/folder/".concat(folderId));
    });

    return _this;
  }

  _createClass(FolderItem, [{
    key: "render",
    value: function render() {
      var isOpen = this.state.isOpen;
      var caretClassNames = classnames__WEBPACK_IMPORTED_MODULE_3___default()({
        fa: true,
        'fa-lg': true,
        'fa-caret-right': !isOpen,
        'fa-caret-down': isOpen
      });
      var folderClassNames = classnames__WEBPACK_IMPORTED_MODULE_3___default()({
        fa: true,
        'fa-lg': true,
        'fa-folder': !isOpen,
        'fa-folder-open': isOpen
      });
      var children = this.props.children;
      var _this$props$folder = this.props.folder,
          title = _this$props$folder.title,
          id = _this$props$folder.id;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "folder-item pl-3"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["ButtonGroup"], null, children.length > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "p-0",
        color: "link",
        onClick: this.toggleCollapse
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        style: {
          width: '11px'
        },
        className: caretClassNames
      })), children.length < 1 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "p-0",
        color: "link"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        style: {
          width: '11px'
        }
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Button"], {
        className: "pl-1",
        color: "link",
        onClick: this.handleClick
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: folderClassNames
      }), " ", title)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Collapse"], {
        isOpen: isOpen
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_FolderItemChildren__WEBPACK_IMPORTED_MODULE_5__["default"], {
        folderId: id
      })));
    }
  }]);

  return FolderItem;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref, _ref2) {
  var documents = _ref.documents;
  var folderId = _ref2.folderId;
  return {
    folder: folderId ? documents.data.find(function (item) {
      return item.id === folderId;
    }) : null,
    children: folderId ? documents.data.filter(function (item) {
      return item.parent === folderId && item.folder;
    }) : []
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["withRouter"])(Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(FolderItem)));

/***/ }),

/***/ "./resources/js/components/views/Documents/ui/FolderItemChildren.js":
/*!**************************************************************************!*\
  !*** ./resources/js/components/views/Documents/ui/FolderItemChildren.js ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _FolderItem__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./FolderItem */ "./resources/js/components/views/Documents/ui/FolderItem.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }








var FolderItemChildren =
/*#__PURE__*/
function (_Component) {
  _inherits(FolderItemChildren, _Component);

  function FolderItemChildren() {
    _classCallCheck(this, FolderItemChildren);

    return _possibleConstructorReturn(this, _getPrototypeOf(FolderItemChildren).apply(this, arguments));
  }

  _createClass(FolderItemChildren, [{
    key: "render",
    value: function render() {
      var children = this.props.children;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "folder-item-children"
      }, children.map(function (item) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_FolderItem__WEBPACK_IMPORTED_MODULE_5__["default"], {
          key: item.id,
          folderId: item.id
        });
      }));
    }
  }]);

  return FolderItemChildren;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref, _ref2) {
  var documents = _ref.documents;
  var folderId = _ref2.folderId;
  return {
    children: documents.data.filter(function (item) {
      return item.parent === folderId && item.folder;
    })
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["withRouter"])(Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(FolderItemChildren)));

/***/ }),

/***/ "./resources/js/components/views/Documents/ui/FolderLocation.js":
/*!**********************************************************************!*\
  !*** ./resources/js/components/views/Documents/ui/FolderLocation.js ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }




var FolderLocation =
/*#__PURE__*/
function (_Component) {
  _inherits(FolderLocation, _Component);

  function FolderLocation() {
    _classCallCheck(this, FolderLocation);

    return _possibleConstructorReturn(this, _getPrototypeOf(FolderLocation).apply(this, arguments));
  }

  _createClass(FolderLocation, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          item = _this$props.item,
          documents = _this$props.documents;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("em", null, "Here"));
    }
  }]);

  return FolderLocation;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref) {
  var documents = _ref.documents;
  return {
    documents: documents.data
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps, null)(FolderLocation));

/***/ }),

/***/ "./resources/js/components/views/Documents/ui/FolderTable.js":
/*!*******************************************************************!*\
  !*** ./resources/js/components/views/Documents/ui/FolderTable.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _FolderTableItem__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./FolderTableItem */ "./resources/js/components/views/Documents/ui/FolderTableItem.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }







var FolderTable =
/*#__PURE__*/
function (_Component) {
  _inherits(FolderTable, _Component);

  function FolderTable() {
    _classCallCheck(this, FolderTable);

    return _possibleConstructorReturn(this, _getPrototypeOf(FolderTable).apply(this, arguments));
  }

  _createClass(FolderTable, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          documents = _this$props.documents,
          currentView = _this$props.currentView;
      if (!currentView.admin) documents = documents.filter(function (item) {
        return !item.draft && item.published;
      });
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, documents.length < 1 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "This folder is empty."), documents.length > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Table"], {
        responsive: true,
        className: "documents-table"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("thead", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
        style: {
          width: '1%'
        },
        className: "pl-0 pr-0"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
        style: {
          width: '90%'
        }
      }, "Name"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
        style: {
          width: '9%'
        }
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tbody", null, documents.map(function (item, idx) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_FolderTableItem__WEBPACK_IMPORTED_MODULE_4__["default"], {
          key: item.id,
          itemDetails: item,
          idx: idx
        });
      }))));
    }
  }]);

  return FolderTable;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref, _ref2) {
  var documents = _ref.documents,
      currentView = _ref.currentView;
  var folderId = _ref2.folderId;
  return {
    documents: documents.data.filter(function (item) {
      return item.parent == folderId;
    }),
    currentView: currentView
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(FolderTable));

/***/ }),

/***/ "./resources/js/components/views/Documents/ui/FolderTableItem.js":
/*!***********************************************************************!*\
  !*** ./resources/js/components/views/Documents/ui/FolderTableItem.js ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_render_html__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-render-html */ "./node_modules/react-render-html/index.js");
/* harmony import */ var react_render_html__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_render_html__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








var FolderTableItem =
/*#__PURE__*/
function (_Component) {
  _inherits(FolderTableItem, _Component);

  function FolderTableItem() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, FolderTableItem);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(FolderTableItem)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      isOpen: false,
      tooltipOpen: false,
      tooltipDownloadOpen: false
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "toggleCollapse", function (e) {
      e.preventDefault();
      var isOpen = _this.state.isOpen;

      _this.setState({
        isOpen: !isOpen
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "toggleTooltip", function (e) {
      _this.setState({
        tooltipOpen: !_this.state.tooltipOpen
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "toggleDownloadTooltip", function (e) {
      _this.setState({
        tooltipDownloadOpen: !_this.state.tooltipDownloadOpen
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleDownload", function (e) {
      e.preventDefault();
      window.location = e.currentTarget.dataset.href;
    });

    return _this;
  }

  _createClass(FolderTableItem, [{
    key: "render",
    value: function render() {
      var _this$state = this.state,
          isOpen = _this$state.isOpen,
          tooltipOpen = _this$state.tooltipOpen,
          tooltipDownloadOpen = _this$state.tooltipDownloadOpen;
      var _this$props = this.props,
          item = _this$props.itemDetails,
          idx = _this$props.idx,
          currentView = _this$props.currentView,
          search = _this$props.search;
      var icon = 'file-o';
      if (item.files[0] !== undefined && _mimeTypes[item.files[0].mime_type] !== undefined) icon = _mimeTypes[item.files[0].mime_type];
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "pr-0 pl-0"
      }, item.folder && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "fa fa-folder-o"
      }), !item.folder && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "fa fa-".concat(icon)
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, item.folder && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
        className: "documents-table-link",
        to: "/documents/folder/".concat(item.id)
      }, item.title), !item.folder && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
        className: "documents-table-link",
        to: "/documents/file/".concat(item.id)
      }, item.title), currentView.admin && item.draft && !search && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Badge"], {
        className: "ml-2",
        color: "secondary"
      }, "Draft"), currentView.admin && !item.draft && item.published && !search && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Badge"], {
        className: "ml-2",
        color: "primary"
      }, "Published"), currentView.admin && !item.draft && !item.published && !search && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Badge"], {
        className: "ml-2",
        color: "success"
      }, "Upcoming"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", null, "Added ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", null, moment__WEBPACK_IMPORTED_MODULE_3___default()(item.updated).fromNow()), " by ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("em", null, item.creator, ", ", item.location)))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, item.about.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
        className: "p-0 pl-1 float-right",
        color: "default"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "ml-2 fa fa-md fa-info-circle",
        id: "itemInfo".concat(idx),
        onClick: this.toggleCollapse
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Tooltip"], {
        placement: "bottom",
        isOpen: tooltipOpen,
        target: "itemInfo".concat(idx),
        toggle: this.toggleTooltip
      }, "Click for more details")), !item.folder && item.files[0] !== undefined && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
        className: "p-0 float-right",
        color: "default",
        "data-href": "/files/".concat(item.files[0].id, "/download"),
        onClick: this.handleDownload
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "fa fa-download mt-1",
        id: "itemDownloadTip".concat(idx)
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Tooltip"], {
        placement: "bottom",
        isOpen: tooltipDownloadOpen,
        target: "itemDownloadTip".concat(idx),
        toggle: this.toggleDownloadTooltip
      }, "Click to download")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Collapse"], {
        tag: "tr",
        isOpen: isOpen
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "animated fadeIn collapse-column",
        colSpan: "4"
      }, react_render_html__WEBPACK_IMPORTED_MODULE_4___default()(item.about), item.about.trim() === "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "There are no details."))));
    }
  }]);

  return FolderTableItem;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref, _ref2) {
  var currentView = _ref.currentView;
  var search = _ref2.search;
  return {
    currentView: currentView,
    search: search
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps)(FolderTableItem));

/***/ }),

/***/ "./resources/js/components/views/Documents/ui/FolderTableItemSearch.js":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/views/Documents/ui/FolderTableItemSearch.js ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_render_html__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-render-html */ "./node_modules/react-render-html/index.js");
/* harmony import */ var react_render_html__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_render_html__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _FolderLocation__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./FolderLocation */ "./resources/js/components/views/Documents/ui/FolderLocation.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }









var FolderTableItemSearch =
/*#__PURE__*/
function (_Component) {
  _inherits(FolderTableItemSearch, _Component);

  function FolderTableItemSearch() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, FolderTableItemSearch);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(FolderTableItemSearch)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      isOpen: false,
      tooltipOpen: false,
      tooltipDownloadOpen: false
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "toggleCollapse", function (e) {
      e.preventDefault();
      var isOpen = _this.state.isOpen;

      _this.setState({
        isOpen: !isOpen
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "toggleTooltip", function (e) {
      _this.setState({
        tooltipOpen: !_this.state.tooltipOpen
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "toggleDownloadTooltip", function (e) {
      _this.setState({
        tooltipDownloadOpen: !_this.state.tooltipDownloadOpen
      });
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleDownload", function (e) {
      e.preventDefault();
      window.location = e.currentTarget.dataset.href;
    });

    return _this;
  }

  _createClass(FolderTableItemSearch, [{
    key: "render",
    value: function render() {
      var _this$state = this.state,
          isOpen = _this$state.isOpen,
          tooltipOpen = _this$state.tooltipOpen,
          tooltipDownloadOpen = _this$state.tooltipDownloadOpen;
      var _this$props = this.props,
          item = _this$props.itemDetails,
          idx = _this$props.idx,
          currentView = _this$props.currentView,
          search = _this$props.search;
      var icon = 'file-o';
      if (item.files[0] !== undefined && _mimeTypes[item.files[0].mime_type] !== undefined) icon = _mimeTypes[item.files[0].mime_type];
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "pr-0 pl-0"
      }, item.folder && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "fa fa-folder-o"
      }), !item.folder && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "fa fa-".concat(icon)
      })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, item.folder && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
        className: "documents-table-link",
        to: "/documents/folder/".concat(item.id)
      }, item.title), !item.folder && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Link"], {
        className: "documents-table-link",
        to: "/documents/file/".concat(item.id)
      }, item.title), currentView.admin && item.draft && !search && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Badge"], {
        className: "ml-2",
        color: "secondary"
      }, "Draft"), currentView.admin && !item.draft && item.published && !search && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Badge"], {
        className: "ml-2",
        color: "primary"
      }, "Published"), currentView.admin && !item.draft && !item.published && !search && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Badge"], {
        className: "ml-2",
        color: "success"
      }, "Upcoming"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_FolderLocation__WEBPACK_IMPORTED_MODULE_6__["default"], {
        item: item
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("small", null, "Added ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("strong", null, moment__WEBPACK_IMPORTED_MODULE_3___default()(item.updated).fromNow()), " by ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("em", null, item.creator, ", ", item.location)))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", null, item.about.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
        className: "p-0 pl-1 float-right",
        color: "default"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "ml-2 fa fa-md fa-info-circle",
        id: "itemInfo".concat(idx),
        onClick: this.toggleCollapse
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Tooltip"], {
        placement: "bottom",
        isOpen: tooltipOpen,
        target: "itemInfo".concat(idx),
        toggle: this.toggleTooltip
      }, "Click for more details")), !item.folder && item.files[0] !== undefined && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Button"], {
        className: "p-0 float-right",
        color: "default",
        "data-href": "/files/".concat(item.files[0].id, "/download"),
        onClick: this.handleDownload
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "fa fa-download mt-1",
        id: "itemDownloadTip".concat(idx)
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Tooltip"], {
        placement: "bottom",
        isOpen: tooltipDownloadOpen,
        target: "itemDownloadTip".concat(idx),
        toggle: this.toggleDownloadTooltip
      }, "Click to download")))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_5__["Collapse"], {
        tag: "tr",
        isOpen: isOpen
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("td", {
        className: "animated fadeIn collapse-column",
        colSpan: "4"
      }, react_render_html__WEBPACK_IMPORTED_MODULE_4___default()(item.about), item.about.trim() === "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "There are no details."))));
    }
  }]);

  return FolderTableItemSearch;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref, _ref2) {
  var currentView = _ref.currentView;
  var search = _ref2.search;
  return {
    currentView: currentView,
    search: search
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["connect"])(mapStateToProps)(FolderTableItemSearch));

/***/ }),

/***/ "./resources/js/components/views/Documents/ui/RecentDocuments.js":
/*!***********************************************************************!*\
  !*** ./resources/js/components/views/Documents/ui/RecentDocuments.js ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _FolderTableItem__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./FolderTableItem */ "./resources/js/components/views/Documents/ui/FolderTableItem.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }







var RecentDocuments =
/*#__PURE__*/
function (_Component) {
  _inherits(RecentDocuments, _Component);

  function RecentDocuments() {
    _classCallCheck(this, RecentDocuments);

    return _possibleConstructorReturn(this, _getPrototypeOf(RecentDocuments).apply(this, arguments));
  }

  _createClass(RecentDocuments, [{
    key: "render",
    value: function render() {
      var documents = this.props.documents;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, documents.length < 1 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "There are no files published recently."), documents.length > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_3__["Table"], {
        responsive: true,
        className: "documents-table"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("thead", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
        style: {
          width: '1%'
        },
        className: "pl-0 pr-0"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
        style: {
          width: '90%'
        }
      }, "Name"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
        style: {
          width: '9%'
        }
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tbody", null, documents.map(function (item, idx) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_FolderTableItem__WEBPACK_IMPORTED_MODULE_4__["default"], {
          key: item.id,
          itemDetails: item,
          idx: idx
        });
      }))));
    }
  }]);

  return RecentDocuments;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref) {
  var documents = _ref.documents;
  return {
    documents: documents.data.filter(function (item) {
      return moment__WEBPACK_IMPORTED_MODULE_2___default()(item.updated).isSame(new Date(), 'week') && !item.folder && item.published && !item.draft;
    })
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(RecentDocuments));

/***/ }),

/***/ "./resources/js/components/views/Documents/ui/SearchResults.js":
/*!*********************************************************************!*\
  !*** ./resources/js/components/views/Documents/ui/SearchResults.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! fuse.js */ "./node_modules/fuse.js/dist/fuse.js");
/* harmony import */ var fuse_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(fuse_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _FolderTableItemSearch__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./FolderTableItemSearch */ "./resources/js/components/views/Documents/ui/FolderTableItemSearch.js");
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }








var SearchResults =
/*#__PURE__*/
function (_Component) {
  _inherits(SearchResults, _Component);

  function SearchResults() {
    _classCallCheck(this, SearchResults);

    return _possibleConstructorReturn(this, _getPrototypeOf(SearchResults).apply(this, arguments));
  }

  _createClass(SearchResults, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          documents = _this$props.documents,
          currentView = _this$props.currentView,
          searchTerms = _this$props.searchTerms;
      if (!currentView.admin) documents = documents.filter(function (item) {
        return !item.draft && item.published;
      });

      if (searchTerms.trim() !== "") {
        var searchOptions = _objectSpread({}, _globalFuseOptions, {
          keys: _globalFuseKeys['documents']
        });

        var fuse = new fuse_js__WEBPACK_IMPORTED_MODULE_2___default.a(documents, searchOptions);
        documents = fuse.search(searchTerms.trim());
      }

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, documents.length < 1 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", null, "No documents found."), documents.length > 0 && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Table"], {
        responsive: true,
        className: "documents-table"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("thead", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tr", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
        style: {
          width: '1%'
        },
        className: "pl-0 pr-0"
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
        style: {
          width: '90%'
        }
      }, "Name"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("th", {
        style: {
          width: '9%'
        }
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("tbody", null, documents.map(function (item, idx) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_FolderTableItemSearch__WEBPACK_IMPORTED_MODULE_5__["default"], {
          key: item.id,
          itemDetails: item,
          idx: idx
        });
      }))));
    }
  }]);

  return SearchResults;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

var mapStateToProps = function mapStateToProps(_ref) {
  var documents = _ref.documents,
      currentView = _ref.currentView;
  return {
    documents: documents.data,
    currentView: currentView
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(mapStateToProps)(SearchResults));

/***/ })

}]);