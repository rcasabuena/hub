(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[55],{

/***/ "./resources/js/components/views/Locations/EditLocation.js":
/*!*****************************************************************!*\
  !*** ./resources/js/components/views/Locations/EditLocation.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _LocationForm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./LocationForm */ "./resources/js/components/views/Locations/LocationForm.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var _ui_PageHeader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../ui/PageHeader */ "./resources/js/components/ui/PageHeader.js");





var EditLocation = function EditLocation() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "animated fadeIn"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Row"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Card"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["CardBody"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_ui_PageHeader__WEBPACK_IMPORTED_MODULE_3__["default"], {
    pageTitle: "Edit Location",
    pageInfo: "\xA0"
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_LocationForm__WEBPACK_IMPORTED_MODULE_1__["default"], null))))));
};

/* harmony default export */ __webpack_exports__["default"] = (EditLocation);

/***/ })

}]);