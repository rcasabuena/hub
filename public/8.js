(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[8],{

/***/ "./resources/js/components/ui/Form/FileInputMulti.js":
/*!***********************************************************!*\
  !*** ./resources/js/components/ui/Form/FileInputMulti.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");



var FileInputMulti = function FileInputMulti(props) {
  var inputName = props.inputName,
      inputDefaultValue = props.inputDefaultValue,
      inputLabel = props.inputLabel,
      handleUpload = props.handleUpload,
      inputHelpText = props.inputHelpText,
      inputError = props.inputError,
      isRequired = props.isRequired,
      horizontal = props.horizontal;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormGroup"], {
    row: true,
    className: "mb-3"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    md: {
      size: !!!horizontal ? 3 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Label"], {
    htmlFor: inputName
  }, inputLabel)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    md: {
      size: !!!horizontal ? 9 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    required: isRequired ? true : false,
    type: "file",
    id: inputName,
    onChange: function onChange(e) {
      return handleUpload(e, inputName);
    },
    multiple: true
  }), inputHelpText && inputHelpText.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormText"], {
    color: "muted"
  }, inputHelpText), inputDefaultValue && inputDefaultValue.trim() != "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: inputDefaultValue,
    style: {
      height: '150px'
    }
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (FileInputMulti);

/***/ }),

/***/ "./resources/js/components/ui/Form/FormSubmitActions.js":
/*!**************************************************************!*\
  !*** ./resources/js/components/ui/Form/FormSubmitActions.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");



var FormSubmitActions = function FormSubmitActions(props) {
  var id = props.id,
      handleDelete = props.handleDelete;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormGroup"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("hr", null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    type: "submit",
    size: "sm",
    color: "primary",
    className: "mr-2"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-dot-circle-o"
  }), " Submit"), id.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    size: "sm",
    color: "danger",
    onClick: function onClick() {
      return handleDelete(id);
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
    className: "fa fa-ban"
  }), " Delete"));
};

/* harmony default export */ __webpack_exports__["default"] = (FormSubmitActions);

/***/ }),

/***/ "./resources/js/components/ui/Form/SelectInput.js":
/*!********************************************************!*\
  !*** ./resources/js/components/ui/Form/SelectInput.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_select__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-select */ "./node_modules/react-select/dist/react-select.esm.js");
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");




var SelectInput = function SelectInput(props) {
  var inputName = props.inputName,
      inputLabel = props.inputLabel,
      selectValue = props.selectValue,
      selectOptions = props.selectOptions,
      handleSelect = props.handleSelect,
      inputHelpText = props.inputHelpText,
      horizontal = props.horizontal;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["FormGroup"], {
    row: true,
    className: "mb-3"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    md: {
      size: !!!horizontal ? 3 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Label"], {
    htmlFor: inputName
  }, inputLabel)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["Col"], {
    md: {
      size: !!!horizontal ? 9 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_select__WEBPACK_IMPORTED_MODULE_1__["default"], {
    options: selectOptions,
    defaultValue: selectValue,
    name: inputName,
    components: {
      DropdownIndicator: null
    },
    isClearable: true,
    onChange: handleSelect
  }), inputHelpText && inputHelpText.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_2__["FormText"], {
    color: "muted"
  }, inputHelpText)));
};

/* harmony default export */ __webpack_exports__["default"] = (SelectInput);

/***/ }),

/***/ "./resources/js/components/ui/Form/TextInput.js":
/*!******************************************************!*\
  !*** ./resources/js/components/ui/Form/TextInput.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");



var TextInput = function TextInput(props) {
  var inputName = props.inputName,
      inputDefaultValue = props.inputDefaultValue,
      inputLabel = props.inputLabel,
      handleChange = props.handleChange,
      inputHelpText = props.inputHelpText,
      inputError = props.inputError,
      isRequired = props.isRequired,
      inputType = props.inputType,
      horizontal = props.horizontal;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormGroup"], {
    row: true,
    className: "mb-3"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    md: {
      size: !!!horizontal ? 3 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Label"], {
    htmlFor: inputName
  }, inputLabel)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Col"], {
    md: {
      size: !!!horizontal ? 9 : 12
    }
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["Input"], {
    type: inputType ? inputType : 'text',
    id: inputName,
    required: isRequired ? true : false,
    onChange: handleChange,
    defaultValue: inputDefaultValue
  }), inputHelpText && inputHelpText.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_1__["FormText"], {
    color: "muted"
  }, inputHelpText)));
};

/* harmony default export */ __webpack_exports__["default"] = (TextInput);

/***/ }),

/***/ "./resources/js/components/ui/Form/WysiwygInput.js":
/*!*********************************************************!*\
  !*** ./resources/js/components/ui/Form/WysiwygInput.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");
/* harmony import */ var draft_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(draft_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var draftjs_to_html__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! draftjs-to-html */ "./node_modules/draftjs-to-html/lib/draftjs-to-html.js");
/* harmony import */ var draftjs_to_html__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(draftjs_to_html__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_draft_wysiwyg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-draft-wysiwyg */ "./node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.js");
/* harmony import */ var react_draft_wysiwyg__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_draft_wysiwyg__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var reactstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! reactstrap */ "./node_modules/reactstrap/es/index.js");
/* harmony import */ var react_draft_wysiwyg_dist_react_draft_wysiwyg_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-draft-wysiwyg/dist/react-draft-wysiwyg.css */ "./node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css");
/* harmony import */ var react_draft_wysiwyg_dist_react_draft_wysiwyg_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_draft_wysiwyg_dist_react_draft_wysiwyg_css__WEBPACK_IMPORTED_MODULE_5__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }








var WysiwygInput =
/*#__PURE__*/
function (_Component) {
  _inherits(WysiwygInput, _Component);

  function WysiwygInput() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, WysiwygInput);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(WysiwygInput)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "state", {
      editorState: draft_js__WEBPACK_IMPORTED_MODULE_1__["EditorState"].createEmpty()
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "handleThisEditorChange", function (editorState) {
      var _this$props = _this.props,
          handleEditorChange = _this$props.handleEditorChange,
          inputName = _this$props.inputName;

      _this.setState({
        editorState: editorState
      });

      handleEditorChange(draftjs_to_html__WEBPACK_IMPORTED_MODULE_2___default()(Object(draft_js__WEBPACK_IMPORTED_MODULE_1__["convertToRaw"])(editorState.getCurrentContent())), inputName);
    });

    _defineProperty(_assertThisInitialized(_assertThisInitialized(_this)), "componentDidMount", function () {
      var body = _this.props.body;

      if (body && body.trim() != "" && Object(draft_js__WEBPACK_IMPORTED_MODULE_1__["convertFromHTML"])(body).contentBlocks !== null) {
        _this.setState({
          editorState: draft_js__WEBPACK_IMPORTED_MODULE_1__["EditorState"].createWithContent(draft_js__WEBPACK_IMPORTED_MODULE_1__["ContentState"].createFromBlockArray(Object(draft_js__WEBPACK_IMPORTED_MODULE_1__["convertFromHTML"])(body)))
        });
      }
    });

    return _this;
  }

  _createClass(WysiwygInput, [{
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          inputLabel = _this$props2.inputLabel,
          inputName = _this$props2.inputName,
          inputHelpText = _this$props2.inputHelpText,
          body = _this$props2.body,
          handleEditorChange = _this$props2.handleEditorChange,
          horizontal = _this$props2.horizontal;
      var editorState = this.state.editorState;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["FormGroup"], {
        row: true,
        className: "mb-3"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Col"], {
        md: {
          size: !!!horizontal ? 3 : 12
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Label"], {
        htmlFor: inputName
      }, inputLabel)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["Col"], {
        md: {
          size: !!!horizontal ? 9 : 12
        }
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_draft_wysiwyg__WEBPACK_IMPORTED_MODULE_3__["Editor"], {
        editorState: editorState,
        editorClassName: "form-control",
        onEditorStateChange: this.handleThisEditorChange
      }), inputHelpText && inputHelpText.trim() !== "" && react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(reactstrap__WEBPACK_IMPORTED_MODULE_4__["FormText"], {
        color: "muted"
      }, inputHelpText)));
    }
  }]);

  return WysiwygInput;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (WysiwygInput);

/***/ }),

/***/ "./resources/js/functions/FormFunctions.js":
/*!*************************************************!*\
  !*** ./resources/js/functions/FormFunctions.js ***!
  \*************************************************/
/*! exports provided: handleSubmit, handleSelect, handleMultiSelect, handleChange, handleToggle, handleUpload, handleUploadMulti, handleEditorChange, handleDelete */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleSubmit", function() { return handleSubmit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleSelect", function() { return handleSelect; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleMultiSelect", function() { return handleMultiSelect; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleChange", function() { return handleChange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleToggle", function() { return handleToggle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleUpload", function() { return handleUpload; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleUploadMulti", function() { return handleUploadMulti; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleEditorChange", function() { return handleEditorChange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleDelete", function() { return handleDelete; });
function handleSubmit(e) {
  e.preventDefault();
  var item = this.state.item;
  var history = this.props.history;
  this.props.save(item.formData, history);
  e.currentTarget.reset();
}
function handleSelect(res, meta) {
  var item = this.state.item;
  item[meta.name] = res ? res.value : "";
  this.setState({
    item: item
  });
}
function handleMultiSelect(res, meta) {
  var item = this.state.item;
  item[meta.name] = [];

  if (res.length > 0) {
    res.forEach(function (i) {
      item[meta.name].push(i.value);
    });
  }

  this.setState({
    item: item
  });
}
function handleChange(e) {
  var item = this.state.item;
  item[e.target.id] = e.target.value;
  this.setState({
    item: item
  });
}
function handleToggle(e) {
  var inverse = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var item = this.state.item;
  item[e.target.id] = inverse ? !e.target.checked : e.target.checked;
  this.setState({
    item: item
  });
}
function handleUpload(e, name) {
  var item = this.state.item;
  item[name] = e.target.files[0];
  e.preventDefault();
  this.setState({
    item: item
  });
}
function handleUploadMulti(e, name) {
  var item = this.state.item;
  Array.from(e.target.files).forEach(function (fileItem, idx) {
    item[name][idx] = fileItem;
  });
  e.preventDefault();
  this.setState({
    item: item
  });
}
function handleEditorChange(data, name) {
  var item = this.state.item;
  item[name] = data;
  this.setState({
    item: item
  });
}
function handleDelete(id) {
  this.props.delete(id);
}

/***/ }),

/***/ "./resources/js/objects/AppItem.js":
/*!*****************************************!*\
  !*** ./resources/js/objects/AppItem.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AppItem; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var AppItem =
/*#__PURE__*/
function () {
  function AppItem(id) {
    _classCallCheck(this, AppItem);

    this.id = id;
  }

  _createClass(AppItem, [{
    key: "formData",
    get: function get() {
      var _this = this;

      var data = new FormData();
      Object.keys(this).forEach(function (key) {
        if (key === 'attachments') {
          _this[key].forEach(function (file) {
            return data.append('attachments[]', file);
          });
        } else {
          data.append(key, _this[key]);
        }
      });
      return data;
    }
  }]);

  return AppItem;
}();



/***/ })

}]);