<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUniqueId;

class UserGroup extends Model
{
    use HasUniqueId;
}
