<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewsPublished extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $news;
    public $user;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($news, $user)
    {
        $this->news = $news;
        $this->user = $user;
        $this->subject = "Hub update! See what's changed...";
        //$this->subject = "News Article:  " . $news->title . " has been published!";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->to($this->user->email)
            ->subject($this->subject)
            ->view('emails.published-news');
    }
}
