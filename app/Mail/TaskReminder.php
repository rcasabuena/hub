<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
use App\Task;
use App\User;

class TaskReminder extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $todo;
    public $user;
    public $task;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($todo)
    {
        $this->todo = $todo;
        $this->user = User::find($todo->user_id);
        $this->task = Task::find($todo->task_id);
        $this->subject = "Hub update! See what's changed...";
        // $this->subject = "Your task \"" . $this->task->title . "\" ";
        // $this->subject .= Carbon::today() == Carbon::parse($this->todo->deadline) ? "is due today." : "was due " . Carbon::parse($this->todo->deadline)->diffForHumans() . ".";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->to($this->user->email)
            ->subject($this->subject)
            ->view('emails.task-reminder');
    }
}
