<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Task;

class TaskAssigned extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $todo;
    public $task;
    public $assignee;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($todo, $status)
    {
        $this->todo = $todo;
        $this->assignee = User::find($todo->user_id);
        $this->task = Task::find($todo->task_id);
        $this->subject = "Hub update! See what's changed...";
        // switch ($status) {
        //     case 'create':
        //         $this->subject = "You have a new task!";
        //         break;

        //     case 'update':
        //         $this->subject = "Your task has been updated!";
        //         break;
            
        //     default:
        //         $this->subject = "";
        //         break;
        // }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)
                ->view('emails.task-assigned');
    }
}
