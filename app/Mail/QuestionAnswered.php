<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class QuestionAnswered extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $question;
    public $answer;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($question, $answer, $user)
    {
        $this->subject = "Hub update! See what's changed...";
        $this->question = $question;
        $this->answer = $answer;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->to($this->user->email)
            ->subject($this->subject)
            ->view('emails.question-answered');
    }
}
