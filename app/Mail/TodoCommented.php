<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TodoCommented extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $task;
    public $todo;
    public $comment;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($task, $todo, $comment, $user)
    {
        //$this->subject = "Assigned Task Comment";
        $this->subject = "Hub update! See what's changed...";
        $this->todo = $todo;
        $this->todo = $todo;
        $this->comment = $comment;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject($this->subject)
            ->view('emails.todo-commented');
    }
}
