<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DocumentUpdated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $document;
    public $user;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($document, $user)
    {
        $this->document = $document;
        $this->user = $user;
        $this->subject = "Hub update! See what's changed...";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->to($this->user->email)
            ->subject($this->subject)
            ->view('emails.document-updated');
    }
}
