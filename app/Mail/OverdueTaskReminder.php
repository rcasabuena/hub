<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
use App\Task;
use App\User;

class OverdueTaskReminder extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $todo;
    public $user;
    public $task;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($todo)
    {
        $this->todo = $todo;
        $this->user = User::find($todo->user_id);
        $this->task = Task::find($todo->task_id);
        $this->subject = "Hub update! See what's changed...";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->to($this->user->email)
            ->subject($this->subject)
            ->view('emails.overdue-task-reminder');
    }
}
