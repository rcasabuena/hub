<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Task;

class TaskRejected extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $todo;
    public $task;
    public $assignee;
    public $assigner;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($todo)
    {
        $this->todo = $todo;
        $this->assignee = User::find($todo->user_id);
        $this->assigner = User::find($todo->added_by);
        $this->task = Task::find($todo->task_id);
        $this->subject = "Hub update! See what's changed...";
        //$this->subject = "Task has been rejected by " . $this->assignee->name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                ->to($this->assigner->email)
                ->subject($this->subject)
                ->view('emails.task-rejected');
    }
}
