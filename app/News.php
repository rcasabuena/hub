<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUniqueId;
use Carbon\Carbon;
use App\Mail\NewsUpdated;
use App\Mail\NewsPublished;
use App\User;

class News extends Model
{
    use HasUniqueId;
    

    protected $primaryKey = 'unique_id';
    public $incrementing = false;

    protected static function boot()
    {
    	parent::boot();
        static::created(function ($news) {
            
            if (!$news->draft && Carbon::parse($news->publish)->isToday())
            {
                if ($users = User::all())
                {
                    foreach ($users as $user) 
                    {
                        \Mail::send(
                            new NewsPublished($news, $user)
                        );
                    }
                }
            }
        });
    	static::updated(function ($news) {
    		
            if (!$news->draft && (Carbon::parse($news->publish)->isPast() || Carbon::parse($news->publish)->isToday()))
            {
                if ($users = User::all())
                {
                    foreach ($users as $user) 
                    {
                        \Mail::send(
                            new NewsUpdated($news, $user)
                        );
                    }
                }
            }
    	});
    }

    
    public function publisher()
    {
        return $this->hasOne('App\User', 'id', 'author');
    }


    public function authorName()
    {
        return $this->publisher ? $this->publisher->name : 'Head Office';
    }

    public function location()
    {
        return $this->publisher && $this->publisher->location ? $this->publisher->location->name : 'Head Office';
    }

    public function isPublished()
    {
        return Carbon::parse($this->publish) < Carbon::tomorrow();
    }
    

    public function scopePublished($query)
    {
    	return $query->where('draft', false);
    }
}
