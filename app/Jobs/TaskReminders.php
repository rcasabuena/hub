<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\TaskReminder;
use Carbon\Carbon;
use App\Todo;

class TaskReminders implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $overdueTodos;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->overdueTodos = Todo::where('completed', false)->where('rejected', false)->where('deadline', '<=', Carbon::today())->get();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (count($this->overdueTodos) > 0)
        {
            foreach ($this->overdueTodos as $todo) 
            {
                \Mail::send(
                    new TaskReminder($todo)
                );
            }
        }

        return true;
    }
}
