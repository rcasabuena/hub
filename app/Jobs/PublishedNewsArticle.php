<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\NewsPublished;
use Carbon\Carbon;
use App\User;
use App\News;

class PublishedNewsArticle implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $users;
    protected $news;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->users = User::all();
        $this->news = News::where('publish', Carbon::today())->get();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (count($this->news) > 0 && count($this->users) > 0) {
            foreach ($this->news as $news) 
            {
                foreach ($this->users as $user) 
                {
                    \Mail::to($user->email)->send(
                        new NewsPublished($news, $user)
                    );
                }
            }
        }
        return true;
    }
}
