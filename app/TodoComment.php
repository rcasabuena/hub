<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUniqueId;
use App\Mail\TodoCommented;
use App\User;
use App\Task;
use App\Todo;

class TodoComment extends Model
{
    use HasUniqueId;

    protected static function boot()
    {
    	parent::boot();
    	static::created(function ($comment) {
    		$todo = Todo::find($comment->todo_id);
    		$users[] = $todo->author;

            $task = Task::find($todo->task_id);
            $users[] = $task->author;

            foreach ($todo->comments as $com) 
            {
            	$users[] = $com->author;
            }

            foreach (array_unique($users) as $id) 
            {
               	if ($user = User::find($id))
               	{
               		\Mail::to($user->email)->send(
	                    new TodoCommented($task, $todo, $comment, $user)
	                );
               	}
            }
    	});
    }
}
