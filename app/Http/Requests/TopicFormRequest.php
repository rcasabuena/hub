<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Topic;

class TopicFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() ? true: false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function persist(Topic $topic) 
    {
        $topic->unique_id = $topic->generateUniqueId();
        $topic->title = trim($this->title);
        $topic->slug = trim(str_slug($this->title));
        $topic->body = trim($this->body);
        $topic->author = $this->user()->id;

        $topic->save();

        return $topic;
    }
}
