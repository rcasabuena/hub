<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
use App\Event;

class EventFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() ? true: false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required'
        ];
    }

    public function persist(Event $event) 
    {
        $event->unique_id = $event->generateUniqueId();
        $event->slug = str_slug(trim($this->title), '-');
        $event->title = trim($this->title);
        $event->body = trim($this->body);
        $event->venue = trim($this->venue);
        $event->author_id = $this->user()->id;

        $event->start_date = $this->start_date != "null" ? Carbon::parse($this->start_date) : Carbon::today();
        $event->end_date = $this->end_date != "null" ? Carbon::parse($this->end_date) : Carbon::today();

        $event->save();

        return $event;
    }
}
