<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
use App\Todo;
use App\User;
use App\UserGroup;
use App\Task;

class TodoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() ? true: false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'users' => 'required',
            'deadline' => 'required',
            'task_id' => 'required'
        ];
    }

    public function persist() {
        $allUsers = explode(',', $this->users);
        $users = [];

        if ($allUsers) 
        {
            foreach ($allUsers as $id) 
            {
                if ($group = UserGroup::where('unique_id', $id)->first())
                {
                    $users = array_merge($users, explode(',', $group->content));
                } else {
                    array_push($users, $id);
                } 
            }
        }

        $users = array_unique($users);
        $task = Task::where('unique_id', $this->task_id)->first();

        if ($users) {
            foreach ($users as $id) 
            {
                $user = User::where('unique_id', $id)->first();

                if ($user && $task) {
                    // $todo = Todo::updateOrCreate([
                    //     'task_id' => $task->id,
                    //     'user_id' => $user->id
                    // ],[
                    //     'deadline' => trim($this->deadline) != "null" ? Carbon::parse($this->deadline) : Carbon::today(),
                    //     'added_by' => $this->user()->id,
                    //     'completed' => 0,
                    //     'completed_at' => null,
                    //     'rejected_at' => null,
                    //     'rejected' => 0
                    // ]);

                    $todo = new Todo;

                    $todo->unique_id = $todo->generateUniqueId();
                    $todo->task_id = $task->id;
                    $todo->user_id = $user->id;
                    $todo->deadline = trim($this->deadline) != "null" ? Carbon::parse($this->deadline) : Carbon::today();
                    $todo->added_by = $this->user()->id;
                    $todo->completed = false;
                    $todo->completed_at = null;
                    $todo->rejected = false;
                    $todo->rejected_at = null;

                    if ($this->user()->director && $this->user()->location_id) 
                    {
                        $todo->location_id = $this->user()->location_id;
                    }

                    $todo->save();
                }
            }
        } 

        return $task;
    }
}
