<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
use App\Question;
use App\Topic;
use App\Attachment;

class QuestionFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() ? true: false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required'
        ];
    }

    public function persist(Question $question) 
    {
        $topic = $this->topic_id ? Topic::where('unique_id', $this->topic_id)->first() : null;

        $question->unique_id = $question->generateUniqueId();
        $question->slug = str_slug($this->title, '-');
        $question->title = trim($this->title);
        $question->body = trim($this->body);
        $question->sticky = filter_var($this->sticky, FILTER_VALIDATE_BOOLEAN);
        $question->author = $this->user()->id;
        $question->topic_id = $topic ? $topic->id : null;

        $question->save();

        if ($attachments = $this->file('attachments'))
        {
            $storage = \Storage::disk('s3');

            foreach ($attachments as $file) 
            {
                if ($file) {
                    $hashed_name = md5(uniqid());
                    $extension = $file->getClientOriginalExtension();
                    //$path = md5(\Hyn\Tenancy\Facades\TenancyFacade::website()->uuid) . "/attachments/{$hashed_name}.{$extension}";
                    $path = md5(env('AWS_FOLDER_ID')) . "/attachments/{$hashed_name}.{$extension}";

                    if ($storage->put($path, file_get_contents($file), 'private'))
                    {
                        $attachment = new Attachment();
                        $attachment->unique_id = $hashed_name;
                        $attachment->parent_id = $question->unique_id;
                        $attachment->mime_type = $file->getMimeType();
                        $attachment->name = $file->getClientOriginalName();
                        $attachment->path = $path;
                        $attachment->size = $storage->size($path);
                        $attachment->uploaded_by = $this->user()->id;

                        $attachment->save();
                    }
                }
            }
        }

        return $question;
    }
}
