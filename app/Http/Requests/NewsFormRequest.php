<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
use App\News;

class NewsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() ? true: false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'headline' => 'required',
            'body' => 'required'
        ];
    }

    public function persist(News $news) 
    {
        $news->unique_id = $news->generateUniqueId();
        $news->slug = str_slug($this->title, '-');
        $news->title = trim($this->title);
        $news->headline = trim($this->headline);
        $news->body = trim($this->body);
        $news->about = trim($this->about);
        $news->keywords = trim($this->keywords);
        $news->author = $this->user()->id;
        $news->draft = filter_var($this->draft, FILTER_VALIDATE_BOOLEAN);

        $news->publish = trim($this->publish) != "null" ? Carbon::parse($this->publish) : Carbon::today();
        $news->expire = trim($this->expire) != "null" ? Carbon::parse($this->expire) : null;

        if ($image = $this->file('featured_image'))
        {                
            $storage = \Storage::disk('s3');
            $name = uniqid(rand(), false);
            $extension = $image->getClientOriginalExtension();
            $file_name = str_slug(str_replace('.' . $image->getClientOriginalExtension(), '', $image->getClientOriginalName()), '-');
            $mimetype = $image->getMimeType();
            //$path = md5(\Hyn\Tenancy\Facades\TenancyFacade::website()->uuid) . "/news/feature/{$news->unique_id}/{$file_name}.{$extension}";
            $path = md5(env('AWS_FOLDER_ID')) . "/news/feature/{$news->unique_id}/{$file_name}.{$extension}";

            if ($storage->put($path, file_get_contents($image), 'public'))
            {
                $news->featured_image_url = $storage->url($path);
            }
        }

        $news->save();

        return $news;
    }
}
