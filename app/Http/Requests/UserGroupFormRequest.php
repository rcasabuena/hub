<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\UserGroup;

class UserGroupFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() ? true: false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }

    public function persist(UserGroup $group) 
    {
        $group->unique_id = $group->generateUniqueId();
        $group->name = trim($this->name);

        $group->content = $this->users;
        $group->creator = $this->user()->id;

        $group->save();

        return $group;
    }
}
