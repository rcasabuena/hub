<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Task;
use App\Attachment;

class TaskFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() ? true: false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required'
        ];
    }

    public function persist(Task $task) 
    {
        $task->unique_id = $task->generateUniqueId();
        $task->slug = str_slug($this->title, '-');
        $task->title = trim($this->title);
        $task->body = trim($this->body);
        $task->repeat = trim(strtoupper($this->repeat));
        $task->author_id = $this->user()->id;

        if ($this->user()->director && $this->user()->location_id) 
        {
            $task->location_id = $this->user()->location_id;
        }

        $task->save();

        if ($attachments = $this->file('attachments'))
        {
            $storage = \Storage::disk('s3');

            foreach ($attachments as $file) 
            {
                if ($file) {
                    $hashed_name = md5(uniqid());
                    $extension = $file->getClientOriginalExtension();
                    //$path = md5(\Hyn\Tenancy\Facades\TenancyFacade::website()->uuid) . "/attachments/{$hashed_name}.{$extension}";
                    $path = md5(env('AWS_FOLDER_ID')) . "/attachments/{$hashed_name}.{$extension}";

                    if ($storage->put($path, file_get_contents($file), 'private'))
                    {
                        $attachment = new Attachment();
                        $attachment->unique_id = $hashed_name;
                        $attachment->parent_id = $task->unique_id;
                        $attachment->mime_type = $file->getMimeType();
                        $attachment->name = $file->getClientOriginalName();
                        $attachment->path = $path;
                        $attachment->size = $storage->size($path);
                        $attachment->uploaded_by = $this->user()->id;

                        $attachment->save();
                    }
                }
            }
        }

        return $task;
    }

}
