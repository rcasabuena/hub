<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
use App\DocumentFile;
use App\Document;

class DocumentFileFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() ? true: false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function persist(DocumentFile $file) 
    {
        $document = Document::where('unique_id', $this->document_id)->firstOrFail();

        $file->unique_id = $file->generateUniqueId();
        $file->name = trim($this->name);
        $file->version = trim($this->version);
        $file->size = trim($this->size);
        $file->mime_type = trim($this->mime_type);
        $file->author = $this->user()->id;
        $file->document_id = $document->id;
        $file->latest = filter_var($this->latest, FILTER_VALIDATE_BOOLEAN);

        $file->publish = $this->publish ? Carbon::parse($this->publish) : Carbon::today();
        $file->draft = false;

        if ($doc = $this->file('file'))
        {                
            $storage = \Storage::disk('s3');
            $name = uniqid(rand(), false);
            $extension = $doc->getClientOriginalExtension();
            $file_name = str_slug(str_replace('.' . $doc->getClientOriginalExtension(), '', $doc->getClientOriginalName()), '-');
            $mimetype = $doc->getMimeType();
            //$path = md5(\Hyn\Tenancy\Facades\TenancyFacade::website()->uuid) . "/documents/{$document->unique_id}/{$file->unique_id}.{$extension}";
            $path = md5(env('AWS_FOLDER_ID')) . "/documents/{$document->unique_id}/{$file->unique_id}.{$extension}";

            if ($storage->put($path, file_get_contents($doc), 'private'))
            {
                $file->path = $path;
                $file->extension = $extension;
            }
        }

        $file->save();

        return $file;
    }
}
