<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Location;

class LocationFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() ? true: false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }

    public function persist(Location $location) 
    {
        $location->unique_id = $location->generateUniqueId();
        $location->slug = str_slug($this->name, '-');
        $location->name = trim($this->name);
        $location->telephone = trim($this->telephone);
        $location->email = trim(strtolower($this->email));
        $location->address_line1 = trim($this->address_line1);
        $location->address_line2 = trim($this->address_line2);
        $location->address_city = trim($this->address_city);
        $location->address_county = trim($this->address_county);
        $location->address_postcode = trim($this->address_postcode);

        $location->save();

        return $location;
    }
}
