<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
use App\User;
use App\Location;

class UserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() ? true: false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required'
        ];
    }

    public function persist(User $user) 
    {
        $location = $this->location_id ? Location::where('unique_id', $this->location_id)->first() : null;

        $user->unique_id = $user->generateUniqueId();
        $user->name = trim($this->name);
        $user->email = trim(strtolower($this->email));
        $user->admin = filter_var($this->admin, FILTER_VALIDATE_BOOLEAN);
        $user->location_id = $location ? $location->id : null;
        $user->director = filter_var($this->director, FILTER_VALIDATE_BOOLEAN);

        if (!$user->password) $user->password = password_hash("@Denfield123!", PASSWORD_BCRYPT);
        if ($this->password) $user->password = password_hash(trim($this->password), PASSWORD_BCRYPT);

        $user->save();

        return $user;
    }
}
