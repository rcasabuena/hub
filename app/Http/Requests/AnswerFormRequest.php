<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Answer;
use App\Question;
use App\Attachment;

class AnswerFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() ? true: false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'body' => 'required'
        ];
    }

    public function persist(Answer $answer) 
    {
        $question = Question::where('unique_id', $this->question_id)->firstOrFail();

        $answer->unique_id = $answer->generateUniqueId();
        $answer->body = trim($this->body);
        $answer->author = $this->user()->id;
        $answer->question_id = $question->id;

        $answer->save();

        if ($attachments = $this->file('attachments'))
        {
            $storage = \Storage::disk('s3');

            foreach ($attachments as $file) 
            {
                if ($file) {
                    $hashed_name = md5(uniqid());
                    $extension = $file->getClientOriginalExtension();
                    //$path = md5(\Hyn\Tenancy\Facades\TenancyFacade::website()->uuid) . "/attachments/{$hashed_name}.{$extension}";
                    $path = md5(env('AWS_FOLDER_ID')) . "/attachments/{$hashed_name}.{$extension}";

                    if ($storage->put($path, file_get_contents($file), 'private'))
                    {
                        $attachment = new Attachment();
                        $attachment->unique_id = $hashed_name;
                        $attachment->parent_id = $answer->unique_id;
                        $attachment->mime_type = $file->getMimeType();
                        $attachment->name = $file->getClientOriginalName();
                        $attachment->path = $path;
                        $attachment->size = $storage->size($path);
                        $attachment->uploaded_by = $this->user()->id;

                        $attachment->save();
                    }
                }
            }
        }

        return $answer;
    }
}
