<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
use App\Todo;
use App\User;
use App\Task;

class SimpleTodoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() ? true: false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function persist() {

        $user = User::where('unique_id', $this->user_id)->firstOrFail();
        $task = Task::where('unique_id', $this->task_id)->firstOrFail();
        $todo = Todo::where('unique_id', $this->id)->firstOrFail();

        $todo->completed = filter_var($this->completed, FILTER_VALIDATE_BOOLEAN);
        $todo->accepted = filter_var($this->accepted, FILTER_VALIDATE_BOOLEAN);
        $todo->rejected = filter_var($this->rejected, FILTER_VALIDATE_BOOLEAN);

        if ($todo->completed) $todo->completed_at = Carbon::now();
        if ($todo->accepted) $todo->accepted_at = Carbon::now();
        if ($todo->rejected) $todo->rejected_at = Carbon::now();

        $todo->save();

        return $todo;
    }
}
