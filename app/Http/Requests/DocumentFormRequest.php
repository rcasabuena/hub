<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
use App\Document;
use App\DocumentFile;

class DocumentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() ? true: false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required'
        ];
    }

    public function persist(Document $document) 
    {
        $parent = Document::where('unique_id', $this->parent)->first();

        $document->unique_id = $document->generateUniqueId();
        $document->title = trim($this->title);
        $document->about = trim($this->about);
        $document->draft = filter_var($this->draft, FILTER_VALIDATE_BOOLEAN);
        $document->creator = $this->user()->id;
        $document->folder = filter_var($this->folder, FILTER_VALIDATE_BOOLEAN);
        $document->parent_id = $parent ? $parent->id : null;

        $document->publish = $this->publish ? Carbon::parse($this->publish) : Carbon::today();
        $document->expire = $this->expire ? Carbon::parse($this->expire) : null;

        if ($upload = $this->file('file'))
        {                
            $document->title = $upload->getClientOriginalName();
            $document->slug = strtolower(str_slug(str_replace('.' . $upload->getClientOriginalExtension(), '', $upload->getClientOriginalName()), '-'));
            $document->save();

            $file = new DocumentFile;
            $file->unique_id = $file->unique_id ? $file->unique_id : md5(uniqid());
            $file->name = $document->title;
            $file->version = "";
            $file->mime_type = $upload->getMimeType();
            $file->author = $this->user()->id;
            $file->document_id = $document->id;
            $file->latest = false;

            $file->publish = $this->publish ? Carbon::parse($this->publish) : Carbon::today();
            $file->draft = false;

            $storage = \Storage::disk('s3');
            $name = uniqid(rand(), false);
            $extension = $upload->getClientOriginalExtension();
            //$path = md5(\Hyn\Tenancy\Facades\TenancyFacade::website()->uuid) . "/documents/{$document->unique_id}/{$file->unique_id}.{$extension}";
            $path = md5(env('AWS_FOLDER_ID')) . "/documents/{$document->unique_id}/{$file->unique_id}.{$extension}";

            if ($storage->put($path, file_get_contents($upload), 'private'))
            {
                $file->path = $path;
                $file->extension = $extension;
                $file->size = $storage->size($path);
            }

            $file->save();
        }

        if (trim(strtoupper($document->title)) === "TEMP") return false;

        $document->save();

        return $document;
    }
}
