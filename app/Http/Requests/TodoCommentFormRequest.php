<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\TodoComment;
use App\Todo;
use App\Attachment;

class TodoCommentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() ? true: false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'body' => 'required'
        ];
    }

    public function persist(TodoComment $comment) 
    {
        $todo = Todo::where('unique_id', $this->todo_id)->firstOrFail();

        $comment->unique_id = $comment->generateUniqueId();
        $comment->body = trim($this->body);
        $comment->author = $this->user()->id;
        $comment->todo_id = $todo->id;

        $comment->save();

        if ($attachments = $this->file('attachments'))
        {
            $storage = \Storage::disk('s3');

            foreach ($attachments as $file) 
            {
                if ($file) {
                    $hashed_name = md5(uniqid());
                    $extension = $file->getClientOriginalExtension();
                    //$path = md5(\Hyn\Tenancy\Facades\TenancyFacade::website()->uuid) . "/attachments/{$hashed_name}.{$extension}";
                    $path = md5(env('AWS_FOLDER_ID')) . "/attachments/{$hashed_name}.{$extension}";

                    if ($storage->put($path, file_get_contents($file), 'private'))
                    {
                        $attachment = new Attachment();
                        $attachment->unique_id = $hashed_name;
                        $attachment->parent_id = $comment->unique_id;
                        $attachment->mime_type = $file->getMimeType();
                        $attachment->name = $file->getClientOriginalName();
                        $attachment->path = $path;
                        $attachment->size = $storage->size($path);
                        $attachment->uploaded_by = $this->user()->id;

                        $attachment->save();
                    }
                }
            }
        }

        return $comment;
    }
}
