<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\LocationCollection;
use App\Http\Resources\Location as LocationResource;
use App\Http\Requests\LocationFormRequest;
use App\Location;

class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(new LocationCollection(Location::orderBy('name', 'asc')->get()), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LocationFormRequest $request)
    {
        $location = $request->persist(new Location);
        
        return response()->json(new LocationResource($location), 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LocationFormRequest $request, $id)
    {
        $location = $request->persist(Location::where('unique_id', $id)->firstOrFail());    

        return response()->json(new LocationResource($location), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = Location::where('unique_id', $id)->firstOrFail();
        $location->delete();

        return response()->json(null, 204);
    }
}
