<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\QuestionCollection;
use App\Http\Resources\Question as QuestionResource;
use App\Http\Requests\QuestionFormRequest;
use Illuminate\Support\Facades\Storage;
use App\Question;
use App\Answer;
use App\Attachment;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(new QuestionCollection(Question::all()), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionFormRequest $request)
    {
        $question = $request->persist(new Question);
        
        return response()->json(new QuestionResource($question), 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(QuestionFormRequest $request, $id)
    {
        $question = $request->persist(Question::where('unique_id', $id)->firstOrFail());    

        return response()->json(new QuestionResource($question), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::where('unique_id', $id)->firstOrFail();
        $storage = Storage::disk('s3');

        $answers = Answer::where('question_id', $question->id)->get();

        if ($answers)
        {
            foreach ($answers as $answer) 
            {
                $attachments = Attachment::where('parent_id', $answer->unique_id);

                if ($attachments)
                {
                    foreach ($attachments as $attachment) 
                    {
                        $storage->delete($attachment->path);
                    }

                    Attachment::where('parent_id', $answer->unique_id)->delete();
                }
            }

            Answer::where('question_id')->delete();
        }

        $attachments = Attachment::where('parent_id', $question->unique_id);

        if ($attachments)
        {
            foreach ($attachments as $attachment) 
            {
                $storage->delete($attachment->path);
            }

            Attachment::where('parent_id', $question->unique_id)->delete();
        }

        $question->delete();

        return response()->json(null, 204);
    }
}
