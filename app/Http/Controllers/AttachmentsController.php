<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\AttachmentCollection;
use App\Http\Resources\Attachment as AttachmentResource;
use App\Http\Requests\AttachmentFormRequest;
use App\Attachment;

class AttachmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(new AttachmentCollection(Attachment::orderBy('created_at', 'desc')->get()), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download($unique_id) 
    {
        $file = Attachment::where('unique_id', $unique_id)->firstOrFail();

        return response()->streamDownload(function () use ($file) {

            $storage = \Storage::disk('s3');

            header('Content-type: ' . $file->mime_type);

            echo $storage->get($file->path);
        
        }, $file->name);

    }
}
