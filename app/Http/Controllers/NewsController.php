<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\NewsCollection;
use App\Http\Resources\News as NewsResource;
use App\Http\Requests\NewsFormRequest;
use App\News;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(new NewsCollection(News::orderBy('publish', 'desc')->get()), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsFormRequest $request)
    {
        $news = $request->persist(new News);
        
        return response()->json(new NewsResource($news), 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsFormRequest $request, News $news)
    {
        
        if (is_null($news))
        {
            return response()->json(null, 404);
        }

        $news = $request->persist($news);    

        return response()->json(new NewsResource($news), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        if (is_null($news))
        {
            return response()->json(null, 404);
        }
        
        $news->delete();

        
        return response()->json(null, 204);
    }
}
