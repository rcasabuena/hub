<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Document;
use App\DocumentFile;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function s3()
    {
        die('here');

        $local = Storage::disk('local');
        $s3 = Storage::disk('s3');

        $files = DocumentFile::where('path', 'LIKE', '%dropbox%')->limit(50)->get();

        foreach ($files as $file) 
        {
            $doc = Document::find($file->document_id);
            $path = "current/{$doc->unique_id}/{$file->unique_id}.{$file->extension}";
            if ($s3->put($path, $local->get($file->path), 'private'))
            {
                $file->path = $path;
                $file->save();
            }
        }

        dd('done');

    }

    public function upload()
    {
        die('here');
        $storage = Storage::disk('local');
        $tree = [];

        $directories = $storage->directories('dropbox');

        foreach ($directories as $dir) 
        {
            $tree[] = $this->makeDoc($dir);
        }

        dd($tree);

    }

    private function getFiles($storage, $dir, $parent = null)
    {
        $files = $storage->files($dir);
        $tmp = [];

        foreach ($files as $file) 
        {
            $tmp[] = $this->makeFile($file, $parent);
        }

        return $tmp;
    }

    private function getFolders($storage, $dir, $parent = null)
    {
        $files = $storage->directories($dir);
        $tmp = [];

        foreach ($files as $file) 
        {
            $tmp[] = $this->makeDoc($file, $parent);
        }

        return $tmp;
    }

    private function makeDoc($dir, $parent = null)
    {
        $storage = Storage::disk('local');
        $parts = explode('/', $dir);

        $doc = new Document;
        $doc->unique_id = md5(uniqid());
        $doc->slug = str_slug(trim(end($parts)));
        $doc->name = trim(end($parts));
        $doc->title = trim(end($parts));
        $doc->about = trim(end($parts));
        $doc->creator = 1;
        $doc->publish = Carbon::now();
        $doc->folder = true;
        $doc->draft = false;
        $doc->parent_id = $parent;

        $doc->save();

        $this->getFiles($storage, $dir, $doc->id);
        $this->getFolders($storage, $dir, $doc->id);

        // $doc = [
        //     'unique_id' => md5(uniqid()),
        //     'slug' => str_slug(trim(end($parts))),
        //     'name' => trim(end($parts)),
        //     'title' => trim(end($parts)),
        //     'creator' => 1,
        //     'publish' => Carbon::now(),
        //     'draft' => false,
        //     'folder' => true,
        //     'path' => $dir,
        //     'files' => $this->getFiles($storage, $dir, $id),
        //     'folders' => $this->getFolders($storage, $dir, $id),
        //     'parent_id' => $parent
        // ];

        //return $doc;

        return true;
    }

    private function makeFile($dir, $parent = null)
    {
        $storage = Storage::disk('local');
        $parts = explode('/', $dir);

        $doc = new Document;
        $doc->unique_id = md5(uniqid());
        $doc->slug = str_slug(trim(end($parts)));
        $doc->name = trim(end($parts));
        $doc->title = trim(end($parts));
        $doc->about = trim(end($parts));
        $doc->creator = 1;
        $doc->publish = Carbon::now();
        $doc->folder = false;
        $doc->draft = false;
        $doc->parent_id = $parent;

        $doc->save();

        $pathinfo = pathinfo($dir);

        $docFile = new DocumentFile;
        $docFile->unique_id = md5(uniqid());
        $docFile->document_id = $doc->id;
        $docFile->size = $storage->size($dir);
        $docFile->mime_type = $storage->mimeType($dir);
        $docFile->author = 1;
        $docFile->extension = $pathinfo['extension'];
        $docFile->name = $pathinfo['basename'];
        $docFile->publish = Carbon::now();
        $docFile->path = $dir;

        $docFile->save();

        return true;
        
        // $file = [
        //     'unique_id' => md5(uniqid()),
        //     'slug' => str_slug(trim(end($parts))),
        //     'name' => trim(end($parts)),
        //     'title' => trim(end($parts)),
        //     'creator' => 1,
        //     'publish' => Carbon::now(),
        //     'draft' => false,
        //     'folder' => false,
        //     'path' => $dir,
        //     'parent_id' => $parent
        // ];

        // return $file;
    }
}
