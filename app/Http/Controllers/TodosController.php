<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Mail\TaskAssigned;
use App\Http\Resources\TodoCollection;
use App\Http\Resources\Todo as TodoResource;
use App\Http\Resources\Task as TaskResource;
use App\Http\Requests\TodoFormRequest;
use App\Http\Requests\SimpleTodoFormRequest;
use App\Mail\TaskRejected;
use App\Mail\TaskCompleted;
use App\Todo;

use Illuminate\Http\Request;

class TodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(new TodoCollection(Todo::orderBy('deadline', 'asc')->get()), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TodoFormRequest $request)
    {
        $task = $request->persist();
        return response()->json(new TaskResource($task), 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SimpleTodoFormRequest $request, $id)
    {
        $todo = $request->persist();    

        if ($todo->rejected) 
        {
            \Mail::send(
                new TaskRejected($todo)
            );
        }

        if ($todo->completed)
        {
            \Mail::send(
                new TaskCompleted($todo)
            );
        }

        return response()->json(new TodoResource($todo), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todo = Todo::where('unique_id', $id)->firstOrFail();
        $todo->delete();

        return response()->json(null, 204);
    }
}
