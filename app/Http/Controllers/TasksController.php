<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\TaskCollection;
use App\Http\Resources\Task as TaskResource;
use App\Http\Requests\TaskFormRequest;
use Illuminate\Support\Facades\Storage;
use App\Task;
use App\Todo;
use App\TodoComment;
use App\Attachment;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(new TaskCollection(Task::all()), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskFormRequest $request)
    {
        $task = $request->persist(new Task);
        
        return response()->json(new TaskResource($task), 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TaskFormRequest $request, $id)
    {
        $task = $request->persist(Task::where('unique_id', $id)->firstOrFail());    

        return response()->json(new TaskResource($task), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::where('unique_id', $id)->firstOrFail();
        $storage = Storage::disk('s3');

        $todos = Todo::where('task_id', $task->id)->get();

        if ($todos) 
        {
            foreach ($todos as $todo) 
            {
                $comments = TodoComment::where('todo_id', $todo->id)->get();

                if ($comments)
                {
                    foreach ($comments as $comment) 
                    {
                        $attachments = Attachment::where('parent_id', $comment->unique_id);

                        if ($attachments)
                        {
                            foreach ($attachments as $attachment) 
                            {
                                $storage->delete($attachment->path);
                            }

                            Attachment::where('parent_id', $comment->unique_id)->delete();
                        }
                    }

                    TodoComment::where('todo_id', $todo->id)->delete();
                }
            }
        }

        Todo::where('task_id', $task->id)->delete();
        $task->delete();

        return response()->json(null, 204);
    }
}
