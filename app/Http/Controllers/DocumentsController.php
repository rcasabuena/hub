<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\DocumentCollection;
use App\Http\Resources\Document as DocumentResource;
use App\Http\Requests\DocumentFormRequest;
use App\Document;

class DocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(new DocumentCollection(Document::orderBy('updated_at', 'desc')->get()), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocumentFormRequest $request)
    {
        $document = $request->persist(new Document);
        
        return response()->json(new DocumentResource($document), 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DocumentFormRequest $request, $id)
    {
        $document = $request->persist(Document::where('unique_id', $id)->firstOrFail());    

        return response()->json(new DocumentResource($document), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $document = Document::where('unique_id', $id)->firstOrFail();

        if (!$document->files->isEmpty())
        {
            $storage = \Storage::disk('s3');

            foreach ($document->files as $file) 
            {
                $storage->delete($file->path);
                $file->delete();
            }
        }

        $document->delete();

        
        return response()->json(null, 204);

    }
}
