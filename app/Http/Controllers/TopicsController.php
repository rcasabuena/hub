<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\TopicCollection;
use App\Http\Resources\Topic as TopicResource;
use App\Http\Requests\TopicFormRequest;
use App\Topic;

class TopicsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(new TopicCollection(Topic::all()), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TopicFormRequest $request)
    {
        $topic = $request->persist(new Topic);
        
        return response()->json(new TopicResource($topic), 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TopicFormRequest $request, $id)
    {
        $topic = $request->persist(Topic::where('unique_id', $id)->firstOrFail());    

        return response()->json(new TopicResource($topic), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topic = Topic::where('unique_id', $id)->firstOrFail();
        $topic->delete();

        return response()->json(null, 204);
    }
}
