<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Todo as TodoResource;
use App\Http\Requests\TodoCommentFormRequest;
use App\Todo;
use App\TodoComment;

class TodoCommentsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TodoCommentFormRequest $request)
    {
        $comment = $request->persist(new TodoComment);
        return response()->json(new TodoResource(Todo::where('id', $comment->todo_id)->firstOrFail()), 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
