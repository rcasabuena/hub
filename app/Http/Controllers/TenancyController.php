<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;

class TenancyController extends Controller
{
    public function currentLoggedInUser(Request $request) {
    	return response()->json(new UserResource($request->user()), 200);
    }

    public function currentTenant(Request $request) {
    	return response()->json([
		    'name' => 'Banana Moon Hub',
			'logo' => [
				'web' => '/images/bananamoon.jpg',
				'mob' => '/images/bananamoon.jpg',
			]
		]);
    }
}
