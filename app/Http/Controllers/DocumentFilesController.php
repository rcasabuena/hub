<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DocumentFileFormRequest;
use App\DocumentFile;
use App\Document;
use App\Download;
use App\Http\Resources\Document as DocumentResource;

class DocumentFilesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocumentFileFormRequest $request)
    {
        $docFile = $request->persist(new DocumentFile);
        return response()->json(new DocumentResource(Document::where('id', $docFile->document_id)->firstOrFail()), 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = DocumentFile::where('unique_id', $id)->firstOrFail();
        $document_id = $file->document_id;

        $storage = \Storage::disk('s3');
        $storage->delete($file->path);
        $file->delete();

        return response()->json(new DocumentResource(Document::find($document_id)), 204);

    }

    public function download($unique_id) 
    {
        $file = DocumentFile::where('unique_id', $unique_id)->firstOrFail();

        $file->file_name = str_slug($file->name);
        if (trim($file->version) != "") $file->file_name .= '-' . str_slug($file->version);
        $file->file_name .= '.' . strtolower($file->extension); 

        // Log user download
        $download = new Download;
        $download->document_id = $file->document_id;
        $download->file_id = $file->id;
        $download->user_id = auth()->user()->id;
        $download->name = $file->name;

        $download->save();

        return response()->streamDownload(function () use ($file) {

            $storage = \Storage::disk('s3');

            header('Content-type: ' . $file->mime_type);

            echo $storage->get($file->path);
        
        }, $file->file_name);

    }
}
