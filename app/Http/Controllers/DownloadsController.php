<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\DownloadCollection;
use App\Download;

class DownloadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(new DownloadCollection(Download::orderBy('created_at', 'desc')->get()), 200);
    }
}
