<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\UserGroupCollection;
use App\Http\Resources\UserGroup as UserGroupResource;
use App\Http\Requests\UserGroupFormRequest;
use App\UserGroup;

class UserGroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(new UserGroupCollection(UserGroup::all()), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserGroupFormRequest $request)
    {
        $group = $request->persist(new UserGroup);
        
        return response()->json(new UserGroupResource($group), 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserGroupFormRequest $request, $id)
    {
        $group = $request->persist(UserGroup::where('unique_id', $id)->firstOrFail());    
        
        return response()->json(new UserGroupResource($group), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
