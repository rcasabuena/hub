<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\TodoCollection;
use App\Http\Resources\AttachmentCollection;
use App\User;
use App\Todo;
use App\Attachment;
use App\Location;

class Task extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $author = User::find($this->author);
        $location = Location::find($this->location_id);
        $attachments = Attachment::where('parent_id', $this->unique_id)->orderBy('created_at', 'desc')->get();

        switch ($this->repeat) 
        {
            case 'DAY':
                $repeat = 'Daily';
                break;

            case 'WEEK':
                $repeat = 'Weekly';
                break;

            case 'MONTH':
                $repeat = 'Monthly';
                break;

            case '3MONTHS':
                $repeat = 'Every 3 months';
                break;

            case '6MONTHS':
                $repeat = 'Every 6 months';
                break;

            case 'YEAR':
                $repeat = 'Yearly';
                break;
            
            default:
                $repeat = "";
                break;
        }

        return [
            'id' => $this->unique_id,
            'title' => $this->title,
            'body' => $this->body,
            'repeat' => $repeat,
            'locationId' => $location ? $location->unique_id : null,
            'author' => $author ? $author->name : 'Admin',
            'updated' => $this->updated_at,
            'todos' => new TodoCollection(Todo::where('task_id', $this->id)->get()),
            'attachments' => new AttachmentCollection($attachments)
        ];
    }
}
