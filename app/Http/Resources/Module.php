<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Module extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->unique_id,
            'name' => $this->name,
            'admin' => filter_var($this->admin, FILTER_VALIDATE_BOOLEAN),
            'active' => filter_var($this->active, FILTER_VALIDATE_BOOLEAN),
            'label' => $this->name,
            'value' => $this->id
        ];
    }
}
