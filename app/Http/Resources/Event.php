<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;

class Event extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $author = User::find($this->author_id);

        return [
            'id' => $this->unique_id,
            'title' => $this->title,
            'body' => $this->body,
            'venue' => $this->venue,
            'author' => $author ? $author->name : 'Admin',
            'updated' => $this->updated_at,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,           
            'draft' => $this->draft
        ];
    }
}
