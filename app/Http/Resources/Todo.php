<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\TodoCommentCollection;
use Carbon\Carbon;
use App\User;
use App\Task;
use App\Location;

class Todo extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::find($this->user_id);
        $task = Task::find($this->task_id);
        $assignee = User::find($this->added_by);
        $location = Location::find($this->location_id);
        return [
            'id' => $this->unique_id,
            'completed' => filter_var($this->completed, FILTER_VALIDATE_BOOLEAN),
            'accepted' => filter_var($this->accepted, FILTER_VALIDATE_BOOLEAN),
            'rejected' => filter_var($this->rejected, FILTER_VALIDATE_BOOLEAN),
            'comments' => new AnswerCollection($this->comments),
            'name' => $user ? $user->name : '',
            'user_id' => $user ? $user->unique_id: "",
            'task_id' => $task ? $task->unique_id: "",
            'locationId' => $location ? $location->unique_id: "",
            'taskName' => $task ? $task->title: "",
            'added_by' => $assignee ? $assignee->unique_id : "",
            'deadline' => $this->deadline,
            'updated_at' => $this->updated_at,
            'completed_at' => Carbon::parse($this->completed_at),
        ];
    }
}
