<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
use App\User;
use App\Location;

class DocumentFile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $author = User::find($this->author);
        $location = $author ? Location::find($author->location_id) : null;

        return [
            'id' => $this->unique_id,
            'name' => $this->name,
            'size' => formatBytes(intval($this->size)),
            'created' => $this->created_at,
            'updated' => Carbon::parse($this->updated_at)->format('j F Y'),
            'mime_type' => $this->mime_type,
            'version' => $this->version,
            'uploaded_by' => $author ? $author->name : 'ADMIN',
            'location' => $location ? $location->name : 'HQ',
            'latest' => $this->latest
        ];
    }
}
