<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
use App\User;
use App\Question;
use App\Location;

class Topic extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $author = User::find($this->author);
        $location = $author ? Location::find($author->location_id) : null;

        return [
            'id' => $this->unique_id,
            'title' => $this->title,
            'body' => $this->body,
            'value' => $this->unique_id,
            'label' => $this->title,
            'created' => $this->created_at,
            'questions' => Question::where('topic_id', $this->id)->count(), 
            'author' => $author ? $author->name : 'Admin',
            'location' => $location ? $location->name: 'HQ'
        ];
    }
}
