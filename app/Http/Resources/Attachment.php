<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\User;

class Attachment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $author = User::find($this->uploaded_by);

        return [
            'id' => $this->unique_id,
            'parentId' => $this->parent_id,
            'name' => $this->name,
            'size' => formatBytes(intval($this->size)),
            'created' => $this->created_at,
            'updated' => $this->updated_at,
            'mime_type' => $this->mime_type,
            'uploaded_by' => $author ? $author->name : 'ADMIN'
        ];
    }
}
