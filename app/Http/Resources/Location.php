<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Location extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->unique_id,
            'name' => $this->name,
            'email' => $this->email,
            'telephone' => $this->telephone,
            'address_line1' => $this->address_line1,
            'address_line2' => $this->address_line2,
            'address_city' => $this->address_city,
            'address_county' => $this->address_county,
            'address_postcode' => $this->address_postcode,
            'address' => implode(', ', array_filter([$this->address_line1, $this->address_line2, $this->address_city, $this->address_county, $this->address_postcode])),
            'label' => $this->name,
            'value' => $this->unique_id
        ];
    }
}
