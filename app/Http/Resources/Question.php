<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\AnswerCollection;
use App\Http\Resources\AttachmentCollection;
use App\Http\Resources\Topic as TopicResource;
use Carbon\Carbon;
use App\User;
use App\Location;
use App\Attachment;

class Question extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $author = User::find($this->author);
        $location = $author ? Location::find($author->location_id) : null;
        $attachments = Attachment::where('parent_id', $this->unique_id)->get();
        return [
            'id' => $this->unique_id,
            'title' => $this->title,
            'body' => $this->body,
            'sticky' => $this->sticky,
            'bodyText' => htmlToRawText($this->body),
            'author' => $author ? $author->name : 'Admin',
            'location' => $location ? $location->name: 'HQ', 
            'topic' => new TopicResource($this->topic),
            'topic_name' => $this->topic ? $this->topic->title : "",
            'answers' => new AnswerCollection($this->answers),
            'attachments' => new AttachmentCollection($attachments),
            'created' => $this->created_at,
            'lastActive' => isset($this->answers[0]) ? $this->answers[(count($this->answers)-1)]->created_at : $this->created_at,
            'search' => array_unique(array_merge(textToArray($this->title), textToArray($this->body)))
        ];
    }
}
