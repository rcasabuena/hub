<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class News extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->unique_id,
            'title' => $this->title,
            'headline' => $this->headline,
            'body' => $this->body,
            'about' => $this->about,
            'keywords' => $this->keywords,
            'featured_image' => $this->featured_image_url,
            'author' => $this->authorName(),
            'location' => $this->location(),
            'updated' => $this->updated_at,
            'publish' => $this->publish,
            'expire' => $this->expire,           
            'published' => $this->isPublished(),
            'draft' => $this->draft
        ];
    }
}
