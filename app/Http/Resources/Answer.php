<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
use App\Http\Resources\AttachmentCollection;
use App\User;
use App\Location;
use App\Attachment;

class Answer extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $author = User::find($this->author);
        $location = $author ? Location::find($author->location_id) : null;
        $attachments = Attachment::where('parent_id', $this->unique_id)->get();
        return [
            'id' => $this->unique_id,
            'body' => $this->body,
            'author' => $author ? $author->name : 'Admin',
            'created' => $this->created_at,
            'attachments' => new AttachmentCollection($attachments),
            'location' => $location ? $location->name: 'Head Office'
        ];
    }
}
