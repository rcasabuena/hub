<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Download extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'documentId' => $this->document->unique_id,
            'filename' => $this->file->name,
            'mime_type' => $this->file->mime_type,
            'username' => $this->user->name,
            'user_id' => $this->user->unique_id,
            'location' => $this->location(),
            'created_at' => $this->created_at
        ];
    }
}
