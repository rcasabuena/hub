<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Location as LocationResource;
use Carbon\Carbon;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'id' => $this->unique_id,
            'name' => $this->name,
            'email' => strtolower($this->email),
            'date_registered' => Carbon::parse($this->created_at)->format('j F Y'),
            'role' => $this->admin ? 'ADMIN' : 'USER', 
            'location' => new LocationResource($this->location),
            'admin' => filter_var($this->admin, FILTER_VALIDATE_BOOLEAN),
            'director' => filter_var($this->director, FILTER_VALIDATE_BOOLEAN),
            'label' => $this->name,
            'value' => $this->unique_id,
            'status' => 'Active'
        ];
    }
}
