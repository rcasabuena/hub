<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\DocumentFileCollection;
use Carbon\Carbon;
use App\Document as Doc;
use App\User;
use App\Location;

class Document extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $parent = Doc::find($this->parent_id);
        $creator = User::find($this->creator);
        $location = $creator ? Location::find($creator->location_id) : null;

        return [
            'id' => $this->unique_id,
            'title' => $this->title,
            'about' => $this->about,
            'aboutText' => htmlToRawText($this->about),
            'updated' => $this->updated_at,
            'publish' => Carbon::parse($this->publish)->format('j F Y'),
            'publish_raw' => Carbon::parse($this->publish)->format('Y-m-d'),
            'expire_raw' => $this->expire ? Carbon::parse($this->expire)->format('Y-m-d') : "",            
            'published' => Carbon::parse($this->publish) < Carbon::tomorrow(),
            'files' => new DocumentFileCollection($this->files),
            'draft' => filter_var($this->draft, FILTER_VALIDATE_BOOLEAN),
            'folder' => filter_var($this->folder, FILTER_VALIDATE_BOOLEAN),
            'parent' => $parent ? $parent->unique_id : null,
            'parentName' => $parent ? $parent->title : null,
            'creator' => $creator ? $creator->name : "Admin",
            'location' => $location ? $location->name: 'HQ',
            'search' => array_unique(array_merge(textToArray($this->title), textToArray($this->about)))
        ];
    }
}
