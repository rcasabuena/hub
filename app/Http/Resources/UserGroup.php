<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\UserCollection;
use App\User;

class UserGroup extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $creator = User::find($this->creator);

        return [
            'id' => $this->unique_id,
            'name' => $this->name,
            'users' => explode(',', $this->content),
            'label' => $this->name,
            'value' => $this->unique_id,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at
        ];
    }
}
