<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUniqueId;
use App\Mail\NewForumTopicCreated;

class Topic extends Model
{
	use HasUniqueId;

    protected static function boot()
    {
    	parent::boot();
    	static::created(function ($topic) {
            $users = User::all();
            foreach ($users as $user) 
            {
                \Mail::send(
                    new NewForumTopicCreated($topic, $user)
                );
            }
    	});
    }

}
