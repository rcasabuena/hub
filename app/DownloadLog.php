<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUniqueId;

class DownloadLog extends Model
{
    use HasUniqueId;
}
