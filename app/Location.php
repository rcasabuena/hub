<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUniqueId;

class Location extends Model
{
    use HasUniqueId;
}
