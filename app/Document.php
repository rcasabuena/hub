<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Mail\DocumentUpdated;
use App\Mail\DocumentPublished;
use App\Traits\HasUniqueId;
use App\User;

class Document extends Model
{
    use HasUniqueId;

    protected static function boot()
    {
    	parent::boot();
        static::created(function ($document) {
            if (!$document->draft && Carbon::parse($document->publish)->isToday())
            {
                if ($users = User::all())
                {
                    foreach ($users as $user) 
                    {
                        \Mail::send(
                            new DocumentPublished($document, $user)
                        );
                    }
                }
            }
        });
    	static::updated(function ($document) {
    		
            if (!$document->draft && (Carbon::parse($document->publish)->isPast() || Carbon::parse($document->publish)->isToday()))
            {
                if ($users = User::all())
                {
                    foreach ($users as $user) 
                    {
                        \Mail::send(
                            new DocumentUpdated($document, $user)
                        );
                    }
                }
            }
    	});
    }

    public function files()
    {
        return $this->hasMany('App\DocumentFile')->orderBy('created_at', 'desc');
    }
}
