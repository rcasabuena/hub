<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUniqueId;

class Attachment extends Model
{
    use HasUniqueId;
}
