<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUniqueId;

class Event extends Model
{
    use HasUniqueId;
}
