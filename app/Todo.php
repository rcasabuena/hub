<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUniqueId;
use App\Mail\TaskAssigned;

class Todo extends Model
{
    use HasUniqueId;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['task_id', 'user_id', 'deadline', 'added_by', 'completed', 'rejected', 'rejected_at', 'completed_at'];

    protected static function boot()
    {
    	parent::boot();
    	static::created(function ($todo) {
    		\Mail::to('ritchie@denfield.co.uk')->send(
	            new TaskAssigned($todo, "create")
	        );
    	});
    }

    public function comments()
    {
        return $this->hasMany('App\TodoComment')->orderBy('created_at', 'asc');
    }
}
