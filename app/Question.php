<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUniqueId;
use App\Mail\NewQuestionAsked;
use App\Mail\QuestionUpdated;
use App\User;

class Question extends Model
{
    use HasUniqueId;

    protected static function boot()
    {
    	parent::boot();
    	static::created(function ($question) {
            $users = User::all();
            foreach ($users as $user) 
            {
                \Mail::send(
                    new NewQuestionAsked($question, $user)
                );
            }
    	});
    	static::updated(function ($question) {
    		$users = User::all();
            foreach ($users as $user) 
            {
                \Mail::to($user->email)->send(
                    new QuestionUpdated($question, $user)
                );
            }
    	});
    }

    public function answers()
    {
        return $this->hasMany('App\Answer')->orderBy('created_at', 'asc');
    }

    public function topic()
    {
    	return $this->belongsTo('App\Topic');
    }
}
