<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUniqueId;

class Task extends Model
{
    use HasUniqueId;
}
