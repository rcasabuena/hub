<?php

namespace App\Console\Commands\Notifications;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewsPublished;
use Carbon\Carbon;
use App\News;
use App\User;


class PublishedNewsArticles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:published-news-articles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily notifications for published news articles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = Carbon::today('gmt');
        $end = Carbon::tomorrow('gmt');

        $articles = News::where('draft', false)->where('publish', '>=', $start)->where('publish', '<', $end)->get();
        $users = User::all();

        if ($articles && count($articles) > 0 && $users && count($users) > 0) 
        {
            foreach ($articles as $news) 
            {
                foreach ($users as $user) 
                {
                    Mail::send(new NewsPublished($news, $user));
                }
            }
        }
    }
}
