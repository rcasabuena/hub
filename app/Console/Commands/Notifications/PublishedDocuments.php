<?php

namespace App\Console\Commands\Notifications;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\DocumentPublished;
use Carbon\Carbon;
use App\Document;
use App\User;

class PublishedDocuments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:published-documents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily notifications for published documents';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = Carbon::today('gmt');
        $end = Carbon::tomorrow('gmt');

        $docs = Document::where('draft', false)->where('publish', '>=', $start)->where('publish', '<', $end)->get();
        $users = User::all();

        if ($docs && count($docs) > 0 && $users && count($users) > 0) 
        {
            foreach ($docs as $doc) 
            {
                foreach ($users as $user) 
                {
                    Mail::send(new DocumentPublished($doc, $user));
                }
            }
        }
    }
}
