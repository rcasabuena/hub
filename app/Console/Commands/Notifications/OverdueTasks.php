<?php

namespace App\Console\Commands\Notifications;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\OverdueTaskReminder;
use Carbon\Carbon;
use App\Todo;

class OverdueTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:overdue-tasks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily notifications for overdue tasks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         $overdueTodos = Todo::where('completed', false)->where('rejected', false)->where('deadline', '<', Carbon::today())->get();

         if ($overdueTodos && count($overdueTodos) > 0)
        {
            foreach ($overdueTodos as $todo) 
            {
                \Mail::send(
                    new OverdueTaskReminder($todo)
                );
            }
        }
    }
}
