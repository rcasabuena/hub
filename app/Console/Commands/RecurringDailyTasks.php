<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Task;
use App\Todo;

class RecurringDailyTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasks:recurring-daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reassign recurring daily tasks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tasks = Task::where('repeat', 'DAY')->get();

        if ($tasks && count($tasks) > 0)
        {
            foreach ($tasks as $task) {
                $todos = Todo::where('task_id', $task->id)->get();
                dd($todos);
            }
        }
    }
}
