<?php 

if (!function_exists('textToArray')) {
    /**
     * Returns an array of words
     *
     * */
    function textToArray($string)
    {
        return array_filter(array_map(function($item) {
            return preg_replace("/[^a-zA-Z0-9@]/", "", $item);
        }, explode(" ", strip_tags(trim(str_replace(["\r\n", html_entity_decode('&ndash;', ENT_COMPAT, 'UTF-8') ,html_entity_decode('&mdash;', ENT_COMPAT, 'UTF-8'), ",", "\xc2\xa0", "'", "\"", "`"], " ", strtolower($string)))))));
        
    }
}

if (!function_exists('htmlToRawText')) {
    function htmlToRawText($html) {
        return strip_tags(trim(str_replace(["\r\n", html_entity_decode('&ndash;', ENT_COMPAT, 'UTF-8') ,html_entity_decode('&mdash;', ENT_COMPAT, 'UTF-8'), ",", "\xc2\xa0", "'", "\"", "`"], " ", strtolower($html))));
    }
}


if (!function_exists('formatBytes')) {
    function formatBytes($bytes, $decimals = 2) { 
        $factor = floor((strlen($bytes) - 1) / 3);
        if ($factor > 0) $sz = 'KMGT';
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor - 1] . 'B';
    } 
}