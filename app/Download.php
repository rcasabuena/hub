<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUniqueId;

class Download extends Model
{
    use HasUniqueId;


    public function file()
    {
    	return $this->hasOne('App\DocumentFile', 'id', 'file_id'); 
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function document()
    {
        return $this->hasOne('App\Document', 'id', 'document_id');
    }

    public function location()
    {
        return $this->user && $this->user->location ? $this->user->location->name : 'Head Office';
    }
}
