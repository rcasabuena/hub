<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Mail\QuestionAnswered;
use App\User;
use App\Question;
use App\Traits\HasUniqueId;

class Answer extends Model
{
    use HasUniqueId;

    protected static function boot()
    {
    	parent::boot();
    	static::created(function ($answer) {
    		$question = Question::find($answer->question_id);
            $users[] = User::find($question->author);
            $tmp[] = $users[0]->id;

            foreach ($question->answers as $ans) 
            {
            	if (!in_array($ans->author, $tmp)) 
            	{
            		$tmp[] = $ans->author;
            		if ($author = User::find($ans->author)) $users[] = $author;
            	}
            }

            foreach ($users as $user) 
            {
                \Mail::send(
                    new QuestionAnswered($question, $answer, $user)
                );
            }
    	});
    }
}
