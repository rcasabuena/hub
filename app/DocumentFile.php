<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUniqueId;

class DocumentFile extends Model
{
    use HasUniqueId;
}
