<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasUniqueId;

class Lookup extends Model
{
    use HasUniqueId;
}
