<?php

namespace App\Traits;

trait HasUniqueId {
	
	public function generateUniqueId()
	{
		return $this->unique_id ? $this->unique_id : md5(uniqid());
	}
	
}