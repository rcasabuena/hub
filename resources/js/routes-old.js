import React from 'react'
import { Route, Switch } from 'react-router-dom'

import Home from './components/home/Home'
import Forum from './components/forum/Forum'
import Documents from './components/documents/Documents'
import CreateDocumentsCategory from './components/documents/CreateDocumentsCategory'
import Events from './components/events/Events'
import News from './components/news/News'
import NewsCreate from './components/news/NewsCreate'
import Users from './components/users/Users'
import Settings from './components/site/Settings'
import Profile from './components/account/Profile'
import Page from './components/pages/Page'

const Routes = () => (
	<Switch>
	    <Route exact path='/' component={Home} />
	    <Route exact path='/forum' component={Forum} />
	    <Route exact path='/documents' component={Documents} />
	    <Route exact path='/documents/new' component={CreateDocumentsCategory} />
	    <Route exact path='/news/new' component={NewsCreate} />
	    <Route exact path='/news' component={News} />
	    <Route exact path='/events' component={Events} />
	    <Route exact path='/users' component={Users} />
	    <Route exact path='/settings' component={Settings} />
	    <Route exact path='/profile' component={Profile} />
	    {/*<Route path='/:page_slug' component={Page} />*/}
	</Switch>
)

export default Routes