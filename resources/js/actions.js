import axios from 'axios'
import C from './constants'
import { toastr } from 'react-redux-toastr'

export const uploadDocumentFile = formData => dispatch => {
	axios.post('/api/document-files', formData).then(res => res.data).then(data => {
		dispatch({
			type: C['UPDATE_DOCUMENTS'],
			payload: data
		})
		toastr.success('Success', 'The document has been uploaded.')
	})
}

export const saveTodos = formData => dispatch => {
	axios.post('/api/todos', formData).then(res => res.data).then(data => {
		dispatch({
			type: C['UPDATE_TASKS'],
			payload: data
		})
		
		data.todos.forEach(item => {
			dispatch({
				type: C['UPDATE_TODOS'],
				payload: item
			})
		})

		toastr.success('Success', 'The tasks has been updated.')
	})
}

export const saveAnswer = formData => dispatch => {
	axios.post('/api/answers', formData).then(res => res.data).then(data => {
		dispatch({
			type: C['UPDATE_QUESTIONS'],
			payload: data
		})

		if (data.answers.length > 0) {
			data.answers.forEach(answer => {
				if (undefined !== answer.attachments) {
					answer.attachments.forEach(item => {
						dispatch({
							type: C['UPDATE_ATTACHMENTS'],
							payload: item
						})
					})
				}
			})
		}

		toastr.success('Success', 'The questions have been updated.')
	})
}

export const saveTodoComment = formData => dispatch => {
	axios.post('/api/todo-comments', formData).then(res => res.data).then(data => {
		dispatch({
			type: C['UPDATE_TODOS'],
			payload: data
		})

		if (data.comments.length > 0) {
			data.comments.forEach(comment => {
				if (undefined !== comment.attachments) {
					comment.attachments.forEach(item => {
						dispatch({
							type: C['UPDATE_ATTACHMENTS'],
							payload: item
						})
					})
				}
			})
		}

		toastr.success('Success', 'Your comment has been added.')
	})
}

export const deleteDocumentFile = id => dispatch => {
	axios.delete(`/api/document-files/${id}`).then(res => res.data).then(data => {
		dispatch({
			type: C['UPDATE_DOCUMENTS'],
			payload: data
		})
		toastr.success('Success', 'The document has been deleted.')
	})
}

export const deleteResource = (resource, id, history, redirect = null) => dispatch => {
	axios.delete(`/api/${resource.toLowerCase()}/${id}`).then(res => {
		dispatch({
			type: C['DELETE_' + resource.toUpperCase()],
			payload: id
		})
		history.push(`/${resource.toLowerCase()}`)
	})
}

export const saveResource = (resource, formData, history = null, redirect = null) => dispatch => {
	
	const save = (resource, data, history, redirect) => {
		dispatch({
			type: C['UPDATE_' + resource.toUpperCase()],
			payload: data
		})
		
		if (undefined !== data.attachments) {
			data.attachments.forEach(item => {
				dispatch({
					type: C['UPDATE_ATTACHMENTS'],
					payload: item
				})
			})
		}

		toastr.success('Success', 'Your form has been submitted successfully.')
		if (history) {
			if (redirect) history.push(redirect)
			else history.push(`/${resource.toLowerCase()}/${data.id}/edit`)
		}
	}

	let path = `/api/${resource.toLowerCase()}`
	if (formData.get('id') !== "") {
		path += `/${formData.get('id')}`
		formData.append('_method', 'PATCH')
	} 

	axios.post(path, formData)
	.then(res => res.data)
	.then(data => {
		save(resource, data, history, redirect)
	})
	.catch(err => {
		toastr.error('Error', 'There has been an error on form submit.')
	    console.log(err)
	})
}

export const fetchCollection = collection => dispatch => {
	
	dispatch({
		type: C['SET_FETCH_' + collection.toUpperCase()],
		payload: true
	})

	axios.get(`/api/${collection.toLowerCase()}`)
	.then(res => res.data)
	.then(data => {
		dispatch({
			type: C['SET_FETCH_' + collection.toUpperCase()],
			payload: false
		})
		dispatch({
			type: C['FETCH_' + collection.toUpperCase()],
			payload: data
		})
	})
	.catch(err => {
		toastr.error(collection, `There has been an error retrieving the ${collection.toLowerCase()}`)
	})
}

export const fetchCurrentUser = () => dispatch => {
	axios.get('/current-logged-in-user').then(res => res.data).then(user => {
		dispatch({
			type: C.FETCH_CURRENT_USER,
			data: user
		})

		dispatch({
			type: C.SET_CURRENT_VIEW,
			payload: user.admin
		})
	})
}

export const fetchTenant = () => dispatch => {
	axios.get('/current-tenant').then(res => res.data).then(tenant => {
		dispatch({
			type: C.FETCH_TENANT,
			data: tenant
		})
	})
}