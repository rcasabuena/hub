import axios from 'axios'
import C from '../constants'
import { toastr } from 'react-redux-toastr'

export const createNews = (values, history) => dispatch => {

	axios.post('/api/news', values)
	.then(res => res.data.data)
	.then(data => {
		dispatch({
			type: C.ADD_NEWS,
			payload: data
		})
		toastr.success('The title', 'The message')
		history.push('/news')
	})
	.catch(err => {
		toastr.error('The title', 'The message')
	    console.log(err.responseJSON)
	})
}

export const fetchNews = () => dispatch => {
	axios.get('/api/news')
	.then(res => res.data.data)
	.then(news => {
		dispatch({
			type: C.CANCEL_FETCH_NEWS,
			payload: false
		})
		dispatch({
			type: C.FETCH_NEWS,
			payload: news
		})
	})
	.catch(err => {
		toastr.error('News', 'There has been an error retrieving the news articles')
	    console.log(err.responseJSON)
	})
}