window._ = require('lodash');
window.Popper = require('popper.js').default;

window._globalFuseOptions = {
	shouldSort: true,
	tokenize: true,
	findAllMatches: true,
	threshold: 0.4,
	location: 0,
	distance: 1000,
	maxPatternLength: 32,
	minMatchCharLength: 1
}

window._globalFuseKeys = {
	news: ["title", "headline", "bodyText"],
	users: ["name", "email", "role"],
	tasks: ["title", "body"],
	topics: ["title", "body"],
	questions: ["title", "topic_name", "body"],
	locations: ["name"],
  documents: ["title", "aboutText"],
  downloads: ["username", "filename", "location"],
  events: ["title", "body", "venue"],
  forum: ["title", "bodyText"]
}

window._mimeTypes = {
  'application/octet-stream' : 'file-o',
  'application/postscript' : 'file-o',
  'audio/x-aiff' : 'file-audio-o',
  'audio/basic' : 'file-audio-o',
  'video/x-msvideo' : 'file-video-o',
  'text/plain' : 'file-text-o',
  'image/x-ms-bmp' : 'file-image-o',
  'application/x-cdf' : 'file-o',
  'application/x-csh' : 'file-o',
  'text/css' : 'file-text-o',
  'application/msword' : 'file-word-o',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document' : 'file-word-o',
  'application/x-dvi' : 'file-video-o',
  'message/rfc822' : 'file-text-o',
  'text/x-setext' : 'file-text-o',
  'image/gif' : 'file-image-o',
  'application/x-gtar' : 'file-o',
  'application/x-hdf' : 'file-o',
  'text/html' : 'file-text-o',
  'image/jpeg' : 'file-image-o',
  'application/x-javascript' : 'file-text-o',
  'application/x-latex' : 'file-o',
  'video/mpeg' : 'file-video-o',
  'application/x-troff-man' : 'file-o',
  'application/x-troff-me' : 'file-o',
  'application/x-mif' : 'file-o',
  'video/quicktime' : 'file-video-o',
  'video/x-sgi-movie' : 'file-video-o',
  'audio/mpeg' : 'file-audio-o',
  'video/mp4' : 'file-video-o',
  'application/x-troff-ms' : 'file-o',
  'application/x-netcdf' : 'file-o',
  'application/oda' : 'file-o',
  'image/x-portable-bitmap' : 'file-image-o',
  'application/pdf' : 'file-pdf-o',
  'application/x-pkcs12' : 'file-o',
  'image/x-portable-graymap' : 'file-image-o',
  'image/png' : 'file-image-o',
  'image/x-portable-anymap' : 'file-image-o',
  'application/vnd.ms-powerpoint' : 'file-powerpoint-o',
  'image/x-portable-pixmap' : 'file-image-o',
  'text/x-python' : 'file-text-o',
  'application/x-python-code' : 'file-o',
  'audio/x-pn-realaudio' : 'file-audio-o',
  'application/x-pn-realaudio' : 'file-audio-o',
  'image/x-cmu-raster' : 'file-image-o',
  'application/xml' : 'file-text-o',
  'image/x-rgb' : 'file-image-o',
  'application/x-troff' : 'file-o',
  'text/richtext' : 'file-text-o',
  'text/x-sgml' : 'file-text-o',
  'application/x-sh' : 'file-o',
  'application/x-shar' : 'file-o',
  'application/x-wais-source' : 'file-o',
  'application/x-shockwave-flash' : 'file-o',
  'application/x-tar' : 'file-archive-o',
  'application/x-tcl' : 'file-o',
  'application/x-tex' : 'file-o',
  'application/x-texinfo' : 'file-o',
  'image/tiff' : 'file-image-o',
  'text/tab-separated-values' : 'file-text-o',
  'application/x-ustar' : 'file-o',
  'text/x-vcard' : 'file-text-o',
  'audio/x-wav' : 'file-audio-o',
  'image/x-xbitmap' : 'file-image-o',
  'application/vnd.ms-excel' : 'file-excel-o',
  'text/xml' : 'file-text-o',
  'text/csv' : 'file-text-o',
  'image/x-xpixmap' : 'file-image-o',
  'image/x-xwindowdump' : 'file-image-o',
  'application/zip' : 'file-archive-o'
}

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.compose = (...fns) => (...args) => {
  return fns.slice(0, -1).reduceRight((res, fn) => fn(res), 
    fns[fns.length -1].apply(null,args)
  );
};

window.pipe = (f1, ...fns) => (...args) => {
  return fns.reduce((res, fn) => fn(res), f1.apply(null,args));
};

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });
