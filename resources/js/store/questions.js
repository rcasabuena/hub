import C from '../constants'

const questions = (state = {}, action) => {
	
	let questionsCollection

	switch (action.type) {
		case C.SET_FETCH_QUESTIONS:
			
			return {
				...state,
				fetching: action.payload
			}

		case C.FETCH_QUESTIONS: 
			
			questionsCollection = action.payload

			return {
				...state,
				data: questionsCollection
			}

		case C.UPDATE_QUESTIONS: 
			let { data } = state
			questionsCollection = [...data.filter(doc => doc.id !== action.payload.id), action.payload]

			return {
				...state,
				data: questionsCollection
			}

		case C.DELETE_QUESTIONS:
			
			let newData = state.data.filter(doc => doc.id !== action.payload)

			return {
				...state,
				data: newData
			}

		default: 
			return state
	}
} 

export default questions