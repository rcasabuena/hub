import C from '../constants'

const currentUser = (state = {}, action) => {
	switch (action.type) {
		case C.FETCH_CURRENT_USER: 
			return action.data

		default: 
			return state
	}
}

export default currentUser