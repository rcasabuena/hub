import C from '../constants'

const events = (state = {}, action) => {
	
	let eventsCollection

	switch (action.type) {
		case C.SET_FETCH_EVENTS:
			
			return {
				...state,
				fetching: action.payload
			}

		case C.FETCH_EVENTS: 
			
			eventsCollection = action.payload

			return {
				...state,
				data: eventsCollection
			}

		case C.UPDATE_EVENTS: 
			let { data } = state
			eventsCollection = [...data.filter(event => event.id !== action.payload.id), action.payload]

			return {
				...state,
				data: eventsCollection
			}

		case C.DELETE_EVENTS:
			
			let newData = state.data.filter(event => event.id !== action.payload)

			return {
				...state,
				data: newData
			}

		default: 
			return state
	}
} 

export default events