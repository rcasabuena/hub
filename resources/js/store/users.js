import C from '../constants'

const users = (state = {}, action) => {
	switch (action.type) {
		case C.SET_FETCH_USERS:
			
			return {
				...state,
				fetching: action.payload
			}

		case C.FETCH_USERS: 
			return {
				...state,
				data: action.payload
			}

		case C.UPDATE_USERS: 

			let { data } = state
			data = data.filter(user => user.id !== action.payload.id)

			return {
				...state,
				data: [action.payload, ...data]
			}

		case C.DELETE_USERS:
			
			let newData = state.data.filter(user => user.id !== action.payload)

			return {
				...state,
				data: newData
			}


		default: 
			return state
	}
} 

export default users