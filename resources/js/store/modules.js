import C from '../constants'

const modules = (state = {}, action) => {
	
	let modulesCollection

	switch (action.type) {
		case C.SET_FETCH_MODULES:
			
			return {
				...state,
				fetching: action.payload
			}

		case C.FETCH_MODULES: 
			
			modulesCollection = action.payload

			return {
				...state,
				data: modulesCollection
			}

		case C.UPDATE_MODULES: 
			let { data } = state
			modulesCollection = [...data.filter(module => module.id !== action.payload.id), action.payload]

			return {
				...state,
				data: modulesCollection
			}

		case C.DELETE_MODULES:
			
			let newData = state.data.filter(module => module.id !== action.payload)

			return {
				...state,
				data: newData
			}

		default: 
			return state
	}
} 

export default modules