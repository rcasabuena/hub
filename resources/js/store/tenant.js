import C from '../constants'

const tenant = (state = {}, action) => {
	switch (action.type) {
		case C.FETCH_TENANT: 
			return action.data
		default: 
			return state
	}
} 

export default tenant