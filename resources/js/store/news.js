import C from '../constants'

const news = (state = {}, action) => {
	
	let newsCollection

	switch (action.type) {
		case C.SET_FETCH_NEWS:
			
			return {
				...state,
				fetching: action.payload
			}

		case C.FETCH_NEWS: 
			
			newsCollection = action.payload

			return {
				...state,
				data: newsCollection
			}

		case C.UPDATE_NEWS: 
			let { data } = state
			newsCollection = [...data.filter(news => news.id !== action.payload.id), action.payload]

			return {
				...state,
				data: newsCollection
			}

		case C.DELETE_NEWS:
			
			let newData = state.data.filter(news => news.id !== action.payload)

			return {
				...state,
				data: newData
			}

		default: 
			return state
	}
} 

export default news