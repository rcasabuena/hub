import C from '../constants'
import initialState from '../initialState'
import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { reducer as toastrReducer} from 'react-redux-toastr'

import tenant from './tenant'
import news from './news'
import events from './events'
import documents from './documents'
import modules from './modules'
import tasks from './tasks'
import todos from './todos'
import locations from './locations'
import attachments from './attachments'
import topics from './topics'
import questions from './questions'
import users from './users'
import groups from './groups'
import downloads from './downloads'
import currentUser from './currentUser'
import currentView from './currentView'

export default combineReducers({
	currentView,
	currentUser,
	tenant,
	news,
	documents,
	topics,
	questions,
	events,
	tasks,
	todos,
	users,
	groups,
	locations,
	attachments,
	downloads,
	modules,
	form: formReducer,
	toastr: toastrReducer
})