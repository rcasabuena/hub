import C from '../constants'

const currentView = (state = {}, action) => {
	switch (action.type) {
		case C.SET_CURRENT_VIEW: 
			return {
				...state,
				admin: action.payload
			}

		default: 
			return state
	}
}

export default currentView