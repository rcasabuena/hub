import C from '../constants'

const locations = (state = {}, action) => {
	
	let locationsCollection

	switch (action.type) {
		case C.SET_FETCH_LOCATIONS:
			
			return {
				...state,
				fetching: action.payload
			}

		case C.FETCH_LOCATIONS: 
			
			locationsCollection = action.payload

			return {
				...state,
				data: locationsCollection
			}

		case C.UPDATE_LOCATIONS: 
			let { data } = state
			locationsCollection = [...data.filter(doc => doc.id !== action.payload.id), action.payload]

			return {
				...state,
				data: locationsCollection
			}

		case C.DELETE_LOCATIONS:
			
			let newData = state.data.filter(doc => doc.id !== action.payload)

			return {
				...state,
				data: newData
			}

		default: 
			return state
	}
} 

export default locations