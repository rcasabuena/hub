import C from '../constants'

const groups = (state = {}, action) => {
	switch (action.type) {
		case C.SET_FETCH_GROUPS:
			
			return {
				...state,
				fetching: action.payload
			}

		case C.FETCH_GROUPS: 
			return {
				...state,
				data: action.payload
			}

		case C.UPDATE_GROUPS: 

			let { data } = state
			data = data.filter(user => user.id !== action.payload.id)

			return {
				...state,
				data: [action.payload, ...data]
			}

		case C.DELETE_GROUPS:
			
			let newData = state.data.filter(user => user.id !== action.payload)

			return {
				...state,
				data: newData
			}


		default: 
			return state
	}
} 

export default groups