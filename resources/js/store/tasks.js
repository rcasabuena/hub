import C from '../constants'

const tasks = (state = {}, action) => {
	
	let tasksCollection

	switch (action.type) {
		case C.SET_FETCH_TASKS:
			
			return {
				...state,
				fetching: action.payload
			}

		case C.FETCH_TASKS: 
			
			tasksCollection = action.payload

			return {
				...state,
				data: tasksCollection
			}

		case C.UPDATE_TASKS: 
			let { data } = state
			tasksCollection = [...data.filter(item => item.id !== action.payload.id), action.payload]

			return {
				...state,
				data: tasksCollection
			}

		case C.DELETE_TASKS:
			
			let newData = state.data.filter(item => item.id !== action.payload)

			return {
				...state,
				data: newData
			}

		default: 
			return state
	}
} 

export default tasks