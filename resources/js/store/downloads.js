import C from '../constants'

const downloads = (state = {}, action) => {
	switch (action.type) {
		case C.SET_FETCH_DOWNLOADS:
			
			return {
				...state,
				fetching: action.payload
			}

		case C.FETCH_DOWNLOADS: 
			return {
				...state,
				data: action.payload
			}

		case C.UPDATE_DOWNLOADS: 

			let { data } = state
			data = data.filter(user => user.id !== action.payload.id)

			return {
				...state,
				data: [action.payload, ...data]
			}

		case C.DELETE_DOWNLOADS:
			
			let newData = state.data.filter(user => user.id !== action.payload)

			return {
				...state,
				data: newData
			}


		default: 
			return state
	}
} 

export default downloads