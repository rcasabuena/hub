import C from '../constants'

const topics = (state = {}, action) => {
	
	let topicsCollection

	switch (action.type) {
		case C.SET_FETCH_TOPICS:
			
			return {
				...state,
				fetching: action.payload
			}

		case C.FETCH_TOPICS: 
			
			topicsCollection = action.payload

			return {
				...state,
				data: topicsCollection
			}

		case C.UPDATE_TOPICS: 
			let { data } = state
			topicsCollection = [...data.filter(doc => doc.id !== action.payload.id), action.payload]

			return {
				...state,
				data: topicsCollection
			}

		case C.DELETE_TOPICS:
			
			let newData = state.data.filter(doc => doc.id !== action.payload)

			return {
				...state,
				data: newData
			}

		default: 
			return state
	}
} 

export default topics