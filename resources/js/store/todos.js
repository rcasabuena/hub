import C from '../constants'

const taskUsers = (state = {}, action) => {
	
	let todosCollection

	switch (action.type) {
		case C.SET_FETCH_TODOS:
			
			return {
				...state,
				fetching: action.payload
			}

		case C.FETCH_TODOS: 

			return {
				...state,
				data: action.payload
			}

		case C.UPDATE_TODOS: 
			let { data } = state
			todosCollection = data.filter(item => item.id !== action.payload.id)
			//if (!action.payload.rejected) todosCollection = [...todosCollection, action.payload]
			todosCollection = [...todosCollection, action.payload]
			todosCollection.sort((a, b) => {
	            let pubA = new Date(a.deadline)
	            let pubB = new Date(b.deadline)

	            if (pubA < pubB) return -1
	            if (pubA > pubB) return 1
	            
	            return 0
	        })
			return {
				...state,
				data: todosCollection
			}

		case C.DELETE_TODOS:
			
			let newData = state.data.filter(item => item.id !== action.payload)

			return {
				...state,
				data: newData
			}

		default: 
			return state
	}
} 

export default taskUsers