import C from '../constants'

const documents = (state = {}, action) => {
	
	let documentsCollection

	switch (action.type) {
		case C.SET_FETCH_DOCUMENTS:
			
			return {
				...state,
				fetching: action.payload
			}

		case C.FETCH_DOCUMENTS: 
			
			documentsCollection = action.payload

			return {
				...state,
				data: documentsCollection
			}

		case C.UPDATE_DOCUMENTS: 
			let { data } = state
			documentsCollection = [...data.filter(doc => doc.id !== action.payload.id), action.payload]

			return {
				...state,
				data: documentsCollection
			}

		case C.DELETE_DOCUMENTS:
			
			let newData = state.data.filter(doc => doc.id !== action.payload)

			return {
				...state,
				data: newData
			}

		default: 
			return state
	}
} 

export default documents