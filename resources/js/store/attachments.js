import C from '../constants'

const attachments = (state = {}, action) => {
	
	let attachmentsCollection

	switch (action.type) {
		case C.SET_FETCH_ATTACHMENTS:
			
			return {
				...state,
				fetching: action.payload
			}

		case C.FETCH_ATTACHMENTS: 
			
			attachmentsCollection = action.payload

			return {
				...state,
				data: attachmentsCollection
			}

		case C.UPDATE_ATTACHMENTS: 
			let { data } = state
			attachmentsCollection = [...data.filter(attachment => attachment.id !== action.payload.id), action.payload]

			return {
				...state,
				data: attachmentsCollection
			}

		case C.DELETE_ATTACHMENTS:
			
			let newData = state.data.filter(attachment => attachment.id !== action.payload)

			return {
				...state,
				data: newData
			}

		default: 
			return state
	}
} 

export default attachments