import C from '../constants'
import appReducer from './reducers'
import thunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'

const consoleMessages = store => next => action => {
	let result
	
	console.groupCollapsed(`dispatching action => ${action.type}`)
	result = next(action)
	console.groupEnd()
	
	return result
}

const anotherMiddleware = store => next => action => {
	let result
	result = next(action)
	return result
}

export default (initialState = {}) => {
	return applyMiddleware(thunk, consoleMessages)(createStore)(appReducer, initialState)
}