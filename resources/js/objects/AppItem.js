export default class AppItem {
	constructor(id) {
		this.id = id
	}

	get formData() {
		let data = new FormData()
		
		Object.keys(this).forEach(key => {
			if (key === 'attachments') {
				this[key].forEach(file => data.append('attachments[]', file))
			} else {
				data.append(key, this[key])
			}
		})
		
		return data
	}
}