import AppItem from './AppItem'

export default class AppTask extends AppItem {
	constructor(id = "", title = "", body = "", repeat = "") {
		super(id)
		this.title = title
		this.body = body
		this.repeat = repeat
		this.attachments = []
	}
}