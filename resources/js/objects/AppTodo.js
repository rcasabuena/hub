import AppItem from './AppItem'

export default class AppTodo extends AppItem {
	constructor(id = "", task_id = "",  user_id = "", deadline = "", completed = false, rejected = false, accepted = false) {
		super(id)
		this.task_id = task_id
		this.user_id = user_id
		this.deadline = deadline
		this.completed = completed
		this.rejected = rejected
		this.accepted = accepted
	}
}