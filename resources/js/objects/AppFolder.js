import AppItem from './AppItem'

export default class AppFolder extends AppItem {
	constructor(id = "", title = "", about = "", draft = true, publish = "", expire = "") {
		super(id)
		this.title = title
		this.about = about
		this.draft = draft
		this.publish = publish
		this.expire = expire
		this.folder = true
		this.parent = ""
		this.file = null
	}
}