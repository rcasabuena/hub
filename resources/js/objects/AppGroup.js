import AppItem from './AppItem'

export default class AppGroup extends AppItem {
	constructor(id = "", name = "", users = []) {
		super(id)
		this.name = name
		this.users = users
	}
}