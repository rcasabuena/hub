import AppItem from './AppItem'

export default class AppNews extends AppItem {
	constructor(id = "", title = "", headline = "", body = "", draft = true, publish = "", expire = "", featured_image = "") {
		super(id)
		this.title = title
		this.headline = headline
		this.body = body
		this.draft = draft
		this.publish = publish
		this.expire = expire
		this.featured_image = featured_image
	}
}