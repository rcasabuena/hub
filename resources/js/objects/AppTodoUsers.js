import AppItem from './AppItem'

export default class AppTodoUsers extends AppItem {
	constructor(id = "", task_id = "",  users = [], deadline = "", completed = false) {
		super(id)
		this.task_id = task_id
		this.users = users
		this.deadline = deadline
		this.completed = completed
	}
}