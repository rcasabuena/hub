import AppItem from './AppItem'

export default class AppTopic extends AppItem {
	constructor(id = "", title = "", body = "") {
		super(id)
		this.title = title
		this.body = body
	}
}