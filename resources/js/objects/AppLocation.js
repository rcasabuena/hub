import AppItem from './AppItem'

export default class AppLocation extends AppItem {
	constructor(id = "", name = "", address_line1 = "", address_line2 = "", address_city = "", address_county = "", address_postcode = "", email = "", telephone = "") {
		super(id)
		this.name = name
		this.address_line1 = address_line1
		this.address_line2 = address_line1
		this.address_city = address_city
		this.address_county = address_county
		this.address_postcode = address_postcode
		this.email = email
		this.telephone = telephone
	}
	
	get address() {
		return "Address"
	}
}