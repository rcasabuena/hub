import AppItem from './AppItem'

export default class AppTodoComment extends AppItem {
	constructor(id = "", question_id = "", body = "") {
		super(id)
		this.question_id = question_id
		this.body = body
		this.attachments = []
	}
}