import AppItem from './AppItem'

export default class AppUser extends AppItem {
	constructor(id = "", name = "", email = "", password = "", admin = false, location_id = "", director = false) {
		super(id)
		this.name = name
		this.email = email
		this.password = password
		this.admin = admin
		this.director = director
		this.location_id = location_id
	}
}