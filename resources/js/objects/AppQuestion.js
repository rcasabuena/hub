import AppItem from './AppItem'

export default class AppQuestion extends AppItem {
	constructor(id = "", title = "", body = "", sticky = false, topic_id = "") {
		super(id)
		this.title = title
		this.body = body
		this.sticky = sticky
		this.topic_id = topic_id
		this.attachments = []
	}
}