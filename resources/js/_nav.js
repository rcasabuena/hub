export default {
  items: [
    {
      name: 'Home',
      url: '/',
      icon: 'icon-home',
      admin: false
    },
    {
      name: 'News',
      url: '/news',
      icon: 'icon-star',
      admin: false
    },
    {
      name: 'Documents',
      url: '/documents',
      icon: 'icon-docs',
      admin: false
    },
    {
      name: 'Downloads Log',
      url: '/downloads',
      icon: 'icon-cloud-download',
      admin: true
    },
    // {
    //   name: 'Events',
    //   url: '/events',
    //   icon: 'icon-calendar',
    //   admin: false
    // },
    {
      name: 'Tasks',
      url: '/tasks',
      icon: 'icon-list',
      admin: false,
      director: true
    },
    {
      name: 'Locations',
      url: '/locations',
      icon: 'icon-location-pin',
      admin: true
    },
    {
      name: 'Topics',
      url: '/topics',
      icon: 'icon-microphone',
      admin: true
    },
    {
      name: 'Questions',
      url: '/questions',
      icon: 'icon-question',
      admin: true
    },
    {
      name: 'Users',
      url: '/users',
      icon: 'icon-people',
      admin: true
    },
    {
      name: 'User Groups',
      url: '/groups',
      icon: 'icon-people',
      admin: true
    },
    {
      name: 'Profile',
      url: '/profile',
      icon: 'icon-user',
      admin: false
    },
    {
      name: 'Forum',
      url: '/forum',
      icon: 'icon-bubbles',
      admin: false
    },
    {
      name: 'Ask a question',
      url: '/forum/ask-a-question',
      icon: 'icon-bubble',
      //class: 'mt-auto',
      variant: 'secondary',
      admin: false
    }
  ],
};
