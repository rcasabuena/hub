const initialState = {
	currentView: {
		admin: true
	},
	currentUser: {
		name: '',
		email: '',
		admin: false
	},
	tenant: {
		name: '',
		logo: {
			web: '',
			mob: ''
		}
	},
	news: {
		fetching: true,
		data: []
	},
	documents: {
		fetching: true,
		data: []
	},
	topics: {
		fetching: true,
		data: []
	},
	questions: {
		fetching: true,
		data: []
	},
	events: {
		fetching: true,
		data: []
	},
	tasks: {
		fetching: true,
		data: []
	},
	todos: {
		fetching: true,
		data: []
	},
	users: {
		fetching: true,
		data: []
	},
	groups: {
		fetching: true,
		data: []
	},
	locations: {
		fetching: true,
		data: []
	},
	attachments: {
		fetching: true,
		data: []
	},
	downloads: {
		fetching: true,
		data: []
	},
	modules: {
		fetching: true,
		data: []
	}
}

export default initialState