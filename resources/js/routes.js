import React, { lazy } from 'react'
import DefaultLayout from './components/layout/DefaultLayout'

const Home = lazy(() => import('./components/views/Home/Home'))
const Documents = lazy(() => import('./components/views/Documents/Documents'))
const AddDocument = lazy(() => import('./components/views/Documents/AddDocument'))
const EditDocument = lazy(() => import('./components/views/Documents/EditDocument'))
const ViewDocument = lazy(() => import('./components/views/Documents/ViewDocument'))
const Downloads = lazy(() => import('./components/views/Downloads/Downloads'))
const News = lazy(() => import('./components/views/News/News'))
const AddNews = lazy(() => import('./components/views/News/AddNews'))
const EditNews = lazy(() => import('./components/views/News/EditNews'))
const NewsView = lazy(() => import('./components/views/News/NewsView'))
const Events = lazy(() => import('./components/views/Events/Events'))
const AddEvent = lazy(() => import('./components/views/Events/AddEvent'))
const EditEvent = lazy(() => import('./components/views/Events/EditEvent'))
const Tasks = lazy(() => import('./components/views/Tasks/Tasks'))
const AddTask = lazy(() => import('./components/views/Tasks/AddTask'))
const EditTask = lazy(() => import('./components/views/Tasks/EditTask'))
const ViewTask = lazy(() => import('./components/views/Tasks/ViewTask'))
const TodoView = lazy(() => import('./components/views/Tasks/TodoView'))
const ViewTodo = lazy(() => import('./components/views/Tasks/ViewTodo'))
const Users = lazy(() => import('./components/views/Users/Users'))
const AddUser = lazy(() => import('./components/views/Users/AddUser'))
const EditUser = lazy(() => import('./components/views/Users/EditUser'))
const ViewUser = lazy(() => import('./components/views/Users/ViewUser'))
const Groups = lazy(() => import('./components/views/Groups/Groups'))
const AddGroup = lazy(() => import('./components/views/Groups/AddGroup'))
const EditGroup = lazy(() => import('./components/views/Groups/EditGroup'))
const Forum = lazy(() => import('./components/views/Forum/Forum'))
const QuestionView = lazy(() => import('./components/views/Forum/QuestionView'))
const AskAQuestion = lazy(() => import('./components/views/Forum/AskAQuestion'))
const Topics = lazy(() => import('./components/views/Topics/Topics'))
const AddTopic = lazy(() => import('./components/views/Topics/AddTopic'))
const EditTopic = lazy(() => import('./components/views/Topics/EditTopic'))
const ViewTopic = lazy(() => import('./components/views/Topics/ViewTopic'))
const Questions = lazy(() => import('./components/views/Questions/Questions'))
const AddQuestion = lazy(() => import('./components/views/Questions/AddQuestion'))
const EditQuestion = lazy(() => import('./components/views/Questions/EditQuestion'))
const ViewQuestion = lazy(() => import('./components/views/Questions/ViewQuestion'))
const TopicView = lazy(() => import('./components/views/Forum/TopicView'))
const Locations = lazy(() => import('./components/views/Locations/Locations'))
const AddLocation = lazy(() => import('./components/views/Locations/AddLocation'))
const EditLocation = lazy(() => import('./components/views/Locations/EditLocation'))
const Profile = lazy(() => import('./components/views/Profile/Profile'))
const Settings = lazy(() => import('./components/views/Settings/Settings'))
const Search = lazy(() => import('./components/views/Search/Search'))

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home', component: DefaultLayout, admin: false },
  { path: '/home', exact: true, name: '', component: Home, admin: false },
  // { path: '/documents/new', exact: true, name: 'New', component: AddDocument, admin: true },
  // { path: '/documents/:unique_id/edit', exact: true, name: 'Edit', component: EditDocument, admin: true },
  // { path: '/documents/:unique_id/view', exact: true, name: 'View', component: ViewDocument, admin: true },
  { path: '/documents*', exact: true, name: '', component: Documents, admin: false },
  { path: '/downloads', exact: true, name: 'Downloads', component: Downloads, admin: true },
  // { path: '/documents/:unique_id', exact: true, name: '', component: Documents, admin: false },
  // { path: '/documents/folder/new', exact: true, name: '', component: Documents, admin: false },
  // { path: '/documents/file/new', exact: true, name: '', component: Documents, admin: false },
  // { path: '/documents/folder/new', exact: true, name: '', component: Documents, admin: false },
  // { path: '/documents/file/new', exact: true, name: '', component: Documents, admin: false },
  { path: '/news', exact: true, name: 'News', component: News, admin: false },
  { path: '/news/new', exact: true, name: 'New', component: AddNews, admin: true },
  { path: '/news/:unique_id/edit', exact: true, name: 'Edit', component: EditNews, admin: true },
  { path: '/news/:id', exact: true, name: '', component: NewsView, admin: false },
  { path: '/events/new', exact: true, name: 'New', component: AddEvent, admin: true },
  { path: '/events/:unique_id/edit', exact: true, name: 'Edit', component: EditEvent, admin: true },
  { path: '/events', exact: true, name: 'Events', component: Events, admin: false },

  { path: '/tasks', exact: true, name: 'Tasks', component: Tasks, admin: false, director: true },
  { path: '/tasks/new', exact: true, name: 'New', component: AddTask, admin: false, director: true },
  { path: '/tasks/:unique_id/edit', exact: true, name: 'Edit', component: EditTask, admin: false, director: true },
  { path: '/tasks/:unique_id/view', exact: true, name: 'View', component: ViewTask, admin: false, director: true },
  { path: '/todos', exact: true, name: 'Todo', component: TodoView, admin: false },
  { path: '/todos/:unique_id/view', exact: true, name: 'Todo', component: ViewTodo, admin: false },


  { path: '/users/new', exact: true, name: 'New', component: AddUser, admin: true },
  { path: '/users/:unique_id/edit', exact: true, name: 'Edit', component: EditUser, admin: true },
  { path: '/users/:unique_id/view', exact: true, name: 'Edit', component: ViewUser, admin: true },
  { path: '/users', exact: true, name: 'Users', component: Users, admin:true },
  { path: '/groups/new', exact: true, name: 'New', component: AddGroup, admin: true },
  { path: '/groups/:unique_id/edit', exact: true, name: 'Edit', component: EditGroup, admin: true },
  { path: '/groups', exact: true, name: 'Groups', component: Groups, admin:true },
  { path: '/locations', exact: true, name: 'Locations', component: Locations, admin:true },
  { path: '/locations/new', exact: true, name: 'New', component: AddLocation, admin: true },
  { path: '/locations/:unique_id/edit', exact: true, name: 'Edit', component: EditLocation, admin: true },
  { path: '/forum', exact: true, name: 'Forum', component: Forum, admin: false },
  { path: '/forum/ask-a-question', exact: true, name: 'Ask a question', component: AskAQuestion, admin: false },
  { path: '/forum/question/:unique_id', exact: true, name: 'Question', component: QuestionView, admin: false },
  { path: '/forum/topic/:unique_id', exact: true, name: 'Topic', component: TopicView, admin: false },
  { path: '/profile', exact: true, name: 'Profile', component: Profile, admin: false },
  { path: '/settings', exact: true, name: 'Settings', component: Settings, admin: true },
  { path: '/search', exact: true, name: 'Search', component: Search, admin: false },
  { path: '/topics', exact: true, name: 'Topics', component: Topics, admin:true },
  { path: '/topics/new', exact: true, name: 'New', component: AddTopic, admin: true },
  { path: '/topics/:unique_id/edit', exact: true, name: 'Edit', component: EditTopic, admin: true },
  { path: '/topics/:unique_id/view', exact: true, name: 'View', component: ViewTopic, admin: true },
  { path: '/questions', exact: true, name: 'Questions', component: Questions, admin:true },
  { path: '/questions/new', exact: true, name: 'New', component: AddQuestion, admin: true },
  { path: '/questions/:unique_id/edit', exact: true, name: 'Edit', component: EditQuestion, admin: true },
  { path: '/questions/:unique_id/view', exact: true, name: 'View', component: ViewQuestion, admin: true },
  // { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  // { path: '/theme', exact: true, name: 'Theme', component: Colors },
  // { path: '/theme/colors', name: 'Colors', component: Colors },
  // { path: '/theme/typography', name: 'Typography', component: Typography },
  // { path: '/base', exact: true, name: 'Base', component: Cards },
  // { path: '/base/cards', name: 'Cards', component: Cards },
  // { path: '/base/forms', name: 'Forms', component: Forms },
  // { path: '/base/switches', name: 'Switches', component: Switches },
  // { path: '/base/tables', name: 'Tables', component: Tables },
  // { path: '/base/tabs', name: 'Tabs', component: Tabs },
  // { path: '/base/breadcrumbs', name: 'Breadcrumbs', component: Breadcrumbs },
  // { path: '/base/carousels', name: 'Carousel', component: Carousels },
  // { path: '/base/collapses', name: 'Collapse', component: Collapses },
  // { path: '/base/dropdowns', name: 'Dropdowns', component: Dropdowns },
  // { path: '/base/jumbotrons', name: 'Jumbotrons', component: Jumbotrons },
  // { path: '/base/list-groups', name: 'List Groups', component: ListGroups },
  // { path: '/base/navbars', name: 'Navbars', component: Navbars },
  // { path: '/base/navs', name: 'Navs', component: Navs },
  // { path: '/base/paginations', name: 'Paginations', component: Paginations },
  // { path: '/base/popovers', name: 'Popovers', component: Popovers },
  // { path: '/base/progress-bar', name: 'Progress Bar', component: ProgressBar },
  // { path: '/base/tooltips', name: 'Tooltips', component: Tooltips },
  // { path: '/buttons', exact: true, name: 'Buttons', component: Buttons },
  // { path: '/buttons/buttons', name: 'Buttons', component: Buttons },
  // { path: '/buttons/button-dropdowns', name: 'Button Dropdowns', component: ButtonDropdowns },
  // { path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups },
  // { path: '/buttons/brand-buttons', name: 'Brand Buttons', component: BrandButtons },
  // { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
  // { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
  // { path: '/icons/flags', name: 'Flags', component: Flags },
  // { path: '/icons/font-awesome', name: 'Font Awesome', component: FontAwesome },
  // { path: '/icons/simple-line-icons', name: 'Simple Line Icons', component: SimpleLineIcons },
  // { path: '/notifications', exact: true, name: 'Notifications', component: Alerts },
  // { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
  // { path: '/notifications/badges', name: 'Badges', component: Badges },
  // { path: '/notifications/modals', name: 'Modals', component: Modals },
  // { path: '/widgets', name: 'Widgets', component: Widgets },
  // { path: '/charts', name: 'Charts', component: Charts },
  // { path: '/users', exact: true,  name: 'Users', component: Users },
  // { path: '/users/:id', exact: true, name: 'User Details', component: User },
];

export default routes
