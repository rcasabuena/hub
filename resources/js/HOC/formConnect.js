import React, { Component } from 'react'
import { connect } from 'react-redux'
import { saveResource, deleteResource } from '../actions'
import { withRouter } from 'react-router-dom'
import * as formUtils from '../functions/FormFunctions.js'

export default (ChildComponent, resource, mapStateToProps) => {
	
	class ComposedComponent extends Component {
		render() {
			return <ChildComponent {...this.props} formUtils={formUtils} />
		}
	}

	const mapDispatchToProps = (dispatch, { history }) => {
		return {
		    save: data => { 
		       dispatch(
		        	saveResource(resource, data, history)
		      	)
		    },
		    delete: id => {
		      	dispatch(
		        	deleteResource(resource, id, history)
		      	)
		    }
		}
	}

	const enhance = compose(
		withRouter,
		connect(mapStateToProps, mapDispatchToProps)
	)

	return enhance(ComposedComponent)
}