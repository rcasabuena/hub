require('./bootstrap')
import React from 'react'
// For IE 9-11 support
import 'react-app-polyfill/ie9'
// For IE 11 support
import 'react-app-polyfill/ie11'
import '../coreui/src/polyfill'
import { render } from 'react-dom'
import * as serviceWorker from './serviceWorker'
import App from './components/App'

import { fetchCurrentUser } from './actions'
import { fetchNews } from './actions/news'
import { Provider } from 'react-redux'

import initialState from './initialState'
import storeFactory from './store'
import { fetchTenant, fetchCollection } from './actions'

let state = initialState

const store = storeFactory(state);

//store.subscribe(() => console.log(store.getState()))

store.dispatch(
	fetchCurrentUser()
)

store.dispatch(
	fetchTenant()
)

store.dispatch(
	fetchCollection('News')
)

store.dispatch(
	fetchCollection('Tasks')
)

store.dispatch(
	fetchCollection('Todos')
)

store.dispatch(
	fetchCollection('Users')
)

store.dispatch(
	fetchCollection('Groups')
)

store.dispatch(
	fetchCollection('Documents')
)

store.dispatch(
	fetchCollection('Questions')
)

store.dispatch(
	fetchCollection('Topics')
)

store.dispatch(
	fetchCollection('Events')
)

store.dispatch(
	fetchCollection('Locations')
)

store.dispatch(
	fetchCollection('Attachments')
)

store.dispatch(
	fetchCollection('Downloads')
)

store.dispatch(
	fetchCollection('Modules')
)

render(
	<Provider store={store} >
		<App />
	</Provider>, 
	document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()