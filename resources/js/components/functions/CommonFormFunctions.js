export function handleSubmit(e) {
    e.preventDefault()
    const { item } = this.state
    this.props.save(item.formData)
}

export function handleSelect(res, meta) {
    let { item } = this.state
    item[meta.name] = res ? res.value : ""
    this.setState({item})
}

export function handleChange(e) {
  let { item } = this.state
  item[e.target.id] = e.target.value
	this.setState({item})
}

export function handleToggle(e, inverse = false) {
    let { item } = this.state
    item[e.target.id] = inverse ? !e.target.checked : e.target.checked
    this.setState({item})
}

export function handleUpload(e, name) {
    let { item } = this.state
    item[name] = e.target.files[0]
    e.preventDefault()
    this.setState({item})
}

export function handleEditorChange(data, name) {
    let { item } = this.state
    item[name] = data
    this.setState({item})
}

export function handleDelete(id) {
    this.props.delete(id)
}