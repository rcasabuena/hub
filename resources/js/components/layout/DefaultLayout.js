import React, { Component, Suspense, lazy } from 'react'
import classNames from 'classnames'
import { Redirect, Route, Switch, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { Container } from 'reactstrap'
import { AppAside, AppBreadcrumb, AppFooter, AppHeader, AppSidebar, AppSidebarFooter, AppSidebarForm, AppSidebarHeader, AppSidebarMinimizer, AppSidebarNav } from '@coreui/react'
// sidebar nav config
import navigation from '../../_nav'
// routes config
import routes from '../../routes'

const DefaultAside = lazy(() => import('./DefaultAside'))
const DefaultFooter = lazy(() => import('./DefaultFooter'))
const DefaultHeader = lazy(() => import('./DefaultHeader'))

const DefaultLayout = props => {
    const loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>
    const { nav, userRoutes, atHome } = props

    return (
      <div className="app">
        <AppHeader fixed>
          <Suspense  fallback={loading()}>
            <DefaultHeader />
          </Suspense>
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader />
            <AppSidebarForm />
            <Suspense>
              <AppSidebarNav navConfig={nav} {...props} />
            </Suspense>
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main 
                className={classNames({
                  'main': true,
                  'at-home': atHome 
                })}
                style={{
                  backgroundImage: "url('/images/bg1.jpg')",
                }}>
            <div className="mb-4"></div>
            {/*<AppBreadcrumb appRoutes={routes}/>*/}
            <Container fluid>
              <Suspense fallback={loading()}>
                <Switch>
                  {userRoutes.map((route, idx) => {
                    return route.component ? 
                      (
                        <Route
                          key={idx}
                          path={route.path}
                          exact={route.exact}
                          name={route.name}
                          render={props => (
                            <route.component {...props} />
                          )} 
                          />
                      ) : null
                    })}
                  <Redirect from="/" to="/home" />
                </Switch>
              </Suspense>
            </Container>
          </main>
          <AppAside fixed>
            <Suspense fallback={loading()}>
              <DefaultAside isOpen={true} />
            </Suspense>
          </AppAside>
        </div>
        <AppFooter>
          <Suspense fallback={loading()}>
            <DefaultFooter />
          </Suspense>
        </AppFooter>
      </div>
    )
}

const mapStateToProps  = (state, ownProps) => {
  let { items } = navigation
  let userRoutes = routes
  let atHome = ownProps.location.pathname === '/home' ? true : false

  if (!state.currentView.admin) {
    items = items.filter(item => !item.admin)
    userRoutes = userRoutes.filter(item => !item.admin)
    
    items = items.filter(item => {
      if (item.director != undefined) return item.director === state.currentUser.director
      return true
    })

    userRoutes = userRoutes.filter(item => {
      if (item.director != undefined) return item.director === state.currentUser.director
      return true
    })
  }
  
  let nav = {
    ...navigation,
    items
  }

  return {nav, userRoutes, atHome}
}

export default connect(mapStateToProps)(DefaultLayout)
