import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

const propTypes = {
  children: PropTypes.node
}

const defaultProps = {}

class DefaultFooter extends Component {
  
  render() {

    const { currentTenant } = this.props

    return (
      <React.Fragment>
        <span><Link to="/">{currentTenant.name}</Link> &copy; {(new Date()).getFullYear()}</span>
      </React.Fragment>
    );
  }
}

DefaultFooter.propTypes = propTypes
DefaultFooter.defaultProps = defaultProps

const mapStateToProps = (state) => {
  return {
    currentTenant: state.tenant
  }
}

export default connect(mapStateToProps)(DefaultFooter)
