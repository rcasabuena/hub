import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { Badge, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem, NavLink, Button } from 'reactstrap'
import PropTypes from 'prop-types'
import { AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react'
import C from '../../constants'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {  

  state = {
    adminView: false
  }

  handleLogout = e => {
    e.preventDefault()
    document.getElementById('logout-form').submit()
  }

  handleClick = (e, path) => {
    e.preventDefault()
    this.props.history.push(path)
  }

  setCurrentView = e => {
    e.preventDefault()
    const { adminView } = this.state
    this.props.switchView(!adminView)
    this.setState({adminView : !adminView})
  }

  componentDidMount() {
    const { admin: adminView } = this.props.currentView
    this.setState({adminView})
  }
  
  render() {
    const { currentUser, currentTenant, children, ...attributes } = this.props
    const token = window.axios.defaults.headers.common['X-CSRF-TOKEN']
    const { adminView } = this.state
    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: currentTenant.logo.web, height: 50, alt: currentTenant.name }}
          minimized={{ src: currentTenant.logo.mob, height: 50, alt: currentTenant.name }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />
        { adminView && <Nav navbar>
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <button type="button" className="d-md-down-none navbar-toggler text-muted"><i className="fa fa-th-large"></i></button>
            </DropdownToggle>
            <DropdownMenu left="true" style={{ left: 'auto' }}>
              <DropdownItem onClick={ e => this.handleClick(e, '/news/new') } ><i className="fa fa-star"></i> News</DropdownItem>
              <DropdownItem onClick={ e => this.handleClick(e, '/users/new') } ><i className="fa fa-user"></i> User</DropdownItem>
              <DropdownItem onClick={ e => this.handleClick(e, '/groups/new') } ><i className="fa fa-users"></i> User Group</DropdownItem>
              <DropdownItem onClick={ e => this.handleClick(e, '/tasks/new') } ><i className="fa fa-list"></i> Task</DropdownItem>
              <DropdownItem onClick={ e => this.handleClick(e, '/topics/new') } ><i className="fa fa-microphone"></i> Topic</DropdownItem>
              <DropdownItem onClick={ e => this.handleClick(e, '/questions/new') } ><i className="fa fa-question-circle"></i> Question</DropdownItem>
              <DropdownItem onClick={ e => this.handleClick(e, '/locations/new') } ><i className="fa fa-map-marker"></i> Location</DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav> }
        <Nav className="ml-auto" navbar>
          {currentUser.admin &&
            <Button className="mr-2" color="ghost-dark" size="sm" active={adminView} title="Switch to admin view" onClick={this.setCurrentView}>
              <i className="fa fa-lg fa-globe"></i>
            </Button>
          }
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <span>{currentUser.name}</span>
            </DropdownToggle>
            <DropdownMenu right style={{ right: 'auto' }}>
              {/*<DropdownItem onClick={ e => this.handleClick(e, '/profile') }><i className="fa fa-user"></i> Profile</DropdownItem>*/}
              <DropdownItem onClick={this.handleLogout}><i className="fa fa-lock"></i> Logout</DropdownItem>
              <form id="logout-form" action="/logout" method="POST" style={{display: 'none'}}>
                  <input type="hidden" name="_token" value={token}></input>
              </form>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
        <AppAsideToggler className="d-md-down-none" data-aside-toggler="true" />
        <AppAsideToggler className="d-lg-none" mobile />
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes
DefaultHeader.defaultProps = defaultProps

const mapStateToProps = state => {
  return {
    currentUser: state.currentUser,
    currentTenant: state.tenant,
    currentView: state.currentView
  }
}

const mapDispatchToProps = dispatch => {
  return {
    switchView: value => {
        dispatch({
          type: C.SET_CURRENT_VIEW,
          payload: value
        })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(DefaultHeader))
