import React, { Component } from 'react'
import moment from 'moment'
import { connect } from 'react-redux'
import { ListGroup, ListGroupItem } from 'reactstrap'
import Todo from './Todo'

class Todos extends Component {
	
	componentDidMount = () => {
	    const { todos, setGlobalTodos } = this.props
	    //console.log(todos.length)
	    //setGlobalTodos(todos.length)
	    // if (todos.length > 0) {
	    //   	document.querySelector('.navbar-toggler[data-aside-toggler="true"]').click()
	    // }
	}


	render() {
		const { todos } = this.props

		const completed = todos.filter(item => item.completed)
		const overdue = todos.filter(item => {
			return moment(item.deadline).isBefore(new Date, 'day')
		})
		const today = todos.filter(item => moment(item.deadline).isSame(new Date(), 'day'))
		const thisWeek = todos.filter(item => moment(item.deadline).isAfter(new Date(), 'day') && moment(item.deadline).isSame(new Date(), 'week'))
		const thisMonth = todos.filter(item => moment(item.deadline).isAfter(new Date(), 'week') && moment(item.deadline).isSame(new Date(), 'month'))
		const thisYear = todos.filter(item => moment(item.deadline).isAfter(new Date(), 'month') && moment(item.deadline).isSame(new Date(), 'year'))
		//console.log(moment(todos[0].deadline).isBefore(new Date, 'day') && (!todos[1].completed || (todos[1].completed && moment(todos[1]).isSame(new Date(), 'day'))))
		return (
			<ListGroup className="list-group-accent" tag={'div'}>
				{ todos.length < 1 && <p className="p-3">You do not have any global tasks outstanding.</p> }
				{ overdue.length > 0 &&  <ListGroupItem className="list-group-item-accent-danger bg-light text-center font-weight-bold text-muted text-uppercase small">Overdue</ListGroupItem> }
				{ overdue.map(item => <Todo key={item.id} {...item} status={'danger'} /> ) }
				{ today.length > 0 &&  <ListGroupItem className="list-group-item-accent-warning bg-light text-center font-weight-bold text-muted text-uppercase small">Today</ListGroupItem> }
				{ today.map(item => <Todo key={item.id} {...item} status={'warning'} /> ) }
				{ thisWeek.length > 0 &&  <ListGroupItem className="list-group-item-accent-primary bg-light text-center font-weight-bold text-muted text-uppercase small">This week</ListGroupItem> }
				{ thisWeek.map(item => <Todo key={item.id} {...item} status={'primary'} /> ) }
				{ thisMonth.length > 0 &&  <ListGroupItem className="list-group-item-accent-primary bg-light text-center font-weight-bold text-muted text-uppercase small">This month</ListGroupItem> }
				{ thisMonth.map(item => <Todo key={item.id} {...item} status={'primary'} /> ) }
				{ thisYear.length > 0 &&  <ListGroupItem className="list-group-item-accent-primary bg-light text-center font-weight-bold text-muted text-uppercase small">This year</ListGroupItem> }
				{ thisYear.map(item => <Todo key={item.id} {...item} status={'primary'} /> ) }
            </ListGroup>
		)
	}
}

const mapStateToProps = ({ todos, currentUser }) => {
	return {
		todos: todos.data.filter(item => item.locationId === "" && item.user_id === currentUser.id && !item.rejected && (!item.completed || (item.completed && moment(item.completed_at).isSame(new Date(), 'day'))))
	}
}

export default connect(mapStateToProps)(Todos)