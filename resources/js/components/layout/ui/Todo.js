import React, { Component } from 'react'
import moment from 'moment'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { ListGroupItem } from 'reactstrap'

class Todo extends Component {

	handleClick = e => {
		e.preventDefault()
		const { id } = this.props
		this.props.history.push({
			pathname: '/todos',
        	todoId: id
		})
	} 

	render() {
		const { task, deadline, id, completed } = this.props
		let { status } = this.props
		if (completed) status = 'success' 

		return (
			<ListGroupItem action tag="a" href={`/todo/${id}`} onClick={this.handleClick} className={`list-group-item-accent-${status} list-group-item-divider`}>
	            <div><strong>{task.title}</strong></div>
	            <small className="text-muted mr-3">
	              <i className="icon-clock"></i>&nbsp; {moment(deadline).format('DD MMMM YYYY')}
	            </small>
	        </ListGroupItem>
		)
	}
}

const mapStateToProps = ({ tasks }, { task_id }) => {
	return {
		task: tasks.data.find(item => item.id == task_id)
	}
}

export default withRouter(connect(mapStateToProps)(Todo))