import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Loadable from 'react-loadable'
import '../../coreui/src/App.scss'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css'
import ReduxToastr from 'react-redux-toastr'

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>

// Containers
const DefaultLayout = Loadable({
  loader: () => import('./layout'),
  loading
});

// Pages
// const Login = Loadable({
//   loader: () => import('./views/Pages/Login'),
//   loading
// });

// const Register = Loadable({
//   loader: () => import('./views/Pages/Register'),
//   loading
// });

// const Page404 = Loadable({
//   loader: () => import('./views/Pages/Page404'),
//   loading
// });

// const Page500 = Loadable({
//   loader: () => import('./views/Pages/Page500'),
//   loading
// });

class App extends Component {

  render() {

    return (
      <div>
        <BrowserRouter>
            <Switch>
              <Route path="/" name="Home" component={DefaultLayout} />
            </Switch>
        </BrowserRouter>
        <ReduxToastr
        timeOut={4000}
        newestOnTop={false}
        preventDuplicates
        position="top-right"
        transitionIn="fadeIn"
        transitionOut="fadeOut"
        closeOnToastrClick/>
      </div>
    );
  }
}

export default App;
