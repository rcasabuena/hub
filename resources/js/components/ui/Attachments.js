import React, { Component } from 'react'
import { connect } from 'react-redux'

class Attachments extends Component {
	render() {
		
		const { attachments } = this.props

		return (
			<div>
				{ attachments.length > 0 && <p className="mb-0 mt-3"><strong><i className="fa fa-paperclip mr-1"></i>Attachments:</strong></p> }
				{ attachments.length > 0 && <p className="mb-3">
					{ attachments.map(item => 
						{
							const icon = _mimeTypes[item.mime_type] ? _mimeTypes[item.mime_type] : 'file-o'
							return <a key={item.id} href={`/attachments/${item.id}/download`} className="mr-3"><i className={`fa fa-${icon} mr-1`}></i>&nbsp;{item.name}</a>
						}
					)}
				</p> }
			</div>
		)
	}
}

const mapStateToProps = ({ attachments }, { parentId }) => {
	return {
		attachments: attachments.data.filter(item => item.parentId == parentId)
	}
}



export default connect(mapStateToProps)(Attachments)