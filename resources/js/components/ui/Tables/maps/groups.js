import React from 'react'
import { Badge } from 'reactstrap'
import DropdownEditMenu from '../../DropdownEditMenu'

const users = [
	{
		label: "Name",
		value: item => {
			return (
				<div>{item.name}</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "",
		value: (item, resource) => {
			return (
				<DropdownEditMenu resource={resource} id={item.id} actions={['EDIT', 'DELETE']} />
			)
		},
		classNames: "text-center",
		admin: true
	}
]

export default users