import React from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'
import { Badge, Progress } from 'reactstrap'
import DropdownEditMenu from '../../DropdownEditMenu'
import DeadlineBadge from '../../DeadlineBadge'

const todos = [
	{
		label: "Name",
		value: item => {
			return (
				<div>
					<Link to={`/todos/${item.id}/view`}>
						{item.name}
					</Link>
					<DeadlineBadge item={item} />
				</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "Deadline",
		value: item => {
			return (
				<div>
					{moment(item.deadline).format('DD MMMM YYYY')}
				</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "Progress",
		value: item => {
			return (
				<div>
					{ item.completed && 
						<div>
							<strong>100%</strong>
                    		<Progress className="progress-xs mt-2" color="success" value="100" />
						</div> 
					}
					{ !item.completed && !item.rejected && 
						<div>
							<strong>0%</strong>
                    		<Progress className="progress-xs mt-2" color="success" value="0" />
						</div> 
					}
				</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "",
		value: (item, resource) => {
			return (
				<DropdownEditMenu resource={resource} id={item.id} actions={['DELETE']} />
			)
		},
		classNames: "text-center",
		admin: true
	}
]

export default todos