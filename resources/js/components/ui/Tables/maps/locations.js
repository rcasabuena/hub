import React from 'react'
import { Badge } from 'reactstrap'
import DropdownEditMenu from '../../DropdownEditMenu'

const locations = [
	{
		label: "Name",
		value: item => {
			return (
				<div>{item.name}</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "Address",
		value: item => {
			return (
				<div>{item.address}</div>
			)
		},
		classNames: "",
		admin: false
	},
	{
		label: "Email",
		value: item => {
			return (
				<div>{item.email}</div>
			)
		},
		classNames: "text-center",
		admin: false
	},
	{
		label: "Telephone",
		value: item => {
			return (
				<div>{item.telephone}</div>
			)
		},
		classNames: "text-center",
		admin: false
	},
	{
		label: "",
		value: item => {
			return (
				<DropdownEditMenu resource="locations" id={item.id} actions={['EDIT', 'DELETE']} />
			)
		},
		classNames: "text-center",
		admin: true
	}
]

export default locations