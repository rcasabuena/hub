import React from 'react'
import { Badge } from 'reactstrap'
import DropdownEditMenu from '../../DropdownEditMenu'

const events = [
	{
		label: "Title",
		value: item => {
			return (
				<div>{item.title}</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "Venue",
		value: item => {
			return (
				<div>{item.venue}</div>
			)
		},
		classNames: "text-center",
		admin: false
	},
	{
		label: "",
		value: item => {
			return (
				<DropdownEditMenu resource="events" id={item.id} actions={['EDIT', 'DELETE']} />
			)
		},
		classNames: "text-center",
		admin: true
	}
]

export default events