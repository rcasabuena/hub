import React from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'

const downloads = [
	{
		label: "Document",
		value: item => {
			const icon = _mimeTypes[item.mime_type] ? _mimeTypes[item.mime_type] : 'file-o'
			return (
				<div>
					<span style={{
						display: 'inline-block',
						width: '20px'
					}}><i className={`fa fa-${icon}`}></i></span>
					<Link to={`/documents/file/${item.documentId}`}>{item.filename}</Link>
					<strong className="pull-right">{moment(item.created_at).fromNow()}</strong>
				</div>
			)
		},
		class: "",
		admin: false
	}
]

export default downloads