import React from 'react'
import { Link } from 'react-router-dom'
import { Badge, Progress } from 'reactstrap'
import DropdownEditMenu from '../../DropdownEditMenu'

const tasks = [
	{
		label: "Title",
		value: item => {
			return (
				<div>
					<Link to={`/tasks/${item.id}/view`}>
						{item.title}
					</Link>
				</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "Progress",
		value: (item, resource, todos) => {
			const taskTodos = todos.filter(todo => todo.task_id == item.id)
			const active = taskTodos.filter(todo => !todo.rejected)
			const rejected = taskTodos.filter(todo => todo.rejected)
			const completed = taskTodos.filter(todo => todo.completed)
			return (
				<div>
					<strong>{completed.length}/{active.length} {active.length == 1 ? `Assigned` : `Assigned`} {active.length > 0 && <span>({((completed.length/active.length)*100).toFixed(2)}%)</span> }</strong>
                    <Progress className="progress-xs mt-2" color="success" value={(completed.length/active.length)*100} />
				</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "",
		value: (item, resource) => {
			return (
				<DropdownEditMenu resource={resource} id={item.id} actions={['EDIT', 'DELETE']} />
			)
		},
		classNames: "text-center",
		admin: true
	}
]

export default tasks