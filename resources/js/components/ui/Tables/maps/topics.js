import React from 'react'
import { Badge } from 'reactstrap'
import { Link } from 'react-router-dom'
import DropdownEditMenu from '../../DropdownEditMenu'

const topics = [
	{
		label: "Title",
		value: item => {
			return (
				<div>
					<Link to={`forum/topic/${item.id}`}>{item.title}</Link>
				</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "",
		value: item => {
			return (
				<DropdownEditMenu resource="topics" id={item.id} actions={['EDIT', 'DELETE']} />
			)
		},
		classNames: "text-center",
		admin: true
	}
]

export default topics