import React from 'react'
import { Badge } from 'reactstrap'
import { Link } from 'react-router-dom'
import DropdownEditMenu from '../../DropdownEditMenu'

const questions = [
	{
		label: "Title",
		value: item => {
			return (
				<div>
					<Link to={`/forum/question/${item.id}`}>{item.title}</Link>
				</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "Topic",
		value: item => {
			return (
				<div>
					{item.topic && <Link to={`forum/topic/${item.topic.id}`}>{item.topic.title}</Link>}
				</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "",
		value: item => {
			return (
				<DropdownEditMenu resource="questions" id={item.id} actions={['EDIT', 'DELETE']} />
			)
		},
		classNames: "text-center",
		admin: true
	}
]

export default questions