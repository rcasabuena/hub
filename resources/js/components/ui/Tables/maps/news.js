import React from 'react'
import { Badge } from 'reactstrap'
import moment from 'moment'
import DropdownEditMenu from '../../DropdownEditMenu'

const news = [
	{
		label: "Title",
		value: item => {
			return (
				<div>{item.title}</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "Status",
		value: item => {
			return (
				<div>
					{item.draft == true && <Badge color="warning">Draft</Badge>}
					{!item.draft && item.published && <Badge color="primary">Published</Badge>}
					{!item.draft && !item.published && <Badge color="success">Upcoming</Badge>}
				</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "Publish Date",
		value: item => {
			return (
				<div>{moment(item.publish).format('DD MMM YYYY')}</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "Featured Image",
		value: item => {
			return (
				<div><img src={item.featured_image} alt="" style={{height: '100px'}} /></div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "",
		value: (item, resource) => {
			return (
				<DropdownEditMenu resource={resource} id={item.id} actions={['EDIT', 'DELETE']} />
			)
		},
		classNames: "text-center",
		admin: true
	}
]

export default news