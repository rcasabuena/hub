import React from 'react'
import { Link } from 'react-router-dom'
import { Badge } from 'reactstrap'
import DropdownEditMenu from '../../DropdownEditMenu'

const users = [
	{
		label: "Name",
		value: item => {
			return (
				<div>
					<Link to={`/users/${item.id}/view`}>{item.name}</Link>
				</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "Email",
		value: item => {
			return (
				<div><a href={`mailto:${item.email}`}>{item.email}</a></div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "Role",
		value: item => {
			return (
				<div>{item.role}</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "",
		value: (item, resource) => {
			return (
				<DropdownEditMenu resource={resource} id={item.id} actions={['EDIT', 'DELETE']} />
			)
		},
		classNames: "text-center",
		admin: true
	}
]

export default users