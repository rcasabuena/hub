import React from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'
import { Badge, Progress } from 'reactstrap'
import DropdownEditMenu from '../../DropdownEditMenu'
import DeadlineBadge from '../../DeadlineBadge'

const userTodos = [
	{
		label: "Task",
		value: item => {
			return (
				<div>
					<Link to={`/todos/${item.id}/view`}>
						{item.taskName}
					</Link>
				</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "Status",
		value: item => {
			return (
				<div>
					<DeadlineBadge item={item} />
				</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "Deadline",
		value: item => {
			return (
				<div>
					{moment(item.deadline).format('DD MMMM YYYY')}
				</div>
			)
		},
		class: "",
		admin: false
	},
	{
		label: "Completed",
		value: item => {
			return (
				<div>
					{ item.completed && 
						<div>
							{moment(item.completed_at).format('DD MMMM YYYY')}
						</div> 
					}
					{ !item.completed && !item.rejected && 
						<div>
							N/A
						</div> 
					}
				</div>
			)
		},
		class: "",
		admin: false
	}
]

export default userTodos