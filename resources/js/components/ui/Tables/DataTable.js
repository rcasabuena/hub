import React from 'react'
import { connect } from 'react-redux'
import Fuse from 'fuse.js'
import { Table, Progress, Fade } from 'reactstrap'
import maps from './maps'
import DropdownEditMenu from '../DropdownEditMenu'
import Spinner from '../Spinner'

const DataTable = props => {

		const { tableMap, isAdmin, searchTerms, fetching, tableData, todos, tableMapActual } = props
    let tableDataFiltered = tableData
		const tMap = maps[tableMap]

    const searchOptions = {
      ..._globalFuseOptions,
      keys: _globalFuseKeys[tableMapActual]
    }

    if (undefined !== searchTerms && searchTerms.trim() !== "") {        
      const fuse = new Fuse(tableDataFiltered, searchOptions)
      tableDataFiltered = fuse.search(searchTerms.trim())
    } else {
      tableDataFiltered = tableData
    }

		return (
      <div>
        {!fetching && tableData.length < 1 && <p>There are no {tableMapActual} yet.</p>}
        {!fetching && tableData.length > 0 &&
          <Table id={tableMapActual} hover responsive className="table-outline mb-0 d-none d-sm-table">
            <thead className="thead-light">
              <tr>
                {
                  tMap.map((tHeader, idx) => !tHeader.admin || (isAdmin && tHeader.admin) ? <th key={idx} className={tHeader.classNames}>{tHeader.label}</th> : null)
                }
              </tr>
            </thead>
            <tbody>
              {
                tableDataFiltered.map((tRow, idx) => {
                  return (
                    <Fade in={true} tag="tr" key={idx}>
                      {
                        tMap.map((item, idx) => !item.admin || (isAdmin && item.admin) ? <td key={idx} className={item.classNames}>{item.value(tRow, tableMap, todos)}</td> : null)
                      }
                    </Fade>
                  )
                })
              }
            </tbody>
          </Table>
         }
         {fetching && <Spinner />}
      </div>
		)
}

const mapStateToProps = (state, { tableMap, filterBy, filterByValue }) => {
  let tableData = undefined == state[tableMap] ? [] : state[tableMap].data
  let fetching = undefined == state[tableMap] ? true : state[tableMap].fetching
  let tableMapActual = undefined == state[tableMap] ? "" : tableMap

  if (tableMap == 'userTodos') {
    tableData = state['todos'].data
    fetching = state['todos'].fetching
    tableMapActual = 'todos'
  }

  if (undefined !== filterBy && undefined !== filterByValue) tableData = tableData.filter(item => item[filterBy] == filterByValue)
  
  if (tableMap === 'tasks') {
    if (state.currentUser.role === 'USER') {
      if (state.currentUser.director) tableData = tableData.filter(item => item.locationId == state.currentUser.location.id)
      else tableData = []
    } else {
      tableData = tableData.filter(item => item.locationId == null)
    }
  }

  if (tableMap === 'tasks' && state.currentUser.role === 'USER') {
     
  }

  return {
    tableData,
    tableMapActual,
    fetching,
    isAdmin: state.currentUser.role === 'ADMIN'
  }
}

export default connect(mapStateToProps)(DataTable)