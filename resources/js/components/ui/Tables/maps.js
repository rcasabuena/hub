import React from 'react'
import { Badge } from 'reactstrap'
import DropdownEditMenu from '../DropdownEditMenu'
import locations from './maps/locations'
import users from './maps/users'
import groups from './maps/groups'
import news from './maps/news'
import tasks from './maps/tasks'
import todos from './maps/todos'
import userTodos from './maps/userTodos'
import events from './maps/events'
import topics from './maps/topics'
import downloads from './maps/downloads'
import questions from './maps/questions'

const maps = {
	locations,
	users,
	groups,
	news,
	events,
	tasks,
	todos,
	userTodos,
	topics,
	downloads,
	questions
}

export default maps