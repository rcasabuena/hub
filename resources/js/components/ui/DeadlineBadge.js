import React from 'react'
import moment from 'moment'
import { Badge } from 'reactstrap'

const DeadlineBadge = props => {
	
	const { item } = props

	return (
		<>
			{item.rejected && <Badge className="ml-1" color="danger">Rejected</Badge> }
			{!item.rejected && item.completed && <Badge className="ml-1" color="success">Completed</Badge> }
			{!item.rejected && !item.completed && moment(item.deadline).isBefore(new Date, 'day') && <Badge className="ml-1" color="danger">Overdue</Badge> }
			{!item.rejected && !item.completed && moment(item.deadline).isSame(new Date, 'day') && <Badge className="ml-1" color="warning">Due today</Badge> }
		</>
	)
}

export default DeadlineBadge