import React, { Component } from 'react'
import { confirmAlert } from 'react-confirm-alert'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { ButtonGroup, ButtonDropdown, DropdownItem, DropdownToggle, DropdownMenu, Card, CardBody, CardFooter } from 'reactstrap'
import { deleteResource } from '../../actions'
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css

class DropdownEditMenu extends Component {

	state = {
		card: false
	}

	handleClick = (e, action) => {
		
		const { id, resource } = this.props

		e.preventDefault()
		switch (action) {
			case "EDIT":
				this.props.history.push(`/${resource}/${id}/edit`)
				return true

			case "DELETE":
				confirmAlert({
				  customUI: ({ onClose }) => {
				    return (
				      <Card>
				      	<CardBody>
				      		<h4>Confirm to delete</h4>
				        	<p>You want to delete this item?</p>
				      	</CardBody>
				      	<CardFooter>
				      		<button className="btn btn-dark mr-2" onClick={onClose}>Cancel</button>
					        <button className="btn btn-danger" onClick={() => {
					            this.props.deleteItem(resource, id, this.props.history)
								onClose()
					        }}>Delete</button>
				      	</CardFooter>
				      </Card>
				    )
				  }
				})
				return false

			default:
				this.props.history.push(`/${resource}/${id}/view`)
				return true
		}
	}

	render() {
		
		const { actions } = this.props

		return (
			<ButtonGroup className="float-right ml-2">
              <ButtonDropdown id='card1' isOpen={this.state.card} toggle={() => { this.setState({ card: !this.state.card }) }}>
                <DropdownToggle className="p-0" color="default">
                  <i className="icon-options-vertical"></i>
                </DropdownToggle>
                <DropdownMenu right>
                	{
                		actions.map((act, idx) => {
                			switch (act) {
                				case 'EDIT':
                					return <DropdownItem key={idx} onClick={e => this.handleClick(e, "EDIT")}><i className="fa fa-edit"></i> Edit</DropdownItem>

                				case 'DELETE': 
                					return <DropdownItem key={idx} onClick={e => this.handleClick(e, "DELETE")}><i className="fa fa-ban"></i> Delete</DropdownItem>

                				default:
                					return <DropdownItem key={idx} onClick={e => this.handleClick(e, "VIEW")}><i className="fa fa-eye"></i> View</DropdownItem>
                			}
                		})
                	}
                </DropdownMenu>
              </ButtonDropdown>
            </ButtonGroup>
		)
	}
}

const mapStateToProps = state => {
	return {}
}

const mapDispatchToProps = (dispatch) => {
	return {
		deleteItem: (resource, id, history) => {
	      	dispatch(
	        	deleteResource(resource, id, history)
	    	)
	    }
	}
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DropdownEditMenu))