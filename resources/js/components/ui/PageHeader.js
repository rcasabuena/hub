import React from 'react'
import { Link } from 'react-router-dom'
import { Button, CardHeader } from 'reactstrap'

const PageHeader = props => {
	
	const { pageTitle, pageInfo, pageResource } = props

	return (
		<div className="mb-4">
			<h4 className="card-title mb-0">{pageTitle}</h4>
			{pageInfo && <p className="text-muted">{pageInfo}</p> }
			{pageResource && 
				<Link to={`/${pageResource}/new`}>
					<Button color="primary">Add New</Button>
				</Link>
			}
		</div>
	)
}

export default PageHeader