import React from 'react'
import { AppSwitch } from '@coreui/react'
import { FormGroup, Col, Label, FormText } from 'reactstrap'

const ToggleSwitch = props => {
	
	const { inputName, inputLabel, handleToggle, switchColour, switchType, inputChecked, inputHelpText, inverseChecked, horizontal } = props

	return (
		<FormGroup row className="mb-3">
            <Col md={{
              size: !!!horizontal ? 3 : 12
            }}>
              	<Label htmlFor={inputName}>{inputLabel}</Label>
            </Col>
            <Col md={{
              size: !!!horizontal ? 9 : 12
            }}>
              	<AppSwitch variant={switchType} color={switchColour} id={inputName} checked={inputChecked} onChange={e => handleToggle(e, inverseChecked)}  />
              	{inputHelpText && inputHelpText.trim() !== ""  && <FormText color="muted">{inputHelpText}</FormText>}
            </Col>
    </FormGroup>
	)
}

export default ToggleSwitch