import React from 'react'
import { FormGroup, Col, Input, FormText, Label } from 'reactstrap'

const FileInput = props => {
	
	const { inputName, inputDefaultValue, inputLabel, handleUpload, inputHelpText, inputError, isRequired, horizontal } = props

	return (
	    <FormGroup row className="mb-3">
            <Col md={{
              size: !!!horizontal ? 3 : 12
            }}>
              	<Label htmlFor={inputName}>{inputLabel}</Label>
            </Col>
            <Col md={{
              size: !!!horizontal ? 9 : 12
            }}>
              	<Input 
                  required={isRequired ? true : false} 
                  type="file" 
                  id={inputName} 
                  onChange={e => handleUpload(e, inputName)} 
                />
              	{inputHelpText && inputHelpText.trim() !== ""  && <FormText color="muted">{inputHelpText}</FormText>}
                { inputDefaultValue && inputDefaultValue.trim() != "" && <img src={inputDefaultValue} style={{
                height: '150px'
                }} /> }
            </Col>
        </FormGroup>
	)
}

export default FileInput