import React from 'react'
import { FormGroup, Col, Input, FormText, Label } from 'reactstrap'

const DateInput = props => {
	
	const { inputName, inputDefaultValue, inputLabel, handleChange, inputHelpText, inputError, isRequired, horizontal } = props

	return (
		<FormGroup row className="mb-3">
        <Col md={{
          size: !!!horizontal ? 3 : 12
        }}>
          <Label htmlFor={inputName}>{inputLabel}</Label>
        </Col>
        <Col md={{
          size: !!!horizontal ? 9 : 12
        }}>
          <Input 
            required={isRequired ? true : false} 
            type="date" 
            id={inputName} 
            onChange={handleChange} 
            defaultValue={inputDefaultValue} 
          />
          {inputHelpText && inputHelpText.trim() !== ""  && <FormText color="muted">{inputHelpText}</FormText>}
        </Col>
    </FormGroup>
	)
}

export default DateInput