import React from 'react'
import { FormGroup, Button } from 'reactstrap'

const FormSubmitActions = props => {
	const { id, handleDelete } = props
	return (
		<FormGroup>
            <hr />
            <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
            {(id.trim() !== "") && <Button size="sm" color="danger" onClick={() => handleDelete(id)}><i className="fa fa-ban"></i> Delete</Button> } 
        </FormGroup>
	)
}

export default FormSubmitActions