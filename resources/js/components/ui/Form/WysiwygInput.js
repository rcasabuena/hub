import React, { Component } from 'react'
import { EditorState, convertToRaw, ContentState, convertFromHTML } from 'draft-js'
import draftToHtml from 'draftjs-to-html'
import { Editor } from 'react-draft-wysiwyg'
import { FormGroup, Col, Label, FormText } from 'reactstrap'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

class WysiwygInput extends Component {
	
	state = {
		editorState: EditorState.createEmpty()
	}

	handleThisEditorChange = editorState => {
		const { handleEditorChange, inputName } = this.props
		this.setState({editorState})
		handleEditorChange(draftToHtml(convertToRaw(editorState.getCurrentContent())), inputName)
	}

	componentDidMount = () => {
		
		const { body } = this.props

		if (body && body.trim() != "" && convertFromHTML(body).contentBlocks !== null) {
	      this.setState({
	        editorState: EditorState.createWithContent(
	          ContentState.createFromBlockArray(
	            convertFromHTML(body)
	          )
	        )
	      })
	    }
	}

	render() {
		
		const { inputLabel, inputName, inputHelpText, body, handleEditorChange, horizontal } = this.props
		let { editorState } = this.state

		return (
			<FormGroup row className="mb-3">
	            <Col md={{
	              size: !!!horizontal ? 3 : 12
	            }}>
	              	<Label htmlFor={inputName}>{inputLabel}</Label>
	            </Col>
	            <Col md={{
	              size: !!!horizontal ? 9 : 12
	            }}>
		            <Editor 
		                editorState = { editorState }
		                editorClassName="form-control"
		                onEditorStateChange={this.handleThisEditorChange}
		            />
	              	{inputHelpText && inputHelpText.trim() !== ""  && <FormText color="muted">{inputHelpText}</FormText>}
	            </Col>
	        </FormGroup>
		)
	}
} 

export default WysiwygInput