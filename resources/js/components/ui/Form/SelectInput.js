import React from 'react'
import Select from 'react-select'
import { FormGroup, Col, Input, FormText, Label } from 'reactstrap'

const SelectInput = props => {

	const { inputName, inputLabel, selectValue, selectOptions, handleSelect, inputHelpText, horizontal } = props

	return (
		<FormGroup row className="mb-3">
            <Col md={{
              size: !!!horizontal ? 3 : 12
            }}>
              	<Label htmlFor={inputName}>{inputLabel}</Label>
            </Col>
            <Col md={{
              size: !!!horizontal ? 9 : 12
            }}>
              	<Select 
                  options={selectOptions}
                  defaultValue={selectValue}
                  name={inputName}
                  components={{
                    DropdownIndicator: null
                  }}
                  isClearable
                  onChange={handleSelect}
                />
              	{inputHelpText && inputHelpText.trim() !== ""  && <FormText color="muted">{inputHelpText}</FormText>}
            </Col>
        </FormGroup>
	)
}

export default SelectInput