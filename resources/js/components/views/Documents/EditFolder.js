import React, { Component } from 'react' 
import FolderForm from './FolderForm'
import addToolbar from './addToolbar'
import addFolder from './addFolder'
import { Nav, NavItem, NavLink, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, Badge, Button, ButtonGroup, Card, CardBody, Row, Col } from 'reactstrap'


class EditFolder extends Component {
	render() {
		return (
			<div className="animated fadeIn">
				<h4 className="card-title mb-4">Edit Folder</h4>
      			<FolderForm isNew={false} />
			</div>
		)
	}
}

export default addFolder(addToolbar(EditFolder))