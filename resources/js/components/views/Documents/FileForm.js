import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import moment from 'moment'
import TextInput from '../../ui/Form/TextInput'
import FileInput from '../../ui/Form/FileInput'
import WysiwygInput from '../../ui/Form/WysiwygInput'
import DateInput from '../../ui/Form/DateInput'
import ToggleSwitch from '../../ui/Form/ToggleSwitch'
import FormSubmitActions from '../../ui/Form/FormSubmitActions'
import * as formUtils from '../../../functions/FormFunctions.js'
import { saveResource, deleteResource } from '../../../actions'
import { Form } from 'reactstrap'
import AppFolder from '../../../objects/AppFolder'
import addFolder from './addFolder'

class FileForm extends Component {

  constructor(props) {
    super(props)
    this.handleChange = formUtils.handleChange.bind(this)
    this.handleToggle = formUtils.handleToggle.bind(this)
    this.handleUpload = formUtils.handleUpload.bind(this)
    this.handleEditorChange = formUtils.handleEditorChange.bind(this)
    this.handleDelete = formUtils.handleDelete.bind(this)
    this.handleSubmit = formUtils.handleSubmit.bind(this)
  }

  state = {
    item: new AppFolder()
  }

  componentDidMount() {
    const { item, isNew, folder } = this.props

    item.parent = folder != undefined ? (isNew ? folder.id : folder.parent) : ""
    item.folder = false
    item.title = "Temp" 
    
    this.setState({item})
  }
  
  render() {
    const { id, title, draft, about, updated, publish, expire } = this.props.item
    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <ToggleSwitch 
            handleToggle={this.handleToggle} 
            inputChecked={!draft} 
            inputLabel="Published" 
            inputName="draft" 
            inputHelpText="" 
            switchType="pill" 
            switchColour="primary" 
            inverseChecked={true}
          />
          <FileInput 
            handleUpload={this.handleUpload} 
            inputDefaultValue={null} 
            inputName="file" 
            inputLabel="File" 
            inputHelpText="This is a help text" 
          />
          {/*<TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={title} 
            inputName="title" 
            inputLabel="Title" 
            inputHelpText="This is a help text" 
          />*/}
          <WysiwygInput 
            inputName="about"
            inputLabel="About"
            inputHelpText="This is a help text" 
            body={about}
            handleEditorChange={this.handleEditorChange}
          />
          <DateInput 
            handleChange={this.handleChange} 
            inputDefaultValue={moment(publish).format('YYYY-MM-DD')} 
            inputName="publish" 
            inputLabel="Publish Date" 
            inputHelpText="This is a help text" 
          />
          <DateInput 
            handleChange={this.handleChange} 
            inputDefaultValue={moment(expire).format('YYYY-MM-DD')} 
            inputName="expire" 
            inputLabel="Expire Date" 
            inputHelpText="This is a help text" 
          />
          <FormSubmitActions 
            id={id}
            handleDelete={this.handleDelete}
          />
        </Form>
      </div>
    )
  }

}

const mapStateToProps = ({ documents, currentUser }, { match, isNew }) => {
  const doc = documents.data.find(item => item.id === match.params.unique_id && !isNew)
  const id = doc ? doc.id : "",
      title = doc ? doc.title: "",
      about = doc ? doc.about: "",
      draft = doc ? doc.draft: true,
      publish = doc ? doc.publish: "",
      expire = doc ? doc.expire: ""
  return {
      item: new AppFolder(id, title, about, draft, publish, expire)
  }
}

const mapDispatchToProps = (dispatch, { match, history }) => {
  
  const { unique_id } = match.params
  let redirect = '/documents'
  redirect += unique_id ? `/folder/${unique_id}` : ""
  
  return {
    save: data => { 
      dispatch(
        saveResource('Documents', data, history, redirect)
      )
    },
    delete: id => {
      dispatch(
        deleteResource('Documents', id, history, redirect)
      )
    }
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(addFolder(FileForm)))