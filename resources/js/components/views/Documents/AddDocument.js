import React, { Component } from 'react'
import DocumentForm from './DocumentForm'
import { Card, CardBody, Col, Row } from 'reactstrap'

const AddDocument = () => {
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardBody>
              <h4 className="card-title mb-0">New Document</h4>
              <p className="text-muted">&nbsp;</p>
              <DocumentForm />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

export default AddDocument