import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Nav, NavItem, NavLink, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, Badge, Button, ButtonGroup, Card, CardBody, Row, Col } from 'reactstrap'

export default ChildComponent => {
	class ComposedComponent extends Component {
		
		constructor(props) {
		    super(props)
		    this.state = {
		      	dropdownOpen: false
		    }
	 	}

	 	handleClick = (e, action) => {
		    e.preventDefault()
			const { folder } = this.props

			let path = '/documents'
			path += folder != undefined ? `/folder/${folder.id}` : ""

			switch (action) {
				case 'NEW_FILE':
					this.props.history.push(`${path}/file/new`)
					return

				case 'EDIT_FOLDER':
					this.props.history.push(`${path}/edit`)
					return

				default:
					this.props.history.push(`${path}/folder/new`)
					return
			} 
		}

		toggle = () => {
		    this.setState({
		      	dropdownOpen: !this.state.dropdownOpen
		    });
	  	}

		render() {
			const { folder, currentView } = this.props 
			return (
				<div>
					<div className="toolbar">
						{currentView.admin &&
							<ButtonDropdown direction="right" size="lg" className="mb-3" isOpen={this.state.dropdownOpen} toggle={this.toggle}>
			                	<DropdownToggle caret color="light">
			                		<i className="fa fa-lg fa-folder-open mr-2"></i>
			                		{folder == undefined && `Home`}
			                		{folder != undefined && `${folder.title}`}
			                	</DropdownToggle>
			                	<DropdownMenu>
			                  		<DropdownItem onClick={ e => this.handleClick(e, "NEW_FOLDER" ) } ><i className="fa fa-folder"></i> Add new folder</DropdownItem>
			                  		<DropdownItem onClick={ e => this.handleClick(e, "NEW_FILE") } ><i className="fa fa-file"></i> Upload a file</DropdownItem>
			                  		{folder != undefined && <DropdownItem onClick={ e => this.handleClick(e, "EDIT_FOLDER") } ><i className="fa fa-edit"></i> Edit folder</DropdownItem>}
			                	</DropdownMenu>
			              	</ButtonDropdown>
				        }
				        {!currentView.admin && 
							<Button direction="right" size="lg" className="mb-3" color="light">
								<i className="fa fa-lg fa-folder-open mr-2"></i>
		                		{folder == undefined && `Home`}
		                		{folder != undefined && `${folder.title}`}
							</Button>
				        }
					</div>
					<ChildComponent {...this.props} />
				</div>
			)
		}
	}

	const mapStateToProps = ({ currentView }) => {
		return {
			currentView
		}
	}

	return connect(mapStateToProps)(ComposedComponent)
}