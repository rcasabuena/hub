import React, { Component } from 'react' 
import addToolbar from './addToolbar'
import FolderTable from './ui/FolderTable'
import RecentDocuments from './ui/RecentDocuments'

class Home extends Component {
	render() {
		
		const { documents } = this.props

		return (
			<div className="animated fadeIn">
				<div>
					<h4>Recent Files</h4>
					<RecentDocuments />
				</div>
				<div className="mt-5">
					<h4>Root Folder</h4>
					<FolderTable folderId={null} />
				</div>
			</div>
		)
	}
}

export default addToolbar(Home)