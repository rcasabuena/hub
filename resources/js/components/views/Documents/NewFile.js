import React, { Component } from 'react'
import { connect } from 'react-redux'
import FileForm from './FileForm'
import { Card, CardBody, Col, Row } from 'reactstrap'
import addToolbar from './addToolbar'
import addFolder from './addFolder'

const NewFile = props => {
	
	const { folder } = props

  	return (
	    <div className="animated fadeIn">
	      	<h4 className="card-title mb-4">Upload a file {folder !== undefined && <span>in <em>{folder.title}</em></span>}</h4>
	      	<FileForm isNew={true} />
	    </div>
  	)
}

export default addFolder(addToolbar(NewFile))