import React, { Component } from 'react'
import { connect } from 'react-redux'
import FolderForm from './FolderForm'
import { Card, CardBody, Col, Row } from 'reactstrap'
import addToolbar from './addToolbar'
import addFolder from './addFolder'

const NewFolder = props => {
	
	const { folder } = props

  	return (
	    <div className="animated fadeIn">
	      	<h4 className="card-title mb-4">New Folder {folder !== undefined && <span>in <em>{folder.title}</em></span>}</h4>
	      	<FolderForm isNew={true} />
	    </div>
  	)
}

export default addFolder(addToolbar(NewFolder))