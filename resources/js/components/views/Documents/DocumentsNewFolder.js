import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import TextInput from '../../ui/Form/TextInput'
import WysiwygInput from '../../ui/Form/WysiwygInput'
import DateInput from '../../ui/Form/DateInput'
import ToggleSwitch from '../../ui/Form/ToggleSwitch'
import FormSubmitActions from '../../ui/Form/FormSubmitActions'
import { connect } from 'react-redux'
import { handleChange, handleToggle, handleUpload, handleEditorChange, handleDelete } from '../../functions/CommonFormFunctions'
import { saveResource, deleteResource } from '../../../actions'
import { Form } from 'reactstrap'

class DocumentsNewFolder extends Component {
	
	constructor(props) {
	    super(props)
	    this.handleChange = handleChange.bind(this)
	    this.handleToggle = handleToggle.bind(this)
	    this.handleUpload = handleUpload.bind(this)
	    this.handleEditorChange = handleEditorChange.bind(this)
	    this.handleDelete = handleDelete.bind(this)
	  }

	render() {
		return (
			<div className="animated fadeIn">
				<h1>Docs new folder</h1>
			</div>
		)
	}
}

export default DocumentsNewFolder