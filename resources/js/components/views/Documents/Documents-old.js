import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Input, InputGroup, InputGroupAddon, InputGroupText, Badge, Button, Card, CardBody, CardFooter, CardHeader, Col, Collapse, Fade, Row, Table } from 'reactstrap'
import DocumentCard from './ui/DocumentCard'
import ModuleSearchBox from '../ModuleSearchBox'

class Documents extends Component {

  state = {
    showAll: false,
    searching: false,
    searchTerm: ""
  }

  toggleShowAll = e => {
    
    let { showAll } = this.state

    e.preventDefault()
    this.setState({
      showAll: !showAll
    })
  }

  handleSearch = e => {
    e.preventDefault()
    this.setState({
      searching: e.target.value.trim() !== "",
      searchTerm: e.target.value.trim()
    })
  }

  componentDidMount = () => {
    
    const { isAdmin } = this.props

    this.setState({
      showAll: isAdmin
    })
  }

  render() {
    
    const { showAll, searching, searchTerm } = this.state
    const { documents, fetching, isAdmin } = this.props
    const alpha = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    const terms = _.split(searchTerm.trim().toLowerCase(), " ")

    documents.sort((a,b) => {
      var textA = a.title.toUpperCase();
      var textB = b.title.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0
    })

    const groupedDocs = _.groupBy(documents, item => {
      return item.title.toUpperCase()[0]
    })

    return (
      <div className="animated fadeIn">
        <ModuleSearchBox isAdmin={isAdmin} showAll={showAll} handleSearch={this.handleSearch} toggleShowAll={this.toggleShowAll} />
        <Row>
          <Col>
            <Card>
              <CardBody>
                <h4 className="card-title mb-0">Documents</h4>
                <p className="text-muted">&nbsp;</p>
                  <div id="accordion">
                    {
                      Array.from(alpha).map((letter, idx) => {
                        return (
                          <div key={idx}>
                            {undefined != groupedDocs[letter] && groupedDocs[letter].length > 0 && 
                              groupedDocs[letter].map((doc, idx) => {
                                if (!searching) {
                                  if (!showAll && (doc.draft || !doc.published)) return null
                                  return <DocumentCard {...doc} key={doc.id} isAdmin={isAdmin} showAll={showAll} isOpen={false} />
                                }

                                if (!showAll && (doc.draft || !doc.published)) return null
                                if (_.intersection(doc.search, terms).length < 1) return null

                                return <DocumentCard {...doc} key={doc.id} isAdmin={isAdmin} showAll={showAll} isOpen={false} />
                              })
                            }
                          </div>
                        )
                      })
                    }
                  </div> 
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    fetching: state.documents.fetching,
    documents: state.documents.data,
    isAdmin: state.currentUser.role === 'ADMIN'
  }
} 

export default connect(mapStateToProps)(Documents)