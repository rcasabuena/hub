import React, { Component } from 'react' 
import FileForm from './FileForm'
import addToolbar from './addToolbar'
import addFolder from './addFolder'
import { Nav, NavItem, NavLink, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, Badge, Button, ButtonGroup, Card, CardBody, Row, Col } from 'reactstrap'


class EditFile extends Component {
	render() {
		return (
			<div className="animated fadeIn">
				<h4 className="card-title mb-4">Edit File</h4>
      			<FileForm isNew={false} />
			</div>
		)
	}
}

export default addFolder(addToolbar(EditFile))