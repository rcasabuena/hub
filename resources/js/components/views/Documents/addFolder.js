import React, { Component } from 'react'
import { connect } from 'react-redux'

export default ChildComponent => {
	class ComposedComponent extends Component {
		render() {
			return <ChildComponent {...this.props} />
		}
	}

	const mapStateToProps = ({ documents }, { match }) => {
		return {
			folder: documents.data.find(item => item.id === match.params.unique_id)
		}
	}

	return connect(mapStateToProps)(ComposedComponent)
}