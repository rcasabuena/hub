import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Card, CardBody, Col, Row } from 'reactstrap'
import DocumentCard from './ui/DocumentCard'

const ViewDocument = props => {
  const { doc } = props
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardBody>
              <h4 className="card-title mb-0">View Document</h4>
              <p className="text-muted">&nbsp;</p>
              <DocumentCard {...doc} isAdmin={false} showAll={true} isOpen={true} fileUpload={true} />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

const mapStateToProps = (state, ownProps) => {
  return {
    doc: state.documents.data.find(item => item.id === ownProps.match.params.unique_id)
  }
}

export default connect(mapStateToProps)(ViewDocument)