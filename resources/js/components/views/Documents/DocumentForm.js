import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import TextInput from '../../ui/Form/TextInput'
import WysiwygInput from '../../ui/Form/WysiwygInput'
import DateInput from '../../ui/Form/DateInput'
import ToggleSwitch from '../../ui/Form/ToggleSwitch'
import FormSubmitActions from '../../ui/Form/FormSubmitActions'
import { connect } from 'react-redux'
import { handleChange, handleToggle, handleUpload, handleEditorChange, handleDelete } from '../../functions/CommonFormFunctions'
import { saveResource, deleteResource } from '../../../actions'
import { Form } from 'reactstrap'

class DocumentForm extends Component {
  constructor(props) {
    super(props)
    this.handleChange = handleChange.bind(this)
    this.handleToggle = handleToggle.bind(this)
    this.handleUpload = handleUpload.bind(this)
    this.handleEditorChange = handleEditorChange.bind(this)
    this.handleDelete = handleDelete.bind(this)
  }

  state = {
    id: "",
    title: "",
    about: "",
    draft: false,
    publish: "",
    expire: ""
  }

  handleSubmit = e => {
    e.preventDefault()

    const { id, title, about, draft, publish, expire } = this.state

    let data = new FormData()
    data.append('id', id)
    data.append('title', title)
    data.append('about', about)
    data.append('draft', draft)
    data.append('publish', publish)
    data.append('expire', expire)

    this.props.save(data, this.props.history)
  }

  componentDidMount() {
    const { id, title, about, draft, publish, expire } = this.props
    this.setState({ id, title, about, draft, publish, expire }) 
  }
  
  render() {

    const { id, title, about, draft, publish, expire } = this.props

    return (
      <div>
        <Form encType="multipart/form-data" className="form-horizontal" onSubmit={this.handleSubmit}>
          <ToggleSwitch 
            handleToggle={this.handleToggle} 
            inputChecked={!draft} 
            inputLabel="Published" 
            inputName="draft" 
            inputHelpText="" 
            switchType="pill" 
            switchColour="primary"
            inverseChecked={true}
          />
          <TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={title} 
            inputName="title" 
            inputLabel="Title" 
            inputHelpText="This is a help text" 
            inputType="text"
          />
          <WysiwygInput 
            inputName="about"
            inputLabel="About"
            inputHelpText="This is a help text" 
            body={about}
            handleEditorChange={this.handleEditorChange}
          />
          <DateInput 
            handleChange={this.handleChange} 
            inputDefaultValue={publish} 
            inputName="publish" 
            inputLabel="Publish Date" 
            inputHelpText="This is a help text" 
          />
          <DateInput 
            handleChange={this.handleChange} 
            inputDefaultValue={expire} 
            inputName="expire" 
            inputLabel="Expire Date" 
            inputHelpText="This is a help text" 
          />
          <FormSubmitActions 
            id={id}
            handleDelete={this.handleDelete}
          />
        </Form>
      </div>
    )
  }

}

const mapStateToProps = (state, ownProps) => {
  const doc = state.documents.data.find(item => item.id === ownProps.match.params.unique_id)

  return {
    id: doc ? doc.id : "",
    title: doc ? doc.title : "",
    about: doc ? doc.about : "",
    draft: doc ? doc.draft : true,
    publish: doc ? doc.publish_raw : "",
    expire: doc ? doc.expire_raw : "",
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    save: (data, history) => { 
      dispatch(
        saveResource('Documents', data, history)
      )
    },
    delete: (id, history) => {
      dispatch(
        deleteResource('Documents', id, history)
      )
    }
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DocumentForm))