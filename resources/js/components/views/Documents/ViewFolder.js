import React, { Component } from 'react' 
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import renderHTML from 'react-render-html'
import addToolbar from './addToolbar'
import addFolder from './addFolder'
import FolderTable from './ui/FolderTable'

class ViewFolder extends Component {
	render() {
		const { id, about, parent, parentName } = this.props.folder
		return (
			<div className="animated fadeIn">
				{renderHTML(about)}
				<FolderTable folderId={id} />
			</div>
		)
	}
}

export default addFolder(addToolbar(ViewFolder))