import React from 'react' 
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import addToolbar from './addToolbar'

const DocumentView = props => {
	
	const { title, id } = props.folder
	const { children } = props

	return (
		<div className="animated fadeIn">
			<h1>{title}</h1>
		</div>
	)
}

const mapStateToProps = ({ documents }, { match }) => {
	return {
		folder: documents.data.find(item => item.id === match.params.unique_id),
		children: documents.data.filter(item => item.parent == match.params.unique_id)
	}
}

export default withRouter(connect(mapStateToProps)(addToolbar(DocumentView)))