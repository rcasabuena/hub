import React, { Component } from 'react'
import { connect } from 'react-redux'

class FolderLocation extends Component {

	render() {
		
		const { item, documents } = this.props
		
		return (
			<p>
				<em>Here</em>
			</p>
		)
	}
}

const mapStateToProps = ({ documents }) => {
	return {
		documents: documents.data
	}
}

export default connect(mapStateToProps, null)(FolderLocation)