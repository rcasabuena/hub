import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import moment from 'moment'
import FileInput from '../../../ui/Form/FileInput'
import FormSubmitActions from '../../../ui/Form/FormSubmitActions'
import * as formUtils from '../../../../functions/FormFunctions.js'
import { saveResource, deleteResource } from '../../../../actions'
import { Form, Card, CardBody } from 'reactstrap'
import AppFolder from '../../../../objects/AppFolder'
import addFolder from '../addFolder'

class FileUploadForm extends Component {
  constructor(props) {
    super(props)
    this.handleUpload = formUtils.handleUpload.bind(this)
    this.handleSubmit = formUtils.handleSubmit.bind(this)
  }

  state = {
    item: new AppFolder()
  }

  componentDidMount() {
    const { item, isNew, folder } = this.props

    item.parent = folder != undefined ? (isNew ? folder.id : folder.parent) : ""
    item.folder = false
    
    this.setState({item})
  }
	
	render() {
		return (
			<Card className="bg-gray-100 animated fadeIn">
				<CardBody>
					<h6 className="mb-2"><i className="fa fa-upload"></i> Upload new version</h6>
					<Form className="form-horizontal" onSubmit={this.handleSubmit}>
				        <FileInput 
				            handleUpload={this.handleUpload} 
				            inputDefaultValue={null} 
				            inputName="file" 
				            inputLabel="" 
				            horizontal={true}
				            inputHelpText="This is a help text" 
				        />
				        <FormSubmitActions 
				            id={""}
				        />
				    </Form>
			    </CardBody>
			</Card>
		)
	}
}

const mapStateToProps = ({ documents, currentUser }, { match, isNew }) => {
  const doc = documents.data.find(item => item.id === match.params.unique_id && !isNew)
  const id = doc ? doc.id : "",
      title = doc ? doc.title: "",
      about = doc ? doc.about: "",
      draft = doc ? doc.draft: true,
      publish = doc ? doc.publish: "",
      expire = doc ? doc.expire: ""
  return {
      item: new AppFolder(id, title, about, draft, publish, expire)
  }
}

const mapDispatchToProps = (dispatch, { match, history }) => {
  
  const { unique_id } = match.params
  let redirect = '/documents'
  redirect += unique_id ? `/file/${unique_id}` : ""
  
  return {
    save: data => { 
      dispatch(
        saveResource('Documents', data, history, redirect)
      )
    }
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(addFolder(FileUploadForm)))