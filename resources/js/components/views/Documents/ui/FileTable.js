import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Table, Badge } from 'reactstrap'
import FileTableItem from './FileTableItem'

class FileTable extends Component {
	render() {
		
		const { files } = this.props.folder

		return (
      <div>
        {files.length < 1 && <p>This folder is empty.</p>}
        {files.length > 0 &&
    			<Table responsive className="documents-table">
            	<thead>
            		<tr>
                  <th style={{width: '1%'}} className="pl-0 pr-0"></th>
              		<th style={{width: '90%'}}>Name</th>
                  <th style={{width: '9%'}}></th>
            		</tr>
            	</thead>
            	<tbody>
            		{
            			files.map((item, idx) => <FileTableItem key={item.id} itemDetails={item} idx={idx} />)
            		}
            	</tbody>
          </Table>
        }
      </div>
		)
	}
}

const mapStateToProps = ({ documents }, { folderId }) => {
  return {
    folder: documents.data.find(item => item.id === folderId)
  }
}

export default connect(mapStateToProps)(FileTable)