import React, { Component } from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { Table, Badge } from 'reactstrap'
import FolderTableItem from './FolderTableItem'

class FolderTable extends Component {
	render() {
		
		let { documents, currentView } = this.props
    if (!currentView.admin) documents = documents.filter(item => !item.draft && item.published)

		return (
      <div>
        {documents.length < 1 && <p>This folder is empty.</p>}
        {documents.length > 0 &&
    			<Table responsive className="documents-table">
            	<thead>
            		<tr>
              		<th style={{width: '1%'}} className="pl-0 pr-0"></th>
                  <th style={{width: '90%'}}>Name</th>
                  <th style={{width: '9%'}}></th>
            		</tr>
            	</thead>
            	<tbody>
            		{documents.map((item, idx) => <FolderTableItem key={item.id} itemDetails={item} idx={idx} />)}
            	</tbody>
          </Table>
        }
      </div>
		)
	}
}

const mapStateToProps = ({ documents, currentView }, { folderId }) => {
	return {
		documents: documents.data.filter(item => item.parent == folderId),
    currentView
	}
}

export default connect(mapStateToProps)(FolderTable)