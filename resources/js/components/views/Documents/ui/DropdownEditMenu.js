import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { ButtonGroup, ButtonDropdown, DropdownItem, DropdownToggle, DropdownMenu } from 'reactstrap'
import { deleteResource } from '../../../../actions'

class DropdownEditMenu extends Component {

	state = {
		card: false
	}

	handleClick = (e, action) => {
		
		e.preventDefault()
		const { id, resource } = this.props

		switch (action) {
			case "EDIT":
				this.props.history.push(`/${resource.toLowerCase()}/${id}/edit`)
				return true

			case "DELETE":
				this.props.deleteItem(resource, id, this.props.history)
				return true

			default:
				this.props.history.push(`/${resource.toLowerCase()}/${id}/view`)
				return true
		}
	}

	render() {
		
		const { actions } = this.props
		
		return (
			<ButtonGroup className="float-right ml-2">
              <ButtonDropdown id='card1' isOpen={this.state.card} toggle={() => { this.setState({ card: !this.state.card }) }}>
                <DropdownToggle className="p-0" color="default">
                  <i className="icon-settings"></i>
                </DropdownToggle>
                <DropdownMenu right>
                	{
                		actions.map((act, idx) => {
                			switch (act) {
                				case 'EDIT':
                					return <DropdownItem key={idx} onClick={e => this.handleClick(e, "EDIT")}><i className="fa fa-edit"></i> Edit</DropdownItem>

                				case 'DELETE': 
                					return <DropdownItem key={idx} onClick={e => this.handleClick(e, "DELETE")}><i className="fa fa-ban"></i> Delete</DropdownItem>

                				default:
                					return <DropdownItem key={idx} onClick={e => this.handleClick(e, "VIEW")}><i className="fa fa-file"></i> Files</DropdownItem>
                			}
                		})
                	}
                </DropdownMenu>
              </ButtonDropdown>
            </ButtonGroup>
		)
	}
}

const mapStateToProps = state => {
	return {}
}

const mapDispatchToProps = (dispatch) => {
	return {
		deleteItem: (resource, id, history) => {
	      	dispatch(
	        	deleteResource(resource, id, history)
	    	)
	    }
	}
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DropdownEditMenu))