import React, { Component } from 'react'
import path from 'path'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { Form, FormText, FormGroup, Col, Label, Input, Button, Card, CardBody } from 'reactstrap'
import { AppSwitch } from '@coreui/react'
import { uploadDocumentFile } from '../../../../actions'

class FileUploadForm extends Component {

	state = {
		file: "",
		name: "",
		version: "",
		latest: false,
		document_id: "",
		mime_type: "",
		size: 0
	}

  	handleSubmit = e => {
    	e.preventDefault()

	    const { file, name, version, latest, document_id, size, mime_type } = this.state
	    
	    let data = new FormData()
	    data.append('file', file)
	    data.append('name', name)
	    data.append('version', version)
	    data.append('latest', latest)
	    data.append('size', size)
	    data.append('mime_type', mime_type)
	    data.append('document_id', document_id)

	    this.props.uploadFile(data)

	    this.setState({
	    	file: "",
			name: "",
			version: "",
			latest: false,
			document_id: "",
			mime_type: "",
			size: 0
	    })

	    document.getElementById('file').value = ""
	    document.getElementById('name').value = ""
	    document.getElementById('version').value = ""
  	}

  	handleChange = e => this.setState({
    	[e.target.id] : e.target.value
  	}) 

  	handleToggle = e => {
	    this.setState({
	      	[e.target.id]: e.target.checked
	    })
  	}

	handleUpload = e => {
	    e.preventDefault()
		const { name } = this.state
	    this.setState({
	      	file: e.target.files[0],
	      	mime_type: e.target.files[0].type,
	      	size: e.target.files[0].size,
	      	name: name.trim() != "" ? name : path.basename(e.target.files[0].name).replace(path.extname(e.target.files[0].name), '')
	    })
	}

	componentDidMount = () => {
		const { id } = this.props
		this.setState({
			document_id: id
		})
	}
	
	render() {

		const { name, version, latest } = this.state

		return (
			<Card className="bg-gray-100">
				<CardBody>
					<h4 className="mb-0"><i className="fa fa-upload"></i> Upload a file</h4>
					<p className="text-muted">&nbsp;</p>
					<Form className="form-horizontal" onSubmit={this.handleSubmit}>
				        <FormGroup row>
				            <Col md="3">
				              <Label htmlFor="name">Name</Label>
				            </Col>
				            <Col xs="12" md="9">
				              <Input type="text" id="name" onChange={this.handleChange} defaultValue={name} />
				              <FormText color="muted">This is a help text</FormText>
				            </Col>
				        </FormGroup>
				        <FormGroup row>
				            <Col md="3">
				              <Label htmlFor="version">Version</Label>
				            </Col>
				            <Col xs="12" md="9">
				              <Input type="text" id="version" onChange={this.handleChange} defaultValue={version} />
				              <FormText color="muted">This is a help text</FormText>
				            </Col>
				        </FormGroup>
				        <FormGroup row>
				            <Col md="3">
				              <Label htmlFor="latest">Latest Version</Label>
				            </Col>
				            <Col xs="12" md="9">
				              <AppSwitch className={'mx-1'} variant={'pill'} color={'primary'} id="latest" checked={latest} onChange={this.handleToggle}  />
				            </Col>
				        </FormGroup>
				        <FormGroup row>
				            <Col md="3">
				              <Label htmlFor="file">File</Label>
				            </Col>
				            <Col xs="12" md="9">
				              <Input type="file" id="file" onChange={this.handleUpload} />
				            </Col>
				        </FormGroup>
				        <FormGroup>
				            <hr />
				            <Button type="submit" size="sm" color="primary" className="mr-2"><i className="fa fa-dot-circle-o"></i> Submit</Button>
				        </FormGroup>
				    </Form>
			    </CardBody>
			</Card>
		)
	}
}

const mapStateToProps = state => {
	return {}
}

const mapDispatchToProps = dispatch => {
	return {
		uploadFile: data => {
			dispatch(
				uploadDocumentFile(data)
			)
		}
	}
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(FileUploadForm))