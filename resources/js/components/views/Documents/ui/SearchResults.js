import React, { Component } from 'react'
import { connect } from 'react-redux'
import Fuse from 'fuse.js'
import moment from 'moment'
import { Table, Badge } from 'reactstrap'
import FolderTableItemSearch from './FolderTableItemSearch'

class SearchResults extends Component {
	render() {
		
		let { documents, currentView, searchTerms } = this.props
    if (!currentView.admin) documents = documents.filter(item => !item.draft && item.published)

    if (searchTerms.trim() !== "") {  
      const searchOptions = {
        ..._globalFuseOptions,
        keys: _globalFuseKeys['documents']
      }      
      const fuse = new Fuse(documents, searchOptions)
      documents = fuse.search(searchTerms.trim())
    }

		return (
      <div>
        {documents.length < 1 && <p>No documents found.</p>}
        {documents.length > 0 &&
    			<Table responsive className="documents-table">
            	<thead>
            		<tr>
              		<th style={{width: '1%'}} className="pl-0 pr-0"></th>
                  <th style={{width: '90%'}}>Name</th>
                  <th style={{width: '9%'}}></th>
            		</tr>
            	</thead>
            	<tbody>
            		{documents.map((item, idx) => <FolderTableItemSearch key={item.id} itemDetails={item} idx={idx} />)}
            	</tbody>
          </Table>
        }
      </div>
		)
	}
}

const mapStateToProps = ({ documents, currentView }) => {
	return {
		documents: documents.data,
    currentView
	}
}

export default connect(mapStateToProps)(SearchResults)