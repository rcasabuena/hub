import React, { Component } from 'react'
import renderHTML from 'react-render-html'
import { Card, CardHeader, CardBody, Button, Table, Badge, Collapse } from 'reactstrap'
import DropdownEditMenu from './DropdownEditMenu'
import DocumentFilesTable from './DocumentFilesTable'
import FileUploadForm from './FileUploadForm'

class DocumentCard extends Component {
	state = {
		isOpen: false
	}

	toggleAccordion = () => {
		this.setState({
			isOpen: !this.state.isOpen
		})
	}

	componentDidMount = () => {
		const { isOpen } = this.props
		this.setState({isOpen})
	}

	render() {
		
		const { id, title, about, isAdmin, showAll, draft, published, files } = this.props
		const draftBadge = (showAll && draft) ? <Badge color="warning">Draft</Badge> : null
		const upcomingBadge = (showAll && !draft && !published) ? <Badge color="success">Upcoming</Badge> : null
		const publishedBadge = (showAll && !draft && published) ? <Badge color="primary">Published</Badge> : null

		return (
			<Card className="mb-2">
                <CardHeader>
                  <span className="h5">{title}</span> {draftBadge}{upcomingBadge}{publishedBadge}
                  <div className="card-header-actions">
                    {isAdmin && <DropdownEditMenu resource="Documents" id={id} actions={['EDIT', 'DELETE']} /> }
                    <a className="card-header-action btn btn-minimize" data-target="#collapseExample" onClick={this.toggleAccordion}><i className="icon-arrow-up"></i></a>
                  </div>
                </CardHeader>
                <Collapse isOpen={this.state.isOpen} id="collapseExample">
                  <CardBody>
                    <div>{renderHTML(about)}</div>
                    { files.length > 0 && <DocumentFilesTable files={files} isAdmin={isAdmin} id={id} title={title} /> }
                    { files.length < 1 && 
                    	<>
						<hr/>
							<p className="text-muted">There are no files yet.</p>
						<hr/>
						</>
                    }
                    { isAdmin && <FileUploadForm id={id} /> }
                  </CardBody>
                </Collapse>
            </Card>
		)
	}
}

export default DocumentCard