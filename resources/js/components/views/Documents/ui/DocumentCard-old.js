import React, { Component } from 'react'
import renderHTML from 'react-render-html'
import { Card, CardHeader, CardBody, Button, Table, Badge, Collapse } from 'reactstrap'
import DropdownEditMenu from '../../DropdownEditMenu'
import DocumentFilesTable from './DocumentFilesTable'

class DocumentCard extends Component {
	state = {
		isOpen: false
	}

	toggleAccordion = () => {
		this.setState({
			isOpen: !this.state.isOpen
		})
	}

	componentDidMount = () => {
		const { isOpen } = this.props
		this.setState({isOpen})
	}

	render() {
		
		const { id, title, about, isAdmin, showAll, draft, published, files } = this.props
		const draftBadge = (showAll && draft) ? <Badge color="warning">Draft</Badge> : null
		const upcomingBadge = (showAll && !draft && !published) ? <Badge color="success">Upcoming</Badge> : null
		const publishedBadge = (showAll && !draft && published) ? <Badge color="primary">Published</Badge> : null

		return (
			<>
			<Card>
                <CardHeader>
                  {title} {draftBadge}{upcomingBadge}{publishedBadge}
                  <div className="card-header-actions">
                    {isAdmin && <DropdownEditMenu resource="Documents" id={id} /> }
                    <a className="card-header-action btn btn-minimize" data-target="#collapseExample" onClick={this.toggleAccordion}><i className="icon-arrow-up"></i></a>
                  </div>
                </CardHeader>
                <Collapse isOpen={this.state.isOpen} id="collapseExample">
                  <CardBody>
                    <div>{renderHTML(about)}</div>
                    <DocumentFilesTable files={files} isAdmin={isAdmin} id={id} title={title} />
                  </CardBody>
                </Collapse>
            </Card>
			<Card className="mb-0">
                <CardHeader id="headingOne">
                  <Button block color="link" className="text-left m-0 p-0" onClick={this.toggleAccordion} aria-expanded={this.state.isOpen} aria-controls="collapseOne">
                    <h5 className="m-0 p-0 pr-1 pull-left">{title}</h5>
                    {draftBadge}{upcomingBadge}{publishedBadge}
                  </Button>
                </CardHeader>
                <Collapse isOpen={this.state.isOpen} data-parent="#accordion" id="collapseOne" aria-labelledby="headingOne">
                  <CardBody>
                  	{isAdmin && <DropdownEditMenu resource="Documents" id={id} /> }
                    <div>
                    	{renderHTML(about)}
                    </div>
                    <DocumentFilesTable files={files} isAdmin={isAdmin} id={id} title={title} />
                  </CardBody>
                </Collapse>
            </Card>
            </>
		)
	}
}

export default DocumentCard