import React from 'react'
import { Table, Badge } from 'reactstrap'
import FileUploadForm from './FileUploadForm'
import DropdownEditMenu from '../../DropdownEditMenu'
import FileRow from './FileRow'

const DocumentFilesTable = props => {
  const { files, title, isAdmin } = props
  const latest = true
  const fileRows = files.map(file => <FileRow key={file.id} isAdmin={isAdmin} {...file} />)

	return (
    <div>
        <Table responsive>
          <tbody>
          <tr>
            <th>Filename</th>
            <th>Size</th>
            <th>Date Uploaded</th>
            <th>Version</th>
            <th></th>
          </tr>
          {fileRows}
          </tbody>
        </Table>
    </div>
	)
} 

export default DocumentFilesTable