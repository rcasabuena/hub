import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import moment from 'moment'
import renderHTML from 'react-render-html'
import { Badge, Collapse, Button, Tooltip } from 'reactstrap'

class FolderTableItem extends Component {
	state = {
		isOpen: false,
		tooltipOpen: false,
		tooltipDownloadOpen: false
	}

	toggleCollapse = e => {
		e.preventDefault()
		const { isOpen } = this.state
		this.setState({
			isOpen: !isOpen
		})
	}
	toggleTooltip = e => {
    	this.setState({
      		tooltipOpen: !this.state.tooltipOpen
    	});
  	}
  	toggleDownloadTooltip = e => {
    	this.setState({
      		tooltipDownloadOpen: !this.state.tooltipDownloadOpen
    	});
  	}

	handleDownload = e => {
    	e.preventDefault()
    	window.location = e.currentTarget.dataset.href
  	}

	render() {
		
		const { isOpen, tooltipOpen, tooltipDownloadOpen } = this.state
		const { itemDetails: item, idx, currentView, search } = this.props
		let icon = 'file-o'
		if (item.files[0] !== undefined && _mimeTypes[item.files[0].mime_type] !== undefined) icon = _mimeTypes[item.files[0].mime_type] 

		return (
			<>
			<tr>
				<td className="pr-0 pl-0">
					{item.folder && <i className="fa fa-folder-o"></i>}
					{!item.folder && <i className={`fa fa-${icon}`}></i>}
				</td>
                <td>
                	{item.folder && <Link className="documents-table-link" to={`/documents/folder/${item.id}`}>{item.title}</Link>} 
                	{!item.folder && <Link className="documents-table-link" to={`/documents/file/${item.id}`}>{item.title}</Link> }
                	{currentView.admin && item.draft && !search && <Badge className="ml-2" color="secondary">Draft</Badge>}
                	{currentView.admin && !item.draft && item.published && !search && <Badge className="ml-2" color="primary">Published</Badge>}
                	{currentView.admin && !item.draft && !item.published && !search && <Badge className="ml-2" color="success">Upcoming</Badge>}
                	<div>
            			<small>Added <strong>{moment(item.updated).fromNow()}</strong> by <em>{item.creator}, {item.location}</em></small>
            		</div>             	
                </td>
                <td>
                	{item.about.trim() !== "" && 
                		<Button className="p-0 pl-1 float-right" color="default">
                			<i className="ml-2 fa fa-md fa-info-circle" id={`itemInfo${idx}`} onClick={this.toggleCollapse}></i>
                			<Tooltip placement="bottom" isOpen={tooltipOpen} target={`itemInfo${idx}`} toggle={this.toggleTooltip}>Click for more details</Tooltip>
                		</Button>
                	} 
                	{!item.folder && item.files[0] !== undefined && 
	                	<Button className="p-0 float-right" color="default" data-href={`/files/${item.files[0].id}/download`} onClick={this.handleDownload}>
	                		<i className="fa fa-download mt-1" id={`itemDownloadTip${idx}`}></i>
	                		<Tooltip placement="bottom" isOpen={tooltipDownloadOpen} target={`itemDownloadTip${idx}`} toggle={this.toggleDownloadTooltip}>Click to download</Tooltip>
	                	</Button>
	                }
                </td>
            </tr>
            <Collapse tag="tr" isOpen={isOpen}>
	            <td className="animated fadeIn collapse-column" colSpan="4">
	            	{renderHTML(item.about)}
	            	{item.about.trim() === "" && <p>There are no details.</p>}
	            </td>
	        </Collapse>
	        </>
		)
	}
}

const mapStateToProps = ({ currentView }, { search }) => {
	return { currentView, search }
}

export default connect(mapStateToProps)(FolderTableItem)