import React from 'react'
import { connect } from 'react-redux'
import { Badge, Button } from 'reactstrap'
import DropdownEditMenu from '../../DropdownEditMenu'
import { deleteDocumentFile } from '../../../../actions'

const FileRow = props => {
  const { name, fileType, latest, size, version, updated, isAdmin, id, handleDelete } = props
  const handleDownload = e => {
    e.preventDefault()
    window.location =e.currentTarget.dataset.href
  }

	return (
		<tr>
          <td>{name}</td>
          <td>{size}</td>
          <td>{updated}</td>
          <td>
            <span>{version}</span>
            {latest && <Badge className="ml-2" color="primary">Latest</Badge>}
          </td>
          <td>
            {isAdmin && <Button className="p-0 float-right ml-2" color="default" onClick={handleDelete}><i className="fa fa-trash mt-1"></i></Button> }
            <Button className="p-0 float-right" color="default" data-href={`/files/${id}/download`} onClick={handleDownload}><i className="fa fa-download mt-1"></i></Button>
          </td>
        </tr>
	)
}

const mapStateToProp = state => {
  return {}
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    handleDelete: () => {
      const { id } = ownProps
      dispatch(
        deleteDocumentFile(id)
      )
    }
  }
}

export default connect(mapStateToProp,mapDispatchToProps)(FileRow)