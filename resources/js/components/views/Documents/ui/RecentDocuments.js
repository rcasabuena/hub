import React, { Component } from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { Table, Badge } from 'reactstrap'
import FolderTableItem from './FolderTableItem'

class RecentDocuments extends Component {
	render() {
		const { documents } = this.props

		return (
			<div>
				{documents.length < 1 && <p>There are no files published recently.</p>}
				{documents.length > 0 &&
					<Table responsive className="documents-table">
		            	<thead>
		            		<tr>
		              			<th style={{width: '1%'}} className="pl-0 pr-0"></th>
		                  		<th style={{width: '90%'}}>Name</th>
		                  		<th style={{width: '9%'}}></th>
		            		</tr>
		            	</thead>
		            	<tbody>
		            		{documents.map((item, idx) => <FolderTableItem key={item.id} itemDetails={item} idx={idx} />)}
		            	</tbody>
		          </Table>
		      }
			</div>
		)
	}
}

const mapStateToProps = ({ documents }) => {
	return {
		documents: documents.data.filter(item => moment(item.updated).isSame(new Date(), 'week') && !item.folder && item.published && !item.draft)
	}
} 

export default connect(mapStateToProps)(RecentDocuments)