import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'
import { Badge, Button } from 'reactstrap'

class FileTableItem extends Component {
	state = {
		isOpen: false
	}

	toggleCollapse = e => {
		e.preventDefault()
		const { isOpen } = this.state
		this.setState({
			isOpen: !isOpen
		})
	}

	handleDownload = e => {
    	e.preventDefault()
    	window.location = e.currentTarget.dataset.href
  	}

	render() {
		
		const { isOpen } = this.state
		const { itemDetails: item, idx } = this.props
		const icon = _mimeTypes[item.mime_type] ? _mimeTypes[item.mime_type] : 'file-o'

		return (
			<>
			<tr>
				<td className="pr-0 pl-0"><i className={`fa fa-${icon}`}></i></td>
                <td>
                	<div>
                		{item.name} ({item.size}) {idx === 0 && <Badge color="primary">Current</Badge>} 
                		<div>
                			<small>Added <strong>{moment(item.created.date).fromNow()}</strong> by <em>{item.uploaded_by}, {item.location}</em></small>
                		</div>
                	</div>         	
                </td>
                <td>
                	<Button className="p-0 float-right" color="default" data-href={`/files/${item.id}/download`} onClick={this.handleDownload}><i className="fa fa-download mt-1"></i></Button>
                </td>
            </tr>
	        </>
		)
	}
}

export default FileTableItem