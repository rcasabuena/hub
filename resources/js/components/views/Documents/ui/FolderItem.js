import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import classNames from 'classnames'
import { Badge, Button, ButtonGroup, Card, CardBody, CardFooter, CardHeader, Col, Collapse, Fade, Row } from 'reactstrap'
import FolderItemChildren from './FolderItemChildren'

class FolderItem extends Component {
	
	state = {
		isOpen: false
	}
	
	toggleCollapse = e => {
		e.preventDefault()
		const { isOpen } = this.state
		this.setState({
			isOpen: !isOpen
		})
	}

	handleClick = e => {
		e.preventDefault()
		const { folderId } = this.props
		this.props.history.push(`/documents/folder/${folderId}`)
	}

	render() {
		const { isOpen } = this.state
		const caretClassNames = classNames({
			fa: true,
			'fa-lg': true,
			'fa-caret-right': !isOpen,
			'fa-caret-down': isOpen
		})
		const folderClassNames = classNames({
			fa: true,
			'fa-lg': true,
			'fa-folder': !isOpen,
			'fa-folder-open': isOpen
		})
		
		const { children } = this.props
		const { title, id } = this.props.folder
		
		return (
			<div className="folder-item pl-3">
				<ButtonGroup>
					{ children.length > 0 && 
              		<Button className="p-0" color="link" onClick={this.toggleCollapse}>
						<i style={{width: '11px'}} className={caretClassNames}></i>
              		</Button>
                  	}
                  	{ children.length < 1 && 
              		<Button className="p-0" color="link">
						<div style={{width: '11px'}}></div>
              		</Button>
                  	}
                  	<Button className="pl-1" color="link" onClick={this.handleClick}>
						<i className={folderClassNames}></i> { title }
                  	</Button>
                </ButtonGroup>
	            <Collapse isOpen={isOpen}>
	            	<FolderItemChildren folderId={id} />
	            </Collapse>
          	</div>
		)
	}
}

const mapStateToProps = ({ documents }, { folderId }) => {
	return {
		folder: folderId ? documents.data.find(item => item.id === folderId) : null,
		children: folderId ? documents.data.filter(item => item.parent === folderId && item.folder) : []
	}
}

export default withRouter(connect(mapStateToProps)(FolderItem))