import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import classNames from 'classnames'
import { Badge, Button, ButtonGroup, Card, CardBody, CardFooter, CardHeader, Col, Collapse, Fade, Row } from 'reactstrap'
import FolderItem from './FolderItem'

class FolderItemChildren extends Component {
	
	render() {
				
		const { children } = this.props

		return (
			<div className="folder-item-children">
				{
					children.map(item => {
						return <FolderItem key={item.id} folderId={item.id} />
					})
				}
          	</div>
		)
	}
}

const mapStateToProps = ({ documents }, { folderId }) => {
	return {
		children: documents.data.filter(item => item.parent === folderId && item.folder)
	}
}

export default withRouter(connect(mapStateToProps)(FolderItemChildren))