import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import classNames from 'classnames'
import { Badge, Button, ButtonGroup, Card, CardBody, CardFooter, CardHeader, Col, Collapse, Fade, Row } from 'reactstrap'

class TableItem extends Component {
	
	state = {
		isOpen: false
	}
	
	toggleCollapse = e => {
		e.preventDefault()
		const { isOpen } = this.state
		this.setState({
			isOpen: !isOpen
		})
	}

	handleClick = e => {
		e.preventDefault()
		const { folderId } = this.props
		this.props.history.push(`/documents/folder/${folderId}`)
	}

	render() {
		const { isOpen } = this.state
		const caretClassNames = classNames({
			fa: true,
			'fa-lg': true,
			'fa-caret-right': !isOpen,
			'fa-caret-down': isOpen
		})
		const folderClassNames = classNames({
			fa: true,
			'fa-lg': true,
			'fa-folder': !isOpen,
			'fa-folder-open': isOpen
		})
		
		//const { title, id } = this.props.folder
		
		return (
			<div className="table-item">
				<div>
					<div>
						<i className="fa-folder-" ></i>
					</div>	
				</div>
				<p onClick={this.toggleCollapse}>View Details</p>
	            <Collapse isOpen={isOpen}>
	            	Data Here
	            </Collapse>
          	</div>
		)
	}
}

const mapStateToProps = ({ documents }, { folderId }) => {
	return {
		folder: folderId ? documents.data.find(item => item.id === folderId) : null
	}
}

export default withRouter(connect(mapStateToProps)(TableItem))