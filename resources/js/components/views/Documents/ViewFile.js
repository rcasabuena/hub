import React, { Component } from 'react' 
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import addFolder from './addFolder'
import FileTable from './ui/FileTable'
import FileUploadForm from './ui/FileUploadForm'

class ViewFile extends Component {
	render() {
	
		const { folder, currentView } = this.props

		return (
			<div className="animated fadeIn">
				<h4><div className="pt-2">Versions of {folder.title}</div></h4>
				{/*<Link to={`/documents/folder/${folder.parent}`}><i className="fa fa-lg fa-folder mr-1 mb-3"></i>{folder.parentName}</Link>*/}
				<p className="text-muted">{`Documents keep older versions and are displayed in the order they were uploaded.`}</p>
				{currentView.admin && <FileUploadForm />}
				<FileTable folderId={folder.id} />
			</div>
		)
	}
}

const mapStateToProps = ({ currentView }) => {
	return { currentView }
}

export default connect(mapStateToProps)(addFolder(ViewFile))