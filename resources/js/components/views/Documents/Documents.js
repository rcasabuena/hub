import React, {Component} from 'react'
import { connect } from 'react-redux'
import { Route, Switch, Link } from 'react-router-dom'
import classNames from 'classnames'
import { Nav, NavItem, NavLink, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, Badge, Button, ButtonGroup, Card, CardBody, Row, Col, Collapse } from 'reactstrap'
import FolderItem from './ui/FolderItem'
import Home from './Home'
import NewFolder from './NewFolder'
import ViewFolder from './ViewFolder'
import EditFolder from './EditFolder'
import NewFile from './NewFile'
import ViewFile from './ViewFile'
import EditFile from './EditFile'
import FolderItemChildren from './ui/FolderItemChildren'
import ModuleSearchBox from '../../ui/ModuleSearchBox'
import Spinner from '../../ui/Spinner'
import { handleSearch2 } from '../../functions/Utils'
import SearchResults from './ui/SearchResults'

class Documents extends Component {

  constructor(props) {
    super(props)
    this.state = {
      searchTerms: "",
      isOpen: true
    }
    this.handleSearch = handleSearch2.bind(this)
  }

  toggleCollapse = e => {
    e.preventDefault()
    const { isOpen } = this.state
    this.setState({
      isOpen: !isOpen
    })
  }

  handleClick = e => {
    e.preventDefault()
    this.props.history.push('/documents')
  }

  render() {
    
    const { documents, fetching } = this.props
    const { isOpen, searchTerms } = this.state

    const caretClassNames = classNames({
      fa: true,
      'fa-lg': true,
      'fa-caret-right': !isOpen,
      'fa-caret-down': isOpen
    })
    const folderClassNames = classNames({
      fa: true,
      'fa-lg': true,
      'fa-folder': !isOpen,
      'fa-folder-open': isOpen
    })

    return (
      <div className="animated fadeIn">
        <ModuleSearchBox handleSearch={this.handleSearch} placeholder="Search..."/>
        <Row>
          <Col>
            <Card>
              <CardBody>
                <h4 className="card-title mb-0">Documents</h4>
                <p className="text-muted">Here you can find all of our latest Banana Moon documents – please ensure you check this section frequently, as documents are regularly updated to reflect current legislation. It is essential that you are using the correct version of each document at your nursery.</p>
                
                {fetching && <Spinner />}
                
                {searchTerms.trim() === "" && !fetching && 
                  <div className="doc-app mb-4">
                  <nav>
                    <Nav>
                      <div className="folder-item">
                        <ButtonGroup>
                            <Button className="p-0" color="link" onClick={this.toggleCollapse}>
                              <i style={{width: '11px'}} className={caretClassNames}></i>
                            </Button>
                            <Button className="pl-1" color="link" onClick={this.handleClick}>
                              <i className={folderClassNames}></i> Home
                            </Button>
                        </ButtonGroup>
                      </div>
                      <Collapse isOpen={isOpen}>
                        <FolderItemChildren folderId={null} />
                      </Collapse>
                    </Nav>
                    </nav>
                    <main className="docs">
                      <Switch>
                        <Route exact path='/documents' component={Home} />
                        <Route exact path='/documents/folder/new' component={NewFolder} />
                        <Route exact path='/documents/file/new' component={NewFile} />
                        <Route exact path='/documents/folder/:unique_id' component={ViewFolder} />
                        <Route exact path='/documents/folder/:unique_id/edit' component={EditFolder} />
                        <Route exact path='/documents/file/:unique_id' component={ViewFile} />
                        <Route exact path='/documents/file/:unique_id/edit' component={EditFile} />
                        <Route exact path='/documents/folder/:unique_id/folder/new' component={NewFolder} />
                        <Route exact path='/documents/folder/:unique_id/file/new' component={NewFile} />
                      </Switch>
                    </main>
                  </div>
                }
                {searchTerms.trim() !== "" && !fetching && 
                  <div className="doc-app mb-4">
                    <main>
                      <h4>Search Results</h4>
                      <SearchResults searchTerms={searchTerms} />
                    </main>
                  </div>
                }
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = ({ documents }) => {
  
  const { data } = documents
  data.sort(function(a, b){
      if(a.title < b.title) { return -1 }
      if(a.title > b.title) { return 1 }
      return 0
  })

  return {
    documents: data,
    fetching: documents.fetching
  }
}

export default connect(mapStateToProps)(Documents)