import React, { Component } from 'react'
import DocumentForm from './DocumentForm'
import { Card, CardBody, Col, Row } from 'reactstrap'

const EditDocument = () => {
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardBody>
              <h4 className="card-title mb-0">Edit Document</h4>
              <p className="text-muted">&nbsp;</p>
              <DocumentForm />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

export default EditDocument