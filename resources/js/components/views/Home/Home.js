import React, { Component } from 'react'
import { Button, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';

class Home extends Component {

    state = {
      searchTerms: ""
    }

    handleChange = e => {
      e.preventDefault()
      this.setState({
        searchTerms: e.target.value
      })
    }
    
    getSearchResult(e, path) {
      e.preventDefault()
      
      const { searchTerms } = this.state

      this.props.history.push({
        pathname: path,
        state: { searchTerms }
      })
    }

     render() {
          return (
              <div className="flex-row align-items-center" style={{
                marginTop: '80px'
              }}>
                <Container>
                  <Row className="justify-content-center">
                    <Col md="6">
                      <span className="clearfix text-center">
                        <h1 className="display-4 mr-4">Banana Moon Hub</h1>
                        <p className="text-muted">Welcome to the new and improved Banana Moon Hub! Here you can discover the latest news, download key documents and interact with the network. You can also use the search option below to get started.</p>
                      </span>
                      <form onSubmit={(e, path) => this.getSearchResult(e, '/search')}>
                        <InputGroup className="input-prepend">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="fa fa-search"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input bsSize="lg" type="text" placeholder="What are you looking for?" onChange={this.handleChange} />
                          {/*<InputGroupAddon addonType="append">
                            <Button color="primary" onClick={(e, path) => this.getSearchResult(e, '/search')}>Search</Button>
                          </InputGroupAddon>*/}
                        </InputGroup>
                      </form>
                    </Col>
                  </Row>
                </Container>
              </div>
          )
     }
}

export default Home