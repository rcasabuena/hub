import React from 'react'
import { Input, FormGroup, Col, Label, FormText } from 'reactstrap'

export const renderField = ({ input, type, name, label, info, required=false, rows=null, meta: { touched, error } }) => (
	<FormGroup row>
        <Col md="3">
          <Label htmlFor={name}>{label} {required && <span className="text-muted small"><br/>* Required</span> }</Label>
        </Col>
        <Col xs="12" md="9">
          <Input {...input} name={name} type={type} rows={rows} className={ ((touched && error) ? "is-invalid" : "") } />
          <FormText color="muted">{info}</FormText>
          {touched && error &&
 			<FormText color="danger"><i className="fa fa-times-circle"></i> {label} field {error}.</FormText>
          }
        </Col>
    </FormGroup>
)