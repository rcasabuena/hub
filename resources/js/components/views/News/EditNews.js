import React, { Component } from 'react'
import NewsForm from './NewsForm'
import { Card, CardBody, Col, Row } from 'reactstrap'

const EditNews = () => {
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardBody>
              <h4 className="card-title mb-0">Edit News</h4>
              <p className="text-muted">&nbsp;</p>
              <NewsForm />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

export default EditNews