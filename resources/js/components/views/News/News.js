import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Card, CardBody, Col, Row, CardColumns, Button, Input, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap'
import ModuleSearchBox from '../../ui/ModuleSearchBox'
import DataTable from '../../ui/Tables/DataTable'
import { handleSearch2 } from '../../functions/Utils'
import { connect } from 'react-redux'
import PageHeader from '../../ui/PageHeader'
import PublishedNews from './ui/PublishedNews'

class News extends Component {

  constructor(props) {
    super(props)
    this.state = {
      searchTerms: ""
    }
    this.handleSearch = handleSearch2.bind(this)
  }

  render() {

    const { searchTerms } = this.state
    const { currentView } = this.props

    return (
      <div className="animated fadeIn">
        <ModuleSearchBox handleSearch={this.handleSearch} placeholder="Search..."/>
        {currentView.admin &&   
          <Row>
            <Col>
              <Card>
                <CardBody>
                    <PageHeader 
                        pageTitle="News"
                        pageInfo="Here you’ll find all the latest news and updates – including training event announcements, inspection results and information about new legislation. Check this page regularly to keep up to date with what’s going on in the Banana Moon network."
                        pageResource="news"
                    />
                    <DataTable tableMap="news" searchTerms={searchTerms} />
                </CardBody>
              </Card>
            </Col>
          </Row>
        }
        {!currentView.admin && <PublishedNews searchTerms={searchTerms} /> } 
      </div>
    )
  }
}

const mapStateToProps = ({ currentView }) => {
  return {
    currentView
  }
}

export default connect(mapStateToProps)(News)