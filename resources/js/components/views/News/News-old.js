import React, { Component } from 'react'
import Fuse from 'fuse.js'
import { Link } from 'react-router-dom'
import { Card, CardBody, Col, Row, CardColumns, Button, Input, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap'
import Featured from './ui/Featured'
import NewsCard from './ui/NewsCard'
import ModuleSearchBox from '../ModuleSearchBox'
import { connect } from 'react-redux'

class News extends Component {
  state = {
    showAll: false,
    searching: false,
    searchTerm: ""
  }

  toggleShowAll = e => {
    
    let { showAll } = this.state

    e.preventDefault()
    this.setState({
      showAll: !showAll
    })
  }

  handleSearch = e => {
    e.preventDefault()
    this.setState({
      searching: e.target.value.trim() !== "",
      searchTerm: e.target.value.trim()
    })
  }

  componentDidMount = () => {
    
    const { isAdmin } = this.props

    this.setState({
      showAll: isAdmin
    })
  }

  render() {

    const { showAll, searching, searchTerm } = this.state
    const { news, fetching, isAdmin } = this.props
    
    const terms = _.split(searchTerm.trim().toLowerCase(), " ")
    
    news.sort((a, b) => {
      let pubA = new Date(a.publish)
      let pubB = new Date(b.publish)
      
      if (pubA < pubB) return 1
      if (pubA > pubB) return -1
      return 0
    })

    const latest = news.find((article) => !article.draft && article.published)

    const latestNews = latest && !showAll && !searching ? 
      (
        <Featured {...latest} isAdmin={isAdmin} />
      ) : 
      (
        null
      )

    const newsList = news.map((article, i) => {
        
        if (!searching) {
          if (!showAll && (article.draft || !article.published || article.id === latest.id)) return null
          return <NewsCard key={i} {...article} isAdmin={isAdmin} showAll={showAll} />
        }
        
        if (!showAll && (article.draft || !article.published)) return null

        if (_.intersection(article.search, terms).length < 1) return null

        return <NewsCard key={i} {...article} isAdmin={isAdmin} showAll={showAll} />
      }
    )

    return (
      <div>
        <ModuleSearchBox isAdmin={isAdmin} showAll={showAll} handleSearch={this.handleSearch} toggleShowAll={this.toggleShowAll} />
        <Row>
          <Col>
            {latestNews}
          </Col>
        </Row>
        <Row>
          <Col>
            <CardColumns>
              { newsList ? newsList : (
                  <Row>
                    <Col md="12">
                      <p>There are no news articles yet.</p>
                    </Col>
                  </Row>
              ) }
            </CardColumns>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = (state) => {

  return {
    fetching: state.news.fetching,
    news: state.news.data,
    isAdmin: state.currentUser.role === 'ADMIN'
  }
}

export default connect(mapStateToProps)(News)