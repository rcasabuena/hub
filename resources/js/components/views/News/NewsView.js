import React, { Component } from 'react'
import { connect } from 'react-redux'
import renderHTML from 'react-render-html'
import { Button, Card, CardBody, CardHeader, Col, Container, Row, CardImg, CardTitle, CardText, CardColumns } from 'reactstrap'

const NewsView = props => {
	
	const { news } = props 

	return (
		<div className="animated fadeIn">
          	<Row>
            	<Col>
              		<Card>
              			{ news.featured_image && <CardImg top width="100%" src={news.featured_image} /> }
                		<CardBody>
                    		<Row>
		                		<Col md="12">
		                  			<h1 className="display-4">{news.title}</h1>
				                  	<p className="lead">{news.headline}</p>
				                  	<hr className="my-2" />
				                  	<p className="text-muted">Published on {news.publish} by <em>{news.author}</em></p>
				                </Col>
		                		<Col md="12">
		                  			<div>{renderHTML(news.body)}</div>
		                		</Col>
		              		</Row>
                		</CardBody>
              		</Card>
            	</Col>
          	</Row>
        </div>
	)
}

const mapStateToProps = (state, ownProps) => {	
	return {
		news: state.news.data.find((news) => {
			return news.id === ownProps.match.params.id
		})
	}
}

export default connect(mapStateToProps)(NewsView)