import React, { Component } from 'react'
import moment from 'moment'
import TextInput from '../../ui/Form/TextInput'
import FileInput from '../../ui/Form/FileInput'
import WysiwygInput from '../../ui/Form/WysiwygInput'
import DateInput from '../../ui/Form/DateInput'
import ToggleSwitch from '../../ui/Form/ToggleSwitch'
import FormSubmitActions from '../../ui/Form/FormSubmitActions'
import { Form } from 'reactstrap'
import AppNews from '../../../objects/AppNews'
import formConnect from '../../../HOC/formConnect'

class NewsForm extends Component {

  constructor(props) {
    super(props)
    const { formUtils } = props
    this.handleChange = formUtils.handleChange.bind(this)
    this.handleToggle = formUtils.handleToggle.bind(this)
    this.handleUpload = formUtils.handleUpload.bind(this)
    this.handleEditorChange = formUtils.handleEditorChange.bind(this)
    this.handleDelete = formUtils.handleDelete.bind(this)
    this.handleSubmit = formUtils.handleSubmit.bind(this)
  }

  state = {
    item: new AppNews()
  }

  componentDidMount() {
    const { item } = this.props
    this.setState({item})
  }
  
  render() {
    const { id, title, headline, draft } = this.state.item
    const { body, updated, publish, expire } = this.props.item
    const { currentFeaturedImage } = this.props
    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <ToggleSwitch 
            handleToggle={this.handleToggle} 
            inputChecked={!draft} 
            inputLabel="Published" 
            inputName="draft" 
            inputHelpText="" 
            switchType="pill" 
            switchColour="primary" 
            inverseChecked={true}
          />
          <TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={title} 
            inputName="title" 
            inputLabel="Title" 
            inputHelpText="This is a help text" 
          />
          <TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={headline} 
            inputName="headline" 
            inputLabel="Headline" 
            inputHelpText="This is a help text" 
          />
          <WysiwygInput 
            inputName="body"
            inputLabel="Body"
            inputHelpText="This is a help text" 
            body={body}
            handleEditorChange={this.handleEditorChange}
          />
          <FileInput 
            handleUpload={this.handleUpload} 
            inputDefaultValue={currentFeaturedImage} 
            inputName="featured_image" 
            inputLabel="Featured Image" 
            inputHelpText="This is a help text" 
          />
          <DateInput 
            handleChange={this.handleChange} 
            inputDefaultValue={moment(publish).format('YYYY-MM-DD')} 
            inputName="publish" 
            inputLabel="Publish Date" 
            inputHelpText="This is a help text" 
          />
          <DateInput 
            handleChange={this.handleChange} 
            inputDefaultValue={moment(expire).format('YYYY-MM-DD')} 
            inputName="expire" 
            inputLabel="Expire Date" 
            inputHelpText="This is a help text" 
          />
          <FormSubmitActions 
            id={id}
            handleDelete={this.handleDelete}
          />
        </Form>
      </div>
    )
  }

}

const mapStateToProps = ({ news: newsList }, { match } ) => {
  const news = newsList.data.find(item => item.id === match.params.unique_id)
  const id = news ? news.id : "",
      title = news ? news.title: "",
      headline = news ? news.headline: "",
      body = news ? news.body: "",
      draft = news ? news.draft: true,
      publish = news ? news.publish: "",
      expire = news ? news.expire: "",
      featured_image = news ? news.featured_image: ""
  return {
      currentFeaturedImage: featured_image,
      item: new AppNews(id, title, headline, body, draft, publish, expire, featured_image)
  }
}

export default formConnect(NewsForm, "News", mapStateToProps)