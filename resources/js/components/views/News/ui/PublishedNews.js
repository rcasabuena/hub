import React, { Component } from 'react'
import Fuse from 'fuse.js'
import { connect } from 'react-redux'
import { Col, Row, CardColumns } from 'reactstrap'
import Featured from './Featured'
import NewsCard from './NewsCard'

class PublishedNews extends Component {
	render() {
		const { latestNews, publishedNews, searchTerms } = this.props
		let { newsGridItems } = this.props

		if (undefined != searchTerms && searchTerms.trim() !== "") {
		    const searchOptions = {
		      ..._globalFuseOptions,
		      keys: _globalFuseKeys['news']
		    }
			const fuse = new Fuse(publishedNews, searchOptions)
			newsGridItems = fuse.search(searchTerms.trim())
		}

		return (
			<div>
				<Row>
		          <Col>
		            { latestNews && undefined !== searchTerms && searchTerms.trim() === "" && <Featured {...latestNews} /> }
		          </Col>
		        </Row>
		        <Row>
		        	<Col>
		        		<CardColumns>
		        			{ newsGridItems.map(item => <NewsCard key={item.id} {...item} />) }
		        		</CardColumns>
		        	</Col>
		        </Row>
			</div>
		)
	}
}

const mapStateToProps = ({ news }) => {
	let publishedNews = news.data.filter(item => !item.draft && item.published)
	let latestNews = publishedNews.length > 0 ? publishedNews[0] : null
	let newsGridItems = publishedNews.length > 0 ? publishedNews.filter(item=> item.id !== latestNews.id) : [] 
	return {
		latestNews,
		newsGridItems,
		publishedNews
	}
}

export default connect(mapStateToProps)(PublishedNews)