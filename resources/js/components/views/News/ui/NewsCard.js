import React from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { Badge, Card, CardBody, CardImg, CardTitle, CardText, ButtonGroup, DropdownItem, DropdownToggle, DropdownMenu, ButtonDropdown } from 'reactstrap'
import DropdownEditMenu from '../../DropdownEditMenu'

const NewsCard = (props) => {

	const { title, headline, publish, author, id, featured_image, isAdmin, showAll, published, updated, draft, location } = props
	const draftBadge = (showAll && draft) ? <p><Badge color="warning">Draft</Badge> Last updated <strong>{moment(updated).fromNow()}</strong> by <em>{author}, {location}</em></p> : null
	const upcomingBadge = (showAll && !draft && !published) ? <p><Badge color="success">Upcoming</Badge> Will be published <strong>{moment(publish).fromNow()}</strong> by <em>{author}, {location}</em></p> : null
	const publishedBadge = (!draft && published) ? <p>{showAll && <Badge color="primary">Published</Badge>} Published <strong>{moment(publish).fromNow()}</strong> by <em>{author}, {location}</em></p> : null

	return (
		<Card className="mb-4 animated fadeIn">
	        <CardImg top width="100%" src={featured_image} />
	        <CardBody>
	        	{isAdmin && <DropdownEditMenu resource="News" id={id} actions={['EDIT', 'DELETE']} /> }
	          	<CardTitle>
	            	<h3 className="display-5">{title}</h3>
	          	</CardTitle>
	          	<CardText>{headline}</CardText>
	          	<hr className="my-2" />
	          	{draftBadge} {upcomingBadge} {publishedBadge}
	          	<Link className="btn btn-primary" to={`/news/${id}`}>Read more</Link>
	        </CardBody>
	    </Card>
	)
}

export default NewsCard