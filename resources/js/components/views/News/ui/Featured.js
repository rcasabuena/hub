import React from 'react'
import { Link } from 'react-router-dom'
import moment from 'moment'
import { Col, Row, Card, CardBody, CardImg } from 'reactstrap'
import DropdownEditMenu from '../../DropdownEditMenu'

const Featured = (props) => {
	const { title, headline, publish, author, id, featured_image, isAdmin, location } = props
	return (
		<Card className="animated fadeIn">
            <CardBody>
            	{isAdmin && <DropdownEditMenu resource="News" id={id} actions={['DELETE', 'EDIT']} /> }
                <h4 className="card-title mb-0">Latest News</h4>
                <p className="text-muted">Here you’ll find all the latest news and updates – including training event announcements, inspection results and information about new legislation. Check this page regularly to keep up to date with what’s going on in the Banana Moon network.</p>
                <Row>
                	{featured_image && 
				        <Col md="6">
				          	<CardImg width="100%" src={featured_image} />
				        </Col>
				    }
				    <Col md={featured_image ? "6" : "12"} >
			          	<h1 className="display-4">{title}</h1>
			          	<p className="lead">{headline}</p>
			          	<hr className="my-2" />
			          	<p>Published <strong>{moment(publish).fromNow()}</strong> by <em>{author}, {location}</em></p>
			          	<p className="lead">
			            <Link className="btn btn-primary btn-lg" to={`/news/${id}`}>Read More</Link>
			          </p>
			        </Col>
		      	</Row>
            </CardBody>
        </Card>
	)
}

export default Featured