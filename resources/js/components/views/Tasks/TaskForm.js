import React, { Component } from 'react'
import TextInput from '../../ui/Form/TextInput'
import FileInputMulti from '../../ui/Form/FileInputMulti'
import SelectInput from '../../ui/Form/SelectInput'
import WysiwygInput from '../../ui/Form/WysiwygInput'
import FormSubmitActions from '../../ui/Form/FormSubmitActions'
import { Form } from 'reactstrap'
import AppTask from '../../../objects/AppTask'
import formConnect from '../../../HOC/formConnect'

class TaskForm extends Component {

  constructor(props) {
    super(props)
    const { formUtils } = props
    this.handleChange = formUtils.handleChange.bind(this)
    this.handleEditorChange = formUtils.handleEditorChange.bind(this)
    this.handleUploadMulti = formUtils.handleUploadMulti.bind(this)
    this.handleSelect = formUtils.handleSelect.bind(this)
    this.handleDelete = formUtils.handleDelete.bind(this)
    this.handleSubmit = formUtils.handleSubmit.bind(this)
  }

  state = {
    item: new AppTask()
  }

  componentDidMount() {
    const { item } = this.props
    this.setState({item})
  }
  
  render() {
    const { id, title, body, repeat } = this.props.item
    console.log(this.props.item)
    console.log(repeat)
    const frequency = [
      {
        value: 'DAY',
        label: 'Daily' 
      },
      {
        value: 'WEEK',
        label: 'Weekly' 
      },
      {
        value: 'MONTH',
        label: 'Monthly' 
      },
      {
        value: '3MONTHS',
        label: 'Every 3 months' 
      },
      {
        value: '6MONTHS',
        label: 'Every 6 months' 
      },
      {
        value: 'YEAR',
        label: 'Yearly' 
      }
    ]
    
    const taskRepeat = frequency.find(item => item.value == repeat)

    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={title} 
            inputName="title" 
            inputLabel="Title" 
            inputHelpText="This is a help text" 
          />
          <SelectInput 
            inputName="repeat"
            inputLabel="Repeat"
            selectValue={taskRepeat}
            selectOptions={frequency}
            inputHelpText="This is a help text"
            handleSelect={this.handleSelect}
          />
          <WysiwygInput 
            inputName="body"
            inputLabel="Body"
            inputHelpText="This is a help text" 
            body={body}
            handleEditorChange={this.handleEditorChange}
          />
          <FileInputMulti 
                handleUpload={this.handleUploadMulti} 
                inputDefaultValue={null} 
                inputName="attachments" 
                inputLabel="Attachments" 
                inputHelpText="" 
            />
          <FormSubmitActions 
            id={id}
            handleDelete={this.handleDelete}
          />
        </Form>
      </div>
    )
  }

}

const mapStateToProps = ({ tasks }, { match }) => {
  const task = tasks.data.find(item => item.id === match.params.unique_id)
  const id = task ? task.id : "",
      title = task ? task.title: "",
      body = task ? task.body: "",
      repeat = task ? task.repeat: ""
  return {
      item: new AppTask(id, title, body, repeat)
  }
}

export default formConnect(TaskForm, "Tasks", mapStateToProps)