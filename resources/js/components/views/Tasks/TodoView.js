import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import moment from 'moment'
import renderHTML from 'react-render-html'
import { Row, Col, Card, CardBody, CardHeader, Button, Badge } from 'reactstrap'
import Attachments from '../../ui/Attachments'
import DeadlineBadge from '../../ui/DeadlineBadge'
import AppTodo from '../../../objects/AppTodo'
import { saveResource } from '../../../actions'
import CommentForm from './ui/CommentForm'
import Comment from './ui/Comment'

class TodoView extends Component {
	
	handleAction = (e, action) => {
		e.preventDefault()
		const { todo, updateTodo } = this.props
		let data = new AppTodo(todo.id, todo.task_id, todo.user_id, moment(todo.deadline).format('YYYY-MM-DD'))
		data[action] = true
		updateTodo(data.formData)
	}

	goToNextTodo = () => {
		const { history, todos } = this.props
		let location = 0
		if (undefined != history.location.todoId) location = todos.findIndex(item => item.id === history.location.todoId)
		let nextLocation = undefined != todos[location + 1] ? todos[location + 1] : todos[0]
		history.push({
			pathName: '/todos',
			todoId: nextLocation.id
		})
	}

	render() {
		const { task, todo, assignee, todos } = this.props

		return (
			<div className="animated fadeIn">
		      <Row>
		        <Col md="12">
		          <Card>
					{ todos.length < 1 && 
						<CardBody>
							<h4 className="card-title">Task</h4>
							<p>You do not have any tasks outstanding.</p>
						</CardBody> 
					}
					{ todos.length > 0 && 
			            <CardBody>
			              	<h4 className="card-title">Task</h4>
			              	<h1 className="display-5">{task.title}</h1>
			              	<p className="mb-0" title="Deadline"><i className="icon-clock"></i>&nbsp;{moment(todo.deadline).format('DD MMMM YYYY')} <DeadlineBadge item={todo} /></p>
			              	<p><i className="icon-user-follow" title="Assignee"></i>&nbsp;{assignee.name} on {moment(todo.updated_at.date).format('DD MMMM YYYY')}</p>
			              	<div>{renderHTML(task.body)}</div>
			              	<Attachments parentId={task.id} />
			              	<hr />
			              	<Button size="sm" color="primary" disabled={todos.length < 2} className="mr-2" onClick={ this.goToNextTodo }>Next Task</Button>
			              	{ !todo.completed && <Button size="sm" color="success" className="mr-2" onClick={ (e,action) => this.handleAction(e, 'completed') }>Complete</Button> }
			              	{ !todo.completed && <Button size="sm" color="danger" className="pull-right" onClick={ (e,action) => this.handleAction(e, 'rejected') }>Reject</Button> }
			            </CardBody>
			        }
		          </Card>
		          	{ todos.length > 0 && 
			          	<Card>
	                        <CardHeader>
	                          { todo.comments.length === 1 && <span>1 Comment</span> } 
	                          { todo.comments.length !== 1 && <span>{todo.comments.length} Comments</span> } 
	                        </CardHeader>
	                        <CardBody>
	                          {todo.comments.length < 1 && <p>There are no comments yet.</p>}
	                          {todo.comments.length > 0 && 
	                            todo.comments.map((comment, idx) => {
	                              return <Comment key={idx} {...comment} />
	                            })
	                          }
	                        </CardBody>
	                    </Card>
	                }
	                { todos.length > 0 && 
	                	<CommentForm id={todo.id} />
	                }
		        </Col>
		      </Row>
		    </div>
		)
	}
}

const mapStateToProps = ({ todos, tasks, users, currentUser }, { history }) => {
	let userTodos = todos.data.filter(item => item.user_id === currentUser.id && !item.rejected && (!item.completed || (item.completed && moment(item.completed_at).isSame(new Date(), 'day')))).sort(function(a, b) {
	    a = new Date(a.deadline);
	    b = new Date(b.deadline);
	    return a>b ? -1 : a<b ? 1 : 0;
	})
	let todo = userTodos[0]
	if (undefined !== history.location.todoId && todos.data.find(item => item.id === history.location.todoId && !item.rejected)) todo = todos.data.find(item => item.id === history.location.todoId)

	return {
		todo,
		todos: userTodos,
		task: undefined != todo ? tasks.data.find(item => item.id === todo.task_id) : null,
		assignee: undefined != todo ? users.data.find(item => item.id === todo.added_by) : null
	}
}

const mapDispatchToProps = dispatch => {
	return {
		updateTodo: data => {
			dispatch(
				saveResource('Todos', data)
			)
		}
	}
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(TodoView))