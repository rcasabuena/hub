import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import moment from 'moment'
import renderHTML from 'react-render-html'
import { Row, Col, Card, CardBody, CardHeader, Button, Badge } from 'reactstrap'
import Attachments from '../../ui/Attachments'
import DeadlineBadge from '../../ui/DeadlineBadge'
import CommentForm from './ui/CommentForm'
import Comment from './ui/Comment'

class ViewTodo extends Component {
	
	render() {
		const { task, todo } = this.props

		return (
			<div className="animated fadeIn">
		      <Row>
		        <Col md="12">
		          <Card>
		            <CardBody>
		              	<h4 className="card-title">Task</h4>
		              	<h1 className="display-5">{task.title}</h1>
		              	<p className="mb-0" title="Deadline"><i className="icon-clock"></i>&nbsp;{moment(todo.deadline).format('DD MMMM YYYY')} <DeadlineBadge item={todo} /></p>
		              	<p><i className="icon-user" title="User"></i>&nbsp;{todo.name}</p>
		              	<div>{renderHTML(task.body)}</div>
		              	<Attachments parentId={task.id} />
		            </CardBody>
		          </Card>
			          	<Card>
	                        <CardHeader>
	                          { todo.comments.length === 1 && <span>1 Comment</span> } 
	                          { todo.comments.length !== 1 && <span>{todo.comments.length} Comments</span> } 
	                        </CardHeader>
	                        <CardBody>
	                          {todo.comments.length < 1 && <p>There are no comments yet.</p>}
	                          {todo.comments.length > 0 && 
	                            todo.comments.map((comment, idx) => {
	                              return <Comment key={idx} {...comment} />
	                            })
	                          }
	                        </CardBody>
	                    </Card>
	                	<CommentForm id={todo.id} />
		        </Col>
		      </Row>
		    </div>
		)
	}
}

const mapStateToProps = ({ todos, tasks, users }, { match }) => {
	const todo = todos.data.find(item => item.id === match.params.unique_id)
	const task = tasks.data.find(item => item.id === todo.task_id)
	return {
		todo,
		task
	}
}

export default withRouter(connect(mapStateToProps,null)(ViewTodo))