import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import DateInput from '../../../ui/Form/DateInput'
import MultiSelectInput from '../../../ui/Form/MultiSelectInput'
import FormSubmitActions from '../../../ui/Form/FormSubmitActions'
import * as formUtils from '../../../../functions/FormFunctions.js'
import { saveTodos } from '../../../../actions'
import { Form } from 'reactstrap'
import AppTodoUsers from '../../../../objects/AppTodoUsers'

class TodoForm extends Component {

  constructor(props) {
    super(props)
    this.handleChange = formUtils.handleChange.bind(this)
    this.handleSelect = formUtils.handleMultiSelect.bind(this)
    this.handleSubmit = formUtils.handleSubmit.bind(this)
  }

  componentDidMount() {
    const { taskId } = this.props
    let item = new AppTodoUsers(null, taskId, [])
    this.setState({item})
  }
  
  render() {
    const { users, groups } = this.props
    const all = [...users, ...groups].sort(function(a, b) {
        return a.name>b.name ? 1 : a.name<b.name ? 0 : 1;
    })
    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <MultiSelectInput 
            inputName="users"
            inputLabel="Users/Groups"
            selectOptions={all}
            inputHelpText="This is a help text"
            handleSelect={this.handleSelect}
            horizontal={true}
          />
          <DateInput 
            handleChange={this.handleChange} 
            inputName="deadline" 
            inputLabel="Deadline" 
            horizontal={true}
            inputHelpText="This is a help text" 
          />
          <FormSubmitActions 
            id=""
            handleDelete={this.handleDelete}
          />
        </Form>
      </div>
    )
  }

}

const mapStateToProps = (state, ownProps) => {
  
  let users = state.users.data
  let groups = state.groups.data

  if (state.currentUser.role === 'USER') {
      groups = []
      if (state.currentUser.director) users = users.filter(item => item.location.id == state.currentUser.location.id)
      else users = []
  }

  return {
      users,
      groups
  }
}

const mapDispatchToProps = dispatch => {
  return {
    save: (data, history) => { 
      dispatch(
        saveTodos(data, history)
      )
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoForm)