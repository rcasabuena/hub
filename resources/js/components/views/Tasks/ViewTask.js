import React from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { Card, CardHeader, CardBody, Col, Row, Table, Button } from 'reactstrap'
import renderHTML from 'react-render-html'
import DataTable from '../../ui/Tables/DataTable'
import TodoForm from './Todos/TodoForm'
import AppTask from '../../../objects/AppTask'

const ViewTask = props => {
  const { title, body, id, repeat } = props.item
  const { todos, attachments } = props

  return (
    <div className="animated fadeIn">
      <Row>
        <Col md="8">
          <Card>
            <CardBody>
              <h4 className="card-title">Task</h4>
              <h1 className="display-5">{title}</h1>
              {repeat !== "" && <h5><i className="icon-refresh"></i>&nbsp;&nbsp;{repeat}</h5> }
              <div>{renderHTML(body)}</div>
              <DataTable tableMap="todos" filterBy='task_id' filterByValue={id} />
            </CardBody>
          </Card>
          { attachments.length > 0 && 
            <Card>
              <CardHeader>
                Attachments
              </CardHeader>
              <CardBody>
                <Table responsive className="documents-table">
                  <thead>
                    <tr>
                      <th style={{width: '1%'}} className="pl-0 pr-0"></th>
                      <th style={{width: '90%'}}>Name</th>
                      <th style={{width: '9%'}}></th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      attachments.map(item => {
                        const icon = _mimeTypes[item.mime_type] ? _mimeTypes[item.mime_type] : 'file-o'
                        return (
                          <tr key={item.id}>
                            <td className="pr-0 pl-0"><i className={`fa fa-${icon}`}></i></td>
                            <td>
                              <div>
                                {item.name} ({item.size})
                                <div>
                                  <small>Added <strong>{moment(item.created.date).fromNow()}</strong> by <em>{item.uploaded_by}, {item.location}</em></small>
                                </div>
                              </div>           
                            </td>
                            <td>
                              <Button tag="a" className="p-0 float-right" color="default" href={`/attachments/${item.id}/download`}><i className="fa fa-download mt-1"></i></Button>
                            </td>
                          </tr>
                        )
                      })
                    }
                  </tbody>
              </Table>
              </CardBody>
            </Card>
          }
        </Col>
        <Col md="4">
          <Card>
            <CardHeader>
              Assign task to users
            </CardHeader>
            <CardBody>
              <TodoForm taskId={id} />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

const mapStateToProps = ({tasks, users, todos, attachments}, {match}) => {
  const task = tasks.data.find(item => item.id === match.params.unique_id)
  const id = task ? task.id : "",
      title = task ? task.title: "",
      body = task ? task.body: "",
      repeat = task ? task.repeat: ""
  return {
      users: users.data,
      todos: todos.data.filter(todo => todo.task_id === task.id),
      item: new AppTask(id, title, body, repeat),
      attachments: attachments.data.filter(item => item.parentId == match.params.unique_id)
  }
}

export default connect(mapStateToProps)(ViewTask)