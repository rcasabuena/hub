import React from 'react'
import renderHTML from 'react-render-html'
import moment from 'moment'
import { Row, Col, Container, Card, CardBody } from 'reactstrap'
import Attachments from '../../../ui/Attachments'

const Comment = props => {
	
	const { body, author, created, location, id } = props

	return (
		<Row>
			<Col md="3">
				<Card className="bg-gray-100">
					<CardBody className="p-2">
						<p className="mb-2">Commented <strong>{moment(created).fromNow()}</strong></p>
						<p className="mb-2">by <em>{author}, {location}</em></p>
					</CardBody>
				</Card>
			</Col>
			<Col md="9">
				{ renderHTML(body) }
				<Attachments parentId={id} />
			</Col>
			<Col md="12">
				<hr/>
			</Col>
		</Row>
	)
}

export default Comment