import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import TextInput from '../../../ui/Form/TextInput'
import FileInputMulti from '../../../ui/Form/FileInputMulti'
import ToggleSwitch from '../../../ui/Form/ToggleSwitch'
import WysiwygInput from '../../../ui/Form/WysiwygInput'
import SelectInput from '../../../ui/Form/SelectInput'
import FormSubmitActions from '../../../ui/Form/FormSubmitActions'
import { saveTodoComment } from '../../../../actions'
import { Form, Card, CardHeader, CardBody } from 'reactstrap'
import AppTodoComment from '../../../../objects/AppTodoComment'
import * as formUtils from '../../../../functions/FormFunctions.js'

class CommentForm extends Component {

  constructor(props) {
    super(props)
    this.handleUploadMulti = formUtils.handleUploadMulti.bind(this)
    this.handleEditorChange = formUtils.handleEditorChange.bind(this)
    this.handleSubmit = formUtils.handleSubmit.bind(this)
  }

  state = {
    item: new AppTodoComment()
  }

  componentDidMount() {
     
     let { item } = this.state
     const { id } = this.props
     item['todo_id'] = id

     this.setState({item})
  }
  
  render() {
    return (
      <Card>
      	<CardHeader>
          Your Comments
        </CardHeader>
      	<CardBody>
      		<Form onSubmit={this.handleSubmit}>
            <WysiwygInput 
              inputName="body"
              inputLabel="Body"
              inputHelpText="" 
              body={""}
              handleEditorChange={this.handleEditorChange}
              horizontal={true}
            />
            <FileInputMulti 
                handleUpload={this.handleUploadMulti} 
                inputDefaultValue={null} 
                inputName="attachments" 
                inputLabel="Attachments" 
                horizontal={true}
                inputHelpText="" 
            />
            <FormSubmitActions 
              id={""}
            />
          </Form>
      	</CardBody>
      </Card>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    save: data => { 
      dispatch(
        saveTodoComment(data)
      )
    },
  }
}

export default withRouter(connect(null, mapDispatchToProps)(CommentForm))