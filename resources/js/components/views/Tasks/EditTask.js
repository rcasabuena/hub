import React from 'react'
import TaskForm from './TaskForm'
import { Card, CardBody, Col, Row } from 'reactstrap'

const EditTask = () => {
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardBody>
              <h4 className="card-title mb-0">Edit Task</h4>
              <p className="text-muted">&nbsp;</p>
              <TaskForm />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

export default EditTask