import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import ModuleSearchBox from '../../ui/ModuleSearchBox'
import DataTable from '../../ui/Tables/DataTable'
import { handleSearch2 } from '../../functions/Utils'
import PageHeader from '../../ui/PageHeader'
import { Button, Badge, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap'

class Tasks extends Component {
    constructor(props) {
      super(props)
      this.state = {
        searchTerms: ""
      }
      this.handleSearch = handleSearch2.bind(this)
    }

     render() {
      const { searchTerms } = this.state
      const { todos } = this.props

      return (
        <div className="animated fadeIn">
          <ModuleSearchBox handleSearch={this.handleSearch} placeholder="Search..."/>
          <Row>
            <Col>
              <Card>
                <CardBody>
                  <PageHeader 
                      pageTitle="Tasks"
                      pageInfo="&nbsp;"
                      pageResource="tasks"
                  />
                  <DataTable todos={todos} tableMap="tasks" searchTerms={searchTerms} />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      )
     }
}

const mapStateToProps = ({ todos }) => {
  return {
    todos: todos.data
  }
}

export default connect(mapStateToProps)(Tasks)