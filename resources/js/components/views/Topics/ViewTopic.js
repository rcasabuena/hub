import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Card, CardBody, Col, Row } from 'reactstrap'

const ViewTopic = () => {
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardBody>
              <h4 className="card-title mb-0">View Topic</h4>
              <p className="text-muted">&nbsp;</p>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

const mapStateToProps = state => {
  return { 
  }
}

export default connect(mapStateToProps)(ViewTopic)