import React, { Component } from 'react'
import TextInput from '../../ui/Form/TextInput'
import WysiwygInput from '../../ui/Form/WysiwygInput'
import FormSubmitActions from '../../ui/Form/FormSubmitActions'
import { Form } from 'reactstrap'
import AppTopic from '../../../objects/AppTopic'
import formConnect from '../../../HOC/formConnect'

class TopicForm extends Component {

  constructor(props) {
    super(props)
    const { formUtils } = props
    this.handleChange = formUtils.handleChange.bind(this)
    this.handleEditorChange = formUtils.handleEditorChange.bind(this)
    this.handleDelete = formUtils.handleDelete.bind(this)
    this.handleSubmit = formUtils.handleSubmit.bind(this)
  }

  state = {
    item: new AppTopic()
  }

  componentDidMount() {
    const { item } = this.props
    this.setState({item})
  }
  
  render() {
    const { id, title, body } = this.props.item
    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={title} 
            inputName="title" 
            inputLabel="Title" 
            inputHelpText="This is a help text" 
          />
          <WysiwygInput 
            inputName="body"
            inputLabel="Body"
            inputHelpText="This is a help text" 
            body={body}
            handleEditorChange={this.handleEditorChange}
          />
          <FormSubmitActions 
            id={id}
            handleDelete={this.handleDelete}
          />
        </Form>
      </div>
    )
  }

}

const mapStateToProps = ({ topics }, { match }) => {
  const topic = topics.data.find(item => item.id === match.params.unique_id)
  const id = topic ? topic.id : "",
      title = topic ? topic.title: "",
      body = topic ? topic.body: ""
  return {
      item: new AppTopic(id, title, body)
  }
}

export default formConnect(TopicForm, "Topics", mapStateToProps)