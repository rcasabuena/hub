import React from 'react'
import TopicForm from './TopicForm'
import { Card, CardBody, Col, Row } from 'reactstrap'

const EditTopic = () => {
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardBody>
              <h4 className="card-title mb-0">Edit Topic</h4>
              <p className="text-muted">&nbsp;</p>
              <TopicForm />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

export default EditTopic