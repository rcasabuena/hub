import React, { Component } from 'react'
import TextInput from '../../ui/Form/TextInput'
import ToggleSwitch from '../../ui/Form/ToggleSwitch'
import WysiwygInput from '../../ui/Form/WysiwygInput'
import SelectInput from '../../ui/Form/SelectInput'
import FormSubmitActions from '../../ui/Form/FormSubmitActions'
import { saveResource, deleteResource } from '../../../actions'
import { Form } from 'reactstrap'
import AppQuestion from '../../../objects/AppQuestion'
import formConnect from '../../../HOC/formConnect'

class QuestionForm extends Component {

  constructor(props) {
    super(props)
    const { formUtils } = props
    this.handleChange = formUtils.handleChange.bind(this)
    this.handleToggle = formUtils.handleToggle.bind(this)
    this.handleSelect = formUtils.handleSelect.bind(this)
    this.handleEditorChange = formUtils.handleEditorChange.bind(this)
    this.handleDelete = formUtils.handleDelete.bind(this)
    this.handleSubmit = formUtils.handleSubmit.bind(this)
  }

  state = {
    item: new AppQuestion()
  }

  componentDidMount() {
    const { item } = this.props
    this.setState({item})
  }
  
  render() {
    const { id, title, body, sticky } = this.props.item
    const { topics, questionTopic } = this.props
    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <ToggleSwitch 
            handleToggle={this.handleToggle} 
            inputChecked={!!sticky} 
            inputLabel="Sticky" 
            inputName="sticky" 
            inputHelpText="" 
            switchType="pill" 
            switchColour="primary" 
            inverseChecked={false}
          />
          <SelectInput 
            inputName="topic_id"
            inputLabel="Topic"
            selectValue={questionTopic}
            selectOptions={topics}
            inputHelpText="This is a help text"
            handleSelect={this.handleSelect}
          />
          <TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={title} 
            inputName="title" 
            inputLabel="Title" 
            inputHelpText="This is a help text" 
          />
          <WysiwygInput 
            inputName="body"
            inputLabel="Body"
            inputHelpText="This is a help text" 
            body={body}
            handleEditorChange={this.handleEditorChange}
          />
          <FormSubmitActions 
            id={id}
            handleDelete={this.handleDelete}
          />
        </Form>
      </div>
    )
  }

}

const mapStateToProps = ({ questions, topics }, { match }) => {
  const question = questions.data.find(item => item.id === match.params.unique_id)
  const id = question ? question.id : "",
      title = question ? question.title: "",
      body = question ? question.body: "",
      sticky = question ? question.sticky: false,
      topic_id = question && question.topic ? question.topic.id : ""
  return {
      topics: topics.data,
      questionTopic: question && question.topic ? question.topic : null,
      item: new AppQuestion(id, title, body, sticky, topic_id)
  }
}

export default formConnect(QuestionForm, "Questions", mapStateToProps)