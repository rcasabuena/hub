import React from 'react'
import QuestionForm from './QuestionForm'
import { Card, CardBody, Col, Row } from 'reactstrap'

const EditQuestion = () => {
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardBody>
              <h4 className="card-title mb-0">Edit Question</h4>
              <p className="text-muted">&nbsp;</p>
              <QuestionForm />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

export default EditQuestion