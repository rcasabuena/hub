import React, { Component } from 'react'
import Fuse from 'fuse.js'
import { connect } from 'react-redux'
import { Col, Row, Input, InputGroup, InputGroupAddon, InputGroupText, Nav, TabContent, TabPane } from 'reactstrap'
import ForumResults from './ui/ForumResults'
import NewsResults from './ui/NewsResults'
import EventsResults from './ui/EventsResults'
import DocumentsResults from './ui/DocumentsResults'
import NavTab from './ui/NavTab'

class Search extends Component {

  state = {
    activeTab: "1",
    searchTerms: "",
    news: [],
    documents: [],
    events: [],
    forum: [],
    newsFiltered: [],
    documentsFiltered: [],
    eventsFiltered: [],
    forumFiltered: []
  }

  toggle = tab => this.setState({ activeTab: tab })

  doSearch = e => {
    e.preventDefault()
    const { news, documents, events, forum } = this.state
    let searchTerms = e.target.value
    this.setState({
      newsFiltered: this.filtered(news, 'news', searchTerms),
      documentsFiltered: this.filtered(documents, 'documents', searchTerms),
      eventsFiltered: this.filtered(events, 'events', searchTerms),
      forumFiltered: this.filtered(forum, 'forum', searchTerms)
    })
  } 

  filtered = (collection, type, searchTerms) => {
    
    const { keys } = this.state
    const options = {
      ..._globalFuseOptions,
      keys: _globalFuseKeys[type]
    }
    const fuse = new Fuse(collection, options)
    return fuse.search(searchTerms)
  }

  componentDidMount = () => {
    const { news, documents, events, forum } = this.props
    const searchTerms = undefined != this.props.history.location.state ? this.props.history.location.state.searchTerms : ""

    this.setState({
      searchTerms,
      news,
      documents,
      events,
      forum,
      newsFiltered: this.filtered(news, 'news', searchTerms),
      documentsFiltered: this.filtered(documents, 'documents', searchTerms),
      eventsFiltered: this.filtered(events, 'events', searchTerms),
      forumFiltered: this.filtered(forum, 'forum', searchTerms)
    })
  }


    
     render() {

          const { activeTab, searchTerms, newsFiltered, documentsFiltered, eventsFiltered, forumFiltered } = this.state

          return (
               <div className="animated fadeIn">
                  <Row>
                    <Col>
                      <InputGroup className="input-prepend">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="fa fa-search"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input bsSize="lg" type="text" placeholder="What are you looking for?" defaultValue={searchTerms} onChange={this.doSearch} />
                      </InputGroup>
                      <p className="text-muted" style={{ paddingTop: '.5rem' }}></p>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="mb-4">
                      <Nav tabs>
                        <NavTab results={newsFiltered} activeTab={activeTab === "1"} onTabClick={() => this.toggle("1")} label={"News"} />
                        <NavTab results={documentsFiltered} activeTab={activeTab === "2"} onTabClick={() => this.toggle("2")} label={"Documents"} />
                        {/*<NavTab results={eventsFiltered} activeTab={activeTab === "3"} onTabClick={() => this.toggle("3")} label={"Events"} />*/}
                        <NavTab results={forumFiltered} activeTab={activeTab === "4"} onTabClick={() => this.toggle("4")} label={"Forum"} />
                      </Nav>
                      <TabContent activeTab={activeTab}>
                        <TabPane tabId="1">
                          <NewsResults results={newsFiltered} />
                        </TabPane>
                        <TabPane tabId="2">
                          <DocumentsResults results={documentsFiltered} />
                        </TabPane>
                        {/*<TabPane tabId="3">
                          <EventsResults results={eventsFiltered} />
                        </TabPane>*/}
                        <TabPane tabId="4">
                          <ForumResults results={forumFiltered} />
                        </TabPane>
                      </TabContent>
                    </Col>
                  </Row>
                </div>
          )
     }
}

const mapStateToProps = state => {
  return {
    documents: state.documents.data.filter(item => !item.draft && item.published),
    news: state.news.data.filter(item => !item.draft && item.published),
    events: [],
    forum: state.questions.data
  }
}

export default connect(mapStateToProps)(Search)