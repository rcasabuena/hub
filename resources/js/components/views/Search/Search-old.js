import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Col, Container, Row, Input, Badge, InputGroup, InputGroupAddon, InputGroupText, Button, Card, CardBody, Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap'
import classnames from 'classnames'
import ResultItemNews from './ui/ResultItemNews'

class Search extends Component {

    state = {
      activeTab: "1",
      searchTerms: "",
      news: [],
      documents: [],
      events: [],
      forum: [],
      newsFiltered: [],
      documentsFiltered: [],
      eventsFiltered: [],
      forumFiltered: []
    }

    newsResults = () => {
      
      const { newsFiltered } = this.state

      return (
        <>
          <h4 className="card-title mb-3">Search results</h4>
          { newsFiltered.length < 1 && <p>No news found.</p> }
          { newsFiltered.map(item => {
            return <ResultItemNews key={item.id} {...item} />
          }) }
        </>
      )
    }

    documentsResults = () => {
      return (
        <>
          <h4 className="card-title mb-3">Search results</h4>
          <h1>Documents Results</h1>
        </>
      )
    }

    eventsResults = () => {
      return (
        <>
          <h4 className="card-title mb-3">Search results</h4>
          <h1>Events Results</h1>
        </>
      )
    }

    forumResults = () => {
      return (
        <>
          <h4 className="card-title mb-3">Search results</h4>
          <h1>Forum Results</h1>
        </>
      )
    }

    tabPane = () => {
      return (
        <>
          <TabPane tabId="1">
            {this.newsResults()}
          </TabPane>
          <TabPane tabId="2">
            {this.documentsResults()}
          </TabPane>
          <TabPane tabId="3">
            {this.eventsResults()}
          </TabPane>
          <TabPane tabId="4">
            {this.forumResults()}
          </TabPane>
        </>
      )
    }

  toggle = tab => this.setState({ activeTab: tab })

  doSearch = e => {
    e.preventDefault()
    
    const { news, documents, events, forum } = this.state
    let searchTerms = e.target.value

    this.setState({
      newsFiltered: this.filtered(news, searchTerms),
      documentsFiltered: this.filtered(documents, searchTerms),
      eventsFiltered: this.filtered(events, searchTerms),
      forumFiltered: this.filtered(forum, searchTerms)
    })
  } 

  filtered = (collection, searchTerms) => {
    
    const terms = _.split(searchTerms.trim().toLowerCase(), " ")
    
    const newCollection = _.filter(collection, item => {
      return _.intersection(item.search, terms).length > 0
    })

    return newCollection
  }

  componentDidMount = () => {
    const { news, documents, events, forum } = this.props
    const searchTerms = undefined != this.props.history.location.state ? this.props.history.location.state.searchTerms : ""

    this.setState({
      searchTerms,
      news,
      documents,
      events,
      forum,
      newsFiltered: this.filtered(news, searchTerms),
      documentsFiltered: this.filtered(documents, searchTerms),
      eventsFiltered: this.filtered(events, searchTerms),
      forumFiltered: this.filtered(forum, searchTerms)
    })
  }


    
     render() {

          let { activeTab, searchTerms, newsFiltered, documentsFiltered, eventsFiltered, forumFiltered } = this.state
          
          return (
               <div className="animated fadeIn">
                  <Row>
                    <Col>
                      <InputGroup className="input-prepend">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="fa fa-search"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input bsSize="lg" type="text" placeholder="What are you looking for?" defaultValue={searchTerms} onChange={this.doSearch} />
                      </InputGroup>
                      <p className="text-muted" style={{ paddingTop: '.5rem' }}></p>
                    </Col>
                  </Row>
                  <Row>
                    <Col className="mb-4">
                      <Nav tabs>
                        <NavItem>
                          <NavLink
                            active={activeTab === "1"}
                            onClick={() => this.toggle("1") }
                          >
                            News
                            {'\u00A0'}{ newsFiltered.length > 0 ? <Badge color="success">{newsFiltered.length}</Badge> : <Badge color="danger">{newsFiltered.length}</Badge> } 
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            active={activeTab === "2"}
                            onClick={() => this.toggle("2") }
                          >
                            Documents
                            {'\u00A0'}{ documentsFiltered.length > 0 ? <Badge color="success">{documentsFiltered.length}</Badge> : <Badge color="danger">{documentsFiltered.length}</Badge> } 
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            active={activeTab === "3"}
                            onClick={() => this.toggle("3") }
                          >
                            Events
                            {'\u00A0'}{ eventsFiltered.length > 0 ? <Badge color="success">{eventsFiltered.length}</Badge> : <Badge color="danger">{eventsFiltered.length}</Badge> } 
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            active={activeTab === "4"}
                            onClick={() => this.toggle("4") }
                          >
                            Forum
                            {'\u00A0'}{ forumFiltered.length > 0 ? <Badge color="success">{forumFiltered.length}</Badge> : <Badge color="danger">{forumFiltered.length}</Badge> } 
                          </NavLink>
                        </NavItem>
                      </Nav>
                      <TabContent activeTab={activeTab}>
                        {this.tabPane()}
                      </TabContent>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Card>
                        <CardBody>
                          <h4 className="card-title mb-3">Search results</h4>
                          <div className="mb-4">
                            <h3 className="display-5 mb-0">
                              <Link to="/news">Guidelines for posting images</Link>
                            </h3>
                            <p className="mb-1"><Link className="text-success" to="/news"><i className="nav-icon icon-star"></i> News</Link></p>
                            <p className="mb-0">Phasellus accumsan dolor at tristique pretium. Quisque interdum rutrum lectus at ultricies. Cras cursus velit sit amet auctor sodales. Maecenas egestas lorem sit amet gravida blandit.</p>
                            <p className="text-muted mb-0">Posted on 10 Dec 2018 by <em>Jane Smith</em></p>
                            <hr/>
                          </div>
                          <div className="mb-4">
                            <h3 className="display-5 mb-0">
                              <Link to="/documents">Guidelines for posting images</Link>
                            </h3>
                            <p className="mb-1"><Link className="text-success" to="/documents"><i className="nav-icon icon-docs"></i> Documents</Link></p>
                            <p className="mb-0">Phasellus accumsan dolor at tristique pretium. Quisque interdum rutrum lectus at ultricies. Cras cursus velit sit amet auctor sodales. Maecenas egestas lorem sit amet gravida blandit.</p>
                            <p className="text-muted mb-0">Posted on 10 Dec 2018 by <em>Jane Smith</em></p>
                            <hr/>
                          </div>
                          <div className="mb-4">
                            <h3 className="display-5 mb-0">
                              <Link to="/forum">Paying Full Tuition When the Daycare Closes for a Snow Day...Is This Fair?</Link>
                            </h3>
                            <p className="mb-1"><Link className="text-success" to="/forum"><i className="nav-icon icon-bubbles"></i> Forum</Link></p>
                            <p className="mb-0">Phasellus accumsan dolor at tristique pretium. Quisque interdum rutrum lectus at ultricies. Cras cursus velit sit amet auctor sodales. Maecenas egestas lorem sit amet gravida blandit.</p>
                            <p className="text-muted mb-0">Posted on 10 Dec 2018 by <em>Jane Smith</em></p>
                            <hr/>
                          </div>
                          <div className="mb-4">
                            <h3 className="display-5 mb-0">
                              <Link to="/news">Guidelines for posting images</Link>
                            </h3>
                            <p className="mb-1"><Link className="text-success" to="/news"><i className="nav-icon icon-star"></i> News</Link></p>
                            <p className="mb-0">Phasellus accumsan dolor at tristique pretium. Quisque interdum rutrum lectus at ultricies. Cras cursus velit sit amet auctor sodales. Maecenas egestas lorem sit amet gravida blandit.</p>
                            <p className="text-muted mb-0">Posted on 10 Dec 2018 by <em>Jane Smith</em></p>
                            <hr/>
                          </div>
                          <div className="mb-4">
                            <h3 className="display-5 mb-0">
                              <Link to="/events">Guidelines for posting images</Link>
                            </h3>
                            <p className="mb-1"><Link className="text-success" to="/events"><i className="nav-icon icon-calendar"></i> Events</Link></p>
                            <p className="mb-0">Phasellus accumsan dolor at tristique pretium. Quisque interdum rutrum lectus at ultricies. Cras cursus velit sit amet auctor sodales. Maecenas egestas lorem sit amet gravida blandit.</p>
                            <p className="text-muted mb-0">Posted on 10 Dec 2018 by <em>Jane Smith</em></p>
                            <hr/>
                          </div>
                          <div className="mb-4">
                            <h3 className="display-5 mb-0">
                              <Link to="/news">Guidelines for posting images</Link>
                            </h3>
                            <p className="mb-1"><Link className="text-success" to="/news"><i className="nav-icon icon-star"></i> News</Link></p>
                            <p className="mb-0">Phasellus accumsan dolor at tristique pretium. Quisque interdum rutrum lectus at ultricies. Cras cursus velit sit amet auctor sodales. Maecenas egestas lorem sit amet gravida blandit.</p>
                            <p className="text-muted mb-0">Posted on 10 Dec 2018 by <em>Jane Smith</em></p>
                          </div>
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                </div>
          )
     }
}

const mapStateToProps = state => {
  return {
    documents: state.documents.data.filter(item => !item.draft && item.published),
    news: state.news.data.filter(item => !item.draft && item.published),
    events: [],
    forum: []
  }
}

export default connect(mapStateToProps)(Search)