import React from 'react'
import QuestionsTable from '../../Forum/ui/QuestionsTable'

const ForumResults = ({ results }) => {
	return (
		<div>
			<h4 className="card-title mb-3">Search results</h4>
			{ results.length < 1 && <p>No items in forum found.</p> }
	        <QuestionsTable questions={results} show="ALL" searching={false} terms={[]}  />
		</div>
	)
}

export default ForumResults