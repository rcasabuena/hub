import React from 'react'

const EventsResults = ({ results }) => {
	return (
		<div>
			<h4 className="card-title mb-3">Search results</h4>
			{ results.length < 1 && <p>No events found.</p> }
	        { results.map(item => {
	            return <p key={item.id} {...item}>Event</p>
	        }) }
		</div>
	)
}

export default EventsResults