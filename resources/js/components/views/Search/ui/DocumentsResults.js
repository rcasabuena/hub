import React from 'react'
import FolderTableItem from '../../Documents/ui/FolderTableItem'
import { Table } from 'reactstrap'

const DocumentsResults = ({ results }) => {
	return (
		<div>
			<h4 className="card-title mb-3">Search results</h4>
			{ results.length < 1 && <p>No documents found.</p> }
			{ results.length > 0 && 
				<Table responsive className="documents-table">
	            	<thead>
	            		<tr>
	              		<th style={{width: '1%'}} className="pl-0 pr-0"></th>
	                  <th style={{width: '90%'}}>Name</th>
	                  <th style={{width: '9%'}}></th>
	            		</tr>
	            	</thead>
	            	<tbody>
	            		{results.map((item, idx) => <FolderTableItem key={item.id} itemDetails={item} idx={idx} search={true} />)}
	            	</tbody>
	          </Table>
			}
		</div>
	)
}

export default DocumentsResults