import React from 'react'
import { NavItem, NavLink, Badge } from 'reactstrap'

const NavTab = ({ activeTab, onTabClick, results, label }) => {
	return (
		<NavItem>
	        <NavLink
	            active={activeTab}
	            onClick={onTabClick}
	        >
            	{label}
            	{'\u00A0'}{ results.length > 0 ? <Badge color="success">{results.length}</Badge> : <Badge color="danger">{results.length}</Badge> } 
          	</NavLink>
        </NavItem>
	)
}

export default NavTab