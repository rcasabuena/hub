import React from 'react'
import ResultItemNews from './ResultItemNews'

const NewsResults = ({ results }) => {
	return (
		<div>
			<h4 className="card-title mb-3">Search results</h4>
			{ results.length < 1 && <p>No news found.</p> }
	        { results.map(item => {
	            return <ResultItemNews key={item.id} {...item} />
	        }) }
		</div>
	)
}

export default NewsResults