import React from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom'

const ResultItemNews = props => {
	const { author, publish, headline, title, id, location } = props
	return (
		<div className="mb-4 animated fadeIn">
	        <h3 className="display-5 mb-0">
	          <Link to={`/news/${id}`}>{title}</Link>
	        </h3>
	        <p className="mb-1"><Link className="text-success" to="/news"><i className="nav-icon icon-star"></i> News</Link></p>
	        <p className="mb-0">{headline}</p>
	        <small>Posted on <strong>{moment(publish).fromNow()}</strong> by <em>{author}, {location}</em></small>
	        <hr/>
	    </div>
	)
}

export default ResultItemNews