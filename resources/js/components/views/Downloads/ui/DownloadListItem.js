import React from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom' 

const DownloadListItem = props => {
	
	const { username, filename, location, created_at, mime_type, user_id } = props
	const icon = _mimeTypes[mime_type] ? _mimeTypes[mime_type] : 'file-o'

	return(
		<tr>
			<td style={{width: '10px'}} className="pl-0 pr-0"><i className={`fa fa-${icon}`}></i></td>
			<td>
				{filename}<br/>
				<p><strong>{moment(created_at).fromNow()}</strong> by <Link to={`/users/${user_id}/view`}>{username}</Link>, {location}</p>
			</td>
		</tr>
	)
}

export default DownloadListItem