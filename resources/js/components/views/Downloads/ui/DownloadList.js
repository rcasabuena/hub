import React, { Component } from 'react'
import Fuse from 'fuse.js'
import { Table, Progress, Fade } from 'reactstrap'
import { connect } from 'react-redux'
import DownloadListItem from './DownloadListItem'

class DownloadList extends Component {
	render() {
		
		const { downloads, searchTerms } = this.props
		let	tableDataFiltered = downloads	
		const searchOptions = {
	      	..._globalFuseOptions,
	      	keys: _globalFuseKeys['downloads']
	    }

	    if (undefined !== searchTerms && searchTerms.trim() !== "") {    
	      	const fuse = new Fuse(tableDataFiltered, searchOptions)
	      	tableDataFiltered = fuse.search(searchTerms.trim())
	    } else {
	    	tableDataFiltered = downloads
	    }

		return (
			<Table responsive className="documents-table mt-3">
				<tbody>
					{tableDataFiltered.map((item, idx) => <DownloadListItem key={idx} {...item} />)}
				</tbody>
			</Table>
		)
	}
}

const mapStateToProps = ({ downloads }) => {
	return {
		downloads: downloads.data
	}
}

export default connect(mapStateToProps)(DownloadList)