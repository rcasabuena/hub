import React, { Component } from 'react'
import { Col, Row, Card, CardBody } from 'reactstrap'
import { connect } from 'react-redux'
import ModuleSearchBox from '../../ui/ModuleSearchBox'
import { handleSearch2 } from '../../functions/Utils'
import DownloadList from './ui/DownloadList'

class Downloads extends Component {
	constructor(props) {
	    super(props)
	    this.state = {
	      searchTerms: ""
	    }
	    this.handleSearch = handleSearch2.bind(this)
  	}
	render() {
		const { searchTerms } = this.state
		
		return (
			<div className="animated fadeIn">
        		<ModuleSearchBox handleSearch={this.handleSearch} placeholder="Search..."/>
				<Row>
		            <Col>
		              	<Card>
			                <CardBody>
			                    <h4 className="card-title mb-0">Downloads Log</h4>
			                    <p className="text-muted">&nbsp;</p>
			                    <DownloadList searchTerms={searchTerms} />
			                </CardBody>
		              	</Card>
		            </Col>
	          	</Row>
			</div>
		)
	}
}

export default Downloads