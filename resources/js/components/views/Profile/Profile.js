import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Row, Col, Card, CardBody, CardHeader, Nav, TabContent, TabPane } from 'reactstrap'
import PageHeader from '../../ui/PageHeader'
import DataTable from '../../ui/Tables/DataTable'
import NavTab from './ui/NavTab'

class ViewUser extends Component {
  
  state = {
    activeTab: "1"
  }

  toggle = tab => this.setState({ activeTab: tab })

  render() {
    const { user } = this.props
    const { activeTab } = this.state

    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardBody>
                <h4>{user.name}</h4>
                <p><span><i className="icon-envelope"></i> <a href={`mailto:${user.email}`}>{user.email}</a></span> <span className="ml-2"><i className="icon-location-pin"></i> {user.location && user.location.name}</span></p>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col className="mb-4">
            <Nav tabs>
              <NavTab activeTab={activeTab === "1"} onTabClick={() => this.toggle("1")} label={"All Tasks"} />
              <NavTab activeTab={activeTab === "2"} onTabClick={() => this.toggle("2")} label={"Downloads Log"} />
              <NavTab activeTab={activeTab === "3"} onTabClick={() => this.toggle("3")} label={"Forum Activity"} />
            </Nav>
            <TabContent activeTab={activeTab}>
              <TabPane tabId="1">
                <div>
                  <DataTable tableMap="userTodos" filterBy="user_id" filterByValue={user.id} />
                </div>
              </TabPane>
              <TabPane tabId="2">
                <div>
                  <DataTable tableMap="downloads" filterBy="user_id" filterByValue={user.id} />
                </div>
              </TabPane>
              <TabPane tabId="3">
                <div>Forum Activity</div>
              </TabPane>
            </TabContent>
          </Col>
        </Row>
      </div>
    )
  }
}

const mapStateToProps = ({currentUser}, ownProps) => {
  return {
    user: currentUser
  }
}

export default connect(mapStateToProps)(ViewUser)