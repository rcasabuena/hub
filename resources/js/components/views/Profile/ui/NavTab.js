import React from 'react'
import { NavItem, NavLink, Badge } from 'reactstrap'

const NavTab = ({ activeTab, onTabClick, label }) => {
	return (
		<NavItem>
	        <NavLink
	            active={activeTab}
	            onClick={onTabClick}
	        >
            	{label}
          	</NavLink>
        </NavItem>
	)
}

export default NavTab