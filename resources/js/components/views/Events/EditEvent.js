import React from 'react'
import EventForm from './EventForm'
import { Card, CardBody, Col, Row } from 'reactstrap'
import PageHeader from '../../ui/PageHeader'

const EditEvent = props => {
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardBody>
              <PageHeader 
                pageTitle="Edit Event"
                pageInfo="&nbsp;"
              />
              <EventForm />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

export default EditEvent