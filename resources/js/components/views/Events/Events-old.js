import React, { Component } from 'react'
import { connect } from 'react-redux'
import BigCalendar from 'react-big-calendar'
import Calendar from './Calendar'
import moment from 'moment'
import { Button, Card, CardBody, CardHeader, Col, Container, Jumbotron, Row } from 'reactstrap'

class Events extends Component {
    state = {

    }
     render() {
          const localizer = BigCalendar.momentLocalizer(moment) 
          const myEventsList = [{
                    id: 0,
                    title: 'Release of new marketing guidelines',
                    allDay: true,
                    start: new Date(2019, 0, 1),
                    end: new Date(2019, 0, 3),
                  },
                  {
                    id: 1,
                    title: 'Ofsted: developments in early years inspection',
                    start: new Date(2019, 0, 7),
                    end: new Date(2019, 0, 10),
                  },

                  {
                    id: 2,
                    title: 'SEMINAR: SEND – Supporting Children’s Behaviour, Emotional and Social Needs',
                    start: new Date(2019, 0, 13, 0, 0, 0),
                    end: new Date(2019, 0, 20, 0, 0, 0),
                  },

                  {
                    id: 3,
                    title: 'Supporting Physical Development through Music and Movement',
                    start: new Date(2019, 0, 6, 0, 0, 0),
                    end: new Date(2019, 0, 13, 0, 0, 0),
                  },

                  {
                    id: 4,
                    title: 'Meeting',
                    start: new Date(2019, 0, 9, 0, 0, 0),
                    end: new Date(2019, 0, 10, 0, 0, 0),
                  },
                  {
                    id: 5,
                    title: 'MASTERCLASS: The brain, learning and the early years',
                    start: new Date(2019, 0, 11),
                    end: new Date(2019, 0, 13),
                    desc: 'Big conference for important people',
                  },
                  {
                    id: 6,
                    title: 'Meeting',
                    start: new Date(2019, 0, 12, 10, 30, 0, 0),
                    end: new Date(2019, 0, 12, 12, 30, 0, 0),
                    desc: 'Pre-meeting meeting, to prepare for the meeting',
                  },
                  {
                    id: 7,
                    title: 'Enabling Environments: making the most of small-world play',
                    start: new Date(2019, 0, 12, 12, 0, 0, 0),
                    end: new Date(2019, 0, 12, 13, 0, 0, 0)
                  },
                  {
                    id: 8,
                    title: 'Meeting',
                    start: new Date(2019, 0, 12, 14, 0, 0, 0),
                    end: new Date(2019, 0, 12, 15, 0, 0, 0),
                  },
                  {
                    id: 9,
                    title: 'Enabling Environments: making the most of small-world play',
                    start: new Date(2019, 0, 12, 17, 0, 0, 0),
                    end: new Date(2019, 0, 12, 17, 30, 0, 0),
                    desc: 'Most important meal of the day',
                  },
                  {
                    id: 10,
                    title: 'Inter-generational care and learning: lifelong lessons',
                    start: new Date(2019, 0, 12, 20, 0, 0, 0),
                    end: new Date(2019, 0, 12, 21, 0, 0, 0),
                  },
                  {
                    id: 11,
                    title: 'Behaviour: a Froebelian approach',
                    start: new Date(2019, 0, 13, 7, 0, 0),
                    end: new Date(2019, 0, 13, 10, 30, 0),
                  },
                  {
                    id: 12,
                    title: 'Inclusion: supporting children with hidden disabilities',
                    start: new Date(2019, 0, 17, 19, 30, 0),
                    end: new Date(2019, 0, 18, 2, 0, 0),
                  },
                  {
                    id: 12.5,
                    title: 'Early years pedagogy: learning and teaching in the EYFS',
                    start: new Date(2019, 0, 17, 19, 30, 0),
                    end: new Date(2019, 0, 17, 23, 30, 0),
                  },
                  {
                    id: 13,
                    title: 'Sensory Play & the EYFS',
                    start: new Date(2019, 0, 20, 19, 30, 0),
                    end: new Date(2019, 0, 22, 2, 0, 0),
                  },
                  {
                    id: 14,
                    title: 'Communication and Language: building children’s vocabulary',
                    start: new Date(new Date().setHours(new Date().getHours() - 3)),
                    end: new Date(new Date().setHours(new Date().getHours() + 3)),
                  },
                  ]
          return (
               <div className="animated fadeIn">
                  <Row>
                    <Col>
                      <Card>
                        <CardBody>
                          <h4 className="card-title mb-0">Events</h4>
                          <p className="text-muted">&nbsp;</p>
                          <Calendar />
                          {/*<BigCalendar
                            localizer={localizer}
                            events={myEventsList}
                            startAccessor="start"
                            endAccessor="end"
                          />*/}
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                </div>
          )
     }
}

const mapStateToProps = state => {
  return {
    fetching: state.events.fetching,
    events: state.events.data,
    isAdmin: state.currentUser.role === 'ADMIN'
  }
}


export default connect(mapStateToProps)(Events)