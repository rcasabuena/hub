import React, { Component } from 'react'
import moment from 'moment'
import TextInput from '../../ui/Form/TextInput'
import FileInput from '../../ui/Form/FileInput'
import WysiwygInput from '../../ui/Form/WysiwygInput'
import DateInput from '../../ui/Form/DateInput'
import ToggleSwitch from '../../ui/Form/ToggleSwitch'
import FormSubmitActions from '../../ui/Form/FormSubmitActions'
import { Form } from 'reactstrap'
import AppEvent from '../../../objects/AppEvent'
import formConnect from '../../../HOC/formConnect'

class EventForm extends Component {

  constructor(props) {
    super(props)
    const { formUtils } = props
    this.handleChange = formUtils.handleChange.bind(this)
    this.handleToggle = formUtils.handleToggle.bind(this)
    this.handleUpload = formUtils.handleUpload.bind(this)
    this.handleEditorChange = formUtils.handleEditorChange.bind(this)
    this.handleDelete = formUtils.handleDelete.bind(this)
    this.handleSubmit = formUtils.handleSubmit.bind(this)
  }

  state = {
    item: new AppEvent()
  }

  componentDidMount() {
    const { item } = this.props
    this.setState({item})
  }
  
  render() {
    const { id, title, body, venue, start_date, end_date, draft } = this.props.item
    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <ToggleSwitch 
            handleToggle={this.handleToggle} 
            inputChecked={!draft} 
            inputLabel="Published" 
            inputName="draft" 
            inputHelpText="" 
            switchType="pill" 
            switchColour="primary" 
            inverseChecked={true}
          />
          <TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={title} 
            inputName="title" 
            inputLabel="Title" 
            inputHelpText="This is a help text" 
          />
          <WysiwygInput 
            inputName="body"
            inputLabel="Body"
            inputHelpText="This is a help text" 
            body={body}
            handleEditorChange={this.handleEditorChange}
          />
          <TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={venue} 
            inputName="venue" 
            inputLabel="Venue" 
            inputHelpText="This is a help text" 
          />
          <DateInput 
            handleChange={this.handleChange} 
            inputDefaultValue={moment(start_date).format('YYYY-MM-DD')} 
            inputName="start_date" 
            inputLabel="Start Date" 
            inputHelpText="This is a help text" 
          />
          <DateInput 
            handleChange={this.handleChange} 
            inputDefaultValue={moment(end_date).format('YYYY-MM-DD')} 
            inputName="end_date" 
            inputLabel="End Date" 
            inputHelpText="This is a help text" 
          />
          <FormSubmitActions 
            id={id}
            handleDelete={this.handleDelete}
          />
        </Form>
      </div>
    )
  }

}

const mapStateToProps = ({ events }, { match }) => {
  const event = events.data.find(item => item.id === match.params.unique_id)
  const id = event ? event.id : "",
      title = event ? event.title: "",
      body = event ? event.body: "",
      venue = event ? event.venue: "",
      start_date = event ? event.start_date: "",
      end_date = event ? event.end_date: "",
      draft = event ? event.draft: true
  return {
      item: new AppEvent(id, title, body, venue, start_date, end_date, draft)
  }
}

export default formConnect(EventForm, "Events", mapStateToProps)