import React from 'react'
import { Row, Col, Input, InputGroup, InputGroupAddon, InputGroupText, Button } from 'reactstrap'

const ModuleSearchBox2 = (props) => {
	
	const { isAdmin, handleSearch, showAll, toggleShowAll } = props

	return (
		<Row>
          <Col>
            <InputGroup className="input-prepend mb-4">
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <i className="fa fa-search"></i>
                </InputGroupText>
              </InputGroupAddon>
              <Input bsSize="lg" type="text" placeholder="What are you looking for?" onChange={handleSearch} />
            </InputGroup>
          </Col>
        </Row>
	)
}

export default ModuleSearchBox2