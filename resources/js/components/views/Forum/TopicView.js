import React, { Component } from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { Card, CardBody, CardHeader, Row, Col } from 'reactstrap'
import QuestionsTable from './ui/QuestionsTable'

class TopicView extends Component {
	render() {
		const { topic, questions } = this.props
		return (
			<div className="animated fadeIn">
              <Row>
                <Col>
                  <Card>
                    <CardBody>
                        <h4 className="card-title mb-3">Topic</h4>
                        <Link to="/forum/ask-a-question" className="btn btn-primary pull-right">Ask a question</Link>
                        <h1 className="display-5">{topic.title}</h1>
                        {/*<div>{renderHTML(body)}</div>*/}
                        <hr className="my-2" />
                        <p className="text-muted">Started {moment(topic.created).fromNow()} by <em>{topic.author}</em></p>
                        <QuestionsTable questions={questions} />
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </div>
		)
	}
}

const mapStateToProps = ({ topics, questions }, {match}) => {

	return {
		questions: questions.data.filter(item => item.topic && item.topic.id === match.params.unique_id),
		topic: topics.data.find(item => item.id === match.params.unique_id)
	}
}

export default connect(mapStateToProps)(TopicView)
