import React, { Component } from 'react'
import { connect } from 'react-redux'
import CreatableSelect from 'react-select/lib/Creatable'
import { Card, CardBody, CardHeader, FormGroup, Label, Button, Form } from 'reactstrap'
import { saveResource } from '../../../../actions'

class NewTopic extends Component {
  state = {
    id: "",
    title: "",
    select: {
      value: null,
      options: []
    }
  }
  
  handleChange = (res, meta) => {
    const { select } = this.state

    if (meta.action == 'create-option') {
      const { value: title } = res
      this.setState({
        title,
        select: {
          ...select,
          value: res
        }
      })
    } else {
      this.setState({
        title: "",
        select: {
          ...select,
          value: null
        }
      })
    }

  }

  handleSubmit = e => {
    e.preventDefault()

    const { title, id, select } = this.state
    if (title.trim() === "") return null
    
    let data = new FormData()
    data.append('id', id)
    data.append('title', title)

    this.props.saveTopic(data)
    
    this.setState({
      select: {
        options: [...select.options, select.value],
        value: null
      }
    })

  }

  componentDidMount = () => {
    
    const { select } = this.state
    const { topics } = this.props

    this.setState({
      select: {
        ...select,
        options: topics
      }
    })
  }

  render() {
      const { select } = this.state
      const { options, value } = select

      return (
        <Card>
                <CardHeader>
                  Start a new topic
                </CardHeader>
                <CardBody>
                  <Form onSubmit={this.handleSubmit}>
                    <FormGroup>
                      {/*<Label>Topic</Label>*/}
                      <CreatableSelect 
                        components={{
                          DropdownIndicator: null
                        }}
                        value={value}
                        placeholder="New topic..."
                        isClearable
                        onChange={this.handleChange}
                        options={options} />
                    </FormGroup>
                    <FormGroup className="mb-0">
                      <Button size="sm" color="primary">Submit</Button>
                    </FormGroup>
                  </Form>
                </CardBody>
            </Card>
      )
  }
}

const mapStateToProps = state => {
  return {
    topics: state.topics.data
  }
}

const mapDisparchToProps = dispatch => {
  return {
    saveTopic: data => { 
      dispatch(
        saveResource('Topics', data)
      )
    }
  }

}

export default connect(mapStateToProps, mapDisparchToProps)(NewTopic)