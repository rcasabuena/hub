import React from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { Row, Col, Badge } from 'reactstrap'

const QuestionListItem = props => {
	
	const { id, title, author, created } = props

	return (
        <div>
        	<Link to={`/forum/question/${id}`}>{title}</Link>
        	<p className="mt-2 mb-0 text-muted">Asked {moment(created).fromNow()} by <em>{author}</em></p>
        	<hr className="mb-4" />
      	</div>
	)
} 

export default QuestionListItem