import React from 'react'
import { Card, CardBody, CardHeader, Badge } from 'reactstrap'

const PopularTags = props => {
	return (
		<Card>
            <CardHeader>
              Popular Tags
            </CardHeader>
            <CardBody>
              <Badge color="success" className="mr-2">Ofsted</Badge>
              <Badge color="success" className="mr-2">Early Development</Badge>
              <Badge color="success" className="mr-2">Adoption</Badge>
              <Badge color="success" className="mr-2">Play</Badge>
              <Badge color="success" className="mr-2">Franchise</Badge>
              <Badge color="success" className="mr-2">Ofsted</Badge>
              <Badge color="success" className="mr-2">Early Development</Badge>
              <Badge color="success" className="mr-2">Adoption</Badge>
              <Badge color="success" className="mr-2">Play</Badge>
              <Badge color="success" className="mr-2">Franchise</Badge>
              <Badge color="success" className="mr-2">Ofsted</Badge>
              <Badge color="success" className="mr-2">Early Development</Badge>
              <Badge color="success" className="mr-2">Adoption</Badge>
              <Badge color="success" className="mr-2">Play</Badge>
              <Badge color="success" className="mr-2">Franchise</Badge>
              <Badge color="success" className="mr-2">Ofsted</Badge>
              <Badge color="success" className="mr-2">Early Development</Badge>
              <Badge color="success" className="mr-2">Adoption</Badge>
              <Badge color="success" className="mr-2">Play</Badge>
              <Badge color="success" className="mr-2">Franchise</Badge>
            </CardBody>
        </Card>
	)
}

export default PopularTags