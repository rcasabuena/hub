import React, { Component } from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { Badge, Card, CardBody, CardHeader, Col, ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText, Row, TabContent, TabPane } from 'reactstrap'

class TopicList extends Component {
	render() {
		const { topics } = this.props
		return (
			<Card>
              <CardHeader>
                Topics
              </CardHeader>
              <CardBody>
              	{ topics.lenght < 1 && <p>There are no topics yet.</p> }
              	{ topics.length > 0 &&
	                <ListGroup>
	                	{ topics.map(({ title, id, author, created, questions, location }) => {
							return (
								<ListGroupItem key={id} action>
			                    	<ListGroupItemHeading className="d-flex w-100 justify-content-between">
			                    		<Link to={`/forum/topic/${id}`}>{title}</Link>
			                    		<small className="text-right"><small>Last active</small><br/><strong>{moment(created).fromNow()}</strong></small>
			                    	</ListGroupItemHeading>
			                    	<ListGroupItemText>
			                    		{questions === 1 && <strong>1 Question<br/></strong>}
			                    		{questions !== 1 && <strong>{questions} Questions<br/></strong>}
			                      		<small>Added <strong>{moment(created).fromNow()}</strong> by <em>{author}, {location}</em></small>
			                    	</ListGroupItemText>
			                  	</ListGroupItem>
							)
	                	}) }
	                </ListGroup>
            	}
              </CardBody>
            </Card>
		)
	}
} 

const mapStateToProps = state => {
	return {
		topics: state.topics.data
	}
}

export default connect(mapStateToProps)(TopicList)