import React from 'react'
import { Link } from 'react-router-dom'
import { Card, CardHeader, CardBody } from 'reactstrap'

const ActiveTopics = props => {
	return (
		<Card>
            <CardHeader>
              Active Topics 
            </CardHeader>
            <CardBody>
              <p>
                <Link to="forum/question">Paying Full Tuition When the Daycare Closes for a Snow Day...Is This Fair?</Link>
              </p>
              <p>
                <Link to="forum/question">Charging on Holidays?</Link>
              </p>
              <p>
                <Link to="forum/question">Registration Fee, What Are They For?</Link>
              </p>
              <p>
                <Link to="forum/question">What is a Normal/Minor Injury at Daycare?</Link>
              </p>
              <p>
                <Link to="forum/question">Getting A Newborn "Daycare Ready"</Link>
              </p>
              <p>
                <Link to="forum/question">Why Do Daycare Centers Not Have 2nd Shift?</Link>
              </p>
            </CardBody>
          </Card>
	)
}

export default ActiveTopics