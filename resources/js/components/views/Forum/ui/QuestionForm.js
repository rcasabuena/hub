import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import TextInput from '../../../ui/Form/TextInput'
import FileInputMulti from '../../../ui/Form/FileInputMulti'
import ToggleSwitch from '../../../ui/Form/ToggleSwitch'
import WysiwygInput from '../../../ui/Form/WysiwygInput'
import SelectInput from '../../../ui/Form/SelectInput'
import FormSubmitActions from '../../../ui/Form/FormSubmitActions'
import { saveResource } from '../../../../actions'
import { Form, Card, CardHeader, CardBody } from 'reactstrap'
import AppQuestion from '../../../../objects/AppQuestion'
import * as formUtils from '../../../../functions/FormFunctions.js'

class QuestionForm extends Component {

  constructor(props) {
    super(props)
    this.handleChange = formUtils.handleChange.bind(this)
    this.handleUploadMulti = formUtils.handleUploadMulti.bind(this)
    this.handleToggle = formUtils.handleToggle.bind(this)
    this.handleSelect = formUtils.handleSelect.bind(this)
    this.handleEditorChange = formUtils.handleEditorChange.bind(this)
    this.handleDelete = formUtils.handleDelete.bind(this)
    this.handleSubmit = formUtils.handleSubmit.bind(this)
  }

  state = {
    item: new AppQuestion()
  }

  componentDidMount() {
    const { item } = this.props
    this.setState({item})
  }
  
  render() {

    const { id, title, body, sticky } = this.props.item
    const { topics, questionTopic } = this.props
    console.log(topics)
    return (
      <Card>
      	<CardHeader>
          Your question
        </CardHeader>
      	<CardBody>
      		<Form onSubmit={this.handleSubmit}>
            <SelectInput 
              inputName="topic_id"
              inputLabel="Topic"
              selectValue={questionTopic}
              selectOptions={topics}
              inputHelpText=""
              handleSelect={this.handleSelect}
              horizontal={true}
            />
            <TextInput 
              handleChange={this.handleChange} 
              inputDefaultValue={title} 
              inputName="title" 
              inputLabel="Title" 
              inputHelpText="" 
              horizontal={true}
            />
            <WysiwygInput 
              inputName="body"
              inputLabel="Body"
              inputHelpText="" 
              body={body}
              handleEditorChange={this.handleEditorChange}
              horizontal={true}
            />
            <FileInputMulti 
                handleUpload={this.handleUploadMulti} 
                inputDefaultValue={null} 
                inputName="attachments" 
                inputLabel="Attachments" 
                horizontal={true}
                inputHelpText="" 
            />
	          <FormSubmitActions 
              id={""}
            />
	        </Form>
      	</CardBody>
      </Card>
    )
  }

}

const mapStateToProps = ({ questions, topics }, { match }) => {
  const question = questions.data.find(item => item.id === match.params.unique_id)
  const id = question ? question.id : "",
      title = question ? question.title: "",
      body = question ? question.body: "",
      sticky = question ? question.sticky: false,
      topic_id = question && question.topic ? question.topic.id : ""
  return {
      topics: topics.data,
      questionTopic: question && question.topic ? question.topic : false,
      item: new AppQuestion(id, title, body, sticky, topic_id)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    save: (data, history) => { 
      dispatch(
        saveResource('Questions', data, history, '/forum')
      )
    },
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(QuestionForm))