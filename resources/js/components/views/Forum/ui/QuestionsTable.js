import React from 'react'
import moment from 'moment'
import Fuse from 'fuse.js'
import { Link } from 'react-router-dom'
import { Table, Badge } from 'reactstrap'

const QuestionsTable = props => {
	const { questions, show, searching, terms, searchTerms } = props
  let tableDataFiltered = questions

  const searchOptions = {
    ..._globalFuseOptions,
    keys: _globalFuseKeys['questions']
  }

  if (undefined !== searchTerms && searchTerms.trim() !== "") {      
    const fuse = new Fuse(tableDataFiltered, searchOptions)
    tableDataFiltered = fuse.search(searchTerms.trim())
  } else {
    tableDataFiltered = questions
  }

	return (
		<Table hover responsive className="table-outline mb-0 d-none d-sm-table">
          <thead className="thead-light">
            <tr>
              <th></th>
              <th className="text-center">Topic</th>
              <th className="text-center">Answers</th>
              <th className="text-center">Latest by</th>
            </tr>
            </thead>
            <tbody>
            {tableDataFiltered.length > 0 && 
              tableDataFiltered.map(question => {
                if (show === 'UNANSWERED' && question.answers.length > 0) return null 
                return (
                  <tr key={question.id}>
                    <td>
                      <div><Link to={`/forum/question/${question.id}`}>{question.title}</Link></div>
                      <div>
                        <small>{moment(question.created).isAfter(moment().subtract(7, 'days').startOf('day')) && <Badge color="primary">New</Badge> } Asked <strong>{moment(question.created).fromNow()}</strong> by <em>{question.author}, {question.location}</em> </small>
                      </div>
                    </td>
                    <td className="text-center">
                      {!question.topic && <div><em>Uncategorised</em></div>}
                      {question.topic && 
                        <div>
                          <Link to={`/forum/topic/${question.topic.id}`}>{question.topic.title}</Link>
                        </div>
                      }
                    </td>
                    <td className="text-center">
                      <div>{question.answers.length}</div>
                    </td>
                    <td className="text-center">
                      { question.answers.length > 0 &&
                        <div>
                          <div className="text-muted"><em>{question.answers[(question.answers.length-1)].author}</em></div>
                          <div>{moment(question.answers[(question.answers.length-1)].created).fromNow()}</div>
                        </div>
                      }
                      { question.answers.length < 1 && <div>-</div>}
                    </td>
                  </tr>
                )
              })
            }
            </tbody>
          </Table>
	)
} 

export default QuestionsTable
