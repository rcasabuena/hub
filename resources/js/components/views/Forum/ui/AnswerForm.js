import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import TextInput from '../../../ui/Form/TextInput'
import FileInputMulti from '../../../ui/Form/FileInputMulti'
import ToggleSwitch from '../../../ui/Form/ToggleSwitch'
import WysiwygInput from '../../../ui/Form/WysiwygInput'
import SelectInput from '../../../ui/Form/SelectInput'
import FormSubmitActions from '../../../ui/Form/FormSubmitActions'
import { saveAnswer } from '../../../../actions'
import { Form, Card, CardHeader, CardBody } from 'reactstrap'
import AppAnswer from '../../../../objects/AppAnswer'
import * as formUtils from '../../../../functions/FormFunctions.js'

class AnswerForm extends Component {

  constructor(props) {
    super(props)
    this.handleUploadMulti = formUtils.handleUploadMulti.bind(this)
    this.handleEditorChange = formUtils.handleEditorChange.bind(this)
    this.handleSubmit = formUtils.handleSubmit.bind(this)
  }

  state = {
    item: new AppAnswer()
  }

  componentDidMount() {
     
     let { item } = this.state
     item['question_id'] = this.props.match.params.unique_id

     this.setState({item})
  }
  
  render() {
    return (
      <Card>
      	<CardHeader>
          Your Answer
        </CardHeader>
      	<CardBody>
      		<Form onSubmit={this.handleSubmit}>
            <WysiwygInput 
              inputName="body"
              inputLabel="Body"
              inputHelpText="" 
              body={""}
              handleEditorChange={this.handleEditorChange}
              horizontal={true}
            />
            <FileInputMulti 
                handleUpload={this.handleUploadMulti} 
                inputDefaultValue={null} 
                inputName="attachments" 
                inputLabel="Attachments" 
                horizontal={true}
                inputHelpText="" 
            />
            <FormSubmitActions 
              id={""}
            />
          </Form>
      	</CardBody>
      </Card>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    save: data => { 
      dispatch(
        saveAnswer(data)
      )
    },
  }
}

export default withRouter(connect(null, mapDispatchToProps)(AnswerForm))