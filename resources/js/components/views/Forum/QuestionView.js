import React, { Component } from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { Link } from 'react-router-dom'
import renderHTML from 'react-render-html'
import { Button, Card, Media, CardBody, CardHeader, Col, Container, Jumbotron, Row, Badge, Form, Input, FormGroup, Label } from 'reactstrap'
import AnswerForm from './ui/AnswerForm'
import Answer from './ui/Answer'
import Attachments from '../../ui/Attachments'

class QuestionView extends Component {
     render() {
          const { title, body, created, author, id, answers, topic, location } = this.props.question
          return (
               <div className="animated fadeIn">
                  <Row>
                    <Col>
                      <Card>
                        <CardBody>
                            <h4 className="card-title mb-3">Question</h4>
                            <Link to="/forum/ask-a-question" className="btn btn-primary pull-right">Ask a question</Link>
                            <h1 className="display-5">{title}</h1>
                            <h5>
                              {topic && <Link to={`/forum/topic/${topic.id}`}>{topic.title}</Link> }
                              {!topic && <em>Uncategorised</em>}
                            </h5>
                            <div>{renderHTML(body)}</div>
                            <Attachments parentId={id} />
                            <hr className="my-2" />
                            <p>Asked <strong>{moment(created).fromNow()}</strong> by <em>{author}, {location}</em></p>
                        </CardBody>
                      </Card>
                      <Card>
                        <CardHeader>
                          { answers.length === 1 && <span>1 Answer</span> } 
                          { answers.length !== 1 && <span>{answers.length} Answers</span> } 
                        </CardHeader>
                        <CardBody>
                          {answers.length < 1 && <p>There are no answers yet.</p>}
                          {answers.length > 0 && 
                            answers.map((answer, idx) => {
                              return <Answer key={idx} {...answer} />
                            })
                          }
                        </CardBody>
                      </Card>
                      <AnswerForm id={id} />
                    </Col>
                  </Row>
                </div>
          )
     }
}

const mapStateToProps = (state, ownProps) => {
  return {
    question: state.questions.data.find(item => item.id === ownProps.match.params.unique_id)
  }
}

export default connect(mapStateToProps)(QuestionView)