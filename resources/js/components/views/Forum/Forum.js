import React, { Component } from 'react'
import moment from 'moment'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { Button, Card, Badge, CardBody, CardHeader, Col, Row, ButtonGroup, Table } from 'reactstrap'
import NewTopic from './ui/NewTopic'
import TopicList from './ui/TopicList'
import ModuleSearchBox from '../ModuleSearchBox'
import QuestionsTable from './ui/QuestionsTable'

class Forum extends Component {
    
    state = {
      searching: false,
      searchTerm: "",
      show: 'ALL'
    }

    handleListToggle = (e, show = 'ALL') => {
      this.setState({show})
    }

    handleSearch = e => {
      e.preventDefault()
      this.setState({
        searching: e.target.value.trim() !== "",
        searchTerm: e.target.value.trim()
      })
    }

     render() {
          const { isAdmin, questions } = this.props
          const { show, searching, searchTerm } = this.state
          const terms = _.split(searchTerm.trim().toLowerCase(), " ")

          questions.sort((a, b) => {
            let pubA = new Date(a.created)
            let pubB = new Date(b.created)
            
            if (show === 'RECENT') {
              pubA = new Date(a.lastActive)
              pubB = new Date(b.lastActive)
            }

            if (pubA < pubB) return 1
            if (pubA > pubB) return -1
            
            return 0
          })

          const unanswered = questions.filter(item => !item.answers.length)

          return (
               <div className="animated fadeIn">
                  <ModuleSearchBox isAdmin={isAdmin} showAll={false} handleSearch={this.handleSearch} toggleShowAll={false} />
                  <Row>
                    <Col md="8">
                      <Card>
                        <CardBody>
                          <h4 className="card-title mb-0">Welcome to Banana Moon Forum</h4>
                          <p className="text-muted">Here you can interact with the whole of the Banana Moon network. Use this online bulletin board to start conversations and discussions, share tips or seek advice, and engage with ongoing topics. Head Office may also start some topics, so keep an eye out for new posts!</p>
                          <div className="clearfix mb-3 mt-3">
                            <Link to="/forum/ask-a-question" className="btn btn-primary pull-left">Ask a question</Link>
                            <ButtonGroup color="dark" className="pull-right">
                              <Button outline color="dark" active={show === 'ALL'} onClick={e => this.handleListToggle(e, 'ALL')}>All</Button>
                              <Button outline color="dark" active={show === 'RECENT'} onClick={e => this.handleListToggle(e, 'RECENT')}>Recently Active</Button>
                              <Button outline color="dark" active={show === 'UNANSWERED'} onClick={e => this.handleListToggle(e, 'UNANSWERED')}><Badge color="primary">{unanswered.length}</Badge> Unanswered</Button>
                            </ButtonGroup>
                          </div>

                          <QuestionsTable questions={questions} show={show} searching={searching} terms={terms} searchTerms={searchTerm} />

                        </CardBody>
                      </Card>
                    </Col>
                    <Col md="4">
                      <NewTopic />
                      <TopicList />
                    </Col>
                  </Row>
                </div>
          )
     }
}

const mapStateToProps = state => {
  return {
    fetching: state.questions.fetching,
    questions: state.questions.data,
    isAdmin: state.currentUser.role === 'ADMIN'
  }
}

export default connect(mapStateToProps)(Forum)