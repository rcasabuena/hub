import React, { Component } from 'react'
import { Button, Card, CardBody, CardHeader, Col, Container, Row, Fade, Form, FormGroup, FormText, FormFeedback, Input, InputGroup, InputGroupAddon, InputGroupText, Label } from 'reactstrap'
import QuestionForm from './ui/QuestionForm'

class AskAQuestion extends Component {
     render() {
          return (
               <div className="animated fadeIn">
                  <Row>
                    <Col>
                      <Card>
                        <CardBody>
                            <h4 className="card-title mb-0">Ask a question</h4>
                            <p className="text-muted">We’d love to help you, but the reality is that not every question gets answered. To improve your chances, here are some tips:</p>
                            <p className="lead">Search, and research</p>
                            <p>Have you thoroughly searched for an answer before asking your question? Sharing your research helps everyone. Tell us what you found (on this site or elsewhere) and why it didn’t meet your needs. This demonstrates that you’ve taken the time to try to help yourself, it saves us from reiterating obvious answers, and above all, it helps you get a more specific and relevant answer!</p>
                            {/*<div className="mb-3">
                              <InputGroup className="input-prepend">
                                <InputGroupAddon addonType="prepend">
                                  <InputGroupText>
                                    <i className="fa fa-search"></i>
                                  </InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" placeholder="" />
                                <InputGroupAddon addonType="append">
                                  <Button color="primary">Search</Button>
                                </InputGroupAddon>
                              </InputGroup>
                            </div>*/}
                            <p className="lead">Be on-topic</p>
                            <p>Our community is defined by a specific set of topics in the help center; please stick to those topics and avoid asking for opinions or open-ended discussion. If your question is about the site itself, ask on our meta-discussion site.</p>
                            <p className="lead">Be specific</p>
                            <p>If you ask a vague question, you’ll get a vague answer. But if you give us details and context, we can provide a useful, relevant answer.</p>
                            <p className="lead">Make it relevant to others</p>
                            <p>We like to help as many people at a time as we can. Make it clear how your question is relevant to more people than just you, and more of us will be interested in your question and willing to look into it.</p>
                            <p className="lead">Keep an open mind</p>
                            <p>The answer to your question may not always be the one you wanted, but that doesn’t mean it is wrong. A conclusive answer isn’t always possible. When in doubt, ask people to cite their sources, or to explain how/where they learned something. Even if we don’t agree with you, or tell you exactly what you wanted to hear, remember: we’re just trying to help.</p>
                        </CardBody>
                      </Card>
                      <QuestionForm />
                    </Col>
                  </Row>
                </div>
          )
     }
}

export default AskAQuestion