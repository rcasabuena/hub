import React, { Component } from 'react'
import UserForm from './UserForm'
import { Card, CardBody, Col, Row } from 'reactstrap'

const EditUser = () => {
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardBody>
              <h4 className="card-title mb-0">Edit User</h4>
              <p className="text-muted">&nbsp;</p>
              <UserForm />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

export default EditUser