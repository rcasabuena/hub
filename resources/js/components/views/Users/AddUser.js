import React, { Component } from 'react'
import UserForm from './UserForm'
import { Card, CardBody, Col, Row } from 'reactstrap'

const UsersNew = () => {
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardBody>
              <h4 className="card-title mb-0">New User</h4>
              <p className="text-muted">&nbsp;</p>
              <UserForm />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

export default UsersNew