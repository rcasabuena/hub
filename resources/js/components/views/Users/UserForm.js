import React, { Component } from 'react'
import TextInput from '../../ui/Form/TextInput'
import SelectInput from '../../ui/Form/SelectInput'
import ToggleSwitch from '../../ui/Form/ToggleSwitch'
import FormSubmitActions from '../../ui/Form/FormSubmitActions'
import { Form } from 'reactstrap'
import AppUser from '../../../objects/AppUser'
import formConnect from '../../../HOC/formConnect'

class UserForm extends Component {

  constructor(props) {
    super(props)
    const { formUtils } = props
    this.handleChange = formUtils.handleChange.bind(this)
    this.handleToggle = formUtils.handleToggle.bind(this)
    this.handleDelete = formUtils.handleDelete.bind(this)
    this.handleSelect = formUtils.handleSelect.bind(this)
    this.handleSubmit = formUtils.handleSubmit.bind(this)
  }

  state = {
    item: new AppUser()
  }

  componentDidMount() {
    let { item } = this.props
    this.setState({ item }) 
  }
  
  render() {

    const { locations, userLocation, modules } = this.props
    const { id, name, email, admin, director } = this.state.item
    console.log(modules)
    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <ToggleSwitch 
            handleToggle={this.handleToggle} 
            inputChecked={admin} 
            inputLabel="Admin" 
            inputName="admin" 
            inputHelpText="" 
            switchType="pill" 
            switchColour="primary"
            inverseChecked={false}
          />
          <TextInput 
            inputType="text"
            inputName="name" 
            inputLabel="Name" 
            inputHelpText="This is a help text"
            isRequired={true}
            inputDefaultValue={name} 
            handleChange={this.handleChange} 
          />
          <TextInput 
            inputType="email"
            inputName="email" 
            inputLabel="Email" 
            inputHelpText="This is a help text"
            isRequired={true}
            inputDefaultValue={email} 
            handleChange={this.handleChange} 
          />
          <TextInput 
            inputType="password"
            inputName="password" 
            inputLabel="Password" 
            inputHelpText="This is a help text"
            isRequired={false}
            handleChange={this.handleChange} 
          />
          <SelectInput 
            inputName="location_id"
            inputLabel="Location"
            selectValue={userLocation}
            selectOptions={locations}
            inputHelpText="This is a help text"
            handleSelect={this.handleSelect}
          />
          <ToggleSwitch 
            handleToggle={this.handleToggle} 
            inputChecked={director} 
            inputLabel="Director" 
            inputName="director" 
            inputHelpText="" 
            switchType="pill" 
            switchColour="primary"
            inverseChecked={false}
          />
          <FormSubmitActions 
            id={id}
            handleDelete={this.handleDelete}
          />
        </Form>
      </div>
    )
  }

}

const mapStateToProps = (state, ownProps) => {
  const user = state.users.data.find(item => item.id === ownProps.match.params.unique_id)
  const id = user ? user.id : "",
        name = user ? user.name : "",
        email = user ? user.email : "",
        password = user ? user.password : "",
        admin = user ? user.admin : false,
        location_id = user && user.location ? user.location.id : "",
        director = user ? user.director : false

  return {
    item: new AppUser(id, name, email, password, admin, location_id, director),
    userLocation: user && user.location ? user.location : null,
    locations: state.locations.data,
    modules: state.modules.data
  }
}

export default formConnect(UserForm, "Users", mapStateToProps)