import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Row, Col, Card, CardBody, CardHeader } from 'reactstrap'
import PageHeader from '../../ui/PageHeader'
import DataTable from '../../ui/Tables/DataTable'

class ViewUser extends Component {
	render() {
		const { user } = this.props

		return (
			<div className="animated fadeIn">
				<Row>
		            <Col>
		              	<Card>
		                	<CardBody>
		                  		<h4>{user.name}</h4>
			                  	<p><span><i className="icon-envelope"></i> <a href={`mailto:${user.email}`}>{user.email}</a></span> <span className="ml-2"><i className="icon-location-pin"></i> {user.location && user.location.name}</span></p>
			                  	<DataTable tableMap="downloads" filterBy="user_id" filterByValue={user.id} />
		                	</CardBody>
		              	</Card>
		            </Col>
	          	</Row>
			</div>
		)
	}
}

const mapStateToProps = ({users}, ownProps) => {
	return {
		user: users.data.find(item => item.id === ownProps.match.params.unique_id)
	}
}

export default connect(mapStateToProps)(ViewUser)