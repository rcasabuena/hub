import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import ModuleSearchBox from '../../ui/ModuleSearchBox'
import DataTable from '../../ui/Tables/DataTable'
import { handleSearch2 } from '../../functions/Utils'
import BootstrapTable from 'react-bootstrap-table-next'
import ToolkitProvider, { CSVExport, Search } from 'react-bootstrap-table2-toolkit'
import { Button, Badge, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';

class Users extends Component {
    constructor(props) {
      super(props)
      this.state = {
        searchTerms: ""
      }
      this.handleSearch = handleSearch2.bind(this)
    }

     render() {
          const { users } = this.props
          const { ExportCSVButton } = CSVExport
          const { SearchBar } = Search

          const { searchTerms } = this.state
          const columns = [
                {
                    dataField: 'name',
                    text: 'Name',
                    sort: true,
                    formatter: (cellContent, row) => (
                      <Link to={`/users/${row.id}/edit`}>{row.name}</Link>
                    )
                  }, 
                  {
                    dataField: 'email',
                    text: 'Email',
                    sort: true,
                    formatter: (cellContent, row) => (
                      <a href={"mailto:" + row.email}>{row.email}</a>
                    )
                  }, 
                  {
                    dataField: 'date_registered',
                    text: 'Date registered',
                    sort: true
                  },
                  {
                    dataField: 'role',
                    text: 'Role',
                    sort: true,
                  },
                  {
                    dataField: 'status',
                    text: 'Status',
                    formatter: (cellContent, row) => row.status === 'Active' ? (
                      <Badge color="success">Active</Badge>
                    ) : (
                      <Badge color="danger">Disabled</Badge>
                    )
                  }
            ]

          return (
               <div className="animated fadeIn">
                  <ModuleSearchBox handleSearch={this.handleSearch} placeholder="Search..."/>
                  <Row>
                    <Col>
                      <Card>
                        <CardBody>
                          <h4 className="card-title mb-0">Users</h4>
                          <p className="text-muted">&nbsp;</p>
                          <ToolkitProvider
                            keyField="id"
                            data={ users }
                            columns={ columns }
                            exportCSV
                            search
                          >
                            {
                              props => (
                                <div>
                                  <ExportCSVButton className="btn btn-primary pull-right mb-3" { ...props.csvProps }>Export CSV</ExportCSVButton>
                                  <SearchBar className="mb-3" { ...props.searchProps } style={{
                                    width: '30%'
                                  }} />
                                  <BootstrapTable { ...props.baseProps } />

                                </div>
                              )
                            }
                            {/*<BootstrapTable keyField='id' data={ users } columns={ columns } noDataIndication="Table is Empty" />*/}
                          </ToolkitProvider>
                          <DataTable tableMap="users" searchTerms={searchTerms} tableResource="Users" />
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                </div>
          )
     }
}

const mapStateToProps = (state) => {
  return {
    fetching: state.users.fetching,
    users: state.users.data
  }
}

export default connect(mapStateToProps)(Users)

