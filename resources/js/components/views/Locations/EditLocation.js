import React, { Component } from 'react'
import LocationForm from './LocationForm'
import { Card, CardBody, Col, Row } from 'reactstrap'
import PageHeader from '../../ui/PageHeader'

const EditLocation = () => {
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardBody>
              <PageHeader 
                pageTitle="Edit Location"
                pageInfo="&nbsp;"
              />
              <LocationForm />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

export default EditLocation