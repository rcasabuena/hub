import React, { Component } from 'react'
import TextInput from '../../ui/Form/TextInput'
import FormSubmitActions from '../../ui/Form/FormSubmitActions'
import { Form } from 'reactstrap'
import { handleChange, handleDelete, handleSubmit } from '../../functions/CommonFormFunctions'
import AppLocation from '../../../objects/AppLocation'
import formConnect from '../../../HOC/formConnect'

class LocationForm extends Component {
  constructor(props) {
    super(props)
    const { formUtils } = props
    this.handleChange = formUtils.handleChange.bind(this)
    this.handleDelete = formUtils.handleDelete.bind(this)
    this.handleSubmit = formUtils.handleSubmit.bind(this)
  }

  state = {
    item: new AppLocation()
  }

  componentDidMount() {
    const { item } = this.props
    this.setState({item})
  }
  
  render() {
    const { id, name, address_line1, address_line2, address_city, address_county, address_postcode, email, telephone } = this.state.item
    return (
        <Form onSubmit={this.handleSubmit}>
          <TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={name} 
            inputName="name" 
            inputLabel="Name" 
            inputHelpText="This is a help text"
            isRequired={true}
          />
          <TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={address_line1} 
            inputName="address_line1" 
            inputLabel="Address Line 1" 
            inputHelpText="This is a help text" 
          />
          <TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={address_line2} 
            inputName="address_line2" 
            inputLabel="Address Line 2" 
            inputHelpText="This is a help text" 
          />
          <TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={address_city} 
            inputName="address_city" 
            inputLabel="Town/City" 
            inputHelpText="This is a help text" 
          />
          <TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={address_county} 
            inputName="address_county" 
            inputLabel="County" 
            inputHelpText="This is a help text" 
          />
          <TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={address_postcode} 
            inputName="address_postcode" 
            inputLabel="Postcode" 
            inputHelpText="This is a help text" 
          />
          <TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={email} 
            inputName="email" 
            inputLabel="Email" 
            inputHelpText="This is a help text" 
          />
          <TextInput 
            handleChange={this.handleChange} 
            inputDefaultValue={telephone} 
            inputName="telephone" 
            inputLabel="Telephone" 
            inputHelpText="This is a help text" 
          />
          <FormSubmitActions 
            id={id}
            handleDelete={this.handleDelete}
          />
        </Form>
    )
  }

}

const mapStateToProps = ({ locations }, { match }) => {
  const loc = locations.data.find(item => item.id === match.params.unique_id) 
  const id = loc ? loc.id : "",
        name = loc ? loc.name: "",
        address_line1 = loc ? loc.address_line1: "",
        address_line2 = loc ? loc.address_line2: "",
        address_city = loc ? loc.address_city: "",
        address_county = loc ? loc.address_county: "",
        address_postcode = loc ? loc.address_postcode: "",
        email = loc ? loc.email: "",
        telephone = loc ? loc.telephone: ""

  return {
    item: new AppLocation(id, name, address_line1, address_line2, address_city, address_county, address_postcode, email, telephone)
  }
}

export default formConnect(LocationForm, "Locations", mapStateToProps)