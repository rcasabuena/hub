import React, { Component } from 'react'
import { Row, Col, Card, CardBody } from 'reactstrap'
import ModuleSearchBox from '../../ui/ModuleSearchBox'
import DataTable from '../../ui/Tables/DataTable'
import { handleSearch2 } from '../../functions/Utils'
import PageHeader from '../../ui/PageHeader'


class Locations extends Component {

	constructor(props) {
		super(props)
		this.state = {
			searchTerms: ""
		}
		this.handleSearch = handleSearch2.bind(this)
	}
	
	render() {
		const { searchTerms } = this.state

		return (
			<div className="animated fadeIn">
				<ModuleSearchBox handleSearch={this.handleSearch} placeholder="Search..."/>
              	<Row>
	                <Col>
	                  	<Card>
		                    <CardBody>
							    <PageHeader 
					                pageTitle="Locations"
					                pageInfo="&nbsp;"
					                pageResource="locations"
					            />
		                      	<DataTable tableMap="locations" searchTerms={searchTerms} />
		                    </CardBody>
	                  	</Card>
	                </Col>
              	</Row>
            </div>
		)
	}
}

export default Locations