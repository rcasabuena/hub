import React from 'react'
import LocationForm from './LocationForm'
import { Card, CardBody, Col, Row } from 'reactstrap'
import PageHeader from '../../ui/PageHeader'

const UsersNew = () => {
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardBody>
              <PageHeader 
                pageTitle="New Location"
                pageInfo="&nbsp;"
              />
              <LocationForm />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

export default UsersNew