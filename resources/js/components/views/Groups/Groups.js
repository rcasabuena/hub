import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import ModuleSearchBox from '../../ui/ModuleSearchBox'
import DataTable from '../../ui/Tables/DataTable'
import { handleSearch2 } from '../../functions/Utils'
import PageHeader from '../../ui/PageHeader'
import { Button, Badge, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';

class Groups extends Component {
    constructor(props) {
      super(props)
      this.state = {
        searchTerms: ""
      }
      this.handleSearch = handleSearch2.bind(this)
    }

     render() {
      const { searchTerms } = this.state
      return (
        <div className="animated fadeIn">
          <ModuleSearchBox handleSearch={this.handleSearch} placeholder="Search..."/>
          <Row>
            <Col>
              <Card>
                <CardBody>
                  <PageHeader 
                      pageTitle="User Groups"
                      pageInfo="&nbsp;"
                      pageResource="groups"
                  />
                  <DataTable tableMap="groups" searchTerms={searchTerms} tableResource="Groups" />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      )
     }
}

export default Groups

