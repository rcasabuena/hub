import React, { Component } from 'react'
import GroupForm from './GroupForm'
import { Card, CardBody, Col, Row } from 'reactstrap'

const GroupsNew = () => {
  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardBody>
              <h4 className="card-title mb-0">New Group</h4>
              <p className="text-muted">&nbsp;</p>
              <GroupForm />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

export default GroupsNew