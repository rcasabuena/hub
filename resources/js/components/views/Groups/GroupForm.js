import React, { Component } from 'react'
import TextInput from '../../ui/Form/TextInput'
import MultiSelectInput from '../../ui/Form/MultiSelectInput'
import ToggleSwitch from '../../ui/Form/ToggleSwitch'
import FormSubmitActions from '../../ui/Form/FormSubmitActions'
import { Form } from 'reactstrap'
import AppGroup from '../../../objects/AppGroup'
import formConnect from '../../../HOC/formConnect'

class GroupForm extends Component {

  constructor(props) {
    super(props)
    const { formUtils } = props
    this.handleChange = formUtils.handleChange.bind(this)
    this.handleDelete = formUtils.handleDelete.bind(this)
    this.handleSelect = formUtils.handleMultiSelect.bind(this)
    this.handleSubmit = formUtils.handleSubmit.bind(this)
  }

  state = {
    item: new AppGroup()
  }

  componentDidMount() {
    let { item } = this.props
    this.setState({ item }) 
  }
  
  render() {

    const { allUsers, defaultUsers } = this.props
    const { id, name } = this.state.item

    console.log(defaultUsers)

    return (
      <div>
        <Form onSubmit={this.handleSubmit}>
          <TextInput 
            inputType="text"
            inputName="name" 
            inputLabel="Name" 
            inputHelpText="This is a help text"
            isRequired={true}
            inputDefaultValue={name} 
            handleChange={this.handleChange} 
          />
          <MultiSelectInput 
            inputName="users"
            inputLabel="Users"
            selectOptions={allUsers}
            selectValue={defaultUsers}
            inputHelpText="This is a help text"
            handleSelect={this.handleSelect}
            isRequired={true}
          />
          <FormSubmitActions 
            id={id}
            handleDelete={this.handleDelete}
          />
        </Form>
      </div>
    )
  }

}

const mapStateToProps = (state, ownProps) => {
  const group = state.groups.data.find(item => item.id === ownProps.match.params.unique_id)
  const id = group ? group.id : "",
        name = group ? group.name : "",
        users = group ? group.users : []

  return {
    item: new AppGroup(id, name, users),
    defaultUsers: state.users.data.filter(item => users.indexOf(item.id) > -1),
    allUsers: state.users.data
  }
}

export default formConnect(GroupForm, "Groups", mapStateToProps)