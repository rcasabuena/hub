require('./bootstrap')

import React from 'react'
import { render } from 'react-dom'
import * as serviceWorker from './serviceWorker'
import App from './components/App'

import { fetchCurrentUser } from './actions'
import { Provider } from 'react-redux'

import initialState from './initialState'
import storeFactory from './store'
import { getCurrentUser, fetchTenant } from './actions'

let state = initialState

const store = storeFactory(state);

//store.subscribe(() => console.log(store.getState()))

store.dispatch(
	fetchCurrentUser()
)

store.dispatch(
	fetchTenant()
)

render(
	<Provider store={store} >
		<App />
	</Provider>, 
	document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
