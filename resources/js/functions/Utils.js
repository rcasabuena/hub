import Fuse from 'fuse.js'

export function handleSearch(e, searchKeys) {
	let searchTerms = e.target.value
	
	searchKeys.forEach(key => {
	    
	    if (searchTerms.trim() !== "") {
	    	const options = {
		      ..._globalFuseOptions,
		      keys: _globalFuseKeys[key]
		    }
		    
		    const fuse = new Fuse(this.state[key], options)

		    this.setState({
		    	[`${key}Filtered`]: fuse.search(searchTerms)
		    })
	    } else {

	    	this.setState({
		    	[`${key}Filtered`]: this.state[key]
		    })
	    }
	})

}

export function handleSearch2(e) {
	this.setState({
		searchTerms: e.target.value
	})
}