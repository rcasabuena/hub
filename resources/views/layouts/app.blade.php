<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta name="description" content="">
        <meta name="keywords" content="">
        <title>Banana Moon Hub</title>
        <link rel="stylesheet" href="{{ asset('/theme/assets/css/vendor.bundle.css') }}">
        <link rel="stylesheet" href="{{ asset('/theme/assets/css/app.bundle.css') }}">
        <link rel="stylesheet" href="{{ asset('/theme/assets/css/theme-a.css') }}">

        <!-- Fonts -->

        <!-- Styles -->
    </head>
    <body id="auth_wrapper">
        <div id="login_wrapper">
            <div class="logo">
                <img src="/images/bananamoon.jpg" alt="Banana Moon Hub" class="logo-img">
            </div>
            @yield('content')
        </div>
        <script src="{{ asset('/js/app.js') }}"></script>
        <script src="{{ asset('/theme/assets/js/vendor.bundle.js') }}"></script>
        <script src="{{ asset('/theme/assets/js/app.bundle.js') }}"></script>
    </body>
</html>