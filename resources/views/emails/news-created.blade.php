@extends('emails.layout')

@section('preheader')
@endsection

@section('content')
	<p>Hi {{ $user->name }}</p>
	<p>Praesent justo enim, tempor a viverra eget, varius vel dolor. Duis vehicula, felis eget iaculis sodales, ex tortor accumsan urna, in hendrerit leo quam a urna.</p>
	<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
	  <tbody>
	    <tr>
	      <td align="left">
	        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
	          <tbody>
	            <tr>
	              <td> <a href="" target="_blank">Login to your Banana Moon Hub account</a> </td>
	            </tr>
	          </tbody>
	        </table>
	      </td>
	    </tr>
	  </tbody>
	</table>
	<p>Praesent justo enim, tempor a viverra eget, varius vel dolor. Duis vehicula, felis eget iaculis sodales, ex tortor accumsan urna, in hendrerit leo quam a urna.</p>
@endsection