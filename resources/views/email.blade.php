<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0014)about:internet -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>index.jpg</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">td img {display: block;}</style>
<!--Fireworks CS6 Dreamweaver CS6 target.  Created Mon Jun 17 10:14:23 GMT+0100 (BST) 2019-->
</head>
<body bgcolor="#ffffff">
	<center>
		<table style="display: inline-table;" border="0" cellpadding="0" cellspacing="0" width="600">
  			<tr>
   				<td>
   					<table style="display: inline-table;" align="left" border="0" cellpadding="0" cellspacing="0" width="600">
	  					<tr>
	   						<td width="330" height="63" bgcolor="#105B7C" valign="top" align="left" style="font-family: Arial, Helvetica, 'sans-serif'; font-size: 12px; color: #FFFFFF; line-height: 16.8px; padding-left: 32px;"><br>01926 338497 | <a style="color: #FFFFFF "href="mailto:info@bananamoonfranchise.com?subject=The Hub">info@bananamoonfranchise.com</a>
	   						</td>
	   						<td width="270" height="63" bgcolor="#105B7C"></td>
	  					</tr>
					</table>
				</td>
  			</tr>
  			<tr>
   				<td>
   					<a href="https://www.bananamoonfranchise.com/">
   						<img name="index_r2_c1" src="{{ asset('/email/images/images1_r1_c1.jpg') }}" width="600" height="138" id="index_r2_c1" alt="" />
   					</a>
   				</td>
			</tr>
			<tr>
   				<td>
   					<table style="display: inline-table;" align="left" border="0" cellpadding="0" cellspacing="0" width="600">
						<tr>
   							<td width="391" height="36" bgcolor="#4DB7A0" valign="top" align="left" style="font-family: Arial, Helvetica, 'sans-serif'; font-size: 40px; color: #FFFFFF; line-height: 26.8px; padding-left: 35px;">A task is overdue!</td>
							<td align="left">
								<img name="index_r2_c1" src="images/images1_r2_c3.jpg" width="209" height="36" id="index_r2_c1" alt="" />
							</td>
	  
  						</tr>
	   				</table>
	   			</td>
	   		</tr>
			<tr>
   				<td>
   					<a href="{{ url('/home') }}">
   						<img name="index_r3_c1" src="{{ asset('/email/images/computer_slice_1.jpg') }}" width="600" height="332" id="index_r3_c1" alt="" />
   					</a>
   				</td>
  			</tr>
			<tr>
   				<td width="600" height="48" valign="top" align="center" style="font-family: Arial, Helvetica, 'sans-serif'; font-color: #105B7C; font-size: 18px;  line-height: 16.8px;">
   					<p style="color: hsla(198,78%,27%,1.00) ">There has been a change on the Hub - see what's new:</p>
   				</td>
  			</tr> 
  			<tr>
   				<td>
   					<a href="{{ url('/home') }}"><img name="index_r5_c1" src="{{ asset('/email/images/index_r5_c1.jpg') }}" width="600" height="99" id="index_r5_c1" alt="" />
   					</a>
   				</td>
  			</tr>
  			<tr>
   				<td width="600" height="90" valign="top" align="center" style="font-family: Arial, Helvetica, 'sans-serif'; font-color: #105B7C; font-size: 14px;  line-height: 16.8px; padding-left: 32px; padding-right: 32px;"><strong>Hub update emails:</strong><br>
	   				<p>To make sure you don't miss a thing, you will receive an automated email every time something changes on the Hub, or when something new is uploaded. If you have any questions, please contact Nursery Support at <a style="color:#000000" href="mailto:nurserysupport@bananamoonfranchise.com?Subject=The Hub">nurserysupport@bananamoonfranchise.com</a></p>
	   			</td>
  			</tr>
  			<tr>
   				<td>
   					<img name="index_r7_c1" src="{{ asset('/email/images/index_r7_c1.jpg') }}" width="600" height="122" id="index_r7_c1" alt="" />
   				</td>
  			</tr>
  			<tr>
   				<td width="600" height="95"  valign="top" align="left"  bgcolor="#105B7C" style="font-family: Arial, Helvetica, 'sans-serif'; font-size: 14px; color: #FFFFFF; line-height: 16.8px; padding-left: 32px;">Banana Moon Day Nursery Limited, 1A Tournament Court<br>
	 				Edgehill Drive, Warwick CV34 6LG<br>
	  				01926 338497 | <a style="color: #FFFFFF" href="mailto:info@bananamoonfranchise.com?Subject=The Hub">info@bananamoonfranchise.com</a>
	  			</td>
  			</tr>
		</table>
	</center>
</body>
</html>
