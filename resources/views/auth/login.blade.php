@extends('layouts.app')

@section('content')
<div id="login_content">
    <h1 class="login-title">
        Login to your account
    </h1>
    <div class="login-body">
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group label-floating is-empty">
                <label class="control-label">Email</label>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group label-floating is-empty">
                <label class="control-label">Password</label>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <a class="forgot-pass pull-left" href="{{ route('password.request') }}">
                {{ __('Forgot Password?') }}
            </a>
            <button type="submit" class="btn btn-info btn-block m-t-40">
                {{ __('Login') }}
            </button>
        </form>
    </div>
</div>
@endsection
