@extends('layouts.app')

@section('content')
<div id="login_content">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <h1 class="login-title">
        Reset Password
    </h1>
    <div class="login-body">
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="form-group label-floating is-empty">
                <label class="control-label">Email</label>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <button type="submit" class="btn btn-info btn-block m-t-40">
                {{ __('Send Password Reset Link') }}
            </button>
        </form>
    </div>
</div>
@endsection
