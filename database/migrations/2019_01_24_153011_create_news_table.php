<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id');
            $table->string('slug')->nullable();
            $table->string('title')->nullable();
            $table->text('headline')->nullable();
            $table->longText('body')->nullable();
            $table->text('about')->nullable();
            $table->text('keywords')->nullable();
            $table->unsignedInteger('position')->nullable();
            $table->boolean('featured')->default(0);
            $table->unsignedInteger('author')->nullable();
            $table->datetime('publish')->nullable();
            $table->datetime('expire')->nullable();
            $table->string('featured_image_url')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
