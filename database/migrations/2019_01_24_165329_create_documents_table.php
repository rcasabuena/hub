<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id');
            $table->string('slug')->nullable();
            $table->string('name')->nullable();
            $table->string('title')->nullable();
            $table->text('about')->nullable();
            $table->unsignedInteger('creator')->nullable();
            $table->datetime('publish')->nullable();
            $table->datetime('expire')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
