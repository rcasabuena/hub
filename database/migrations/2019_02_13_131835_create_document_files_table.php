<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_id')->nullable();
            $table->unsignedInteger('document_id')->nullable();
            $table->float('size')->nullable();
            $table->string('mime_type')->nullable();
            $table->string('version')->nullable();
            $table->unsignedInteger('author')->nullable();
            $table->datetime('publish')->nullable();
            $table->boolean('draft')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_files');
    }
}
